
import sys
import os
import cv2

class VideoWriter:

    '''
    Member fields:
        fr_shape_yx: tuple or array-like of length 2, the frame shape: (size_y, size_x)
        fpath: file path to write video

        released: bool
        vid_writer: cv2.VideoWriter instance
    '''

    def __init__(self, fr_shape_yx, fpath, vr_fps=25.):
        self.released = False
        assert len(fr_shape_yx) == 2
        self.fr_shape_yx = fr_shape_yx
        foldername = os.path.dirname(fpath)
        os.makedirs(foldername, exist_ok=True)

        if sys.version_info.major == 2:
            vr_fourcc = cv2.VideoWriter_fourcc(*'H264')     # use this codec with avi, Python2
        else:
            vr_fourcc = cv2.VideoWriter_fourcc(*'MJPG')     # use this codec with avi, Python3
        vr_fps = vr_fps
        vr_frSize_xy = (fr_shape_yx[1],fr_shape_yx[0])
        self.vid_writer = cv2.VideoWriter(fpath, fourcc=vr_fourcc, fps=vr_fps, frameSize=vr_frSize_xy)
        assert self.vid_writer.isOpened(), "Unable to open video file for writing: " + fpath

    def write(self, im):
        '''
        Parameters:
            im: ndarray(size_y, size_x, n_ch:bgr) of uint8
        '''
        assert im.shape == (self.fr_shape_yx) + (3,)
        self.vid_writer.write(im)

    def release(self):
        self.vid_writer.release()
        self.released = True