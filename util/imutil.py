
import numpy as np
import skimage.measure
import cv2
import time
import util.util as Util

def get_segment_pixels_from_regionprop(img, regionprops_obj, seg_ids):
    '''
    Applies func on given value image computed only over the segment area.
    Parameters:
        img: ndarray(size_y, size_x, n_chans) of ?
        regionprops_obj: skimage.measure.regionprops
        seg_ids: ndarray(n_segs_to_get,) of uint32; !!! IDs must match rprop labels (no seg_id+1 here) !!!
    Returns:
        seg_pixels_ls: list(n_segs_to_get) of ndarray(n_seg_pixels, n_chans) of img.dtype
    '''
    #assert img.ndim == 3 and img.shape[0] > img.shape[2]    # assertion to avoid wrong axis order
    seg_pixels_ls = []
    for region in regionprops_obj:
        lab = region.label
        seg_idxs = np.where(seg_ids == lab)[0]
        if len(seg_idxs) == 0:
            continue    # skip if not interested in this segment
        
        region_mask_in_bbox = region.image
        img_in_bbox = img[region.bbox[0]:region.bbox[2], region.bbox[1]:region.bbox[3],:]  # (bb_sy, bb_sx, n_chans)
        masked_img_in_bbox = img_in_bbox[region_mask_in_bbox]
        seg_pixels_ls.append(masked_img_in_bbox)
    return seg_pixels_ls

def get_segment_pixels_from_bbox_and_mask(img, seg, bbox_tlbr, seg_ids):
    '''
    Applies func on given value image computed only over the segment area.
    Parameters:
        img: ndarray(size_y, size_x, n_chans) of ?
        seg: ndarray(size_y, size_x) of int
        bbox_tlbr: tuple(4:[tlbr])
        seg_ids: ndarray(n_segs_to_get,) of uint32;
    Returns:
        seg_pixels_ls: list(n_segs_to_get) of ndarray(n_seg_pixels, n_chans) of img.dtype
    '''
    #assert img.ndim == 3 and img.shape[0] > img.shape[2]    # assertion to avoid wrong axis order
    seg_pixels_ls = []
    im_in_bbox = img[bbox_tlbr[0]:bbox_tlbr[2], bbox_tlbr[1]:bbox_tlbr[3]]
    seg_in_bbox = seg[bbox_tlbr[0]:bbox_tlbr[2], bbox_tlbr[1]:bbox_tlbr[3]]
    return [im_in_bbox[seg_in_bbox == seg_id,:] for seg_id in seg_ids]

def fast_resize_nearest(im, target_size_yx):
    '''
    Resizes image to target size, without smoothing.
    Approx 20 times faster than skimage.transform.resize().
    Parameters:
        im: ndarray(size_y, size_x, n_ch) of ?
        target_size_yx: tuple(2) of int
    Returns:
        ndarray(<target_size_yx>, n_ch) of ?
    '''
    assert len(target_size_yx) == 2
    assert im.ndim == 3
    assert im.shape[0] < im.shape[1]        # safety check
    ls0 = np.linspace(0., im.shape[0], target_size_yx[0], endpoint=False, dtype=np.int32)  # NOTE: dtype was changed from float32
    ls1 = np.linspace(0., im.shape[1], target_size_yx[1], endpoint=False, dtype=np.int32)
    #idxs = np.meshgrid(ls0, ls1, indexing='ij')  # tuple(2) of ndarray(<target_size_yx>)  # TODO alternative method disabled
    #imz = im[(idxs[0], idxs[1])]
    imf = im[ls0[:,None], ls1[None,:], :]
    #assert np.all(imz == imf)
    return imf

def fast_resize_video_nearest_singlech(ims, target_size_yx):
    '''
    Resizes video to target size, without smoothing, single channel.
    Parameters:
        ims: ndarray(n_frs, size_y, size_x) of ?
        target_size_yx: tuple(2) of int
    Returns:
        ndarray(n_frs, <target_size_yx>) of ?
    '''
    assert len(target_size_yx) == 2
    assert ims.ndim == 3
    assert ims.shape[1] < ims.shape[2]        # safety check
    ls0 = np.linspace(0., ims.shape[1], target_size_yx[0], endpoint=False, dtype=np.int32)
    ls1 = np.linspace(0., ims.shape[2], target_size_yx[1], endpoint=False, dtype=np.int32)
    return ims[:, ls0[:,None], ls1[None,:]]

# IM -> SP reduce funcs

def imsp_reduce_mean(seg):
    # ndarray(n_pixels, n_chans) -> ndarray(out_vec_len,)
    assert seg.ndim == 2
    return np.mean(seg.astype(np.float64), axis=0, dtype=np.float64).astype(np.float32)

def imsp_reduce_std(seg):
    # ndarray(n_pixels, n_chans) -> ndarray(out_vec_len,)
    assert seg.ndim == 2
    return np.std(seg.astype(np.float64), axis=0, dtype=np.float64).astype(np.float32)

def imsp_reduce_any(seg):
    # ndarray(n_pixels, n_chans) -> ndarray(out_vec_len,)
    assert seg.ndim == 2
    assert seg.dtype == np.bool_
    return np.any(seg, axis=0)

def imsp_reduce_most_frequent_item(seg):
    # ndarray(n_pixels, n_chans) -> ndarray(out_vec_len,)
    assert seg.ndim == 2
    return np.argmax(np.bincount(seg.reshape(-1)))

def imsp_image_histograms(seg, hist_len_per_ch, chs_to_keep):
    # ndarray(n_pixels, n_chans) -> ndarray(hist_len_per_ch*len(chs_to_keep),)
    assert seg.ndim == 2
    assert len(chs_to_keep) >= 1
    chan_results = [np.histogram(seg[:,ch_idx], bins=hist_len_per_ch, range=(0,255)) for ch_idx in chs_to_keep]
    return np.concatenate(chan_results)

# IM -> downscaledIM reduce funcs

def imdownscale(ims, target_size_yx, interp_smooth=True):
    # ndarray(n_frames, size_y, size_x, n_chans), tuple(size_y, size_x) -> ndarray(n_frames, target_size_y, target_size_x, n_chans)
    assert ims.ndim == 4
    assert len(target_size_yx) == 2
    interp = cv2.INTER_AREA if interp_smooth else cv2.INTER_NEAREST
    ims = ims.astype(np.float32)
    ret = np.empty((ims.shape[0], target_size_yx[0], target_size_yx[1], ims.shape[3]), dtype=np.float32)
    for fr_idx in range(ims.shape[0]):
        for ch_idx in range(ims.shape[3]):
            im = ims[fr_idx,:,:,ch_idx]
            ret[fr_idx,:,:,ch_idx] = cv2.resize(im, dsize=(target_size_yx[1], target_size_yx[0]), interpolation=interp)
    return ret.astype(np.float32)

# (SP, SP -> edge) features reduce funcs

def pairwise_l2dist(fs1, fs2):
    # Euclidean distance over the last axis
    # ndarray(n_edges, n_features), ndarray(n_edges, n_features) -> ndarray(n_edges, 1)
    assert fs1.shape == fs2.shape
    assert fs1.ndim == 2
    return np.linalg.norm(fs1.astype(np.float32) - fs2.astype(np.float32), axis=-1, keepdims=True)

def pairwise_optflow_diff_features(ofs1, ofs2):
    # Difference features of two vectors: returns 3 values -> [abs_angular_diff, min_mag, rel_mag]
    #   abs_angular_diff: in [0,pi]; pi if vectors point to opposite dir, 0 if same dir
    #   min_mag: length of shorter vec; positive float
    #   rel_mag: shorter_vec_len/longer_vec_len, in range [0,1], 1 for similar lengths, 0 for infinitely different lengths
    # ndarray(n_edges, 2:[dy, dx]), ndarray(n_edges, 2:[dy, dx]) -> ndarray(n_edges, 3)
    assert ofs1.shape == ofs2.shape
    assert ofs1.shape[1:] == (2,)
    EPS = 1e-9
    ret = np.empty((ofs1.shape[0], 3), dtype=np.float32)
    angle1 = np.arctan2(ofs1[:,0], ofs1[:,1])
    angle2 = np.arctan2(ofs2[:,0], ofs2[:,1])
    ret[:,0] = np.fabs((angle1 - angle2 + np.pi) % (2.*np.pi) - np.pi)
    len1 = np.linalg.norm(ofs1, axis=-1)
    len2 = np.linalg.norm(ofs2, axis=-1)
    ret[:,1] = np.minimum(len1, len2)
    max_len = np.maximum(len1, len2)
    ret[:,2] = ret[:,1]/(max_len + EPS)
    return ret


# SP EdgeList generation

def get_adj_graph_edge_list_fast(seg, ignore_axes=[], return_counts=False):
    '''
    Returns the edge list of the adjacency graph of the given SV segmentation.
    Faster method than the one in skimage, with fixed connectivity=1.
    Parameters:
        seg: ndarray(?) of int/uint up to 32bits; int64, uint64 are not supported
        ignore_axes: list of ints; these axes are ignored in the adjacency test.
        return_counts: bool; return the number of adjacencies for each seg pair
    Returns:
        edges: ndarray(n_edges, 2:[ID_from, ID_to]) of seg.dtype; unique edges where ID_from < ID_to
        (OPTIONAL if 'return_counts' is True) counts: ndarray(n_edges,) of i32
    '''
    ids_from = []
    ids_to = []
    MAXUINT32 = 2**32
    orig_dtype = seg.dtype
    assert np.can_cast(seg.dtype, np.uint32, casting='safe') or (seg.dtype == np.int32 and np.amin(seg) >= 0)
    seg = seg.astype(np.uint32, copy=False)

    for dim_idx in range(seg.ndim):
        if dim_idx in ignore_axes:
            continue

        # get each pixel along the given axis which have different IDs than the consecutive pixel along axis
        m = seg[(slice(None),)*dim_idx + (slice(None,-1,None),)] != seg[(slice(None),)*dim_idx + (slice(1,None,None),)]
        ids_from.append(seg[(slice(None),)*dim_idx + (slice(None,-1,None),)][m])
        ids_to.append(seg[(slice(None),)*dim_idx + (slice(1,None,None),)][m])

    # create edge arrays, sort: ids_from < ids_to
    edges = np.stack([np.concatenate(ids_from), np.concatenate(ids_to)], axis=1)  # (n_edges, 2)
    swap_mask = edges[:,0] > edges[:,1]
    edges[swap_mask,0], edges[swap_mask,1] = edges[swap_mask,1], edges[swap_mask,0]   # safe if advanced indexing

    # unique edges: unique 1D is faster than unique 2D
    edges_1d = edges[:,0] * MAXUINT32 + edges[:,1]
    if return_counts is True:
        edges_1d_u, edges_1d_c = np.unique(edges_1d, return_counts=True)
        edges = np.stack([edges_1d_u // MAXUINT32, edges_1d_u % MAXUINT32], axis=1)
        return edges.astype(orig_dtype, copy=False), edges_1d_c
    else:
        edges_1d_u = np.unique(edges_1d)
        edges = np.stack([edges_1d_u // MAXUINT32, edges_1d_u % MAXUINT32], axis=1)
        return edges.astype(orig_dtype, copy=False)


def find_spatial_neighbors(sp_seg):
    '''
    Two segments are connected with an edge if they share borders (4-connectivity) along axis#1 or #2.
    Parameters:
        sp_seg: ndarray(n_frames, size_y, size_x) of int32
    Returns:
        edges: ndarray(n_edges, 2:[ID_from, ID_to]) of seg.dtype; unique edges where ID_from < ID_to
    '''
    assert sp_seg.ndim == 3
    return get_adj_graph_edge_list_fast(sp_seg, ignore_axes=[0])

def find_temporal_neighbors(sp_seg):
    '''
    Two segments are connected with an edge if they share borders (4-connectivity) along axis#0.
    Parameters:
        sp_seg: ndarray(n_frames, size_y, size_x) of int32
    Returns:
        edges: ndarray(n_edges, 2:[ID_from, ID_to]) of seg.dtype; unique edges where ID_from < ID_to
    '''
    assert sp_seg.ndim == 3
    return get_adj_graph_edge_list_fast(sp_seg, ignore_axes=[1,2])
    
def find_sps_sharing_svs(sv_to_sp):
    '''
    Two SP segments are connected with an edge if they are in a shared SV.
    !!! Requires that an SV is represented by a single SP in each frame.
    Parameters:
        sv_to_sp: ndarray(n_svs, n_frames) of uint32; SP IDs for each SV in each frame;
                                                         -1 if SV does not exist in given frame
    Returns:
        edges: ndarray(n_edges, 2:[ID_from, ID_to]) of seg.dtype; unique edges where ID_from < ID_to
    '''
    shared_sv_ids = np.where((sv_to_sp[:,:-1] >= 0) & (sv_to_sp[:,1:] >= 0))   # tuple(2) of idx arr
    edges = np.stack([sv_to_sp[shared_sv_ids], sv_to_sp[:,1:][shared_sv_ids]], axis=1)
    edges_to_reverse = edges[:,0] > edges[:,1]
    edges[edges_to_reverse,:] = edges[edges_to_reverse,::-1]
    return edges

def find_sps_associated_by_flow(sp_seg, flow, flow_dir_forward):
    '''
    Two SP segments are connected with an edge if they have flow vectors pointing from one to the another.
    Parameters:
        sp_seg: ndarray(n_frames, size_y, size_x) of int32
        flow: ndarray(n_frames-1, size_y, size_x, 2:[dy,dx]) of float32
        flow_dir_forward: bool; if True, flow is considered to be forward flow, otherwise backwards flow
    Returns:
        edges: ndarray(n_edges, 2:[ID_from, ID_to]) of seg.dtype; unique edges where ID_from < ID_to
                    includes the -1 ID where flow points to invalid locations
        directions: ndarray(n_edges,) of bool_; True if flow in ID_from -> ID_to direction, False if opposite dir
        counts: ndarray(n_edges,) of int32; the number of flow vectors between each segment pair
    '''
    n_fr = sp_seg.shape[0]
    sy, sx = sp_seg.shape[1:3]
    assert flow.shape == (n_fr-1, sy, sx, 2)
    edges = []
    for fr_idx in range(n_fr-1):
        base = np.mgrid[:sy, :sx]  # (2, sy, sx)
        warped_base = base.astype(np.float32, copy=True)  # (2, sy, sx)
        warped_base[0,:,:] += flow[fr_idx,:,:,0]
        warped_base[1,:,:] += flow[fr_idx,:,:,1]
        warped_base_invalid = (warped_base[0,:,:] < 0.) | (warped_base[0,:,:] >= sy) |\
                              (warped_base[1,:,:] < 0.) | (warped_base[1,:,:] >= sx)     # (sy, sx)
        warped_base = np.clip(warped_base, np.array([0., 0.])[:,None,None], np.array([sy-1, sx-1])[:,None,None])
        warped_base = warped_base.astype(np.int32)  # (2, sy, sx)
        seg0 = sp_seg[fr_idx,:,:] if flow_dir_forward else sp_seg[fr_idx+1,:,:]
        seg1 = sp_seg[fr_idx+1,:,:] if flow_dir_forward else sp_seg[fr_idx,:,:]
        ids_in_fr0 = seg0[base[0], base[1]]   # (sy, sx)
        ids_in_fr1 = seg1[warped_base[0], warped_base[1]].astype(np.int32)   # (sy, sx)
        ids_in_fr1[warped_base_invalid] = -1
        edges.append(np.stack([ids_in_fr0.reshape(-1), ids_in_fr1.reshape(-1)], axis=-1)) # (sy*sx, 2)
    edges = np.concatenate(edges, axis=0)   # (n_edges, 2)

    # get unique and counts
    edges_singlechan, max_vals = Util.convert_multichannel_to_int(edges+1, np.uint64)   # +1 beacuse of -1 invalid IDs
    u_edges_singlechan, c_edges = np.unique(edges_singlechan, return_counts=True)
    u_edges = Util.restore_multichannel_from_int(u_edges_singlechan, max_vals, edges.dtype)-1

    # reverse edges if necessary
    reverse_edges = u_edges[:,1] < u_edges[:,0]
    u_edges[reverse_edges,:] = u_edges[reverse_edges,::-1]
    return u_edges, ~reverse_edges, c_edges

def merge_edge_lists(edge_lists, edge_weights, missing_weights, fun_weight_merge=None):
    '''
    Parameters:
        edge_lists: list(n_edge_lists) of ndarray(n_edges, 2:[ID_from, ID_to]) of int;
                    unique edges where ID_from < ID_to
                    includes the -1 ID where flow points to invalid locations
        edge_weights: list(n_edge_lists) of ndarray(n_edges,) of float
        missing_weights: ndarray(n_edge_lists) of float; the weight value for each edge_list if edge is missing
        fun_weight_merge: None OR Callable(ndarray(n_total_edges, n_edge_lists) -> ndarray(n_total_edges));
                            function that merges a list of weight arrays of same length into a single weight array
            if fun_weight_merge is None, weight lists are returned as an array without merging them
    Returns:
        merged_edges: ndarray(n_all_edges, 2:[ID_from, ID_to]) of int;
        merged_weights: ndarray(n_all_edges,) of float
                OR ndarray(n_all_edges, n_edge_lists) of float IF fun_weight_merge is None
    '''
    assert all(np.all(els[:,0] < els[:,1]) for els in edge_lists)
    assert all(np.amin(els) >= -1 for els in edge_lists)
    assert all(ew.ndim == 1 for ew in edge_weights)
    assert all(els.shape[0] == ew.shape[0] for els, ew in zip(edge_lists, edge_weights))
    offsets = np.cumsum([0] + [els.shape[0] for els in edge_lists])   # (n_edge_lists+1)

    # get unique edges (encode edges as single uint64)
    edges = np.concatenate(edge_lists, axis=0)+1   # +1 because of the -1 node
    es, mv_es = Util.convert_multichannel_to_int(edges[:,::-1], np.uint64)   
                                # reverse edge dir: this way final edges will be ordered alphabetically
    assert all([np.unique(es[offsets[oidx]:offsets[oidx+1]]).shape[0] == es[offsets[oidx]:offsets[oidx+1]].shape[0]\
                     for oidx in range(len(edge_lists)-1)])  # assert all edges in each edge_lists are unique
    u_es = np.unique(es)  # sorted

    # collect all weights in a single array, then apply merge function over them
    all_weights = np.empty((u_es.shape[0], len(edge_lists)), dtype=np.float32)   # (n_all_edges, n_edge_lists)
    all_weights[:] = missing_weights
    for ls_idx in range(len(edge_lists)):
        es_part = es[offsets[ls_idx]:offsets[ls_idx+1]]
        es_part_idxs = np.searchsorted(u_es, es_part)
        all_weights[es_part_idxs,ls_idx] = edge_weights[ls_idx]

    if fun_weight_merge is not None:
        merged_weights = fun_weight_merge(all_weights)

    # decode edges back into [from_sp_id, to_sp_id form]
    merged_edges = Util.restore_multichannel_from_int(u_es, mv_es, edges.dtype)-1

    return merged_edges[:,::-1], merged_weights   # undo reversing edge directions

def compute_pairwise_edge_features(edges, sp_chan, fun_pairwise, invalid_value):
    '''
    Computes a pairwise function for each edge from node features.
    Parameters:
        edges: ndarray(n_edges, 2:[ID_from, ID_to]) of int; unique edges where ID_from < ID_to
                    may include the -1 ID where edge points to invalid locations
        sp_chan: ndarray(n_sps, n_sp_chans) of ?; same format as Segmentation class SP channels
        fun_pairwise: Callable(ndarray(n_edges, n_node_features) - from_node, ndarray(n_edges, n_node_features) - to_node
                                                             -> ndarray(n_edges, n_edge_features));
        invalid_value: ?; if edge points to invalid node (-1 ID), the returned edge features are filled with this value
    Returns:
        edge_features: ndarray(n_edges, n_edge_features)) of <'fun_pairwise' output type>
    '''
    assert edges.shape[1:] == (2,)
    assert sp_chan.ndim == 2
    valid_edges_mask = ~np.any(edges == -1, axis=-1)
    valid_edges = edges[valid_edges_mask,:]
    edge_features_valid = fun_pairwise(sp_chan[valid_edges[:,0],:], sp_chan[valid_edges[:,1],:])
    edge_features = np.full((edges.shape[0], edge_features_valid.shape[1]), dtype=edge_features_valid.dtype,\
                                                                    fill_value=invalid_value)
    edge_features[valid_edges_mask,:] = edge_features_valid
    return edge_features



def find_flow_edges_and_weights(sp_seg, flow_fwd, flow_bwd, sp_sizes):
    '''
    Finds flow edges and computes weights (used for MRF label propagation).
    Weight calculation:
        SP_from <-> SP_to weight = min(c_from_to/size_from, c_to_from/size_to)
    Parameters:
        sp_seg: ndarray(n_frames, size_y, size_x) of int32
        flow_fwd: ndarray(n_frames-1, size_y, size_x, 2:[dy,dx]) of float32
        flow_bwd: ndarray(n_frames-1, size_y, size_x, 2:[dy,dx]) of float32
        sp_sizes: ndarray(n_sps,) of int32
    Returns:
        edges_all: ndarray(n_edges, 2:[ID_from, ID_to]) of seg.dtype; unique edges where ID_from < ID_to;
                                        the union of edges in 'edges_fwd' and 'edges_bwd'
        weights: ndarray(n_edges,) of float32
    '''
    print("    SEG EDGES2 flow1", time.time())
    edges_fwd, dir_fwd, c_fwd = find_sps_associated_by_flow(sp_seg, flow_fwd, flow_dir_forward=True)
    print("    SEG EDGES2 flow2", time.time())
    edges_bwd, dir_bwd, c_bwd = find_sps_associated_by_flow(sp_seg, flow_bwd, flow_dir_forward=False)
    print("    SEG EDGES2 flow3, edges shape:", edges_fwd.shape, edges_bwd.shape, ", t:", time.time())
    assert np.all(dir_fwd[edges_fwd[:,0] >= 0])          # assert all valid fwd edges point to a higher SP ID
    assert not np.any(dir_fwd[edges_fwd[:,0] < 0])          # assert all invalid fwd edges are reversed
    assert not np.any(dir_bwd)      # assert all bwd edges point to a lower SP ID
    assert np.all(edges_fwd[:,0] < edges_fwd[:,1])   # TODO remove
    assert np.all(edges_bwd[:,0] < edges_bwd[:,1])   # TODO remove
    source_sizes_fwd = sp_sizes[edges_fwd[np.arange(dir_fwd.shape[0]), 1-dir_fwd]].astype(np.float32)
    source_sizes_bwd = sp_sizes[edges_bwd[np.arange(dir_bwd.shape[0]), 1-dir_bwd]].astype(np.float32)
                                        # source ID should never be negative (since edges originate from valid SPs)
    edge_w_fwd = c_fwd / source_sizes_fwd
    edge_w_bwd = c_bwd / source_sizes_bwd
    assert np.all(edge_w_fwd) <= 1.
    assert np.all(edge_w_bwd) <= 1.
    print("    SEG EDGES2 flow4", time.time())
    edges_all, weights = merge_edge_lists([edges_fwd, edges_bwd], [edge_w_fwd, edge_w_bwd],\
                             missing_weights=np.array([0., 0.], dtype=np.float32),
                             fun_weight_merge=lambda ws: np.amin(ws, axis=1))
    print("    SEG EDGES2 flow5", time.time())
    # zero out all edges with -1 on one end 
    # (if there (K -> -1) edges exist in both fwd and bwd flow, weight will be non-zero)
    weights[np.any(edges_all < 0, axis=1)] = 0.
    return edges_all, weights

# GNN flow features

def transform_dense_with_flow_fwdbwd(flow_fwd, flow_bwd):
    '''
    Transforms a (size_y, size_x) shaped mgrid with forward then with backward flow. 
        (fwd/bwd can be swapped to apply tranform in reversed direction)
    Parameters:
        flow_fwd: ndarray(size_y, size_x, 2:[dy,dx]) of float32
        flow_bwd: ndarray(size_y, size_x, 2:[dy,dx]) of float32
    Returns:
        fwd_mgrid: ndarray(size_y, size_x, 2:[y,x]) of float32; the fwd transformed mgrid point coordinates
        fwdbwd_mgrid: ndarray(size_y, size_x, 2:[y,x]) of float32; the fwd-bwd transformed mgrid point coordinates
                            invalid coordinates should be masked out with the following masks
        invalid_fwd: ndarray(size_y, size_x) of bool_; True where fwd transformation pointed out of screen
        invalid_fwdbwd: ndarray(size_y, size_x) of bool_; True where fwd or fwd-bwd transformation pointed out of screen
    '''
    assert flow_fwd.shape == flow_bwd.shape
    assert flow_fwd.shape[2:] == (2,)
    size_arr = np.array(flow_fwd.shape[:2], dtype=np.float32)
    base = np.mgrid[:size_arr[0], :size_arr[1]]  # (2, sy, sx)
    # forward transform
    fwd_mgrid = base.astype(np.float32, copy=True)  # (2, sy, sx)

    fwd_mgrid += flow_fwd.transpose((2,0,1))
    # TODO remove, lines below replaced by single line above
    #fwd_mgrid[0,:,:] += flow_fwd[:,:,0]
    #fwd_mgrid[1,:,:] += flow_fwd[:,:,1]
    
    invalid_fwd = np.any((fwd_mgrid < 0.) | (fwd_mgrid >= size_arr[:,None,None]), axis=0)
    fwd_mgrid[:,invalid_fwd] = 0.
    # backward transform
    fwd_mgrid_i = fwd_mgrid.astype(np.int32, copy=True)  # (2, sy, sx) i32

    delta_bwd = flow_bwd.transpose((2,0,1))[:, fwd_mgrid_i[0,:,:], fwd_mgrid_i[1,:,:]]
    # TODO remove, lines below replaced by single line above
    #delta_bwd = np.empty_like(fwd_mgrid_i, dtype=np.float32)  # (2, sy, sx) fl32
    #delta_bwd[0,:,:] = flow_bwd[fwd_mgrid_i,0]
    #delta_bwd[1,:,:] = flow_bwd[fwd_mgrid_i,1]

    fwdbwd_mgrid = fwd_mgrid + delta_bwd        # (2, sy, sx) fl32
    invalid_fwdbwd = np.any((fwdbwd_mgrid < 0.) | (fwdbwd_mgrid >= size_arr[:,None,None]), axis=0)
    invalid_fwdbwd |= invalid_fwd
    fwdbwd_mgrid[:,invalid_fwdbwd] = 0.
    return fwd_mgrid.transpose((1,2,0)), fwdbwd_mgrid.transpose((1,2,0)), invalid_fwd, invalid_fwdbwd

def get_flow_edge_features(sp_seg, flows_fwd, flows_bwd, sp_sizes=None):
    '''
    Finds flow edges and generate some edge-wise features (using directed edges)
    Parameters:
        sp_seg: ndarray(n_frames, size_y, size_x) of int32
        flows_fwd: ndarray(n_frames-1, size_y, size_x, 2:[dy,dx]) of float32
        flows_bwd: ndarray(n_frames-1, size_y, size_x, 2:[dy,dx]) of float32
        sp_sizes: None OR ndarray(n_sps,) of int32; speed up calculation with precomputed segment sizes.
    Returns:
        edges_all: ndarray(n_edges, 2:[ID_from, ID_to]) of seg.dtype; unique edges where ID_from < ID_to;
                                        the union of edges in 'edges_fwd' and 'edges_bwd'
        edge_fs: ndarray(n_edges, 2:[ID_from -> ID_to, ID_to -> ID_from], n_features) of float32
                        FEATURES (per direction):
                            #0: ratio of ID_from OF vecs pointing to ID_to; (0,1]
                            #1: ratio of these (pointing to ID_to) vectors returning to ID_from with flow_bwd; (0,1]
                            #2-3: mean, std of distances of the returning bwd vectors (positive in #1) target points from the
                                        original starting points of fwd vectors
    '''
    n_fr = sp_seg.shape[0]
    sy, sx = sp_seg.shape[1:3]
    assert flows_fwd.shape == flows_bwd.shape == (n_fr-1, sy, sx, 2)
    base_mgrid = np.mgrid[:sy, :sx].transpose((1,2,0))  # (sy, sx, 2)
    EPS = 1e-9
    if sp_sizes is None:
        _, sp_sizes = np.unique(sp_seg, return_counts=True)
        assert sp_sizes.shape[0] == np.amax(sp_seg)+1

    edges_fwd = []
    edges_bwd = []
    fs_fwd = []
    fs_bwd = []
    for direction in ["fwd", "bwd"]:
        edges_ls = edges_fwd if direction == "fwd" else edges_bwd
        fs_ls = fs_fwd if direction == "fwd" else fs_bwd

        for fr_idx in range(n_fr-1):
            # fwd/bwd is swapped when direction is backwards
            flow_to = flows_fwd[fr_idx] if direction == "fwd" else flows_bwd[fr_idx]
            flow_back = flows_bwd[fr_idx] if direction == "fwd" else flows_fwd[fr_idx]
            sp_seg_orig = sp_seg[fr_idx] if direction == "fwd" else sp_seg[fr_idx+1]
            sp_seg_target = sp_seg[fr_idx+1] if direction == "fwd" else sp_seg[fr_idx]

            #
            fwd_mgrid, fwdbwd_mgrid, invalid_fwd, invalid_fwdbwd = transform_dense_with_flow_fwdbwd(flow_to, flow_back)
            fwd_mgrid, fwdbwd_mgrid = fwd_mgrid.astype(np.int32), fwdbwd_mgrid.astype(np.int32)   # (sy, sx, 2) each
            fwdbwd_deltalen = np.linalg.norm(fwdbwd_mgrid - base_mgrid, axis=-1)   # (sy, sx)
            ids_fwd = sp_seg_target[fwd_mgrid[:,:,0], fwd_mgrid[:,:,1]]    # (sy, sx) i32
            ids_fwdbwd = sp_seg_orig[fwdbwd_mgrid[:,:,0], fwdbwd_mgrid[:,:,1]]    # (sy, sx) i32

            # feature#0, #1: count flow vectors for each edge and count how many of them return to the origin segment
            flow_vec_ids = np.stack([sp_seg_orig, ids_fwd], axis=-1).astype(np.uint32, copy=False)   # (sy, sx, 2) ui32
            flow_vec_ret_mask = sp_seg_orig == ids_fwdbwd                                  # (sy, sx) bool
            flow_vec_ids64 = Util.convert_multichannel_ui32_to_ui64(flow_vec_ids)       # (sy, sx) ui64
            u_edges64, inv_edges64, c_edges64 = np.unique(flow_vec_ids64, return_inverse=True, return_counts=True)
            returning_inv_edges64 = inv_edges64.reshape(flow_vec_ids64.shape)[flow_vec_ret_mask & (~invalid_fwdbwd)]
            c_edges_ret64 = np.bincount(returning_inv_edges64, minlength=u_edges64.shape[0])
            u_edges = Util.restore_multichannel_ui32_from_ui64(u_edges64)       # (n_edges, 2:[from, to]) ui32
            edge_from_sizes = sp_sizes[u_edges[:,0]].astype(np.float32)
            f0 = c_edges64/edge_from_sizes
            f1 = c_edges_ret64/(c_edges64 + EPS)
            assert np.all(f1 <= 1.)

            # feature#2, #3: select returning edges and get their deltas: mean/std over them grouped by the edge IDs
            fwdbwd_deltalen_returning = fwdbwd_deltalen[flow_vec_ret_mask & (~invalid_fwdbwd)]
            f2 = Util.apply_func_with_groupby_manyIDs(fwdbwd_deltalen_returning, returning_inv_edges64, np.mean,\
                                                                         assume_arange_to=u_edges64.shape[0], empty_val=0)  
            f3 = Util.apply_func_with_groupby_manyIDs(fwdbwd_deltalen_returning, returning_inv_edges64, np.std,\
                                                                         assume_arange_to=u_edges64.shape[0], empty_val=0) 
            edges_ls.append(u_edges)
            fs_ls.append(np.stack([f0, f1, f2, f3], axis=-1))

    # merge fwd and bwd edges
    edges = np.concatenate(edges_fwd + edges_bwd, axis=0)
    fs = np.concatenate(fs_fwd + fs_bwd, axis=0)
    edges_reversed = edges[:,0] > edges[:,1]
    edges[edges_reversed,:] = edges[edges_reversed,::-1]
    edges64 = Util.convert_multichannel_ui32_to_ui64(edges)
    edges64_u, edges_inv = np.unique(edges64, return_inverse=True)
    edges_u = Util.restore_multichannel_ui32_from_ui64(edges64_u)
    edge_fs = np.zeros((edges_u.shape[0], 2, fs.shape[1]), dtype=fs.dtype)
    edge_fs[edges_inv, edges_reversed.astype(np.int32),:] = fs
    return edges_u, edge_fs

def merge_edge_lists_only(edge_lists, return_index=False):
    '''
    Parameters:
        edge_lists: list(n_edge_lists) of ndarray(n_edges, 2:[ID_from, ID_to]) of int;
                    UNIQUE edges where ID_from < ID_to;
        return_index: bool; if True, returns index array which produces the merged
                                     edgelist from the concatenation of the source edgelists
    Returns:
        merged_edges: ndarray(n_all_edges, 2:[ID_from, ID_to]) of int; no uniqueness check if len(edge_lists) == 1
        (OPTIONAL) merger_indices: ndarray(n_all_edges,) of int32; see 'return_index' param for details
    '''
    assert all([earr.shape[1:] == (2,) and np.all(earr[:,0] < earr[:,1]) for earr in edge_lists])
    assert len(edge_lists) >= 1
    if len(edge_lists) == 1:
        return edge_lists[0]
    edges = np.concatenate(edge_lists, axis=0)
    edges64 = Util.convert_multichannel_i32_to_i64(edges)
    if return_index:
        edges64_u, merger_indices = np.unique(edges64, return_index=True)
        merged_edges = Util.restore_multichannel_i32_from_i64(edges64_u)
        return merged_edges, merger_indices
    else:
        edges64_u,  = np.unique(edges64)
        merged_edges = Util.restore_multichannel_i32_from_i64(edges64_u)
        return merged_edges


def stack_edge_features_bidir(edge_lists, feature_lists, missing_features_vals=0.):
    '''
    Parameters:
        edge_lists: list(n_edge_lists) of ndarray(n_edges, 2:[ID_from, ID_to]) of int;
                                                        unique edges where ID_from < ID_to
        feature_lists: list(n_edge_lists) of ndarray(n_edges, 2, n_features) for bidir features OR
                                             ndarray(n_edges, n_features) for unidir features
    Returns:
        all_edges: ndarray(n_all_edges, 2:[ID_from, ID_to]) of uint32;
                                                        unique edges where ID_from < ID_to
        all_features: ndarray(n_all_edges, 2:[orig_dir, reversed], n_all_features) of float32
    '''
    assert len(edge_lists) == len(feature_lists)
    assert all([earr.shape[1:] == (2,) for earr in edge_lists])
    assert all([np.all(earr[:,0] < earr[:,1]) for earr in edge_lists])
    assert all([(farr.ndim == 2) or (farr.ndim == 3 and farr.shape[1] == 2) for farr in feature_lists])
    n_total_features = sum([farr.shape[-1] for farr in feature_lists])

    # concat edge lists and get final edge list
    edges = np.concatenate(edge_lists, axis=0)
    assert np.amin(edges) >= 0
    edges = edges.astype(np.uint32)
    edges64 = Util.convert_multichannel_ui32_to_ui64(edges)
    edges64_u, edges_inv = np.unique(edges64, return_inverse=True)
    all_edges = Util.restore_multichannel_ui32_from_ui64(edges64_u)

    # create merged feature array, init with "missing_features_vals"
    all_features = np.full((all_edges.shape[0], 2, n_total_features), dtype=np.float32, fill_value=missing_features_vals)
    edge_offset, f_offset = 0, 0
    for fls_idx in range(len(feature_lists)):
        edge_end_offset = edge_offset + feature_lists[fls_idx].shape[0]
        f_end_offset = f_offset + feature_lists[fls_idx].shape[-1]

        # if unidir features: duplicate them to both directions
        f_arr = feature_lists[fls_idx] if feature_lists[fls_idx].ndim == 3 else feature_lists[fls_idx][:,None,:]
        all_features[edges_inv[edge_offset:edge_end_offset], :, f_offset:f_end_offset] = f_arr

        edge_offset, f_offset = edge_end_offset, f_end_offset

    return all_edges, all_features


# faster SP border visualization image generation

def mark_boundaries_fast(seg, mode='upper-left', bordertype='4way'):
    '''
    A faster alternative to skimage.segmentation.mark_boundaries. Produces a mask instead of a colored image.
    Parameters:
        seg: ndarray(sy, sx) of int
        mode: str; any of ['upper-left', 'lower-right', 'both']; indicates which side of the border should be marked
        bordertype: str; any of ['4way', '8way']; '8way' will include corners
    Returns:
        boundaries_mask: ndarray(sy, sx) of bool_
    '''
    assert seg.ndim == 2
    assert mode in ['upper-left', 'lower-right', 'both']
    assert bordertype in ['4way', '8way']
    boundaries_mask = np.zeros_like(seg, dtype=np.bool_)
    if mode == 'upper-left' or 'both':
        boundaries_mask[:-1,:] |= seg[:-1,:] != seg[1:,:]
        boundaries_mask[:,:-1] |= seg[:,:-1] != seg[:,1:]
        if bordertype == '8way':
            boundaries_mask[:-1,:-1] |= seg[:-1,:-1] != seg[1:,1:]
            boundaries_mask[:-1,1:] |= seg[:-1,1:] != seg[1:,:-1]
    if mode == 'lower-right' or 'both':
        boundaries_mask[1:,:] |= seg[:-1,:] != seg[1:,:]
        boundaries_mask[:,1:] |= seg[:,:-1] != seg[:,1:]
        if bordertype == '8way':
            boundaries_mask[1:,1:] |= seg[:-1,:-1] != seg[1:,1:]
            boundaries_mask[1:,:-1] |= seg[1:,:-1] != seg[:-1,1:]
    return boundaries_mask


# superpixel segmentation error metrics


def compute_segmentation_labeling_error(true_labels, pred_labels, n_labels, method, seg_sizes=None):
    '''
    Evaluates predicted labeling compared to the true labeling.
    Paramters:
        true_labels: ndarray(n_segments,) of int
        pred_labels: ndarray(n_segments,) of int
        n_labels: int; number of label categories
        method: str; any of ['sp_accuracy', 'sp_size_accuracy', 'mean_j_sp'];
        seg_sizes: None OR ndarray(n_segments,) of int
    '''
    assert method in ['sp_accuracy', 'sp_size_accuracy', 'mean_j_sp']

    if method == 'sp_accuracy':
        return np.count_nonzero(pred_labels == true_labels) / float(pred_labels.shape[0])
    elif method == 'sp_size_accuracy':
        assert seg_sizes is not None
        matching = pred_labels == true_labels
        return np.sum(seg_sizes[matching]) / float(np.sum(seg_sizes))
    elif method == 'mean_j_sp':
        assert seg_sizes is not None
        masks_pred = np.zeros(pred_labels.shape + (n_labels,), dtype=np.int32)  # (n_sps, n_labels)
        masks_true = np.zeros(true_labels.shape + (n_labels,), dtype=np.int32)       # (n_sps, n_labels)
        mask_pred_valid = pred_labels >= 0       # (n_sps)
        masks_pred[mask_pred_valid, pred_labels[mask_pred_valid]] = 1
        masks_true[np.arange(true_labels.shape[0]), true_labels] = 1
        masks_i = masks_pred & masks_true    # (n_sps, n_labels)
        masks_u = masks_pred | masks_true    # (n_sps, n_labels)
        ious = np.sum(seg_sizes[:,None] * masks_i, axis=0) / \
                         np.sum(seg_sizes[:,None] * masks_u, axis=0)   # (n_labels)
        return np.mean(ious)

def compute_pixelwise_labeling_error(true_lab_im, pred_lab_im, n_labels):
    '''
    Computes mean fg-only IoU error of true and predicted pixelwise labelings.
    Paramters:
        true_lab_im: ndarray(n_frames, sy, sx) of int
        pred_lab_im: ndarray(n_frames, sy, sx) of int
        n_labels: int; number of label categories
    '''
    assert true_lab_im.ndim == 3
    assert true_lab_im.shape == pred_lab_im.shape
    # Note: one-hot encoding below
    #       the ops below seem inefficient as O(n_pixels*n_labels) writing ops are necessary, instead of the minimum O(n_pixels) ops
    #       however, this is a much faster solution than creating one-hot encoding using mgrid advanced indexing or indexing np.eye(n_labels)
    # Following DAVIS benchmark, IoU is computed for each frame, each foreground object, and the mean is taken (bg labels are ignored)
    pred_labels = pred_lab_im[:,:,:,None] == np.arange(1, n_labels)   # (n_fr, sy, sx, n_fg_labels) of bool_
    true_labels = true_lab_im[:,:,:,None] == np.arange(1, n_labels)   # (n_fr, sy, sx, n_fg_labels) of bool_
    c_intersection = np.count_nonzero(pred_labels & true_labels, axis=(1,2))   # (n_fr, n_labels)
    c_union = np.count_nonzero(pred_labels | true_labels, axis=(1,2))   # (n_fr, n_labels)
    # replace indices where c_union is zero (label is not present -> iout is set to 1.)
    no_true_label_mask = c_union == 0
    c_intersection[no_true_label_mask] = 1
    c_union[no_true_label_mask] = 1
    return np.mean(np.mean(c_intersection / c_union, axis=1))   # first, taking mean of fg labels, then taking mean of frames


