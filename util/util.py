
#
# Shared utils
#
#   @author Viktor Varga
#

import time
import datetime
import numpy as np

def generate_session_id():
    ts = time.time()
    tsf = datetime.datetime.fromtimestamp(ts).strftime('%y%m%d_%H%M%S')
    return tsf + '_' + str(np.random.randint(1000)).zfill(3)

def to_logscale_bidir(data, base=2., min_exp=-4., max_exp=8.):
    '''
    Transforms data to bidirectional logscale.
        abs(x) <= base ^ min_exp    -> 0.5
        x <= - base ^ max_exp       -> 0.
        x >= base ^ max_exp         -> 1.
    Parameters:
        data: ndarray(?) of float
        base, min_exp, max_exp: float
    Returns:
        data_logscale: ndarray(?) of float32; values are between 0. and 1.
    '''
    assert base > 1.
    assert min_exp < max_exp
    EPS = 0.0000001
    assert np.power(base, min_exp) > EPS
    data_fabs = np.fabs(data)
    data_fabs = np.maximum(data_fabs, EPS)
    logbase_data = np.log(data_fabs)/np.log(base)
    logbase_data = np.clip(logbase_data, min_exp, max_exp)
    logbase_data = (logbase_data-min_exp)/((max_exp-min_exp)*2.)   # 0 .. 0.5
    sign = np.sign(data)
    data_logscale = np.full_like(data, dtype=np.float32, fill_value=np.nan)
    pos_mask = sign == 1.
    zero_mask = sign == 0.
    neg_mask = sign == -1.
    data_logscale[pos_mask] = logbase_data[pos_mask] + 0.5
    data_logscale[zero_mask] = 0.5
    data_logscale[neg_mask] = -logbase_data[neg_mask] + 0.5
    return data_logscale

def get_multirange_idxs(ranges):
    '''
    Returns index array which indexes each row of the array with a differnt range.
    Does not support negative indices.
    Parameters:
        ranges: ndarray(n_slices, 2:[start_range, end_range]) of int
    Returns:
        idxs: tuple(ndarray(n_idxs,), ndarray(n_idxs,)), tuple(2) index array
    '''
    assert np.amin(ranges) >= 0
    n_ranges = ranges.shape[0]
    assert ranges.shape[1:] == (2,)
    counts = np.maximum(ranges[:,1]-ranges[:,0], 0)   # (n_ranges,)
    idxs0 = np.repeat(np.arange(n_ranges), counts)   # (n_ranges,)
    nonempty_ranges = counts > 0
    ranges = ranges[nonempty_ranges,:]  # removing empty ranges
    counts = counts[nonempty_ranges]  # removing empty ranges
    idxs1 = np.repeat(ranges[:,0], counts)
    idxs1_in_range = np.arange(idxs1.shape[0])-np.maximum.accumulate(np.pad(idxs1[1:] != idxs1[:-1], (1,0))*np.arange(idxs1.shape[0]))
    idxs1 = idxs1+idxs1_in_range
    return (idxs0, idxs1)

def get_idx_array_from_multirange(ranges):
    '''
    Returns 1D index array from multiple ranges.
    Does not support negative indices.
    Parameters:
        ranges: ndarray(n_slices, 2:[start_range, end_range]) of int
    Returns:
        idxs: ndarray(n_idxs,) index array
    '''
    idxs0, idxs1 = get_multirange_idxs(ranges)
    idxs = np.unique(idxs1)
    return idxs

# REPLACE

def replace(arr, vals_from, vals_to, copy=True):
    '''
    Replaces values in array.
    Parameters:
        arr: ndarray(?) of int; MODIFIED if copy is False
        vals_from: ndarray(n_seg_ids_to_replace) of int
        vals_to: ndarray(n_seg_ids_to_replace) of int
        copy: bool; if False, 'arr' is modified and returned
    Returns:
        arr: ndarray(?) of int; if copy is False, the modified 'arr' param is returned.
    '''
    assert len(np.unique(vals_from)) == len(vals_from)  # assert unique vals_from
    assert arr.ndim > 0
    assert arr.dtype == vals_from.dtype == vals_to.dtype
    assert vals_from.ndim == 1
    assert vals_from.shape == vals_to.shape
    if copy:
        arr = np.copy(arr)
    val_sorter = np.argsort(vals_from)
    vals_from = vals_from[val_sorter]
    vals_to = vals_to[val_sorter]
    segs_replace_mask = np.isin(arr, vals_from)     # bool mask for arr
    seg_idxs = np.searchsorted(vals_from, arr)      # same shape as arr
    np.place(arr, segs_replace_mask, vals_to[seg_idxs[segs_replace_mask]])
    return arr

def replace_all(arr, vals_from, vals_to, copy=True):
    '''
    UNTESTED
    Replaces all values in array. All values in 'arr' must be present in 'vals_from'.
    Parameters:
        arr: ndarray(?) of int; MODIFIED if copy is False
        vals_from: ndarray(n_seg_ids_to_replace) of int
        vals_to: ndarray(n_seg_ids_to_replace) of int
        copy: bool; if False, 'arr' is modified and returned
    Returns:
        arr: ndarray(?) of int; if copy is False, the modified 'arr' param is returned.
    '''
    assert len(np.unique(vals_from)) == len(vals_from)  # assert unique vals_from
    assert arr.ndim > 0
    assert arr.dtype == vals_from.dtype == vals_to.dtype
    assert vals_from.ndim == 1
    assert vals_from.shape == vals_to.shape
    assert np.all(np.isin(arr, vals_from))
    if copy:
        arr = np.copy(arr)
    assert False, "Not implemented"

    # TODO implement, Laci's work:
    #    (requires all arr values to be present in vals_from)
    #    (handle case when arr does not have values from np.arange(max_val+1) with np.unique())
    #    (separate case could be to not use unique if vals are in np.arange(max_val+1))
    # def map_labels_all(arr, vals_from, vals_to):
    #   max_id = np.max(arr)
    #   lookup_table = np.empty((max_id + 1), dtype=np.uint8)
    #   lookup_table[vals_from] = vals_to
    #   relabeled = lookup_table[arr.reshape(-1)].reshape(arr.shape)
    #   return relabeled

    return arr

# SINGLE CHANNEL <-> MULTI CHANNEL

def convert_multichannel_to_int(arr, out_dtype):
    '''
    Stores a multichannel non-negative array in a single channel uint array.
    The last axis is merged into a single value.
    Paramters:
        arr: ndarray(..., n_ch) of int or uint type
        out_dtype: output dtype; np.uint8, np.uint16, np.uint32 or np.uint64
    Returns:
        arr_singlech: ndarray(...) of <out_dtype>
        max_vals: ndarray(n_ch); the maximum values of each channel required to restore the original array
    '''
    assert arr.ndim >= 2
    assert arr.shape[-1] >= 2
    assert arr.dtype in [np.bool_, np.int8, np.int16, np.int32, np.int64, np.uint8, np.uint16, np.uint32, np.uint64]
    assert out_dtype in [np.uint8, np.uint16, np.uint32, np.uint64]
    assert np.amin(arr) >= 0
    max_vals = np.amax(arr, axis=tuple(range(arr.ndim-1))).astype(np.uint64)   # (n_ch,)
    assert np.prod((max_vals+1).astype(np.object))-1 <= np.iinfo(out_dtype).max  # conversion to object type to support bigint
    muls = np.insert(np.cumprod(max_vals[:-1]+1), obj=0, values=1)  # (n_ch,)
    arr_singlech = np.sum(arr*muls, axis=-1).astype(out_dtype)
    return arr_singlech, max_vals

def convert_multichannel_ui8_to_ui32(arr, copy=True):
    '''
    Stores a multichannel (up to 4 channels) uint8 array in a single channel uint32 array. 
    The last axis is merged into a single value.
    Paramters:
        arr: ndarray(..., n_ch<=4) of uint8
        copy: bool; IF 'arr' is a 4-channel uint8 array and 'copy' is False, a view of 'arr' is returned
    Returns:
        arr_singlech: ndarray(...) of uint32
    '''
    assert arr.ndim >= 2
    assert arr.shape[-1] >= 2
    assert arr.dtype == np.uint8
    n_ch = arr.shape[-1]
    if (n_ch == 4) and (not copy):
        arr_singlech = arr.view(dtype=np.uint32)
    else:
        arr_singlech = np.empty(arr.shape[:-1] + (4,), dtype=np.uint8)   # (..., 4)
        arr_singlech[...,:n_ch] = arr
        arr_singlech[...,n_ch:] = 0
        arr_singlech = arr_singlech.view(dtype=np.uint32)
    return arr_singlech[..., 0]

def convert_multichannel_ui32_to_ui64(arr, copy=True):
    '''
    Stores a multichannel (2 channels) uint32 array in a single channel uint64 array. 
    The last axis is merged into a single value.
    Paramters:
        arr: ndarray(..., n_ch=2) of uint32
        copy: bool; IF 'copy' is False, a view of 'arr' is returned
    Returns:
        arr_singlech: ndarray(...) of uint64
    '''
    assert arr.ndim >= 2
    assert arr.shape[-1] == 2
    assert arr.dtype == np.uint32
    arr_singlech = arr.view(dtype=np.uint64)
    if copy:
        arr_singlech = arr_singlech.copy()
    return arr_singlech[..., 0]

def convert_multichannel_i32_to_i64(arr, copy=True):
    '''
    Stores a multichannel (2 channels) int32 array in a single channel int64 array. 
    The last axis is merged into a single value.
    Paramters:
        arr: ndarray(..., n_ch=2) of int32
        copy: bool; IF 'copy' is False, a view of 'arr' is returned
    Returns:
        arr_singlech: ndarray(...) of int64
    '''
    assert arr.ndim >= 2
    assert arr.shape[-1] == 2
    assert arr.dtype == np.int32
    arr_singlech = arr.view(dtype=np.int64)
    if copy:
        arr_singlech = arr_singlech.copy()
    return arr_singlech[..., 0]

def restore_multichannel_from_int(arr_singlech, max_vals, out_dtype):
    '''
    Restores the original array after it was converted with 'convert_multichannel_to_int()'.
    Paramters:
        arr_singlech: ndarray(...)
        max_vals: ndarray(n_ch); the maximum values of each channel required to restore the original array
        out_dtype: output array dtype;
    Returns:
        arr: ndarray(..., n_ch) of <out_dtype>
    '''
    import time
    t1 = time.time()
    assert np.amin(arr_singlech) >= 0
    assert np.all(max_vals <= np.iinfo(out_dtype).max)
    muls = np.insert(np.cumprod(max_vals[:-1]+1), obj=0, values=1)    # (n_ch,)
    t2 = time.time()
    arr = np.empty(arr_singlech.shape + (max_vals.shape[0],), dtype=out_dtype)
    arr_singlech_rem = arr_singlech
    # reversed channel iteration: 19msec
    #   (tried vectorize version with two ops (// and %) only, 2-3x slower for unknown reason in 3xuint8 conversion)
    t3 = time.time()
    for ch_idx in range(max_vals.shape[0]-1,-1,-1):
        arr[..., ch_idx] = arr_singlech_rem // muls[ch_idx]
        arr_singlech_rem = arr_singlech_rem % muls[ch_idx]
    t4 = time.time()
    return arr

    # TODO rename (incorrect function name)
def restore_ui32_from_multichannel_ui8(arr_singlech, n_ch, copy=True):
    '''
    Restores a multichannel (up to 4 channels) uint8 array from a single channel uint32 array.
    Paramters:
        arr_singlech: ndarray(...) of uint32
        n_ch: int; number of output channels
        copy: bool; if False, a view of 'arr_singlech' is returned
    Returns:
        arr: ndarray(..., n_ch) of uint8
    '''
    assert 2 <= n_ch <= 4
    assert arr_singlech.dtype == np.uint32
    arr = arr_singlech[..., None].view(dtype=np.uint8)[..., :n_ch]
    if copy:
        arr = arr.copy()
    return arr

def restore_multichannel_ui32_from_ui64(arr_singlech, copy=True):
    '''
    Restores a 2-channel uint32 array from a single channel uint64 array.
    Paramters:
        arr_singlech: ndarray(...) of uint64
        copy: bool; if False, a view of 'arr_singlech' is returned
    Returns:
        arr: ndarray(..., n_ch) of uint32
    '''
    assert arr_singlech.dtype == np.uint64
    arr = arr_singlech[..., None].view(dtype=np.uint32)
    assert arr.shape == arr_singlech.shape + (2,)
    if copy:
        arr = arr.copy()
    return arr

def restore_multichannel_i32_from_i64(arr_singlech, copy=True):
    '''
    Restores a 2-channel int32 array from a single channel int64 array.
    Paramters:
        arr_singlech: ndarray(...) of int64
        copy: bool; if False, a view of 'arr_singlech' is returned
    Returns:
        arr: ndarray(..., n_ch) of int32
    '''
    assert arr_singlech.dtype == np.int64
    arr = arr_singlech[..., None].view(dtype=np.int32)
    assert arr.shape == arr_singlech.shape + (2,)
    if copy:
        arr = arr.copy()
    return arr

# APPLY FUNC/UFUNC WITH GROUPBY

def apply_ufunc_with_groupby(values_arr, ids_arr, npy_ufunc, init_val, assume_arange_to=None):
    '''
    Accumulates values within ID groups independently with 'npy_ufunc'. Implemented with ufunc.at().
    Parameters:
        values_arr: ndarray(?) of ?
        ids_arr: ndarray(<values_arr.shape>) of int
        npy_ufunc: numpy.ufunc function
        init_val: ?; initial value for the accumulation
        assume_arange_to: None or int; if given, assumes ids_arr to be in a subset of range(0,assume_arange_to);
                                            thus the unique operation is skipped.
    Returns:
        results: ndarray(n_IDs) of ?; for each ID in 'ids_arr', returns the result; IDs are sorted
            IF 'assume_arange_to' is given, return array shape is ndarray(assume_arange_to).
    '''
    if assume_arange_to is None:
        u_ids, ids_arr = np.unique(ids_arr, return_inverse=True)
        assume_arange_to = u_ids.shape[0]
    results = np.full((assume_arange_to,), fill_value=init_val)
    npy_ufunc.at(results, ids_arr, values_arr)   # results[ids_arr] += values_arr
    return results

def apply_func_with_groupby_manyIDs(values_arr, ids_arr, func, assume_arange_to=None, empty_val=0):
    '''
    Applies 'func' to all values within ID groups independently.
        Can be used instead of 'apply_ufunc_with_groupby()' when accumulation with a specific ufunc is not possible.
        Note, that the required signature of 'func' is different.
        Use this version if there are many IDs, otherwise use apply_func_with_groupby_fewIDs.
    Parameters:
        values_arr: ndarray(?) of ?
        ids_arr: ndarray(<values_arr.shape>) of int
        func: Callable with signature ndarray(n_items) of <T> -> scalar <T>
        assume_arange_to: None or int; if given, assumes ids_arr to be in a subset of range(0,assume_arange_to);
                                            thus the unique operation is skipped.
        empty_val: scalar <type of 'values_arr'>; if 'assume_arange_to' is given, result for non-existent IDs
                                                                    in range are set to 'empty_val'
    Returns:
        results: ndarray(n_IDs) of ?; for each ID in 'ids_arr', returns the result; IDs are sorted
            IF 'assume_arange_to' is given, return array shape is ndarray(assume_arange_to).
    '''
    if assume_arange_to is None:
        u_ids, ids_arr = np.unique(ids_arr, return_inverse=True)
        assume_arange_to = u_ids.shape[0]
    # algorithm: argsorting IDs and reordering ID indices with it, splitting ID indices where there are skips in sorted IDs
    #   a naive solution (with masks for each ID) would have O(values_arr_size * n_IDs) computation cost
    #   this solution has O(values_arr_size*log(values_arr_size) + values_arr_size) computation cost
    values_arr, ids_arr = values_arr.reshape(-1), ids_arr.reshape(-1)
    id_sorter = np.argsort(ids_arr)
    ids_sorted = ids_arr[id_sorter]
    split_idxs = np.where(ids_sorted[1:] != ids_sorted[:-1])[0]+1
    idx_groups = np.split(id_sorter, split_idxs)
    group_ids = ids_sorted[np.pad(split_idxs, (1,0))]
    assert len(idx_groups) == group_ids.shape[0]
    results = np.full((assume_arange_to,), fill_value=empty_val, dtype=values_arr.dtype)
    for group_id_idx in range(group_ids.shape[0]):
        group_id, group_idxs = group_ids[group_id_idx], idx_groups[group_id_idx]
        if group_idxs.shape[0] == 0:
            continue
        vals_in_group = values_arr[group_idxs]
        results[group_id] = func(vals_in_group)
    return results

def apply_func_with_groupby_fewIDs(values_arr, ids_arr, func, assume_arange_to=None, empty_val=0):
    '''
    Applies 'func' to all values within ID groups independently.
        Can be used instead of 'apply_ufunc_with_groupby()' when accumulation with a specific ufunc is not possible.
        Note, that the required signature of 'func' is different.
        Use this version if there are few IDs, otherwise use apply_func_with_groupby_manyIDs.
    Parameters:
        values_arr: ndarray(?) of ?
        ids_arr: ndarray(<values_arr.shape>) of int
        func: Callable with signature ndarray(n_items) of <T> -> scalar <T>
        assume_arange_to: None or int; if given, assumes ids_arr to be in a subset of range(0,assume_arange_to);
                                            thus the unique operation is skipped.
        empty_val: scalar <type of 'values_arr'>; if 'assume_arange_to' is given, result for non-existent IDs
                                                                    in range are set to 'empty_val'
    Returns:
        results: ndarray(n_IDs) of ?; for each ID in 'ids_arr', returns the result; IDs are sorted
            IF 'assume_arange_to' is given, return array shape is ndarray(assume_arange_to).
    '''
    if assume_arange_to is None:
        u_ids, ids_arr = np.unique(ids_arr, return_inverse=True)
        assume_arange_to = u_ids.shape[0]
    # algorithm: naive solution (with masks for each ID), O(values_arr_size * n_IDs) computation cost
    results = np.full((assume_arange_to,), fill_value=empty_val, dtype=values_arr.dtype)
    for id in range(assume_arange_to):
        results[id] = func(values_arr[ids_arr == id])
    return results

# SAMPLING

def split_equally_random_remainders(n_samples, n_buckets):
    '''
    Splits integer 'n_samples' to integer numbers z_i, i=1..'n_buckets', that sum_i(z_i) = 'n_samples'.
    z_i = floor(n_samples/n_buckets) [+1]. Plus ones are distributed randomly.
    E.g. split_random_equally(7,2) could return [3,4] or [4,3]
    Parameters:
        n_samples, n_buckets: int
    Returns:
        bucket_sizes: ndarray(n_buckets) of int32
    '''
    bucket_size = n_samples // n_buckets
    remainder = n_samples % n_buckets
    bucket_sizes = np.full((n_buckets,), dtype=np.int32, fill_value=bucket_size)
    remainder_idxs = np.random.choice(np.arange(n_buckets), size=remainder, replace=False)
    bucket_sizes[remainder_idxs] += 1
    return bucket_sizes

def random_sample_balanced(labels, n_samples_per_cat, n_cats=None):
    '''
    Randomly sample idxs from 'labels' with balanced labels.
    Parameters:
        labels: ndarray(n_samples,) of int32; labels from 0..n_cats-1, all labels must be present
        n_samples_per_cat: int
        n_cats: None or int; the number of categories
    Returns:
        idxs: ndarray(n_cats, n_samples_per_cat) of int32
    '''
    assert labels.ndim == 1
    if n_cats is None:
        n_cats = np.amax(labels)+1
    idxs = np.empty((n_cats, n_samples_per_cat), dtype=np.int32)
    for cat in range(n_cats):
        cat_idxs = np.where(labels == cat)[0]
        assert cat_idxs.shape[0] > 0
        idxs[cat,:] = np.random.choice(cat_idxs, size=(n_samples_per_cat,))
    return idxs

# SORTED

def is_sorted(arr, axis=-1):
    '''
    O(n) test of sortedness.
    Parameters:
        arr: ndarray(?)
        axis: int; the axis along which elements are tested whether sorted
    Returns:
        (bool) whether elements are sorted along axis in arr
    '''
    return np.all(np.diff(arr, axis=axis) >= 0)

def isin_sorted(elems_to_test, arr, check_sorted=True):
    '''
    Same functionality as np.isin(), but assumes 'arr' to be sorted. Log-time search is used instead of linear search.
    Parameters:
        elems_to_test: ndarray(<elems_to_test_shape>) of <sortable numpy type>
        arr: ndarray(arr_len,) of <sortable numpy type>
    Returns:
        elems_in: ndarray(<elems_to_test_shape>) of bool; True if specific element was in 'arr'
    '''
    assert arr.ndim == 1
    if check_sorted:
        assert is_sorted(arr)
    insert_idxs = np.searchsorted(arr, elems_to_test)
    valid_insert_idxs_mask = insert_idxs < arr.shape[0]
    elems_in = np.zeros_like(elems_to_test, dtype=np.bool_)
    elems_in[valid_insert_idxs_mask] = arr[insert_idxs[valid_insert_idxs_mask]] == elems_to_test[valid_insert_idxs_mask]
    return elems_in

