
#
# Visualization utils
#
#   @author Viktor Varga
#

import skimage.segmentation
import skimage.measure

import util.util as Util
import util.imutil as ImUtil
from util.vid_writer import VideoWriter

import os
import cv2
import numpy as np


def render_segmentation_labels(segs, seg_labels, seg_color_max_alpha=None, bg_img=None, label_colors=None,\
                                         color_alpha=.5, bg_alpha=.5, boundary_color_rgb=None, render_info_str=None):
    '''
    Visualizes segmentation with labeling and boundaries in image.
    Parameters:
        segs: ndarray(size_y, size_x) of uint/int; the segment IDs
        seg_labels: ndarray(n_segs,) of int; the label for each segment
        seg_color_max_alpha: None OR ndarray(n_segs,) of bool_; 
                        for each segment, if True, color alpha is set to 1., while img alpha to 0.
                        if None, behaviour is similar to passing an all False array
        bg_img: None OR ndarray(size_y, size_x, 3:n_ch:[bgr]) of uint8; the background image with 'bg_alpha' opacity
        label_colors: None OR dict{lab_id - int: color - array-like(3:rgb) of uint8}; the fill colors of the segmentation.
                        if None segments are not filled
                        if segment ID is not present in the dict keys, the corresponding segment is not filled
        color_alpha: float; the alpha-channel value of the segment filling
        bg_alpha: float; the alpha-channel value of the background image
        boundary_color_rgb: None or array-like(3) of uint8 the boundary color
        render_info_str: None or str
    Returns:
        viz_img: ndarray(size_y, size_x, 3:n_ch:[bgr]) of uint8
    '''
    assert segs.ndim == 2
    assert seg_labels.ndim == 1
    assert seg_color_max_alpha is None or seg_color_max_alpha.shape == seg_labels.shape
    assert bg_img is None or segs.shape == bg_img.shape[:2]
    assert boundary_color_rgb is None or len(boundary_color_rgb) == 3
    assert 0. <= color_alpha <= 1.
    assert 0. <= bg_alpha <= 1.

    # if background is not specified, use plain black image
    if bg_img is None:
        bg_img = np.full(segs.shape + (3,), dtype=np.uint8, fill_value=0)

    viz_img = np.zeros_like(bg_img)   # color fill and boundaries

    # rendering color fill
    if label_colors is not None:
        # getting color for each seg
        seg_colors_bgr = np.zeros((seg_labels.shape[0], 3), dtype=np.uint8)   # (n_segs, 3:bgr)
        for lab, color_rgb in label_colors.items():
            seg_colors_bgr[seg_labels == lab,:] = color_rgb[::-1]

        # creating color filled image
        seg_colors_bgr_flat = Util.convert_multichannel_ui8_to_ui32(seg_colors_bgr)
                                                                         # rgb stored in single chan int32 img
        seg_img_colors = Util.replace(segs, np.arange(seg_labels.shape[0], dtype=np.uint32),\
                                                 seg_colors_bgr_flat, copy=True)
                                          # segs shaped image with colors coded into single chan int32
        seg_img_colors = Util.restore_ui32_from_multichannel_ui8(seg_img_colors, n_ch=3) # (sy, sx, 3) ui8
        viz_img = cv2.addWeighted(bg_img, bg_alpha, seg_img_colors, color_alpha, 0.)

        # rendering color fill for maximum opacity segments
        if (seg_color_max_alpha is not None) and np.any(seg_color_max_alpha):
            regions = skimage.measure.regionprops(segs+1, cache=True)   # rprops ignore label 0
            for region in regions:
                bbox = region.bbox
                reg_id = region.label
                if seg_color_max_alpha[reg_id-1]:
                    target_in_bbox = viz_img[bbox[0]:bbox[2], bbox[1]:bbox[3]]  # only a view of viz_img
                    target_in_bbox[region.image,:] = seg_colors_bgr[reg_id-1]   # overwrite segment with color

    # rendering boundaries
    if boundary_color_rgb is not None:
        boundary_color_fl_rgb = tuple(np.asarray(boundary_color_rgb, dtype=np.float32)[::-1] / 255.)
        viz_img = viz_img.astype(np.float32) / 255.
        viz_img = skimage.segmentation.mark_boundaries(viz_img, segs, color=boundary_color_fl_rgb)
        viz_img = (viz_img * 255.).astype(np.uint8)

    # render info text to upper left corner
    if render_info_str is not None:
        cv2.putText(viz_img, render_info_str, (10,20), fontFace=cv2.FONT_HERSHEY_SIMPLEX, \
                        fontScale=.5, color=(255,255,0), thickness=2)

    return viz_img


def render_predictions_video(fpath_out, pred_ys, sp_is_gt, img_data, seg_data):
    '''
    Parameters:
        fpath_out: str
        pred_ys: ndarray(n_sps,) of i32
        sp_is_gt: ndarray(n_sps,) of bool_
        img_data: ndarray(n_imgs, size_y, size_x, 3:RGB) of ui8
        seg_data: Segmentation instance
    '''
    LABEL_COLORS = {0:(192,192,192), 1:(255,0,0), 2:(0,0,255), 3:(0,255,0), 4:(255,0,255),\
                    5:(0,255,255), 6:(255,127,0), 7:(192,127,255)}

    vid_writer = VideoWriter(img_data.shape[1:3], fpath_out, vr_fps=25.)
    for fr_idx in range(img_data.shape[0]):
        bg_img = img_data[fr_idx,:,:,::-1]

        sp_id_offset, sp_id_end_offset = seg_data.get_sp_id_range_in_frame(fr_idx)
        seg_img_fr = seg_data.sp_seg[fr_idx,:,:] - sp_id_offset
        pred_ys_fr = pred_ys[sp_id_offset:sp_id_end_offset]
        sp_is_gt_fr = sp_is_gt[sp_id_offset:sp_id_end_offset]

        render_info_str = "Frame idx: " + str(fr_idx)
        viz_img = render_segmentation_labels(segs=seg_img_fr, seg_labels=pred_ys_fr,\
                             seg_color_max_alpha=sp_is_gt_fr, bg_img=bg_img,\
                             label_colors=LABEL_COLORS, color_alpha=.5, bg_alpha=.5,\
                              boundary_color_rgb=(200,200,0), render_info_str=render_info_str)
        vid_writer.write(viz_img)
    vid_writer.release()


def render_segmentation_edges_BGRA(segs, boundary_color_rgb, boundary_alpha=1.):
    '''
    Renders segmentation edges into a BGRA format image.
    Parameters:
        segs: ndarray(size_y, size_x) of uint/int; the segment IDs
        boundary_color_rgb: tuple(3) of uint8
        boundary_alpha: float; the alpha-channel value of the boundaries
    Returns:
        viz_img: ndarray(size_y, size_x, 4:n_ch:[bgra]) of uint8
    '''
    assert segs.ndim == 2
    assert len(boundary_color_rgb) == 3
    assert 0. <= boundary_alpha <= 1.
    '''
    # another version using skimage.segmentation.mark_boundaries()
    alpha_img = np.zeros_like(segs, dtype=np.float32)
    alpha_img = skimage.segmentation.mark_boundaries(alpha_img, segs, color=(1,1,1))  # (sy, sx, 3)
    alpha_img = (np.mean(alpha_img, axis=-1)*255.).astype(np.uint8)       # (sy, sx)
    viz_img = np.zeros(segs.shape + (4,), dtype=np.uint8)
    viz_img[alpha_img > 0, :3] = boundary_color_rgb[::-1]
    viz_img[:,:,3] = alpha_img
    '''
    border_mask = ImUtil.mark_boundaries_fast(segs, mode='upper-left', bordertype='4way')
    viz_img = np.zeros(segs.shape + (4,), dtype=np.uint8)
    viz_img[border_mask, :3] = boundary_color_rgb[::-1]
    viz_img[border_mask, 3] = int(boundary_alpha*255.)
    return viz_img

def render_segmentation_edges_BGR(bg_img, segs, boundary_color_rgb):
    '''
    Renders segmentation edges into a BGR format image.
    Parameters:
        (MODIFIED) bg_img: ndarray(size_y, size_x, 3:n_ch:[bgr]) of uint8; the background image with 'bg_alpha' opacity
        segs: ndarray(size_y, size_x) of uint/int; the segment IDs
        boundary_color_rgb: tuple(3) of uint8
    Returns:
        bg_img: ndarray(size_y, size_x, 3:n_ch:[bgr]) of uint8
    '''
    assert bg_img.shape[2:] == (3,)
    assert segs.shape == bg_img.shape[:2]
    edge_img = render_segmentation_edges_BGRA(segs, boundary_color_rgb, boundary_alpha=1.)  # (sy, sx, 4:bgra)
    bg_img[np.any(edge_img > 0, axis=-1), :] = boundary_color_rgb[::-1]
    return bg_img


def render_segmentation_labels_BGRA(segs, seg_labels, label_colors_rgb, color_alpha, seg_is_gt=None, color_alpha_gt=1.0):
    '''
    Renders segmentation labeling into a BGRA format image.
    Parameters:
        segs: ndarray(size_y, size_x) of uint/int; the segment IDs (from 0..n_segs)
        seg_labels: ndarray(n_segs,) of int; the label for each segment; if negative, not filled
        label_colors_rgb: array-like(n_labels, 3:rgb) of uint8
        color_alpha: float; the alpha-channel value of the segment filling
        seg_is_gt: None OR ndarray(n_segs,) of bool_; whether to use 'color_alpha_gt' (True)
                                                                  or 'color_alpha' for each segment
        color_alpha_gt: float; the alpha-channel value of the segment filling for segments where 'seg_is_gt' is True
    Returns:
        viz_img: ndarray(size_y, size_x, 4:n_ch:[bgra]) of uint8
    '''
    assert segs.ndim == 2
    assert seg_labels.ndim == 1
    assert (seg_is_gt is None) or (seg_is_gt.shape == seg_labels.shape)
    label_colors_bgr = np.asarray(label_colors_rgb, dtype=np.uint8)[:,::-1]
    assert label_colors_bgr.shape[1:] == (3,)
    assert 0. <= color_alpha <= 1.
    assert 0. <= color_alpha_gt <= 1.

    viz_img = np.zeros(segs.shape + (4,), dtype=np.uint8)

    # create BGR channels
    label_colors_bgr_flat = Util.convert_multichannel_ui8_to_ui32(label_colors_bgr)
    seg_labeled = seg_labels[segs]
    assert seg_labeled.shape == segs.shape  # TODO remove
    label_colors_bgr_flat = np.insert(label_colors_bgr_flat, 0, 0)  # add dummy color for label -1
    seg_flatcolored = label_colors_bgr_flat[seg_labeled+1]
    assert seg_flatcolored.shape == segs.shape  # TODO remove
    viz_img[:,:,:3] = Util.restore_ui32_from_multichannel_ui8(seg_flatcolored, n_ch=3)

    # create A channel
    alpha_values = np.array([0, int(color_alpha*255.), int(color_alpha_gt*255.)], dtype=np.uint8) # (3:[unlabeled, pred, gt])
    viz_img[:,:,3] = seg_labeled >= 0  # set labeled to 1
    if seg_is_gt is not None:
        seg_gt_image = seg_is_gt[segs]
        assert seg_gt_image.shape == segs.shape  # TODO remove
        viz_img[:,:,3][seg_gt_image] = 2  # set gt to 2
    viz_img[:,:,3] = alpha_values[viz_img[:,:,3]]

    return viz_img


def render_segmentation_labels_RGB(segs, seg_labels, label_colors_rgb, color_alpha, \
                                                bg=None, seg_is_gt=None, color_alpha_gt=1.):
    '''
    Renders segmentation labeling into an RGB format image with optional background.
    Parameters:
        segs: ndarray(size_y, size_x) of uint/int; the segment IDs (from 0..n_segs)
        seg_labels: ndarray(n_segs,) of int; the label for each segment; if negative, not filled
        label_colors_rgb: array-like(n_labels, 3:rgb) of uint8
        color_alpha: float; the alpha-channel value of the segment filling
        bg: None OR ndarray(size_y, size_x) of uint8 OR ndarray(size_y, size_x, 3:BGR) of uint8; optional bacground image
        seg_is_gt: None OR ndarray(n_segs,) of bool_; whether to use 'color_alpha_gt' opacity (True)
                                                                  or 'color_alpha' for each segment
        color_alpha_gt: float; the alpha-channel value of the segment filling for segments where 'seg_is_gt' is True
    Returns:
        viz_img: ndarray(size_y, size_x, 3:n_ch:[rgb]) of uint8
    '''
    assert segs.ndim == 2
    assert seg_labels.ndim == 1
    assert (seg_is_gt is None) or (seg_is_gt.shape == seg_labels.shape)
    label_colors_rgb = np.asarray(label_colors_rgb, dtype=np.uint8)
    assert label_colors_rgb.shape[1:] == (3,)
    assert 0. <= color_alpha <= 1.
    assert (bg is None) or (bg.shape == segs.shape) or (bg.shape == segs.shape + (3,))

    if (bg is not None) and (bg.ndim == 2):
        bg = np.broadcast_to(bg[:,:,None], bg.shape + (3,))

    # create label image
    label_colors_rgb_flat = Util.convert_multichannel_ui8_to_ui32(label_colors_rgb)

    seg_labeled = seg_labels[segs]
    assert seg_labeled.shape == segs.shape  # TODO remove
    label_colors_rgb_flat = np.insert(label_colors_rgb_flat, 0, 0)  # add dummy color for label -1
    seg_flatcolored = label_colors_rgb_flat[seg_labeled+1]
    assert seg_flatcolored.shape == segs.shape  # TODO remove
    viz_img = Util.restore_ui32_from_multichannel_ui8(seg_flatcolored, n_ch=3)

    # create alpha img
    alpha_values = np.array([0, float(color_alpha), float(color_alpha_gt)], dtype=np.float32) # (3:[unlabeled, pred, gt])
    alpha_img = np.zeros_like(segs, dtype=np.int32)
    alpha_img[:] = seg_labeled >= 0  # set labeled to 1
    if seg_is_gt is not None:
        seg_gt_image = seg_is_gt[segs]
        assert seg_gt_image.shape == segs.shape  # TODO remove
        alpha_img[seg_gt_image] = 2  # set gt to 2
    alpha_img = alpha_values[alpha_img]   # alpha_img type becomes float32
    alpha_img = alpha_img[:,:,None]    # (size_y, size_x, 1)

    # blend bg and labels with alpha
    if bg is None:
        viz_img = (alpha_img*viz_img).astype(np.uint8)
    else:
        viz_img = ((1.-alpha_img)*bg + alpha_img*viz_img).astype(np.uint8)

    return viz_img
