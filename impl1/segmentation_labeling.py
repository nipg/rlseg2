
#
# Class storing segmentation and true labeling (from user). RLseg2 implementation #1
#
#   @author Viktor Varga
#

import os
import numpy as np

class SegLabeling:

    '''
    Member fields:
        
        seg: Segmentation instance
        
        n_labels: int
        user_annot: list(n_user_annots) of ndarray(n_segs_in_user_annot, 2:[SP id, label])
        user_labels: ndarray(n_sps,) of int32; -1 if missing; labels added by user (all others are -1)
        pred_labels: ndarray(n_sps,) of int32; -1 if missing; labels predicted (-1 if no labels predicted)

        label_estimation: None OR LabelEstimation subclass instance
        prev_update: int; the number of user_annot items that was included in the previous label prediction update;
                                    0 if there were no updates

    '''

    def __init__(self, seg):
        self.seg = seg
        self.label_estimation = None

        u_labels = np.unique(self.seg.sp_chans['gt_rounded'])
        assert np.array_equal(u_labels, np.arange(u_labels.shape[0])), "Assert labels from 0 to n_labels-1"
        assert u_labels.shape[0] >= 2
        self.n_labels = u_labels.shape[0]

        self.user_labels = np.empty((self.seg.get_n_sps_total(),), dtype=np.int32)
        self.pred_labels = np.empty_like(self.user_labels)
        self.reset()

    def get_n_labels(self):
        return self.n_labels

    def get_true_labels(self):
        '''
        Returns:
            ndarray(n_sps) of int
        '''
        return self.seg.sp_chans['gt_rounded'].reshape(-1)

    def set_label_estimation(self, lab_est):
        '''
        Parameters:
            lab_est: LabelEstimation subclass instance
        '''
        self.label_estimation = lab_est

    def reset(self):
        '''
        Clears all user annotations.
        Parameters:
            lab_est: LabelEstimation subclass instance
        '''
        self.user_annot = []
        self.user_labels[:] = -1
        self.pred_labels[:] = -1
        self.prev_update = 0
    #

    def get_n_user_inputs(self):
        # Returns the number of submits (with possibly multiple clicks), not the actual number of clicks.
        return len(self.user_annot)

    def get_n_user_annots_per_category(self):
        '''
        Returns:
            n_user_annots_per_cat: ndarray(n_cats,) of int; the number of user annots in each category
        '''
        if len(self.user_annot) == 0:
            return np.zeros((self.n_labels), dtype=np.int32)
        all_annots = np.concatenate(self.user_annot, axis=0)[:,1]
        n_user_annots_per_cat = np.bincount(all_annots, minlength=self.n_labels)
        return n_user_annots_per_cat

    def add_true_user_annot(self, sp_ids):
        '''
        Adds user annot using the true labels. Requires 'update_predictions()' call to recompute label predictions.
        Parameters:
            sp_ids: ndarray(n_segs_annotated) of int32
        '''
        labels = self.get_true_labels()[sp_ids]
        self.add_custom_user_annot(sp_ids, labels)

    def add_custom_user_annot(self, sp_ids, labels):
        '''
        Adds user annot with custom labels. Requires 'update_predictions()' call to recompute label predictions.
        Parameters:
            sp_ids: ndarray(n_segs_annotated) of int32
            labels: ndarray(n_segs_annotated) of int32
        '''
        assert sp_ids.size > 0
        assert np.all(labels >= 0) and np.all(labels < self.n_labels)
        assert np.all(self.user_labels[sp_ids] == -1)  # assert segments annotated were not already annotated
        self.user_annot.append(np.stack([sp_ids, labels], axis=-1))
        self.user_labels[sp_ids] = labels
        self.pred_labels[sp_ids] = labels

    def remove_last_user_annot(self):
        '''
        Removes the last user annot. Requires 'update_predictions()' call to recompute label predictions.
        '''
        assert self.get_n_user_inputs() > 0
        popped = self.user_annot.pop()   # (n_sps, 2:[SP id, label])
        self.user_labels[popped[:,0]] = -1

    def get_user_input_mask(self):
        '''
        Returns:
            user_input_mask: ndarray(n_sps) of bool_
        '''
        return self.user_labels >= 0
        

    def update_predictions(self):
        '''
        Updates self.pred_labels, self.prev_update.
        '''
        assert self.label_estimation is not None
        n_new_annots = len(self.user_annot)-self.prev_update
        self.label_estimation.fit(self.user_annot, n_new_annots)
        self.pred_labels = self.label_estimation.predict_all(return_probs=False)
        assert self.pred_labels.shape == self.user_labels.shape
        user_annot_mask = self.user_labels >= 0
        self.pred_labels[user_annot_mask] = self.user_labels[user_annot_mask]
        self.prev_update = len(self.user_annot)

    def evaluate_predictions(self, method):
        '''
        Evaluates predicted labeling compared to the true labeling.
        Paramters:
            method: str; any of ['sp_accuracy', 'sp_size_accuracy', 'mean_j_sp']; 
                            TODO implement 'mean_j_raw' using gt_raw (keep gt_raw image in cache)
        '''
        true_labels = self.get_true_labels()
        assert method in ['sp_accuracy', 'sp_size_accuracy', 'mean_j_sp']
        if method == 'sp_accuracy':
            return np.count_nonzero(self.pred_labels == self.get_true_labels()) / float(self.pred_labels.shape[0])
        elif method == 'sp_size_accuracy':
            matching = self.pred_labels == true_labels
            return np.sum(self.seg.sp_sizes[matching]) / float(np.sum(self.seg.sp_sizes))
        elif method == 'mean_j_sp':
            masks_pred = np.zeros(self.pred_labels.shape + (self.n_labels,), dtype=np.int32)  # (n_sps, n_labels)
            masks_true = np.zeros(true_labels.shape + (self.n_labels,), dtype=np.int32)       # (n_sps, n_labels)
            mask_pred_valid = self.pred_labels >= 0       # (n_sps)
            masks_pred[mask_pred_valid, self.pred_labels[mask_pred_valid]] = 1
            masks_true[np.arange(true_labels.shape[0]), true_labels] = 1
            masks_i = masks_pred & masks_true    # (n_sps, n_labels)
            masks_u = masks_pred | masks_true    # (n_sps, n_labels)
            ious = np.sum(self.seg.sp_sizes[:,None] * masks_i, axis=0) / \
                             np.sum(self.seg.sp_sizes[:,None] * masks_u, axis=0)   # (n_labels)
            return np.mean(ious)

