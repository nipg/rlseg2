
#
# A SV/SP segmentation of a video clip and all corresponding data. RLseg2 implementation #1
#
#   @author Viktor Varga
#

import sys
sys.path.append('..')

import os
import numpy as np
import skimage.measure
import time

import impl_util as ImplUtil
import util.imutil as ImUtil


class Segmentation:

    '''
    Member fields:

        data_source: str; either 'cache' or 'init' based on whether 'self' object was
                     created by loading from cache or throug __init__ from raw data
                     (NOT SERIALIZED) 

        sv_seg: ndarray(n_frames, size_y, size_x) of uint32; SV IDs
        sp_seg: ndarray(n_frames, size_y, size_x) of uint32; SP IDs

        sv_sizes: ndarray(n_svs) of uint32
        sp_sizes: ndarray(n_sps) of uint32

        sp_id_fr_end_offsets: ndarray(n_frames,) of uint32; SP ID offests
        sv_to_sp: ndarray(n_svs, n_frames) of uint32; SP IDs for each SV in each frame
        sp_to_sv: ndarray(n_sps) of uint32; SV IDs for each SP

        rprops: dict{fr_idx - int: skimage.measure.regionprops instance}; IDs are SP IDs, not SV IDs
                                !!! SP IDs stored in rprops are greater by 1 than original IDs to avoid zero label !!!
                    (NOT SERIALIZED)

        # 

        ims: dict{name - str: ndarray(n_frames, size_y, size_x, n_chans) of ?;}
                    (NOT SERIALIZED)
        downscaled_ims: dict{name - str: dict{ds_size_yx - tuple: ndarray(n_frames, ds_size_y, ds_size_x, n_chans) of ?;}}
        sp_chans: dict{name - str: ndarray(n_sps, n_sp_chans) of ?;}

        imsp_reduce_funcs: dict{output_seg_attr_key - str: (input_chan_key - str, func - callable, named_args - dict)}
                    where callable receives a single positional arg: ndarray(n_pixels, n_chans) and 
                        named args passed in the named_args dict, while returns a vector of fixed length.
                    (NOT SERIALIZED)
        image_downscale_funcs: dict{output_seg_attr_key - str: (input_chan_key - str, func - callable, named_args - dict)}
                    where callable receives a single positional arg: ndarray(n_frames, size_y, size_x, n_chans) and 
                        named args passed in the named_args dict, while returns a downscaled array of any size.
                    (NOT SERIALIZED)

        downscale_sizes_yx: list of tuple of ints

        downscaled_sv_segs: dict{tuple(size_y, size_x) of ints: ndarray(n_frames, ds_size_y, ds_size_x) of uint32}
        downscaled_sp_segs: dict{tuple(size_y, size_x) of ints: ndarray(n_frames, ds_size_y, ds_size_x) of uint32}
        
        sp_edges_unidir: dict{name - str: ndarray(n_edges, 2:[from_SP_id, to_SP_id]) of ?};
                    where from_SP_id < to_SP_id; SP graph edge lists
        sp_edge_chans_unidir: dict{name - str: ndarray(n_edges, n_ch) of ?}; edge order matching self.sp_edges_unidir
        sp_edges_bidir: dict{name - str: ndarray(n_edges, 2:[from_SP_id, to_SP_id]) of ?};
                    where from_SP_id < to_SP_id; SP graph edge lists
        sp_edge_chans_bidir: dict{name - str: ndarray(n_edges, n_ch) of ?}; edge order matching self.sp_edges_bidir


    '''

    def __init__(self, sv_seg, downscale_sizes_yx):
        self.data_source = 'init'

        assert sv_seg.ndim == 3
        self.sv_seg = sv_seg.astype(np.uint32)

        assert len(downscale_sizes_yx) >= 1
        assert all([ds[0] < ds[1]] for ds in downscale_sizes_yx)  # safety check: yx order
        self.downscale_sizes_yx = downscale_sizes_yx

        self.rprops = {}
        self.ims = {}
        self.sp_chans = {}
        self.downscaled_ims = {}
        self.imsp_reduce_funcs = {}
        self.image_downscale_funcs = {}
        self.sp_edges_unidir = {}
        self.sp_edge_chans_unidir = {}
        self.sp_edges_bidir = {}
        self.sp_edge_chans_bidir = {}

        # create regionprops, compute 
        self._init_segs()

        # downscale lvl0_seg
        self._compute_downscaled_segs()

    # CUSTOM SERIALIZATION: to avoid serializing functions

    def __getstate__(self):
        assert self.data_source == 'init', "Object loaded from cache cannot be serialized again"
        d = {}
        d['sv_seg'] = self.sv_seg
        d['sp_seg'] = self.sp_seg
        d['sv_sizes'] = self.sv_sizes
        d['sp_sizes'] = self.sp_sizes
        d['sp_id_fr_end_offsets'] = self.sp_id_fr_end_offsets
        d['sv_to_sp'] = self.sv_to_sp
        d['sp_to_sv'] = self.sp_to_sv
        d['downscaled_ims'] = self.downscaled_ims
        d['sp_chans'] = self.sp_chans
        d['downscale_sizes_yx'] = self.downscale_sizes_yx
        d['downscaled_sv_segs'] = self.downscaled_sv_segs
        d['downscaled_sp_segs'] = self.downscaled_sp_segs
        d['sp_edges_unidir'] = self.sp_edges_unidir
        d['sp_edge_chans_unidir'] = self.sp_edge_chans_unidir
        d['sp_edges_bidir'] = self.sp_edges_bidir
        d['sp_edge_chans_bidir'] = self.sp_edge_chans_bidir

        # fields not serialized
        d['rprops'] = None  # self.rprops
        d['ims'] = None  # self.ims
        d['imsp_reduce_funcs'] = None  # self.imsp_reduce_funcs
        d['image_downscale_funcs'] = None  # self.image_downscale_funcs

        return d

    def __setstate__(self, d):
        self.__dict__ = d
        self.data_source = 'cache'

    # PUBLIC

    def get_shape(self):
        return self.sv_seg.shape

    def get_n_frames(self):
        return self.get_shape()[0]

    def get_n_svs(self):
        return self.sv_sizes.shape[0]

    def get_n_sps_total(self):
        return self.sp_sizes.shape[0]

    def get_sp_id_range_in_frame(self, fr_idx):
        '''
        Returns:
            offset, end_offset: int; end_offset is exclusive
        '''
        sp_id_fr_offset = 0 if fr_idx == 0 else self.sp_id_fr_end_offsets[fr_idx-1]
        return sp_id_fr_offset, self.sp_id_fr_end_offsets[fr_idx]

    def get_n_sps_in_frame(self, fr_idx):
        offs, end_offs = self.get_sp_id_range_in_frame(fr_idx)
        return end_offs - offs

    def get_fr_idxs_from_sp_ids(self, sp_ids):
        '''
        Paramters:
            sp_ids: ndarray(n_sps) of int
        Returns:
            fr_idxs: ndarray(n_sps) of int
        '''
        assert sp_ids.ndim == 1
        m = sp_ids[:,None] < self.sp_id_fr_end_offsets[None,:]   # (n_sps, n_fr)
        assert np.all(np.any(m, axis=1))
        return np.argmax(m, axis=1)

    def get_sp_ids_from_coords(self, fr_idx, points_yx):
        '''
        Paramters:
            fr_idx: int
            points_yx: array-like(n_points, 2:[y, x]) OR array-like(2:[y, x])
        Returns:
            sp_ids: ndarray(n_points,) of int OR int (if 'points_yx' was 1D)
        '''
        points_yx = np.asarray(points_yx, dtype=np.int32)
        if points_yx.shape == (2,):
            points_yx_arr = points_yx.reshape(1, 2)
        else:
            points_yx_arr = points_yx
        assert points_yx_arr.shape[1:] == (2,)
        assert np.all(points_yx_arr >= 0) and np.all(points_yx_arr < self.sp_seg.shape[1:])
        assert 0 <= fr_idx < self.sp_seg.shape[0]
        ret = self.sp_seg[fr_idx, points_yx_arr[:,0], points_yx_arr[:,1]]
        if points_yx.shape == (2,):
            assert ret.shape == (1,)
            ret = ret[0]
        return ret

    def add_im(self, name, img):
        '''
        Adds an image channel.
        Parameters:
            name: str; the ID of the image channel
            img: ndarray(n_frames, size_y, size_x, n_ch) of ?;
        '''
        assert name not in self.ims.keys()
        assert img.shape[:3] == self.sv_seg.shape
        assert img.ndim == 4
        self.ims[name] = img

    def add_downscaled_im(self, name, img):
        '''
        Adds an image channel.
        Parameters:
            name: str; the ID of the image channel
            img: ndarray(n_frames, downscaled_size_y, downscaled_size_x, n_ch) of ?;
        '''
        assert img.shape[0] == self.sv_seg.shape[0]
        im_shape_yx = img.shape[1:3]
        assert name not in self.downscaled_ims.keys() or\
               im_shape_yx not in self.downscaled_ims[name].keys()
        assert im_shape_yx in self.downscale_sizes_yx
        assert img.ndim == 4
        if name not in self.downscaled_ims.keys():
            self.downscaled_ims[name] = {}
        self.downscaled_ims[name][im_shape_yx] = img

    def add_sp_channel(self, name, vals):
        '''
        Adds a seg channel.
        Parameters:
            name: str; the ID of the seg channel
            vals: ndarray(n_sps, n_sp_chans) of ?;
        '''
        assert vals.ndim == 2
        assert vals.shape[0] == self.get_n_sps_total()
        assert name not in self.sp_chans.keys()
        for fr_idx in range(self.lvl0_seg.shape[0]):
            vals = values_per_frame[fr_idx]
            assert vals.ndim == 2
            self.sp_chans[name] = vals

    def add_sp_edges_unidir(self, name, sp_edges_unidir, sp_edge_chans_unidir):
        '''
        Parameters:
            name: str; the ID of the image channel
            sp_edges_unidir: ndarray(n_edges, 2:[from_SP_id, to_SP_id]) of ?
            sp_edge_chans_unidir: ndarray(n_edges, n_ch) of ?
        '''
        assert name not in self.sp_edges_unidir.keys() and name not in self.sp_edge_chans_unidir.keys()
        assert sp_edges_unidir.shape[1:] == (2,)
        assert sp_edge_chans_unidir.ndim == 2
        assert sp_edges_unidir.shape[0] == sp_edge_chans_unidir.shape[0]
        assert np.amax(sp_edges_unidir) < self.get_n_sps_total()
        assert np.all(sp_edges_unidir[:,0] < sp_edges_unidir[:,1])
        self.sp_edges_unidir[name] = sp_edges_unidir
        self.sp_edge_chans_unidir[name] = sp_edge_chans_unidir

    def add_sp_edges_bidir(self, name, sp_edges_bidir, sp_edge_chans_bidir):
        '''
        Parameters:
            name: str; the ID of the image channel
            sp_edges_bidir: ndarray(n_edges, 2:[from_SP_id, to_SP_id]) of ?
            sp_edge_chans_bidir: ndarray(n_edges, 2:[from_SP_id -> to_SP_id, to_SP_id -> from_SP_id], n_ch) of ?
        '''
        assert name not in self.sp_edges_bidir.keys() and name not in self.sp_edge_chans_bidir.keys()
        assert sp_edges_bidir.shape[1:] == (2,)
        assert sp_edge_chans_bidir.ndim == 3
        assert sp_edges_bidir.shape == sp_edge_chans_bidir.shape[:2]
        assert np.amax(sp_edges_bidir) < self.get_n_sps_total()
        assert np.all(sp_edges_bidir[:,0] < sp_edges_bidir[:,1])
        self.sp_edges_bidir[name] = sp_edges_bidir
        self.sp_edge_chans_bidir[name] = sp_edge_chans_bidir


    def add_imsp_reduce_funcs(self, out_name, img_chan_name, func, func_named_args):
        '''
        Adds a function which reduces image channels in regions to a fixed length vector.
            (functions create a new SP channel from an image channel).
        Parameters:
            out_name: str; name of the output seg channel
            img_chan_name: str; name of the input img name
            func: callable; receives a single positional arg: ndarray(n_pixels, n_chans) and 
                        named args passed in the 'func_named_args' dict, while returns a vector of fixed length.
            func_named_args: dict; named args for func
        '''
        assert self.data_source == 'init', "This object was loaded from cache, no more functions can be added"
        assert out_name not in self.imsp_reduce_funcs.keys()
        self.imsp_reduce_funcs[out_name] = (img_chan_name, func, func_named_args)

    def add_im_downscale_funcs(self, out_name, img_chan_name, func, func_named_args):
        '''
        Adds a function which downscales image channels.
        Parameters:
            out_name: str; name of the output region channel
            img_chan_name: str; name of the input img name
            func: callable; receives two positional args: ndarray(n_sps, n_chans), tuple(size_y, size_x) and 
                        named args passed in the 'func_named_args' dict, while returns a vector of fixed length.
            func_named_args: dict; named args for func
        '''
        assert self.data_source == 'init', "This object was loaded from cache, no more functions can be added"
        assert out_name not in self.image_downscale_funcs.keys()
        self.image_downscale_funcs[out_name] = (img_chan_name, func, func_named_args)

    def compute_seg_chans(self):
        '''
        Creates seg channels by executing all added functions from self.imsp_reduce_funcs over image channels.
        Modifies self.sp_chans.
        '''
        assert self.data_source == 'init', "This object was loaded from cache, compute methods are not available"
        for out_sp_key in self.imsp_reduce_funcs.keys():
            print("Computing SP channel:", out_sp_key)
            input_chan_key, func, named_args = self.imsp_reduce_funcs[out_sp_key]
            input_img = self.ims[input_chan_key]
            out_arr = None
            written_mask = None  # same shape as out_arr, to check all items were written
                                 #      (np.nans not used here, because type can be int as well)

            for fr_idx in range(self.get_n_frames()):
                sp_id_range = self.get_sp_id_range_in_frame(fr_idx)
                sp_ids_in_fr = np.arange(sp_id_range[0], sp_id_range[1], dtype=np.int32)
                seg_pixs_ls = ImUtil.get_segment_pixels_from_regionprop(input_img[fr_idx],\
                                             self.rprops[fr_idx], sp_ids_in_fr+1)

                sp_vals = np.stack([func(seg_pixs, **named_args) for seg_pixs in seg_pixs_ls], axis=0)
                if sp_vals.ndim == 1:
                    assert len(seg_pixs_ls) >= 1
                    sp_vals = sp_vals[:, None]
                assert sp_vals.ndim == 2

                if out_arr is None:
                    out_arr = np.empty((self.get_n_sps_total(), sp_vals.shape[1]), dtype=sp_vals.dtype)
                    written_mask = np.zeros_like(out_arr, dtype=np.bool_)
                out_arr[sp_id_range[0]:sp_id_range[1],:] = sp_vals
                written_mask[sp_id_range[0]:sp_id_range[1],:] = True

            assert np.all(written_mask)
            self.sp_chans[out_sp_key] = out_arr
        #

    def compute_downscaled_im_chans(self):
        '''
        Creates downscaled img channels by executing all added functions from 
                                    self.downscaled_image_chans over image channels.
        '''
        assert self.data_source == 'init', "This object was loaded from cache, compute methods are not available"
        for out_ds_key in self.image_downscale_funcs.keys():
            print("Computing DS IM channel:", out_ds_key)
            input_chan_key, func, named_args = self.image_downscale_funcs[out_ds_key]
            assert out_ds_key not in self.downscaled_ims
            self.downscaled_ims[out_ds_key] = {}
            for size_yx in self.downscale_sizes_yx:
                self.downscaled_ims[out_ds_key][size_yx] = \
                                    func(self.ims[input_chan_key], size_yx, **named_args)
        #


    # PRIVATE

    def _init_segs(self):
        '''
        Sets self.sp_seg, self.sv_sizes, self.sp_sizes, self.sp_id_fr_end_offsets,
             self.sv_to_sp, self.sp_to_sv, self.rprops.
        '''
        n_frames = self.sv_seg.shape[0]
        u_sv_seg, c_sv_seg = np.unique(self.sv_seg, return_counts=True)
        n_svs = u_sv_seg.shape[0]
        assert n_svs == np.amax(self.sv_seg)+1   # assert sv IDs are from 0..n_svs-1

        self.sp_seg = np.empty_like(self.sv_seg)
        self.sp_id_fr_end_offsets = np.empty((n_frames,), dtype=np.uint32)
        sp_id_offset = 0
        sp_sizes_ls = []
        sp_to_sv_ls = []
        assert n_svs*n_frames < 10000000
        self.sv_to_sp = np.full((n_svs, n_frames), dtype=np.int32, fill_value=-1)

        for fr_idx in range(n_frames):
            sp_id_offset = 0 if fr_idx == 0 else self.sp_id_fr_end_offsets[fr_idx-1]

            # create SP IDs
            u_sp_fr_ids, inv_sp_fr_ids, c_sp_fr_ids = np.unique(self.sv_seg[fr_idx], return_inverse=True, return_counts=True)
            self.sp_id_fr_end_offsets[fr_idx] = sp_id_offset + u_sp_fr_ids.shape[0]
            self.sp_seg[fr_idx] = inv_sp_fr_ids.reshape(self.sv_seg.shape[1:]) + sp_id_offset
            sp_sizes_ls.append(c_sp_fr_ids)
            sp_to_sv_ls.append(u_sp_fr_ids)
            self.sv_to_sp[u_sp_fr_ids, fr_idx] = sp_id_offset + np.arange(u_sp_fr_ids.shape[0])

            # create regionprop from SP IDs; rprops ignores 0 labels, so we add 1 to all seg labels stored in rprops
            regions = skimage.measure.regionprops(self.sp_seg[fr_idx]+1, cache=True)
            self.rprops[fr_idx] = regions

        self.sp_sizes = np.concatenate(sp_sizes_ls, axis=0)
        self.sp_to_sv = np.concatenate(sp_to_sv_ls, axis=0)
        self.sv_sizes = np.zeros((n_svs,), dtype=self.sp_sizes.dtype)
        np.add.at(self.sv_sizes, self.sp_to_sv, self.sp_sizes)   # unbuffered self.sv_sizes += self.sp_sizes[self.sp_to_sv]
        
        # TODO TEMP
        assert np.array_equal(self.sv_sizes, c_sv_seg)
        # TEMP END

    def _compute_downscaled_segs(self):
        '''
        Sets self.downscaled_sv_seg, self.downscaled_sv_seg.
        '''
        self.downscaled_sv_segs = {}
        self.downscaled_sp_segs = {}
        for ds_size_yx in self.downscale_sizes_yx:
            ds_im = ImUtil.imdownscale(self.sv_seg[:,:,:,None], ds_size_yx, interp_smooth=False)
            self.downscaled_sv_segs[ds_size_yx] = ds_im[:,:,:,0].astype(np.int32)
            ds_im = ImUtil.imdownscale(self.sp_seg[:,:,:,None], ds_size_yx, interp_smooth=False)
            self.downscaled_sp_segs[ds_size_yx] = ds_im[:,:,:,0].astype(np.int32)

        
