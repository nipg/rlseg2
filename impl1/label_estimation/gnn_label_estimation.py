
#
# GNN label estimation. RLseg2 implementation #1
#   Segment labels are estimated and propagated with a graph neural network.
#
#   @author Viktor Varga
#

#
# INFO: original benchmarking-gnn repo run command: 
#    python main_molecules_graph_regression.py --dataset ZINC --gpu_id 0 --config 'configs/molecules_graph_regression_GatedGCN_ZINC.json'
#    python main_TUs_graph_classification.py --dataset PROTEINS_full --gpu_id 0 --config 'configs/TUs_graph_classification_GatedGCN_PROTEINS_full.json'
#



import os
import numpy as np
import math
from contextlib import ExitStack
import time

import dgl
import torch
import torch.nn as Tnn
import torch.nn.functional as Tnnf

import torch.optim as Toptim

from .abstract_label_estimation import LabelEstimation
import util.imutil as ImUtil
import impl_util as ImplUtil
import config as Config

from label_estimation.graphs.gated_gcn_net_custom import GatedGCNNet_nodecl
from label_estimation.graphs.graph_datagen import GraphDatagen



class GNNLabelEstimation(LabelEstimation):
    '''
    Member fields:
        seg: Segmentation instance
        n_labels: int
        multiclass_n_cats: None OR int; if None, binary classification mode, if an int is given, native multiclass support

        label_model: LabelModel subclass instance

        user_annots: ndarray(n_sps_annotated, 2:[SP ids, labels])
        pred_generator: GraphDatagen instance, the generator used for generating the prediction
        
        net: GatedGCNNet_nodecl instance
    '''

    def __init__(self, seg_feature_key, label_model, n_features_to_use=None, multiclass_n_cats=None):
        self.seg = None
        self.n_labels = None
        self.seg_feature_key = seg_feature_key
        self.n_features = n_features_to_use
        self.multiclass_n_cats = multiclass_n_cats

        self.label_model = label_model
        self.net = None

        self.user_annots = None
        self.pred_generator = None


    def __str__(self):
        if self.multiclass_n_cats is not None:
            return "GNNmul_" + str(self.label_model)
        else:
            return "GNNbin_" + str(self.label_model)

    def set_prediction_video(self, seg, n_labels):
        '''
        Set video used for prediction. Needs to be called when we would like to make predictions on another video.
        Does not influence pretraining.
        '''
        self.seg = seg
        self.n_labels = n_labels
        self.label_model.reset(self.n_features, self.n_labels)
        self.pred_generator = GraphDatagen("TESTvid", self.seg, self.n_labels, "predict", \
                    self.label_model, self.seg_feature_key, p_dim=self.multiclass_n_cats)

        # set number of features used by label model
        fvecs = self.seg.sp_chans[self.seg_feature_key]
        assert fvecs.ndim == 2
        assert self.n_features is None or self.n_features <= fvecs.shape[1]
        self.n_features = fvecs.shape[1] if self.n_features is None else self.n_features


    def pretrain_graphs(self, train_dataloader=None, val_dataloader=None):
        '''
        Pretrain with graphs and corresponding node label arrays.
        Paramters:
            train_dataloader: Pytorch DataLoader instance
            val_dataloader: Pytorch DataLoader instance
                if None is passed as any argument and a checkpoint is available, the model is loaded from the checkpoint
        '''
        self._init_net()
        if (train_dataloader is not None) and (val_dataloader is not None):
            self._pretrain_gnn_model(train_dataloader, val_dataloader)
        else:
            checkpoint_path = os.path.join(Config.GNNBIN_CHECKPOINT_DIR, Config.GNNBIN_CHECKPOINT_NAME_TO_LOAD)
            self.net.load_state_dict(torch.load(checkpoint_path))
        

    def fit(self, user_annot, n_new_annots):
        '''
        Parameters:
            user_annot: list(n_user_annots) of ndarray(n_segs_in_user_annot, 2:[SP id, label])
            n_new_annots: int; number of unseen annot items in the end of 'user_annot' list.
        '''
        self.label_model.reset(self.n_features, self.n_labels)
        self.user_annots = np.concatenate(user_annot, axis=0)   # (n_train_sps, 2)
        self.pred_generator.reset_prediction_iterator(self.user_annots)


    def predict_all(self, return_probs=False, verbose=True):
        '''
        Parameters:
            return_probs: bool
        Returns:
            ys_pred: ndarray(n_sps, n_cat) of fl32 (IF return_probs == True)
                     ndarray(n_sps,) of i32        (IF return_probs == False)
        '''
        EPS = 1e-9
        ys_pred_probs = self._predict_with_net(self.pred_generator, Config.GNNBIN_BATCH_SIZE)
        if self.multiclass_n_cats is None:
            # binary case
            assert ys_pred_probs.shape == (self.n_labels*2, self.seg.get_n_sps_total(), 1)
            ys_pred_probs = (ys_pred_probs[:self.n_labels,:,0] + (1.-ys_pred_probs[self.n_labels:,:,0])).T  # (n_nodes, n_labels)
            if ys_pred_probs.shape[1] == 1:
                ys_pred_probs = np.concatenate([ys_pred_probs, 1.-ys_pred_probs], axis=1)
                assert ys_pred_probs.shape[1:] == (2,)
            ys_pred_probs_norm = ys_pred_probs / (np.sum(ys_pred_probs, axis=1, keepdims=True) + EPS)
        else:
            # multiclass case
            assert ys_pred_probs.shape == (1, self.seg.get_n_sps_total(), self.multiclass_n_cats)
            ys_pred_probs = ys_pred_probs[0,:,:self.n_labels]
            ys_pred_probs_norm = ys_pred_probs / (np.sum(ys_pred_probs, axis=1, keepdims=True) + EPS)

        if return_probs:
            return ys_pred_probs_norm
        else:
            am = np.argmax(ys_pred_probs_norm, axis=1)
            return am


    # PRIVATE

    def _init_net(self):
        device = GNNLabelEstimation.gpu_setup(use_gpu=True, gpu_id=0)
        net_params = {}
        net_params['device'] = device
        net_params['in_dim_node'] = 11 if Config.GNNBIN_NATIVE_MULTICLASS else 13
        net_params['edge_feat'] = True
        net_params['in_dim_edge'] = 13
        net_params['native_multiclass'] = Config.GNNBIN_NATIVE_MULTICLASS
        net_params['w_mlp'] = Config.GNNBIN_MULTICLASS_W_MLP      # (multilabel only) if True, a 2 layer MLP estimates the w_i, w_j weights instead of a linear layer
        net_params['shared_params'] = Config.GNNBIN_SHARED_PARAMS   # if True, all param matrices are shared across all intermediate layers
        net_params['no_edge_repr'] = Config.GNNBIN_NO_EDGE_REPR   # if True, no unique edge representations for all layers
        net_params['hidden_dim'] = Config.GNNBIN_HIDDEN_DIM
        net_params['out_dim'] = net_params['hidden_dim']
        net_params['n_classes'] = self.multiclass_n_cats if Config.GNNBIN_NATIVE_MULTICLASS else 2
        net_params['L'] = Config.GNNBIN_N_LAYERS    # n_layers
        net_params['dropout'] = Config.GNNBIN_DROPOUT
        net_params['in_feat_dropout'] = Config.GNNBIN_INFEAT_DROPOUT
        net_params['readout'] = Config.GNNBIN_READOUT
        net_params['graph_norm'] = Config.GNNBIN_GRAPH_NORM
        net_params['batch_norm'] = Config.GNNBIN_BATCH_NORM
        net_params['residual'] = Config.GNNBIN_RESIDUAL
        net = GatedGCNNet_nodecl(net_params)
        self.net = net.to(device)

    def _pretrain_gnn_model(self, train_dataloader, val_dataloader):
        '''
        Pretrain with graphs and corresponding node label arrays.
        Paramters:
            train_dataloader: Pytorch DataLoader instance
            val_dataloader: Pytorch DataLoader instance
        '''
        assert self.net is not None
        avg_test_acc = []
        avg_train_acc = []

        t0 = time.time()
        per_epoch_time = []
        print("PRETRAINING GNN MODEL...")

        try:
            optimizer = Toptim.Adam(self.net.parameters(), lr=Config.GNNBIN_INIT_LR, weight_decay=Config.GNNBIN_WEIGHT_DECAY)
            scheduler = Toptim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=Config.GNNBIN_LR_REDUCE_FACTOR,
                                                             patience=Config.GNNBIN_LR_SCHEDULE_PATIENCE, verbose=True)

            for epoch_idx in range(Config.GNNBIN_N_EPOCHS):

                print("    Epoch", epoch_idx) 

                time_start = time.time()
                epoch_train_loss, epoch_train_acc, optimizer = self._run_net_one_epoch(train_dataloader,\
                                    Config.GNNBIN_BATCH_SIZE, Config.GNNBIN_TR_EPOCH_SIZE, optimizer=optimizer, is_training=True)
                epoch_val_loss, epoch_val_acc, _ = self._run_net_one_epoch(val_dataloader, \
                                    Config.GNNBIN_BATCH_SIZE, Config.GNNBIN_VAL_EPOCH_SIZE, optimizer=None, is_training=False)
                epoch_time = time.time()-time_start

                print("        t:", epoch_time, ", lr:", optimizer.param_groups[0]['lr'],\
                                ", tr_loss:", epoch_train_loss, ", val_loss:", epoch_val_loss,\
                                ", tr_acc:", epoch_train_acc, ", val_acc:", epoch_val_acc)

                # Saving checkpoint
                if (epoch_idx % Config.GNNBIN_N_EPOCHS_CHECKPOINT_FREQ == 0) and (epoch_idx > 0):
                    os.makedirs(Config.GNNBIN_CHECKPOINT_DIR, exist_ok=True)
                    ckpt_path = os.path.join(Config.GNNBIN_CHECKPOINT_DIR, "epoch_" + str(epoch_idx) + ".pkl")
                    print(ckpt_path)
                    torch.save(self.net.state_dict(), ckpt_path)

                scheduler.step(epoch_val_loss)

                if optimizer.param_groups[0]['lr'] < Config.GNNBIN_MIN_LR:
                    print("\n!! LR EQUAL TO MIN LR SET.")
                    break
    
        except KeyboardInterrupt:
            print('-' * 89)
            print('Exiting from training early because of KeyboardInterrupt')
            
        
        print("TOTAL TIME TAKEN: {:.4f}hrs".format((time.time()-t0)/3600))
        #

    def _run_net_one_epoch(self, train_dataloader, batch_size, ep_size, optimizer, is_training):
        '''
        Parameters:
            train_dataloader: Pytorch DataLoader
            batch_size, ep_size: int
            optimizer: None OR torch Optimizer instance; must not be non if training
            is_training: bool; can be used for validation loss/acc computation when 'is_training' is False
        Returns:
            epoch_loss, epoch_acc: float
            optimizer: torch Optimizer instance
        '''
        if is_training:
            assert optimizer is not None
            self.net.train()
        else:
            self.net.eval()
        epoch_loss = 0.
        epoch_acc = 0.
        n_batches = 0
        if is_training:
            print("--- TRAINING")
        else:
            print("--- VALIDATION")

        # if not training enter "with torch.no_grad()" block
        train_dataloader_it = train_dataloader.__iter__()
        with ExitStack() as context_stack:
            if not is_training:
                context_stack.enter_context(torch.no_grad())

            for sample_offset in range(0, ep_size, batch_size):
                time_start1 = time.time()
                batch_gs, batch_ys, batch_snorm_n, batch_snorm_e, batch_cat_ws = next(train_dataloader_it)

                # to gpu, train step
                batch_n = batch_gs.ndata['fs'].to(self.net.device)  # num x feat
                batch_e = batch_gs.edata['fs'].to(self.net.device)
                batch_p_in = batch_gs.ndata['ps'].to(self.net.device) if Config.GNNBIN_NATIVE_MULTICLASS else None
                batch_ys = batch_ys.to(self.net.device)
                batch_snorm_e = batch_snorm_e.to(self.net.device)
                batch_snorm_n = batch_snorm_n.to(self.net.device)         # num x 1
                batch_cat_ws = batch_cat_ws.to(self.net.device) \
                                            if Config.GNNBIN_MULTICLASS_LOSS == 'weighted_ce' else None

                time_start2 = time.time()
                print("T delta batchgen: ", time_start2-time_start1)

                if is_training:
                    optimizer.zero_grad()

                batch_ys_preds = self.net.forward(batch_gs, batch_n, batch_e, batch_snorm_n, batch_snorm_e, p=batch_p_in)
                loss = self.net.loss(batch_ys_preds, batch_ys, batch_cat_ws)
                batch_cat_ws_npy = None if batch_cat_ws is None else batch_cat_ws.detach().cpu().numpy()
                print(" --> sample loss: ", loss.detach().cpu().numpy(), " - cat ws:", batch_cat_ws_npy)

                if is_training:
                    loss.backward()
                    optimizer.step()

                time_start3 = time.time()
                print("T delta training per batch: ", time_start3-time_start2)

                epoch_loss += loss.detach().item()
                if Config.GNNBIN_NATIVE_MULTICLASS:
                    batch_ys_preds = Tnn.Softmax(dim=-1)(batch_ys_preds)
                    sample_acc = GNNLabelEstimation.balanced_accuracy_multiclass_torch(batch_ys_preds, batch_ys)
                    epoch_acc += sample_acc
                    print(" --> sample total acc: ", sample_acc)
                else:
                    epoch_acc += GNNLabelEstimation.accuracy_binary_torch(batch_ys_preds, batch_ys)
                n_batches += 1

            epoch_loss /= n_batches
            epoch_acc /= n_batches

        return epoch_loss, epoch_acc, optimizer

    def _predict_with_net(self, test_gen, batch_size):
        '''
        Returns both the true and the predicted labels as torch tensors.
        Parameters:
            test_gen: GraphDatagen instance in 'predict' mode; should NOT be infinite
            batch_size: int
        Returns:
            all_ys_preds: ndarray(n_samples, n_nodes, n_output_features) of float
        '''
        self.net.eval()
        epoch_test_loss = 0.
        epoch_acc = 0.
        n_batches = 0
        batch_gs, batch_snorm_n, batch_snorm_e = [], [], []
        all_ys_preds = []
        n_nodes_in_g = None

        with torch.no_grad():
            while True:
                for g in test_gen:
                    assert isinstance(g, dgl.DGLGraph)
                    batch_gs.append(g)
                    n_nodes, n_edges = g.number_of_nodes(), g.number_of_edges()
                    batch_snorm_n.append(torch.FloatTensor(n_nodes,1).fill_(1./float(n_nodes)))
                    batch_snorm_e.append(torch.FloatTensor(n_edges,1).fill_(1./float(n_edges)))
                    if n_nodes_in_g is None:
                        n_nodes_in_g = n_nodes
                    assert n_nodes_in_g == n_nodes
                    if len(batch_gs) >= batch_size:
                        break

                if len(batch_gs) == 0:
                    break

                # batch is full
                batch_snorm_n = torch.cat(batch_snorm_n, dim=0).sqrt()
                batch_snorm_e = torch.cat(batch_snorm_e, dim=0).sqrt()
                batch_gs = dgl.batch(batch_gs)

                batch_n = batch_gs.ndata['fs'].to(self.net.device)  # num x feat
                batch_e = batch_gs.edata['fs'].to(self.net.device)
                batch_p_in = batch_gs.ndata['ps'].to(self.net.device) if Config.GNNBIN_NATIVE_MULTICLASS else None
                batch_snorm_e = batch_snorm_e.to(self.net.device)
                batch_snorm_n = batch_snorm_n.to(self.net.device)         # num x 1
                
                batch_ys_preds = self.net.forward(batch_gs, batch_n, batch_e, batch_snorm_n, batch_snorm_e, p=batch_p_in)
                batch_ys_preds = Tnn.Softmax(dim=-1)(batch_ys_preds)
                all_ys_preds.append(batch_ys_preds)
                batch_gs, batch_snorm_n, batch_snorm_e = [], [], []

        all_ys_preds = torch.cat(all_ys_preds, dim=0).cpu().numpy()
        # isolate samples
        if all_ys_preds.ndim == 1:
            all_ys_preds = all_ys_preds.reshape(-1, n_nodes_in_g, 1)
        else:
            all_ys_preds = all_ys_preds.reshape(-1, n_nodes_in_g, all_ys_preds.shape[-1])
        return all_ys_preds


    # HELPER

    @staticmethod
    def gpu_setup(use_gpu, gpu_id):
        os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
        os.environ["CUDA_VISIBLE_DEVICES"] = str(gpu_id)  

        if torch.cuda.is_available() and use_gpu:
            print('cuda available with GPU:',torch.cuda.get_device_name(0))
            device = torch.device("cuda")
        else:
            print('cuda not available')
            device = torch.device("cpu")
        return device

    @staticmethod
    def accuracy_binary_torch(pred_ys, true_ys):
        # expects 1D inputs
        pred_ys = torch.round(pred_ys.detach()).long()
        true_ys = true_ys.detach().long()
        acc = (pred_ys == true_ys).float().mean().item()
        return acc

    @staticmethod
    def accuracy_multiclass_torch(pred_ys, true_ys):
        # pred_ys: (n_samples, n_cats), true_ys: (n_samples,)
        pred_ys = pred_ys.detach()

        # DEBUG temp
        n_cats = pred_ys.shape[1]
        pred_ys_npy = pred_ys.cpu().numpy()
        print("     min: ", np.amin(pred_ys_npy))
        print("     min: ", np.amax(pred_ys_npy))
        # DEBUG temp end

        pred_ys = torch.argmax(pred_ys, dim=1).long()
        #true_ys = torch.argmax(true_ys, dim=1).long()
        true_ys = true_ys.detach().long()
        assert pred_ys.shape == true_ys.shape
        acc = (pred_ys == true_ys).float().mean().item()

        # DEBUG temp
        true_ys = true_ys.cpu().numpy()
        pred_ys = pred_ys.cpu().numpy()
        for cat_idx in range(n_cats):
            true_mask = true_ys == cat_idx
            pred_mask = pred_ys == cat_idx
            c_true = np.count_nonzero(true_mask)
            c_pred = np.count_nonzero(pred_mask)
            c_match = np.count_nonzero(pred_mask & true_mask)
            print(" --> acc detailed", cat_idx, ":", c_match, "of", c_true, " (c_pred:", c_pred, ")")
        # DEBUG END
        return acc


    @staticmethod
    def balanced_accuracy_multiclass_torch(pred_ys, true_ys):
        # pred_ys: (n_samples, n_cats), true_ys: (n_samples,)
        pred_ys = pred_ys.detach()

        # DEBUG temp
        n_cats = pred_ys.shape[1]
        pred_ys_npy = pred_ys.cpu().numpy()
        #print("     min: ", np.amin(pred_ys_npy))
        #print("     min: ", np.amax(pred_ys_npy))
        # DEBUG temp end

        pred_ys = torch.argmax(pred_ys, dim=1).long()
        #true_ys = torch.argmax(true_ys, dim=1).long()
        true_ys = true_ys.detach().long()
        assert pred_ys.shape == true_ys.shape

        # DEBUG temp
        true_ys = true_ys.cpu().numpy()
        pred_ys = pred_ys.cpu().numpy()
        acc_cats = []
        for cat_idx in range(n_cats):
            true_mask = true_ys == cat_idx
            pred_mask = pred_ys == cat_idx
            c_true = np.count_nonzero(true_mask)
            c_pred = np.count_nonzero(pred_mask)
            c_match = np.count_nonzero(pred_mask & true_mask)
            if c_true > 0:
                acc_cats.append(float(c_match)/float(c_true))
            print(" --> acc detailed", cat_idx, ":", c_match, "of", c_true, " (c_pred:", c_pred, ")")
        bal_acc = np.mean(acc_cats)
        # DEBUG END
        return bal_acc