
#
# Basic label estimation. RLseg2 implementation #1.
#   Segment labels are estimated independently (no graphical label propagation implemented).
#
#   @author Viktor Varga
#

import os
import numpy as np
from .abstract_label_estimation import LabelEstimation

class BasicLabelEstimation(LabelEstimation):

    '''
    Member fields:
        seg: Segmentation instance
        label_model: LabelModel subclass instance

        seg_feature_key: str
        n_features: int
        n_labels: int
    '''

    def __init__(self, seg, seg_feature_key, n_labels, label_model, n_features_to_use=None):
        self.seg = seg
        self.n_labels = n_labels
        self.seg_feature_key = seg_feature_key
        fvecs = self.seg.sp_chans[seg_feature_key]
        assert fvecs.ndim == 2
        assert n_features_to_use is None or n_features_to_use <= fvecs.shape[1]
        self.n_features = fvecs.shape[1] if n_features_to_use is None else n_features_to_use

        self.label_model = label_model
        self.label_model.reset(self.n_features, self.n_labels)

    def __str__(self):
        return "Basic_" + str(self.label_model)

    def fit(self, user_annot, n_new_annots):
        '''
        Parameters:
            user_annot: list(n_user_annots) of ndarray(n_segs_in_user_annot, 2:[SP id, label])
            n_new_annots: int; number of unseen annot items in the end of 'user_annot' list.
        '''
        self.label_model.reset(self.n_features, self.n_labels)
        train_sps = np.concatenate(user_annot, axis=0)   # (n_train_sps, 2)
        train_xs = self.seg.sp_chans[self.seg_feature_key][train_sps[:,0],:self.n_features]   # (n_train_sps, n_features)
        self.label_model.fit(train_xs, train_sps[:,1])

    def predict_all(self, return_probs=False):
        '''
        Parameters:
            return_probs: bool
        Returns:
            ys_pred: ndarray(n_sps, n_cat) of fl32 (IF return_probs == True)
                     ndarray(n_sps,) of i32        (IF return_probs == False)
        '''
        xs_to_pred = self.seg.sp_chans[self.seg_feature_key][:,:self.n_features]   # (n_sps, n_features)
        ys_pred = self.label_model.predict(xs_to_pred, return_probs=return_probs)
        return ys_pred
