
import numpy as np

import tkinter as tk
from PIL import Image, ImageTk
import cv2
import skimage.measure

import util.viz as Viz

import sys
sys.path.append('../../')
from segmentation import Segmentation

class GUI:

    '''
    Parameters:
        master: Tk; parent GUI item
        canvas: Tk.Canvas class for displaying image layers and text info
        vidname: str
        seg: Segmentation instance
        rprops: dict{int - id: RegionProperties}

        node_labels: ndarray(n_nodes) of int32; the true labels for each node
        edges: ndarray(n_edges, 2:[fromID, toID]); directed edges, may contain reversed edge separately
        edge_fs: ndarray(n_edges, n_edge_Features) of float32
        node_fs: ndarray(n_nodes, n_node_Features) of float32

        (BASE IMG ARRAY)
        bg_ims: ndarray(n_frs, size_y, size_x, n_ch=3 [BGR]) of uint8; original color images
        bg_ims_grayedge: ndarray(n_frs, size_y, size_x, n_ch=3 [BGR]) of uint8; original gray images + SP edges
        bg_ims_graylabel: ndarray(n_frs, size_y, size_x, n_ch=3 [BGR]) of uint8; original images with colored highlight for labels
        bg_ims_grayuserlab0: ndarray(n_frs, size_y, size_x, n_ch=3 [BGR]) of uint8; original images with user given label#0 highlight
        bg_ims_grayuserlab1: ndarray(n_frs, size_y, size_x, n_ch=3 [BGR]) of uint8; original images with user given label#1 highlight
        bg_im_dict_graydynamic: dict{frame_idx - int: ndarray(size_y, size_x, n_ch=3 [BGR]) of uint8}; 
                                        gray+edge images which are modified with the selected and its adjacent SPs highlighted
        optflow_fw: ndarray(n_frs, size_y, size_x, 2:[dy, dx]) of fl32
        optflow_bw: ndarray(n_frs, size_y, size_x, 2:[dy, dx]) of fl32
        occl_fw: ndarray(n_frs, size_y, size_x) of bool_
        occl_bw: ndarray(n_frs, size_y, size_x) of bool_

        (DISPLAYED PHOTO IMAGES)
        photo_ims_rgb: list(n_frs) of tk.PhotoImage, RBG mode (3 channel uint8); CONSTANT
        photo_ims_grayedge: list(n_frs) of tk.PhotoImage, RBG mode (3 channel uint8); CONSTANT
        photo_ims_graylabels: list(n_frs) of tk.PhotoImage, RBG mode (3 channel uint8); CONSTANT
        photo_ims_grayuserlab0: list(n_frs) of tk.PhotoImage, RBG mode (3 channel uint8); CONSTANT
        photo_ims_grayuserlab1: list(n_frs) of tk.PhotoImage, RBG mode (3 channel uint8); CONSTANT
        photo_ims_optflow_fw: list(n_frs) of tk.PhotoImage, RBG mode (3 channel uint8); CONSTANT
        photo_ims_optflow_bw: list(n_frs) of tk.PhotoImage, RBG mode (3 channel uint8); CONSTANT
        photo_ims_occl_fw: list(n_frs) of tk.PhotoImage, L mode (1 channel uint8); CONSTANT
        photo_ims_occl_bw: list(n_frs) of tk.PhotoImage, L mode (1 channel uint8); CONSTANT
        photo_ims_occl_bw: list(n_frs) of tk.PhotoImage, L mode (1 channel uint8); CONSTANT

        (DISPLAY STATE)
        curr_fr_idx: int
        bg_display_mode: str; any of ["rgb", "gray+edge", "gray+labels", "of_fw", "of_bw", "occl_fw", "occl_bw",\
                                      "gray+userlab0", "gray+userlab1"]

    '''

    def __init__(self, master, vidname, bg_ims, seg, dgl_graph, node_labels,\
                         optflow_fw, optflow_bw, occl_fw, occl_bw):
        '''
        Parameters:
            master: Tk instance, parent GUI item
            vidname: str
            bg_ims: ndarray(n_frs, size_y, size_x, n_ch=3 [BGR]) of uint8
            seg: Segmentation instance
            dgl_graph: DGLGraph instance
            node_labels: ndarray(n_nodes) of int32; the true labels for each node
            optflow_fw: ndarray(n_frs-1, size_y, size_x, 2:[dy, dx]) of fl16
            optflow_bw: ndarray(n_frs-1, size_y, size_x, 2:[dy, dx]) of fl16
            occl_fw: ndarray(n_frs-1, size_y, size_x) of bool_
            occl_bw: ndarray(n_frs-1, size_y, size_x) of bool_
        '''
        self.master = master
        master.title("Graph feature brwoser window")
        self.vidname = vidname
        self.seg = seg

        # unpack DGLGraph (edges, node & edge features) to numpy arrays
        edges_from, edges_to = dgl_graph.edges()   # PyTorch tensors
        self.edges = np.stack([edges_from.numpy(), edges_to.numpy()], axis=1)
        self.nodefs = dgl_graph.ndata['fs'].numpy()
        self.edgefs = dgl_graph.edata['fs'].numpy()

        self.node_labels = node_labels
        assert bg_ims.shape[0] == optflow_fw.shape[0]+1 == occl_fw.shape[0]+1 == optflow_bw.shape[0]+1 == occl_bw.shape[0]+1
        self.optflow_fw = np.pad(optflow_fw, ((0,1), (0,0), (0,0), (0,0)), constant_values=0).astype(np.float32)
        self.optflow_bw = np.pad(optflow_bw, ((1,0), (0,0), (0,0), (0,0)), constant_values=0).astype(np.float32)
        self.occl_fw = np.pad(occl_fw, ((0,1), (0,0), (0,0)), constant_values=0)
        self.occl_bw = np.pad(occl_bw, ((1,0), (0,0), (0,0)), constant_values=0)

        # create constant images: rgb, optflow
        assert bg_ims.shape[3:] == (3,)
        assert bg_ims.dtype == np.uint8
        self.bg_ims = bg_ims[...,::-1]  # BGR -> RGB
        self.curr_fr_idx = 0
        self.bg_display_mode = "gray+edge"
        self.photo_ims_rgb = [ImageTk.PhotoImage(Image.fromarray(im)) for im in self.bg_ims]
        self.photo_ims_optflow_fw = [ImageTk.PhotoImage(Image.fromarray(self._create_flowviz_img(im))) for im in self.optflow_fw]
        self.photo_ims_optflow_bw = [ImageTk.PhotoImage(Image.fromarray(self._create_flowviz_img(im))) for im in self.optflow_bw]
        self.photo_ims_occl_fw = [ImageTk.PhotoImage(Image.fromarray(self._create_occl_img(im))) for im in self.occl_fw]
        self.photo_ims_occl_bw = [ImageTk.PhotoImage(Image.fromarray(self._create_occl_img(im))) for im in self.occl_bw]
        self.photo_ims_grayedge = []

        # create constant images: gray+edge, gray+labels, gray+userlab0, gray+userlab1
        bg_ims_gray = np.broadcast_to(np.mean(self.bg_ims, axis=-1, keepdims=True).astype(np.uint8), (self.bg_ims.shape))
        self.bg_ims_grayedge = bg_ims_gray.copy()
        for fr_idx in range(self.bg_ims_grayedge.shape[0]):
            Viz.render_segmentation_edges_BGR(self.bg_ims_grayedge[fr_idx], self.seg.sp_seg[fr_idx], (255,0,255))
            self.photo_ims_grayedge.append(ImageTk.PhotoImage(Image.fromarray(self.bg_ims_grayedge[fr_idx])))

        self.photo_ims_graylabels = []
        self.photo_ims_grayuserlab0 = []
        self.photo_ims_grayuserlab1 = []
        self.bg_ims_graylabels = np.empty_like(bg_ims_gray)
        self.bg_ims_grayuserlab0 = np.empty_like(bg_ims_gray)
        self.bg_ims_grayuserlab1 = np.empty_like(bg_ims_gray)
        n_labels = np.amax(self.node_labels)+1
        LABEL_COLORS = [(192,192,192), (255,0,0), (0,255,0), (0,0,255), (255,255,0), (255,0,255), (0,255,255), (255,127,0)]
        for fr_idx in range(self.bg_ims_graylabels.shape[0]):
            true_lab_img = Viz.render_segmentation_labels_RGB(self.seg.sp_seg[fr_idx], self.node_labels, LABEL_COLORS[:n_labels],\
                                         color_alpha=0.7, bg=bg_ims_gray[fr_idx], seg_is_gt=None, color_alpha_gt=1.)
            self.bg_ims_graylabels[fr_idx] = true_lab_img
            self.photo_ims_graylabels.append(ImageTk.PhotoImage(Image.fromarray(true_lab_img)))
            #
            user_node_labels0 = np.where(self.nodefs[:,10] == 1., 1, -1)
            userlab0_img = Viz.render_segmentation_labels_RGB(self.seg.sp_seg[fr_idx], user_node_labels0, LABEL_COLORS[:n_labels],\
                                         color_alpha=0.7, bg=bg_ims_gray[fr_idx], seg_is_gt=None, color_alpha_gt=1.)
            user_node_labels1 = np.where(self.nodefs[:,11] == 1., 1, -1)
            userlab1_img = Viz.render_segmentation_labels_RGB(self.seg.sp_seg[fr_idx], user_node_labels1, LABEL_COLORS[:n_labels],\
                                         color_alpha=0.7, bg=bg_ims_gray[fr_idx], seg_is_gt=None, color_alpha_gt=1.)
            self.bg_ims_grayuserlab0[fr_idx] = userlab0_img
            self.photo_ims_grayuserlab0.append(ImageTk.PhotoImage(Image.fromarray(userlab0_img)))
            self.bg_ims_grayuserlab1[fr_idx] = userlab1_img
            self.photo_ims_grayuserlab1.append(ImageTk.PhotoImage(Image.fromarray(userlab1_img)))

        self.bg_im_dict_graydynamic = {}

        # init regionprops
        self.rprops = {}
        for fr_idx in range(self.seg.sp_seg.shape[0]):
            rprops_fr = skimage.measure.regionprops(self.seg.sp_seg[fr_idx]+1, cache=True)
            for rprop in rprops_fr:
                self.rprops[rprop.label-1] = rprop

        # create canvas
        self.canvas = tk.Canvas(self.master, width=854, height=480)
        self.canvas.pack()

        # bind keypress functions, see Event key details: https://anzeljg.github.io/rin2/book2/2405/docs/tkinter/key-names.html
        self.master.bind("<Left>", self.on_keypress_left)   # frame -= 1
        self.master.bind("<Right>", self.on_keypress_right)   # frame += 1
        self.master.bind("<Prior>", self.on_keypress_pageup)   # frame -= 5
        self.master.bind("<Next>", self.on_keypress_pagedown)   # frame += 5
        self.master.bind("<Home>", self.on_keypress_home)   # frame = 0
        self.master.bind("<End>", self.on_keypress_end)   # frame = <last frame idx>
        self.master.bind("<Key-0>", self.on_keypress_0)     # show bgr
        self.master.bind("<Key-1>", self.on_keypress_1)     # show gray+edges
        self.master.bind("<Key-2>", self.on_keypress_2)     # show gray + true labels (binary)
        self.master.bind("<Key-3>", self.on_keypress_3)     # show optflow fw
        self.master.bind("<Key-4>", self.on_keypress_4)     # show optflow bw
        self.master.bind("<Key-5>", self.on_keypress_5)     # show occl fw
        self.master.bind("<Key-6>", self.on_keypress_6)     # show occl fw
        self.master.bind("<Key-7>", self.on_keypress_7)     # show gray + user given labels: label#0
        self.master.bind("<Key-8>", self.on_keypress_8)     # show gray + user given labels: label#1
        self.master.bind("<Button-1>", self.on_click_left)   # left click - select SP segment

        # draw images
        self._redraw_bg_img()
        self._redraw_info_textbox()

    def _generate_info_text(self):
        '''
        Returns:
            text: str
        '''
        text = []
        text.append("Video name: " + str(self.vidname))
        text.append("Frame idx: " + str(self.curr_fr_idx))
        text.append("Display mode: " + str(self.bg_display_mode))
        return '\n'.join(text)

    def _cart2polar(self, x, y):
        """
        Elementwise Cartesian2Polar for arrays. x and y should be of the same size.
        Parameters:
            x, y: ndarray(...); cartesian coordinate arrays
        Returns:
            r: ndarray(...); radius
            phi: ndarray(...); angle in radians, -pi..pi
        """
        r = np.sqrt(np.square(x) + np.square(y))
        phi = np.arctan2(y, x)
        return r, phi

    def _create_flowviz_img(self, flow, brightness_mul=10.):
        """
        Parameters:
            flow: ndarray(size_y, size_x, 2:[y,x]) of float32; output of optical flow algorithms
            brightness_mul: float;
        Returns:
            flowviz: ndarray(size_y, size_x, 3) of uint8
        """
        assert flow.ndim == 3
        assert flow.shape[2] == 2
        MAX_HUE = 179.
        flowviz = np.empty((flow.shape[0], flow.shape[1], 3), dtype=np.uint32)
        flowviz.fill(255)
        r, phi = self._cart2polar(flow[:, :, 1], flow[:, :, 0])
        flowviz[:, :, 0] = ((phi + np.pi) / (2. * np.pi) * MAX_HUE).astype(np.uint32)
        flowviz[:, :, 2] = (r * brightness_mul).astype(np.uint32)
        flowviz[:, :, 1:] = np.clip(flowviz[:, :, 1:], 0, 255)
        flowviz[:, :, 0] = np.clip(flowviz[:, :, 0], 0, int(MAX_HUE))
        flowviz = flowviz.astype(np.uint8)
        flowviz = cv2.cvtColor(flowviz, cv2.COLOR_HSV2BGR)
        return flowviz

    def _create_occl_img(self, occl_arr):
        assert occl_arr.ndim == 2
        return occl_arr.astype(np.uint8)*255

    # QUERIES

    def _get_bg_photoimg(self):
        '''
        Returns:
            ImageTk.PhotoImage: the rgb or feature map image at the current frame
        '''
        assert self.bg_display_mode in ["rgb", "of_fw", "of_bw", "occl_fw", "occl_bw", "gray+edge",\
                                        "gray+labels", "gray+userlab0", "gray+userlab1"]
        if self.bg_display_mode == "rgb":
            return self.photo_ims_rgb[self.curr_fr_idx]
        elif self.bg_display_mode == "of_fw":
            return self.photo_ims_optflow_fw[self.curr_fr_idx]
        elif self.bg_display_mode == "of_bw":
            return self.photo_ims_optflow_bw[self.curr_fr_idx]
        elif self.bg_display_mode == "occl_fw":
            return self.photo_ims_occl_fw[self.curr_fr_idx]
        elif self.bg_display_mode == "occl_bw":
            return self.photo_ims_occl_bw[self.curr_fr_idx]
        elif self.bg_display_mode == "gray+edge":
            return self.photo_ims_grayedge[self.curr_fr_idx]
        elif self.bg_display_mode == "gray+labels":
            return self.photo_ims_graylabels[self.curr_fr_idx]
        elif self.bg_display_mode == "gray+userlab0":
            return self.photo_ims_grayuserlab0[self.curr_fr_idx]
        elif self.bg_display_mode == "gray+userlab1":
            return self.photo_ims_grayuserlab1[self.curr_fr_idx]


    # CONTROL

    def _redraw_bg_img(self):
        '''
        Redraws background image.
        '''
        self.canvas.delete('bg')
        self.canvas.create_image((0,0), image=self._get_bg_photoimg(), anchor='nw', tags=('bg',))
        self.canvas.tag_lower('bg')

    def _redraw_info_textbox(self):
        '''
        Redraws info textbox.
        '''
        self.canvas.delete('infobox')
        info_text = self._generate_info_text()
        FONT = ("Courier", 12, 'bold')
        self.canvas.create_text((420,20), text=info_text, fill='#FF00FF', anchor='nw', \
                                width=400, font=FONT, tags=('infobox',))


    # ON KEY EVENTS

    def on_keypress_left(self, event):
        self.curr_fr_idx = max(self.curr_fr_idx-1, 0)
        self._redraw_bg_img()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_right(self, event):
        self.curr_fr_idx = min(self.curr_fr_idx+1, len(self.photo_ims_rgb)-1)
        self._redraw_bg_img()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_pageup(self, event):
        self.curr_fr_idx = max(self.curr_fr_idx-5, 0)
        self._redraw_bg_img()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_pagedown(self, event):
        self.curr_fr_idx = min(self.curr_fr_idx+5, len(self.photo_ims_rgb)-1)
        self._redraw_bg_img()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_home(self, event):
        self.curr_fr_idx = 0
        self._redraw_bg_img()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_end(self, event):
        self.curr_fr_idx = len(self.photo_ims_rgb)-1
        self._redraw_bg_img()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_0(self, event):
        self.bg_display_mode = "rgb"
        self._redraw_bg_img()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_1(self, event):
        self.bg_display_mode = "gray+edge"
        self._redraw_bg_img()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_2(self, event):
        self.bg_display_mode = "gray+labels"
        self._redraw_bg_img()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_3(self, event):
        self.bg_display_mode = "of_fw"
        self._redraw_bg_img()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_4(self, event):
        self.bg_display_mode = "of_bw"
        self._redraw_bg_img()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_5(self, event):
        self.bg_display_mode = "occl_fw"
        self._redraw_bg_img()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_6(self, event):
        self.bg_display_mode = "occl_bw"
        self._redraw_bg_img()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_7(self, event):
        self.bg_display_mode = "gray+userlab0"
        self._redraw_bg_img()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_8(self, event):
        self.bg_display_mode = "gray+userlab1"
        self._redraw_bg_img()
        self._redraw_info_textbox()
        self.canvas.update()


    # ON MOUSE EVENTS

    def on_click_left(self, event):
        '''
        Updates gray+edge photo images with highlighting selected segment and all adjacent segments to it.
        '''
        # clearing photoimages with highlight from the previous selection
        ONLY_PRINT_SELECTED_NODE = False
        for fr_idx in self.bg_im_dict_graydynamic.keys():
            self.photo_ims_grayedge[fr_idx] = ImageTk.PhotoImage(Image.fromarray(self.bg_ims_grayedge[fr_idx]))
        self.bg_im_dict_graydynamic = {}

        # get affected segment IDs and edges
        selected_seg_id = self.seg.sp_seg[self.curr_fr_idx, int(event.y), int(event.x)]
        selected_edge_idxs = np.where(np.any(self.edges == selected_seg_id, axis=1))[0]
        selected_edges = self.edges[selected_edge_idxs]
        adj_nodes = np.setdiff1d(np.unique(selected_edges), [selected_seg_id], assume_unique=True)

        # generate highlighted overlay in gray+edge images in affected frames
        NODE_COLORS_RGB = [(255,0,0), (255,127,0), (255,255,0), (127,255,0), (0,180,0), (0,255,127), (0,255,255), (0,127,255),\
                           (0,0,255), (180,0,255), (255,0,255), (180,0,100)]
        NODE_COLORNAMES = ["red", "orange", "yellow", "lime", "green", "aqua", "cyan", "lightblue",\
                           "blue", "purple", "magenta", "rose"]
        adj_node_colors = np.resize(NODE_COLORS_RGB, (adj_nodes.shape[0], 3))
        ls_q, ls_r = divmod(adj_nodes.shape[0], len(NODE_COLORNAMES))
        adj_node_colornames = ls_q * NODE_COLORNAMES + NODE_COLORNAMES[:ls_r]
        adj_node_texts = list(np.repeat(np.arange(ls_q), len(NODE_COLORNAMES))) + [ls_q]*ls_r
        adj_node_texts = [str(item) for item in adj_node_texts]

        self._update_grayedge_images_with_selection(selected_seg_id, adj_nodes, adj_node_colors, adj_node_texts)

        # print selected & adjacent node features and all edge feature leading to them
        print("----------------------------- SELECTION INFO -----------------------------")
        print("SELECTED NODE (white), ID:", selected_seg_id)
        print("    adjacent nodes:", adj_nodes)
        print("    node features:\n", self._nodefs_to_text(selected_seg_id))

        if not ONLY_PRINT_SELECTED_NODE:
            for adj_node_idx in range(adj_nodes.shape[0]):
                adj_node_id = adj_nodes[adj_node_idx]
                adj_node_colorsstr = adj_node_colornames[adj_node_idx] + adj_node_texts[adj_node_idx]
                print("ADJ NODE (" + adj_node_colorsstr + "), ID:", adj_node_id)
                print("    node features:\n", self._nodefs_to_text(adj_node_id))
                edge1_idx_in_selected = np.where(np.all(selected_edges == [selected_seg_id, adj_node_id], axis=-1))[0]
                assert edge1_idx_in_selected.shape[0] == 1
                edge_idx1 = selected_edge_idxs[edge1_idx_in_selected[0]]
                edge2_idx_in_selected = np.where(np.all(selected_edges == [adj_node_id, selected_seg_id], axis=-1))[0]
                assert edge2_idx_in_selected.shape[0] == 1
                edge_idx2 = selected_edge_idxs[edge2_idx_in_selected[0]]
                print("    edge (->) features:\n", self._edgefs_to_text(edge_idx1))
                print("    edge (<-) features:\n", self._edgefs_to_text(edge_idx2))

        #
        self._redraw_bg_img()
        self._redraw_info_textbox()
        self.canvas.update()

    def _update_grayedge_images_with_selection(self, selected_seg_id, adj_node_ids, node_colors_rgb, node_texts):
        '''
        Modifies self.photo_ims_grayedge, self.bg_im_dict_graydynamic.
        Parameters:
            selected_seg_id: int
            adj_node_ids: ndarray(n_adjacent_nodes) of int
            node_colors_rgb: array-like(n_adjacent_nodes, 3) of int
            node_texts: list(n_adjacent_nodes) of str
        '''
        affected_nodes = np.concatenate([[selected_seg_id], adj_node_ids], axis=0)
        affected_node_fr_idxs = self.seg.get_fr_idxs_from_sp_ids(affected_nodes)
        for fr_idx in np.unique(affected_node_fr_idxs):
            affected_node_idxs_in_fr = np.where(affected_node_fr_idxs == fr_idx)[0]
            bg_im = self.bg_ims_grayedge[fr_idx].copy()

            # fill affected segments
            for affnode_idx in affected_node_idxs_in_fr:

                curr_seg_id = affected_nodes[affnode_idx]
                seg_miny, seg_minx, seg_maxy, seg_maxx = self.rprops[curr_seg_id].bbox
                seg_mask_in_bbox = self.rprops[curr_seg_id].image

                if curr_seg_id == selected_seg_id:
                    # selected segment is rendered in white color
                    bg_im[seg_miny:seg_maxy, seg_minx:seg_maxx,:][seg_mask_in_bbox,:] = (255,255,255)
                else:
                    # other (adjacent) segments are rendered with color
                    bg_im[seg_miny:seg_maxy, seg_minx:seg_maxx,:][seg_mask_in_bbox,:] = node_colors_rgb[affnode_idx-1]

            # draw text ID of affected segments
            for affnode_idx in affected_node_idxs_in_fr:
                curr_seg_id = affected_nodes[affnode_idx]
                cy, cx = self.rprops[curr_seg_id].centroid

                if curr_seg_id != selected_seg_id:
                    text_color = [int(255-item) for item in node_colors_rgb[affnode_idx-1]]
                    cv2.putText(bg_im, node_texts[affnode_idx-1], (int(cx), int(cy)), fontFace=cv2.FONT_HERSHEY_SIMPLEX,\
                                     fontScale=0.4, color=text_color, thickness=2)

            # store image
            self.bg_im_dict_graydynamic[fr_idx] = bg_im
            self.photo_ims_grayedge[fr_idx] = ImageTk.PhotoImage(Image.fromarray(bg_im))
        #

    def _nodefs_to_text(self, node_id):
        # Node features to text: int -> str
        nfs = self.nodefs[node_id]
        assert nfs.shape == (13,)
        text = []
        text.append("        occl: {:8.4f} {:8.4f} | mLAB:  {:8.4f} {:8.4f} {:8.4f} | sLAB:  {:8.4f} {:8.4f} {:8.4f}".format(*nfs[:8]))
        text.append("        sz/br: {:8.4f} {:8.4f} | tr/pr: {:8.4f} {:8.4f} {:8.4f}".format(*nfs[8:]))
        return '\n'.join(text)

    def _edgefs_to_text(self, edge_idx):
        # Node features to text: int -> str
        efs = self.edgefs[edge_idx]
        assert efs.shape == (13,)
        text = []
        text.append("        LABd: {:8.4f} | OFdfw: {:8.4f} {:8.4f} {:8.4f} | OFdbw: {:8.4f} {:8.4f} {:8.4f}".format(*efs[:7]))
        text.append("        OFfea: {:8.4f} {:8.4f} {:8.4f} {:8.4f} | sp/fl: {:8.4f} {:8.4f}".format(*efs[7:]))
        return '\n'.join(text)


