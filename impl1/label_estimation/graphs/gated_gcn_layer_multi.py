import numpy as np

import torch
import torch.nn as nn
import torch.nn.functional as F

"""
    ResGatedGCN: Residual Gated Graph ConvNets
    An Experimental Study of Neural Networks for Variable Graphs (Xavier Bresson and Thomas Laurent, ICLR 2018)
    https://arxiv.org/pdf/1711.07553v2.pdf
"""

class GatedGCNLayer_custom(nn.Module):
    """
    MODIFIED:
        - Native multi-class support by maintinaing 'p' label representation. Pass an int value to 'p_dim' to enable.
        - Parameters A, B, (C), D, E, (q, z) can be passed to the constructor from outside
        - Maintaining hidden edge representation can be disabled by passing 'no_edge_repr=True' to the constructor
                                                                                             (C matrix is not used then)
    """
    def __init__(self, input_dim, output_dim, dropout, graph_norm, batch_norm, residual,\
                       no_edge_repr=False, shared_params_dict={}, p_dim=None, p_rescale=True, w_mlp=False):
        '''
        Parameters:
            input_dim, output_dim: int
            dropout: float
            graph_norm, batch_norm, residual: bool
            no_edge_repr: bool
            shared_params_dict: dict{matrix_name - str: nn.Module}
        '''
        super().__init__()
        assert input_dim == output_dim  # assert condition was added
        self.in_channels = input_dim
        self.out_channels = output_dim
        self.dropout = dropout
        self.graph_norm = graph_norm
        self.batch_norm = batch_norm
        self.residual = residual
        self.no_edge_repr = no_edge_repr
        assert isinstance(shared_params_dict, dict)
        assert set(['A', 'B', 'C', 'D', 'E', 'q', 'z']) >= set(shared_params_dict.keys())
        self.shared_params_dict = shared_params_dict
        assert (p_dim is None) or (p_dim >= 2)
        self.p_dim = p_dim
        self.p_rescale = p_rescale
        self.w_mlp = w_mlp
        
        if input_dim != output_dim:
            self.residual = False
        
        self.A = shared_params_dict['A'] if 'A' in shared_params_dict else nn.Linear(input_dim, output_dim, bias=True)
        self.B = shared_params_dict['B'] if 'B' in shared_params_dict else nn.Linear(input_dim, output_dim, bias=True)
        self.D = shared_params_dict['D'] if 'D' in shared_params_dict else nn.Linear(input_dim, output_dim, bias=True)
        self.E = shared_params_dict['E'] if 'E' in shared_params_dict else nn.Linear(input_dim, output_dim, bias=True)
        self.bn_node_h = nn.BatchNorm1d(output_dim)
        if not self.no_edge_repr:
            self.C = shared_params_dict['C'] if 'C' in shared_params_dict else nn.Linear(input_dim, output_dim, bias=True)
            self.bn_node_e = nn.BatchNorm1d(output_dim)
        if p_dim is not None:
            if self.w_mlp:
                assert len(set(['q', 'z']) & set(self.shared_params_dict.keys())) == 0, "Shared params (q, z) are not supported when 'w_mlp' is enabled."
                self.q = MLP_w(input_dim, output_dim)
                self.z = MLP_w(input_dim, output_dim)
            else:
                self.q = shared_params_dict['q'] if 'q' in shared_params_dict else nn.Linear(output_dim, 1, bias=True)
                self.z = shared_params_dict['z'] if 'z' in shared_params_dict else nn.Linear(output_dim, 1, bias=True)

    def message_func(self, edges):
        Bh_j = edges.src['Bh']

        if self.no_edge_repr:
            e_ij = edges.data['e'] +  edges.src['Dh'] + edges.dst['Eh'] # e_ij = Ce_ij + Dhi + Ehj
        else:
            e_ij = edges.data['Ce'] +  edges.src['Dh'] + edges.dst['Eh'] # e_ij = Ce_ij + Dhi + Ehj
            edges.data['e'] = e_ij

        if self.p_dim is not None:
            p_j = edges.src['p']
            return {'Bh_j' : Bh_j, 'e_ij' : e_ij, 'p_j' : p_j}
        else:
            return {'Bh_j' : Bh_j, 'e_ij' : e_ij}

    def reduce_func(self, nodes):
        Ah_i = nodes.data['Ah']          # (n_nodes, output_dim)
        Bh_j = nodes.mailbox['Bh_j']     # (n_nodes, n_adj, output_dim)
        e = nodes.mailbox['e_ij']        # (n_nodes, n_adj, output_dim)
        sigma_ij = torch.sigmoid(e)             # sigma_ij = sigmoid(e_ij)
        #h = Ah_i + torch.mean( sigma_ij * Bh_j, dim=1 ) # hi = Ahi + mean_j alpha_ij * Bhj 
        sb_ij = sigma_ij * Bh_j     # (n_nodes, n_adj, output_dim)

        if self.p_dim is not None:
            p_j = nodes.mailbox['p_j']     # (n_nodes, n_adj, p_dim)
            w_ij = self.z(sb_ij)     # (n_nodes, n_adj, 1)
            p_i = nodes.data['p']     # (n_nodes, p_dim)
            w_i = nodes.data['qh']     # (n_nodes, 1)

            # FIXED ARCHITECTURE
            ws = torch.cat([w_ij, w_i[:,None,:]], axis=1)     # (n_nodes, n_adj+1, 1)
            ps = torch.cat([p_j, p_i[:,None,:]], axis=1)     # (n_nodes, n_adj+1, p_dim)
            if self.p_rescale:
                ws = torch.softmax(ws, dim=1)   # sum == 1 along axis#1
            p = torch.sum(ws*ps, dim=1)     # (n_nodes, p_dim)

            # INCORRECT ARCHITECTURE mod
            # p = torch.sum(w_ij*p_j, dim=1) + w_i*p_i   # (n_nodes, p_dim)
            # if self.p_rescale:
            #     p = torch.softmax(p, dim=-1)
                
            # END

            h = Ah_i + torch.sum( sb_ij, dim=1 ) / ( torch.sum( sigma_ij, dim=1 ) + 1e-6 )  # hi = Ahi + sum_j eta_ij/sum_j' eta_ij' * Bhj <= dense attention       
            return {'h' : h, 'p': p}
        else:
            h = Ah_i + torch.sum( sb_ij, dim=1 ) / ( torch.sum( sigma_ij, dim=1 ) + 1e-6 )  # hi = Ahi + sum_j eta_ij/sum_j' eta_ij' * Bhj <= dense attention       
            return {'h' : h}
    
    def forward(self, g, h, e, snorm_n, snorm_e, p):

        h_in = h # for residual connection
        e_in = e # for residual connection
        
        g.ndata['h']  = h
        g.ndata['Ah'] = self.A(h)
        g.ndata['Bh'] = self.B(h)
        g.ndata['Dh'] = self.D(h)
        g.ndata['Eh'] = self.E(h)
        g.edata['e']  = e 

        if not self.no_edge_repr:
            g.edata['Ce'] = self.C(e)
        if self.p_dim is not None:
            g.ndata['qh'] = self.q(h)
            g.ndata['p'] = p

        g.update_all(self.message_func, self.reduce_func)
        
        h = g.ndata['h'] # result of graph convolution
        e = g.edata['e'] if not self.no_edge_repr else e   # result of graph convolution

        if self.p_dim is not None:
            p = g.ndata['p']
        
        if self.graph_norm:
            h = h* snorm_n # normalize activation w.r.t. graph size
            e = e* snorm_e if not self.no_edge_repr else e      # normalize activation w.r.t. graph size
        
        if self.batch_norm:
            h = self.bn_node_h(h) # batch normalization
            e = self.bn_node_e(e) if not self.no_edge_repr else e       # batch normalization
        
        h = F.relu(h) # non-linear activation
        e = F.relu(e) if not self.no_edge_repr else e       # non-linear activation
        
        if self.residual:
            h = h_in + h # residual connection
            e = e_in + e if not self.no_edge_repr else e       # residual connection
        
        h = F.dropout(h, self.dropout, training=self.training)
        e = F.dropout(e, self.dropout, training=self.training) if not self.no_edge_repr else e

        return h, e, p
    
    def __repr__(self):
        return '{}(in_channels={}, out_channels={})'.format(self.__class__.__name__,
                                             self.in_channels,
                                             self.out_channels)

    #

class MLP_w(nn.Module):

    def __init__(self, input_dim, hidden_dim):
        super().__init__()
        self.FC_layers = nn.ModuleList([nn.Linear(input_dim, hidden_dim, bias=True),\
                                        nn.Linear(hidden_dim, 1, bias=True)])
        
    def forward(self, x):
        y = x
        for layer in self.FC_layers[:-1]:
            y = layer(y)
            y = F.relu(y)
        y = self.FC_layers[-1](y)
        return y

    #

class LearnableSoftmax(nn.Module):

    def __init__(self, dim=-1, expbase_init_value=1.):
        super().__init__()
        self.dim = dim
        self.expbase = torch.nn.Parameter(data=torch.as_tensor([expbase_init_value], dtype=torch.float32),\
                                                                                             requires_grad=True)

    def forward(self, x):
        exps = torch.pow(input=self.expbase, exponent=x)
        sumexp = torch.sum(exps, dim=self.dim)
        return exps / (sumexp + 1e-6)

    #