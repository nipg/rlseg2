
#
# Runner script for interactive graph node/edge feature visualizer GUI
#
#   @author Viktor Varga
#

import sys
sys.path.append('../..')
sys.path.append('../../..')

import os
import numpy as np
import pickle
import h5py
import cv2

from data_if import DataIF
from segmentation_labeling import SegLabeling
from graph_datagen_binary import GraphDatagen_binary

from feature_viz_gui import GUI
import tkinter as tk

#CURR_VIDNAME = 'bear'
CURR_VIDNAME = 'soapbox'
#CURR_VIDNAME = 'boat'
#CURR_VIDNAME = 'sheep'

DATA_FOLDER_OPTFLOWS = '/home/vavsaai/databases/DAVIS/rl_seg_data/davis2017_optflows/'

if __name__ == '__main__':

    # LOAD DATA, image data, INIT SegLabeling

    data_if = DataIF()
    all_vidnames = set(data_if.get_train_vids() + data_if.get_validation_vids() + data_if.get_test_vids())
    assert CURR_VIDNAME in all_vidnames

    curr_seg = data_if.get_seg_obj(CURR_VIDNAME)
    curr_seg_lab = SegLabeling(curr_seg)
    img_data = DataIF.load_img_data([CURR_VIDNAME])[CURR_VIDNAME]

    # LOAD full optflow DATA

    flows_h5_path = os.path.join(DATA_FOLDER_OPTFLOWS, 'flownet2_' + CURR_VIDNAME + '.h5')
    h5f = h5py.File(flows_h5_path, 'r')
    optflow_fw_raw_data = h5f['flows'][:]
    optflow_bw_raw_data = h5f['inv_flows'][:]
    occl_fw_raw_data = h5f['occls'][:]
    occl_bw_raw_data = h5f['inv_occls'][:]
    h5f.close()

    # INIT Graph data generator, sample a random graph

    datagen = GraphDatagen_binary(curr_seg, curr_seg_lab.get_n_labels(), label_model=None, labmodel_feature_key='')
    random_graph, node_labels = next(datagen)

    # INIT graph feature visualizer gui

    root_widget = tk.Tk()
    gui = GUI(root_widget, CURR_VIDNAME, img_data, curr_seg, random_graph, node_labels,\
                 optflow_fw_raw_data, optflow_bw_raw_data, occl_fw_raw_data, occl_bw_raw_data)
    root_widget.mainloop()    # takes over control of the main thread
