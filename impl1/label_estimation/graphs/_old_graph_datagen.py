
#
# Graph training data generator for the GNN label estimation, binary GNN setup. RLseg2 implementation #1
#   Iterator. Generates training samples from a single graph by varying labels and labeled nodes.
#
#   @author Viktor Varga
#

import sys
sys.path.append('../..')
sys.path.append('../../..')
import os
import pickle
import math
import random

import numpy as np
from sklearn.linear_model import LogisticRegression   # temp for safety assert
import dgl
import torch
from torch.utils.data import IterableDataset
from torch.utils.data import DataLoader

from segmentation import Segmentation
from segmentation_labeling import SegLabeling
import util.imutil as ImUtil
import impl_util as ImplUtil
import config as Config

class GraphDatagen:

    '''
    TWO MODES (train/predict), TWO LABEL CONFIGS (binary/multi-label):
        - 'train' (binary, p_dim=None): 
            infinitely generates random training samples with random label partitioning and randomly revealed node labels
        - 'train' (multiclass, p_dim=n_max_cats): 
            infinitely generates random training samples with original multiclass labels and randomly revealed node labels
        - 'predict' (binary, p_dim=None):
            finite iteration of all one vs all node partitioning setups with previously given revealed nodes
            must reset and specify revealed nodes first with reset_prediction_iterator()
            2*n_labels iterations, then iterator becomes exhausted
        - 'predict' (multiclass, p_dim=n_max_cats):
            generation of a single sample with previously given revealed nodes
            must reset and specify revealed nodes first with reset_prediction_iterator()
            1 iteration, then iterator becomes exhausted


    Member fields:
        seg: Segmentation instance
        label_model: None OR LabelModel subclass instance
        labmodel_feature_key: str; the SP channel key to be used as input features for the label model
        n_labmodel_features: int; number of input features for the label model

        n_labels: int

        true_label_node_idxs: list(n_labels) of ndarray(n_nodes_with_label) of int32; node idxs for each label
        user_annots: ndarray(n_sps_annotated, 2:[SP ids, labels]); only used in 'predict' mode

        nodefs: ndarray(n_nodes, n_features) of float32;
        edges: ndarray(n_edges, 2:[from_ID, to_ID]) of int32; unique edges where ID_from < ID_to
        edgefs: ndarray(n_edges, 2:[from -> to, to -> from], n_features) of float32;

        mode: str; 'train' or 'predict';
            'train' (binary) mode generates samples with random label partition; generator does not expire
            'train' (multiclass) mode generates samples with original multiclass labels; generator does not expire
            'predict' (binary) mode generates each possible one-vs-all category label partition; generator expires
            'predict' (multiclass) mode generates a single sample; generator expires
        iter_idx: int; idx of previous sample generated

    '''

    def __init__(self, vidname, seg, n_labels, mode, label_model, labmodel_feature_key, p_dim=None):
        self.vidname = vidname  # debug info
        self.seg = seg
        assert n_labels >= 2
        self.n_labels = n_labels
        assert mode in ['train', 'predict']
        self.mode = mode
        self.user_annots = None
        self.iter_idx = -1
        assert (p_dim is None) or (p_dim >= self.n_labels)
        self.p_dim = p_dim

        # init label model
        self.labmodel_feature_key = labmodel_feature_key
        self.label_model = label_model
        self.n_labmodel_features = None
        if self.label_model is None:
            assert mode == 'train'
        else:
            fvecs = self.seg.sp_chans[labmodel_feature_key]
            assert fvecs.ndim == 2
            self.n_labmodel_features = fvecs.shape[1]
            self.label_model.reset(self.n_labmodel_features, 2)

        self._init_label_idxs()
        self._init_constant_features()

        #

    def __iter__(self):
        return self

    def __next__(self):
        '''
        Returns:
            'train' MODE:
                (DGLGraph - the graph including the node/edge features,
                 ndarray(n_nodes) of int32 - the node labels for the graph)
            'predict' MODE:
                DGLGraph - the graph including the node/edge features
        '''
        self.iter_idx += 1
        if self.mode == 'train':
            if self.p_dim is None:
                return self._generate_binary_training_sample()
            else:
                return self._generate_multiclass_training_sample()
        else:
            if self.p_dim is None:
                g = self._generate_binary_prediction_sample()
            else:
                g = self._generate_multiclass_prediction_sample()
            if g is None:
                raise StopIteration
            return g

    def reset_prediction_iterator(self, user_annots):
        '''
        Reset iterator with new user annots given. Only for prediction mode.
        Parameters:
            user_annots: ndarray(n_sps_annotated, 2:[SP ids, labels])
        '''
        assert self.mode == 'predict'
        assert user_annots.shape[1:] == (2,)
        self.user_annots = user_annots
        self.iter_idx = -1

    # PRIVATE

    def _init_label_idxs(self):
        '''
        Sets self.true_label_node_idxs.
        '''
        true_labels = self.seg.sp_chans['gt_rounded'].reshape(-1)
        self.true_label_node_idxs = [np.where(true_labels == lab)[0] for lab in range(self.n_labels)]

    '''
    FEATURE LIST (binary):
    Constant node features:
        #0..1: mean occl fw,bw [0..1]
        #2..7: mean_im_lab, std_im_lab [0..1]
        #8: (log2 / 20.) node size
        #9: is image border node (border_sp) {0,1}
    Dynamic node features:
        #10..11: true bg, fg annotation
        #12: label model fg/bg prediction probability [0,1] - 1 is fg, 0 is bg
    Constant edge features (per direction):
        #0: lab_dist [approx. 0..1] (SAME IN BOTH DIRS)
        #1..6: of_fw_diff, of_bw_diff [[0,pi], any real number, [0,1], [0,pi], any real number, [0,1]] (SAME IN BOTH DIRS)
            [abs_angular_diff, min_mag, rel_mag]
        #7..10: flow_edges_gnn (VARIES BY DIR), all zero if not flow edge
        #11: is spatial edge {0,1} (SAME IN BOTH DIRS)
        #12: is flow edge {0,1} (SAME IN BOTH DIRS)

    FEATURE LIST (multiclass):
    Constant node features:
        #0..1: mean occl fw,bw [0..1]
        #2..7: mean_im_lab, std_im_lab [0..1]
        #8: (log2 / 20.) node size
        #9: is image border node (border_sp) {0,1}
    Dynamic node features:
        #10: true annotation
    Multiclass label node features:
        #0..#p_dim-1: label model probabilities
    Constant edge features (per direction):
        #0: lab_dist [approx. 0..1] (SAME IN BOTH DIRS)
        #1..6: of_fw_diff, of_bw_diff [[0,pi], any real number, [0,1], [0,pi], any real number, [0,1]] (SAME IN BOTH DIRS)
            [abs_angular_diff, min_mag, rel_mag]
        #7..10: flow_edges_gnn (VARIES BY DIR), all zero if not flow edge
        #11: is spatial edge {0,1} (SAME IN BOTH DIRS)
        #12: is flow edge {0,1} (SAME IN BOTH DIRS)
    '''

    def _init_constant_features(self):
        '''
        Sets self.nodefs, self.edges, self.edgefs.
        '''
        # constant node features
        nodefs_0 = self.seg.sp_chans['mean_occl_fw']
        nodefs_1 = self.seg.sp_chans['mean_occl_bw']
        nodefs_2_4 = self.seg.sp_chans['mean_im_lab'] / 255.
        nodefs_5_7 = self.seg.sp_chans['std_im_lab'] / 255.
        nodefs_8 = np.log2(self.seg.sp_sizes)[:,None] / 20.
        nodefs_9 = self.seg.sp_chans['border_sp']
        nodefs = np.concatenate([nodefs_0, nodefs_1, nodefs_2_4, nodefs_5_7, nodefs_8, nodefs_9], axis=1).astype(np.float32, copy=False)
        assert nodefs.shape == (self.seg.get_n_sps_total(), 10)

        # add empty place for dynamic node features
        n_dynamic_node_fs = 3 if self.p_dim is None else 1
        self.nodefs = np.pad(nodefs, [(0,0), (0,n_dynamic_node_fs)], mode='constant', constant_values=0.)

        # constant edge features
        edges_all_unidir = self.seg.sp_edges_unidir['lab_dist']
        edgefs_0 = self.seg.sp_edge_chans_unidir['lab_dist'] / 255.
        edgefs_1_3 = self.seg.sp_edge_chans_unidir['of_fw_diff']
        edgefs_4_6 = self.seg.sp_edge_chans_unidir['of_bw_diff']
        edgefs_0_6 = np.concatenate([edgefs_0, edgefs_1_3, edgefs_4_6], axis=1).astype(np.float32, copy=False)
        edges_flow_bidir = self.seg.sp_edges_bidir['flow_edges_gnn']
        edgefs_7_10 = self.seg.sp_edge_chans_bidir['flow_edges_gnn'].astype(np.float32, copy=False)  # bidir
        edges_spatial_unidir = self.seg.sp_edges_unidir['spatial_edges']
        edgefs_11 = np.ones((edges_spatial_unidir.shape[0], 1), dtype=np.float32)
        edgefs_12 = np.ones((edges_flow_bidir.shape[0], 1), dtype=np.float32)
        edge_lists = [edges_all_unidir, edges_flow_bidir, edges_spatial_unidir, edges_flow_bidir]
        feature_lists = [edgefs_0_6, edgefs_7_10, edgefs_11, edgefs_12]

        # stack features
        self.edges, self.edgefs = ImUtil.stack_edge_features_bidir(edge_lists, feature_lists, missing_features_vals=0.) # (n_edges, 12)
        self.edgefs = self.edgefs.astype(np.float32, copy=False)
        print("    (GRAPH datagen INFO:", self.nodefs.shape[0], "nodes,", self.edgefs.shape[0], 'edges)')


    # PRIVATE

    def _generate_binary_training_sample(self):
        '''
        Generates a random binary training sample.
        Returns:
            g: DGLGraph - the graph including the node/edge features
            true_labels_bin: ndarray(n_nodes) of fl32 - the node labels for the graph
        '''
        assert Config.DATAGENTR_SIMULATE_PRED_ALPHA > 1.
        assert Config.DATAGENTR_SIMULATE_PRED_BETA > Config.DATAGENTR_SIMULATE_PRED_ALPHA
        assert Config.DATAGENTR_SIMULATE_LABEL_MODEL or (self.label_model is not None)
        assert (not Config.DATAGENTR_DISABLE_LABEL_MODEL) or Config.DATAGENTR_SIMULATE_LABEL_MODEL

        # generate dynamic node features #10..11 (true user annots)
        n_revealed_nodes_per_cat = GraphDatagen.random_sample_n_nodes_revealed()
        labels0, labels1 = self._generate_rnd_binary_label_partition()
        labels0_idxs_ls = [arr for lab, arr in enumerate(self.true_label_node_idxs) if lab in labels0]
        labels1_idxs_ls = [arr for lab, arr in enumerate(self.true_label_node_idxs) if lab in labels1]
        true_node_idxs0 = self._select_random_node_idxs(labels0_idxs_ls, n_revealed_nodes_per_cat, replace=True)
        true_node_idxs1 = self._select_random_node_idxs(labels1_idxs_ls, n_revealed_nodes_per_cat, replace=True)
        self.nodefs[:,10:12] = 0.
        self.nodefs[true_node_idxs0, 10] = 1.
        self.nodefs[true_node_idxs1, 11] = 1.

        # generate true labels
        label_replace_arr = np.zeros((self.n_labels,), dtype=np.int32)
        label_replace_arr[labels1] = 1
        true_labels_orig = self.seg.sp_chans['gt_rounded'].reshape(-1)
        true_labels_bin = label_replace_arr[true_labels_orig].astype(np.float32)   # (n_nodes,) of int (0 or 1)

        # generate dynamic node feature #12 (label model prediction or its simulated version)
        if Config.DATAGENTR_SIMULATE_LABEL_MODEL:
            # simulate label model without actually using it - simulate predictions with noisy gt labels
            if Config.DATAGENTR_DISABLE_LABEL_MODEL:
                # disable label model (replace feature with uniform random noise)
                self.nodefs[:, 12] = np.random.rand(self.seg.get_n_sps_total())
            else:
                rnd_beta = np.random.beta(Config.DATAGENTR_SIMULATE_PRED_ALPHA, Config.DATAGENTR_SIMULATE_PRED_BETA,\
                                                                                         size=(self.seg.get_n_sps_total()))
                self.nodefs[:, 12] = np.fabs(true_labels_bin - rnd_beta)

        else:
            # train & predict with label model
            true_node_idxs = np.concatenate([true_node_idxs0, true_node_idxs1], axis=0)
            true_node_ys = np.zeros(true_node_idxs.shape[0], dtype=np.int32)
            true_node_ys[true_node_idxs0.shape[0]:] = 1
            self.label_model.reset(self.n_labmodel_features, 2)
            labmodel_train_xs = self.seg.sp_chans[self.labmodel_feature_key][true_node_idxs, :self.n_labmodel_features]   # (n_train_sps, n_features)
            self.label_model.fit(labmodel_train_xs, true_node_ys)
            #
            xs_to_pred = self.seg.sp_chans[self.labmodel_feature_key][:,:self.n_labmodel_features]   # (n_sps, n_features)
            ys_pred = self.label_model.predict(xs_to_pred, return_probs=True)   # (n_nodes, 2)
            assert ys_pred.shape == (xs_to_pred.shape[0], 2)
            assert np.amin(ys_pred) >= 0.
            assert np.amax(ys_pred) <= 1.
            self.nodefs[:, 12] = ys_pred[:,1]
        self.nodefs[true_node_idxs0, 12] = 0.
        self.nodefs[true_node_idxs1, 12] = 1.

        # create DGL graph from self.edges and self.nodefs, self.edgefs copies
        g = dgl.DGLGraph((self.edges.T.reshape(-1), self.edges[:,::-1].T.reshape(-1)))   # similar to concatenating e[:,0] and e[:,1]
        g.ndata['fs'] = self.nodefs.copy()
        edgefs_directed = self.edgefs.transpose((1,0,2)).reshape((-1, self.edgefs.shape[-1]))
                                                               # similar to concatenating efs[:,0,:] and efs[:,1,:]
        g.edata['fs'] = edgefs_directed.copy()
        print("GEN mc vidname: ", self.vidname, " - n_nodes_revealed_per_cat: ", n_revealed_nodes_per_cat)

        return g, true_labels_bin

    def _generate_multiclass_training_sample(self):
        '''
        Generates a random binary training sample.
        Returns:
            g: DGLGraph - the graph including the node/edge features & input label distribution vector
            true_labels: ndarray(n_nodes,) of int32 - the true node category labels for the graph
            cat_weights: ndarray(p_dim,) of float32 - the category weights to be used when balancing the loss
        '''
        assert not Config.DATAGENTR_SIMULATE_LABEL_MODEL, "Simulated label model is only implemented for the binary case."
        # select nodes where true labels are revealed
        true_labels_orig = self.seg.sp_chans['gt_rounded'].reshape(-1)
        true_node_idxs = []
        c_true_labs = []
        n_revealed_nodes_per_cat = GraphDatagen.random_sample_n_nodes_revealed()
        for lab in range(self.n_labels):
            node_idxs_with_lab = np.where(true_labels_orig == lab)[0]         # (n_nodes_with_given_label,)
            node_idxs_with_lab_sel = np.random.choice(node_idxs_with_lab, size=(n_revealed_nodes_per_cat,))
            true_node_idxs.append(node_idxs_with_lab_sel)
            c_true_labs.append(node_idxs_with_lab.shape[0])
        all_true_node_idxs = np.concatenate(true_node_idxs, axis=0)
        c_true_labs = np.array(c_true_labs, dtype=np.float32)
        assert np.all(c_true_labs > 0.)
        cat_weights = 1000./c_true_labs
        cat_weights = np.pad(cat_weights, [(0, self.p_dim - cat_weights.shape[0])], mode='constant', constant_values=0.)

        # fit label model to true nodes
        if isinstance(self.label_model, LogisticRegression):
            assert self.label_model.solver == 'lbfgs', "'lbfgs' solver in 'auto' multi_class mode will use CE loss, which is faster than OVR method"
        self.label_model.reset(self.n_labmodel_features, self.n_labels)
        labmodel_train_xs = self.seg.sp_chans[self.labmodel_feature_key][all_true_node_idxs, :self.n_labmodel_features]   # (n_train_sps, n_features)
        labmodel_train_ys = np.repeat(np.arange(self.n_labels), [len(true_node_idxs[lab]) for lab in range(self.n_labels)])
        assert labmodel_train_xs.shape[0] == labmodel_train_ys.shape[0]
        self.label_model.fit(labmodel_train_xs, labmodel_train_ys)

        # predict all node labels with trained label model
        xs_to_pred = self.seg.sp_chans[self.labmodel_feature_key][:,:self.n_labmodel_features]   # (n_sps, n_features)
        ys_pred = self.label_model.predict(xs_to_pred, return_probs=True)   # (n_nodes, n_labels)
        assert ys_pred.shape == (xs_to_pred.shape[0], self.n_labels)
        assert np.amin(ys_pred) >= 0.
        assert np.amax(ys_pred) <= 1.

        # add variable node feature: feature#10
        self.nodefs[:, 10] = 0.
        self.nodefs[all_true_node_idxs, 10] = 1.

        # generate input label probability vectors
        ys_pred_padded = np.pad(ys_pred, [(0,0), (0, self.p_dim-self.n_labels)])
        ys_pred_padded[all_true_node_idxs, :] = 0.
        ys_pred_padded[all_true_node_idxs, labmodel_train_ys] = 1.

        # generate graph & true label vectors
        g = dgl.DGLGraph((self.edges.T.reshape(-1), self.edges[:,::-1].T.reshape(-1)))   # similar to concatenating e[:,0] and e[:,1]
        g.ndata['fs'] = self.nodefs.copy()
        g.ndata['ps'] = ys_pred_padded.astype(np.float32)
        edgefs_directed = self.edgefs.transpose((1,0,2)).reshape((-1, self.edgefs.shape[-1]))
                                                               # similar to concatenating efs[:,0,:] and efs[:,1,:]
        g.edata['fs'] = edgefs_directed.copy()
        print("GEN mc vidname: ", self.vidname, " - n_nodes_revealed_per_cat: ", n_revealed_nodes_per_cat)

        #true_labels_pvec = np.zeros((true_labels_orig.shape[0], self.p_dim), dtype=np.float32)
        #true_labels_pvec[np.arange(true_labels_orig.shape[0]), true_labels_orig] = 1.
        #return g, true_labels_pvec

        # TEMP
        # print("  Graph stats:")
        # dists = ImplUtil.get_graph_route_lengths(self.edges, all_true_node_idxs, directed=False, n_nodes=self.nodefs.shape[0])
        # u_dists, c_dists = np.unique(dists, return_counts=True)
        # print(list(zip(u_dists, c_dists)))
        # TEMP END


        return g, true_labels_orig, cat_weights


    def _generate_binary_prediction_sample(self):
        '''
        Generates the next one-vs-all binary label partitioning.
        Returns:
            DGLGraph - the graph including the node/edge features
        '''
        assert self.user_annots is not None
        if self.iter_idx >= 2*self.n_labels:
            return None

        # generates 2*n_labels samples; 
        #   i=0..n_labels-1 where label#i -> 1, all others -> 0
        #   i=n_labels..2*n_labels-1 where label#i -> 0, all others -> 1

        # generate dynamic node features #10..11 (true user annots)
        curr_fg_label = self.iter_idx % self.n_labels
        fg_mask = self.user_annots[:,1] == curr_fg_label
        true_node_idxs1 = self.user_annots[fg_mask, 0]
        true_node_idxs0 = self.user_annots[~fg_mask, 0]
        true_node_idxs0, true_node_idxs1 = (true_node_idxs1, true_node_idxs0) if self.iter_idx >= self.n_labels\
                                                                                    else (true_node_idxs0, true_node_idxs1)
        self.nodefs[:,10:12] = 0.
        self.nodefs[true_node_idxs0, 10] = 1.
        self.nodefs[true_node_idxs1, 11] = 1.

        # fit label model
        true_node_idxs = np.concatenate([true_node_idxs0, true_node_idxs1], axis=0)
        true_node_ys = np.zeros(true_node_idxs.shape[0], dtype=np.int32)
        true_node_ys[true_node_idxs0.shape[0]:] = 1
        self.label_model.reset(self.n_labmodel_features, 2)
        labmodel_train_xs = self.seg.sp_chans[self.labmodel_feature_key][true_node_idxs, :self.n_labmodel_features]   # (n_train_sps, n_features)
        self.label_model.fit(labmodel_train_xs, true_node_ys)
        
        # predict with label model -> node feature#12
        xs_to_pred = self.seg.sp_chans[self.labmodel_feature_key][:,:self.n_labmodel_features]   # (n_sps, n_features)
        ys_pred = self.label_model.predict(xs_to_pred, return_probs=True)   # (n_nodes, 2)
        assert ys_pred.shape == (xs_to_pred.shape[0], 2)
        assert np.amin(ys_pred) >= 0.
        assert np.amax(ys_pred) <= 1.
        self.nodefs[:, 12] = ys_pred[:,1]
        self.nodefs[true_node_idxs0, 12] = 0.
        self.nodefs[true_node_idxs1, 12] = 1.

        # create DGL graph from self.edges and self.nodefs, self.edgefs copies
        g = dgl.DGLGraph((self.edges.T.reshape(-1), self.edges[:,::-1].T.reshape(-1)))   # similar to concatenating e[:,0] and e[:,1]
        g.ndata['fs'] = self.nodefs.copy()
        edgefs_directed = self.edgefs.transpose((1,0,2)).reshape((-1, self.edgefs.shape[-1]))
                                                               # similar to concatenating efs[:,0,:] and efs[:,1,:]
        g.edata['fs'] = edgefs_directed.copy()
        return g

    def _generate_multiclass_prediction_sample(self):
        '''
        Generates a single sample (then, the iterator is exhausted).
        Returns:
            DGLGraph - the graph including the node/edge features
        '''
        assert self.user_annots is not None
        if self.iter_idx >= 1:
            return None

        # fit label model to true nodes (user annots)
        if isinstance(self.label_model, LogisticRegression):
            assert self.label_model.solver == 'lbfgs', "'lbfgs' solver in 'auto' multi_class mode will use CE loss, which is faster than OVR method"
        true_node_idxs = self.user_annots[:,0]
        true_node_ys = self.user_annots[:,1]
        true_node_xs = self.seg.sp_chans[self.labmodel_feature_key][true_node_idxs, :self.n_labmodel_features]   # (n_train_sps, n_features)
        assert true_node_xs.shape[0] == true_node_ys.shape[0]
        self.label_model.fit(true_node_xs, true_node_ys)

        # predict all node labels with trained label model
        xs_to_pred = self.seg.sp_chans[self.labmodel_feature_key][:,:self.n_labmodel_features]   # (n_sps, n_features)
        ys_pred = self.label_model.predict(xs_to_pred, return_probs=True)   # (n_nodes, n_labels)
        assert ys_pred.shape == (xs_to_pred.shape[0], self.n_labels)
        assert np.amin(ys_pred) >= 0.
        assert np.amax(ys_pred) <= 1.

        # add variable node feature: feature#10
        self.nodefs[:, 10] = 0.
        self.nodefs[true_node_idxs, 10] = 1.

        # generate input label probability vectors
        ys_pred_padded = np.pad(ys_pred, [(0,0), (0, self.p_dim-self.n_labels)])
        ys_pred_padded[true_node_idxs, :] = 0.
        ys_pred_padded[true_node_idxs, true_node_ys] = 1.

        # generate graph & true label vectors
        g = dgl.DGLGraph((self.edges.T.reshape(-1), self.edges[:,::-1].T.reshape(-1)))   # similar to concatenating e[:,0] and e[:,1]
        g.ndata['fs'] = self.nodefs.copy()
        g.ndata['ps'] = ys_pred_padded.astype(np.float32)
        edgefs_directed = self.edgefs.transpose((1,0,2)).reshape((-1, self.edgefs.shape[-1]))
                                                               # similar to concatenating efs[:,0,:] and efs[:,1,:]
        g.edata['fs'] = edgefs_directed.copy()
        return g


    def _generate_rnd_binary_label_partition(self):
        '''
        Generates a random partitioning of the label set into two (disjoint) subsets.
        Returns:
            labels0: ndarray(n_labels0,) of int
            labels1: ndarray(n_labels1,) of int
        '''
        labels = np.arange(self.n_labels, dtype=np.int32)
        np.random.shuffle(labels)
        labels0, labels1 = np.split(labels, [np.random.randint(1, labels.shape[0]),])
        return labels0, labels1

    def _select_random_node_idxs(self, node_idxs_ls, n_samples, replace=False):
        '''
        Efficiently selects 'n_samples' number of random node idxs from list of idx arrays.
        Parameters:
            node_idxs_ls: list(n_sub_labels) of ndarray(n_nodes_in_sublabel,) of int
            n_samples: int
            replace: bool; if False, all selected idxs are unique
        Returns:
            selected_idxs: ndarray(n_samples,) of int32
        '''
        ls_cumlens = np.cumsum([0] + [len(arr) for arr in node_idxs_ls])   # [0, len(idxarr0), len(idxarr0)+len(idxarr1), ...]
        assert replace or (n_samples <= ls_cumlens[-1])
        idxs_over_lists = np.random.choice(ls_cumlens[-1], size=(n_samples,), replace=replace)
        selected_idxs = []
        for ls_idx in range(len(node_idxs_ls)):
            offset, end_offset = ls_cumlens[ls_idx], ls_cumlens[ls_idx+1]
            idxs_in_list = idxs_over_lists[(idxs_over_lists >= offset) & (idxs_over_lists < end_offset)]
            selected_idxs.append(node_idxs_ls[ls_idx][idxs_in_list - offset])
        selected_idxs = np.concatenate(selected_idxs, axis=0)
        return selected_idxs

    @staticmethod
    def random_sample_n_nodes_revealed():
        '''
        Generates a random number to be used as "number of revealed nodes" by using Config cosntants.
        Returns:
            n_nodes_revealed: int
        '''
        assert Config.DATAGENTR_N_NODES_REVEALED_PER_CAT_MIN <= Config.DATAGENTR_N_NODES_REVEALED_PER_CAT_MAX
        assert Config.DATAGENTR_N_NODES_REVEALED_PER_CAT_EXPBASE > 0.
        # if min == max, return them
        if Config.DATAGENTR_N_NODES_REVEALED_PER_CAT_MIN == Config.DATAGENTR_N_NODES_REVEALED_PER_CAT_MAX:
            n_nodes_revealed = int(Config.DATAGENTR_N_NODES_REVEALED_PER_CAT_MAX)
        else:
            logmin = math.log(Config.DATAGENTR_N_NODES_REVEALED_PER_CAT_MIN, Config.DATAGENTR_N_NODES_REVEALED_PER_CAT_EXPBASE)
            logmax = math.log(Config.DATAGENTR_N_NODES_REVEALED_PER_CAT_MAX, Config.DATAGENTR_N_NODES_REVEALED_PER_CAT_EXPBASE)
            logrnd = random.uniform(logmin, logmax)
            n_nodes_revealed = int(Config.DATAGENTR_N_NODES_REVEALED_PER_CAT_EXPBASE ** logrnd)
        assert 1 <= n_nodes_revealed <= 2000
        return n_nodes_revealed


    # END OF GraphDatagen class

class GraphTrainingDataset(IterableDataset):

    '''
    Training/validation dataset subclassing the PyTorch IterableDataset class to support parallel sample generation.
    A single instance should be initialized for training and another one for validation.

    Member fields:
        segs: dict{vidname - str: Segmentation}
        seg_labs: dict{vidname - str: SegLabeling}
        lab_model_init_fn: Callable, signature is (,) -> LabelModel
        n_workers: int; if 0, Multiprocessing is disabled; otherwise designates the number of parallel asynchronous workers

        datagens: dict{vidname - str: GraphDatagen}
        lab_models: list(n_workers) of LabelModel
        worker_datagen_names: list(n_workers) of list(n_datagen_in_worker) of strs - the lists of vidnames handled by each worker
    '''

    def __init__(self, segs, seg_labs, lab_model_init_fn, labmodel_feature_key, n_workers=0, p_dim=None):
        super(GraphTrainingDataset).__init__()
        self.segs = segs
        assert set(seg_labs.keys()) == set(segs.keys())
        self.seg_labs = seg_labs

        self.lab_model_init_fn = lab_model_init_fn
        assert n_workers >= 0
        assert n_workers <= len(self.segs.keys()), "The number of workers must be equal or less than the number of videos in the dataset."
        self.n_workers = n_workers
        self.labmodel_feature_key = labmodel_feature_key
        self.p_dim = p_dim

        # init label models: one instance for each worker
        self.lab_models = [lab_model_init_fn() for _ in range(self.get_n_threads())]

        # init data generators and split them up into bins (one bin for each worker)
        self.worker_datagen_names = np.array_split(list(self.segs.keys()), self.get_n_threads())
        self.worker_datagen_names = [list(charr) for charr in self.worker_datagen_names]
        datagen_lab_model_dict = {vidname: self.lab_models[worker_idx] for worker_idx, worker_vids in enumerate(self.worker_datagen_names) for vidname in worker_vids}

        self.datagens = {vidname: GraphDatagen(vidname, self.segs[vidname], self.seg_labs[vidname].n_labels,\
                                 'train', datagen_lab_model_dict[vidname], labmodel_feature_key, p_dim) \
                                                         for vidname in list(self.segs.keys())}

    def get_n_threads(self):
        return max(1, self.n_workers)

    def __iter__(self):
        worker_info = torch.utils.data.get_worker_info()
        assert ((worker_info is None) and (self.n_workers == 0)) or ((worker_info.num_workers == self.get_n_threads()))
        current_worker_idx = 0 if worker_info is None else worker_info.id
        vidnames_for_current_worker = self.worker_datagen_names[current_worker_idx]   # list of str
        datagens_for_current_worker = [self.datagens[vidname] for vidname in vidnames_for_current_worker]
        return ImplUtil.merge_iterators_random_select(datagens_for_current_worker)

    # END OF GraphTrainingDataset class

def collate_training_samples(batch, multiclass):
    '''
    Can be used as 'collate_fn' in a PyTorch DataLoader.
    Assembles batches from samples generated by training data generators.
    Additional parameters:
        multiclass: bool
    '''
    batch_gs, batch_ys, batch_snorm_n, batch_snorm_e, batch_cat_ws = [], [], [], [], []
    for (g, node_ys, cat_weights) in batch:
        batch_gs.append(g)
        if not multiclass:
            node_ys = node_ys.reshape(node_ys.shape + (1,))   # binary case
        batch_ys.append(node_ys)
        n_nodes, n_edges = g.number_of_nodes(), g.number_of_edges()
        batch_snorm_n.append(torch.FloatTensor(n_nodes,1).fill_(1./float(n_nodes)))
        batch_snorm_e.append(torch.FloatTensor(n_edges,1).fill_(1./float(n_edges)))
        batch_cat_ws.append(cat_weights[None,:])
    batch_snorm_n = torch.cat(batch_snorm_n, dim=0).sqrt()
    batch_snorm_e = torch.cat(batch_snorm_e, dim=0).sqrt()
    batch_ys = torch.tensor(np.concatenate(batch_ys, axis=0))
    batch_gs = dgl.batch(batch_gs)
    batch_cat_ws = torch.tensor(np.concatenate(batch_cat_ws, axis=0))
    return batch_gs, batch_ys, batch_snorm_n, batch_snorm_e, batch_cat_ws


