
#
# Augmented label estimation. RLseg2 implementation #1.
#   Segment labels are estimated independently (no graphical label propagation implemented).
#   When fit() is called, more training samples are generated using SP graph edges and weights.
#
#   @author Viktor Varga
#

import os
import numpy as np
from .abstract_label_estimation import LabelEstimation
import util.imutil as ImUtil
import util.util as Util

OPTFLOW_WEIGHT_ALPHA = 1.    # between 0 and 1
N_PROPAGATION_ITER = 20
MIN_PROPAGATION_SCORE = 0.6
FIT_SAMPLE_SIZE_LIMIT = 2000   # if n_samples exceed this limit after augmentation, randomly select this many from them

# good settings on individual videos: 20iter 0.3, 0.6, optflow alpha 1., sample size 2000

class AugmentedLabelEstimation(LabelEstimation):

    '''
    Member fields:
        seg: Segmentation instance
        label_model: LabelModel subclass instance

        seg_feature_key: str
        n_features: int
        n_labels: int

        user_annots: ndarray(n_sps_annotated, 2:[SP ids, labels])
        graph_edges: ndarray(n_all_edges*2, 2:[id0, id1]) of int; only valid edges; sorted by id0
                                edges are duplicated in the array, both original and reversed is stored
        graph_weights: ndarray(n_all_edges*2,) of float
    '''

    def __init__(self, seg, seg_feature_key, n_labels, label_model, n_features_to_use=None):
        self.seg = seg
        self.n_labels = n_labels
        self.seg_feature_key = seg_feature_key
        fvecs = self.seg.sp_chans[seg_feature_key]
        assert fvecs.ndim == 2
        assert n_features_to_use is None or n_features_to_use <= fvecs.shape[1]
        self.n_features = fvecs.shape[1] if n_features_to_use is None else n_features_to_use

        self.label_model = label_model
        self.label_model.reset(self.n_features, self.n_labels)

        self.user_annots = None
        self._create_edge_lists()


    def __str__(self):
        return "Aug_" + str(self.label_model)

    def fit(self, user_annot, n_new_annots):
        '''
        Parameters:
            user_annot: list(n_user_annots) of ndarray(n_segs_in_user_annot, 2:[SP id, label])
            n_new_annots: int; number of unseen annot items in the end of 'user_annot' list.
        '''
        self.label_model.reset(self.n_features, self.n_labels)
        self.user_annots = np.concatenate(user_annot, axis=0)   # (n_train_sps, 2:[SP id, label])
        
        train_segs = self._collect_samples_with_optflow_propagation(self.user_annots, N_PROPAGATION_ITER,\
                                     MIN_PROPAGATION_SCORE, balance_sample=True)   # (n_train_sps, 2:[SP id, label])
        if train_segs.shape[0] > FIT_SAMPLE_SIZE_LIMIT:
            subsample_idxs = np.random.choice(train_segs.shape[0], size=(FIT_SAMPLE_SIZE_LIMIT,))
            train_segs = train_segs[subsample_idxs,:]

        print("Augmented sample shape:", train_segs.shape)

        train_xs = self.seg.sp_chans[self.seg_feature_key][train_segs[:,0], :self.n_features]   # (n_train_sps, n_features)
        self.label_model.fit(train_xs, train_segs[:,1])

    def predict_all(self, return_probs=False):
        '''
        Parameters:
            return_probs: bool
        Returns:
            ys_pred: ndarray(n_sps, n_cat) of fl32 (IF return_probs == True)
                     ndarray(n_sps,) of i32        (IF return_probs == False)
        '''
        xs_to_pred = self.seg.sp_chans[self.seg_feature_key][:,:self.n_features]   # (n_sps, n_features)
        ys_pred = self.label_model.predict(xs_to_pred, return_probs=return_probs)
        return ys_pred

    # PRIVATE

    def _create_edge_lists(self):
        '''
        Sets self.graph_edges, self.graph_weights.
        '''
        assert 0. <= OPTFLOW_WEIGHT_ALPHA <= 1.
        temporal_edges = self.seg.sp_edges_unidir['temporal_edges']
        flow_edges = self.seg.sp_edges_unidir['flow_edges']
        flow_weights = self.seg.sp_edge_chans_unidir['flow_edges']

        # merge edge lists from multiple sources (temporal edges, optflow edges)
        merge_fun = lambda ews: (1.-OPTFLOW_WEIGHT_ALPHA)*ews[:,0] + OPTFLOW_WEIGHT_ALPHA*ews[:,1]
        graph_edges, graph_weights = ImUtil.merge_edge_lists([temporal_edges, flow_edges],\
                           [np.ones((temporal_edges.shape[0],),  dtype=np.float32),\
                            flow_weights.reshape(-1)], np.zeros((2,), dtype=np.float32), fun_weight_merge=merge_fun)

        # filter invalid edges (pointint out of screen)
        valid_edges = np.all(graph_edges >= 0, axis=1)   # (n_edges)
        graph_edges = graph_edges[valid_edges]
        graph_weights = graph_weights[valid_edges]

        # duplicating each edge (adding reversed edges to array)
        #    and sorting them to be able to query the neighbors of a SP efficiently
        graph_edges = np.concatenate([graph_edges, graph_edges[:,::-1]], axis=0)
        graph_weights = np.concatenate([graph_weights, graph_weights], axis=0)
        edge_sorter = np.argsort(graph_edges[:,0])
        self.graph_edges = graph_edges[edge_sorter,:]
        self.graph_weights = graph_weights[edge_sorter]
        assert np.all(self.graph_edges[:-1,0] <= self.graph_edges[1:,0])   # assert sorted by sp_id0, TODO can remove


    def _get_all_adjacent_sps_and_weights(self, sp_ids_from):
        '''
        Returns all adjacent SPs to 'sp_ids_from' and the weights of the edges leading to them.
        Expects self.graph_edges to be sorted.
        Parameters:
            sp_ids_from: ndarray(n_sps) of int32
        Returns:
            adj_sp_ids: ndarray(n_all_adj_sps) of int32; not in any particular order, NOT UNIQUE
            edge_weights: ndarray(n_all_adj_sps) of float32
        '''
        # dropping sps with no edges
        sps_with_edges_mask = np.isin(sp_ids_from, self.graph_edges[:,0])
        sp_ids_from = sp_ids_from[sps_with_edges_mask]

        # getting idx ranges of sp IDs in self.graph_edges
        sp_ids_from_idx_offsets = np.searchsorted(self.graph_edges[:,0], sp_ids_from)   # range starts
        sp_ids_from_idx_end_offsets = np.searchsorted(self.graph_edges[:,0], sp_ids_from+1)  # range ends, excl.
        edge_idxs = Util.get_multirange_idxs(\
                        np.stack([sp_ids_from_idx_offsets, sp_ids_from_idx_end_offsets], axis=1))[1] # (n_idxs,)

        return self.graph_edges[edge_idxs,1], self.graph_weights[edge_idxs]

    def _collect_samples_with_optflow_propagation(self, orig_samples, n_iter, min_prop_score, balance_sample=True):
        '''
        Paramters:
            orig_samples: ndarray(n_train_sps, 2:[SP id, label]) of int
            n_iter: int; number of propagation iterations
            min_prop_score: float; minimum edge weight needed to propagate through edge
            balance_sample: bool; whether to return equal number of samples in each category ()
        Returns:
            ret_samples_arr: ndarray(n_aug_train_sps, 2:[SP id, label]) of int
        '''
        assert orig_samples.shape[0] > 0

        # store in array the (propagated or true) label for each SP in the video; -1: no label assigned yet
        all_sp_labels = np.full((self.seg.get_n_sps_total()), dtype=np.int32, fill_value=-1)   # (n_sps,) of int32
        all_sp_labels[orig_samples[:,0]] = orig_samples[:,1]

        # store in mask whether given SP can be returned (True) or is ignored (False) because multiple
        #        categories could be assigned to it
        valid_sps_mask = np.ones((self.seg.get_n_sps_total()), dtype=np.bool_)   # (n_sps,) of bool_, TRUE by default
        # store in mask the SPs from which propagation has already been done
        propagated_sps_mask = np.zeros_like(valid_sps_mask)   # (n_sps,) of bool_, FALSE by default
        # store in mask the SPs that are in the original SPs array
        orig_sps_mask = propagated_sps_mask.copy()   # (n_sps,) of bool_, FALSE by default
        orig_sps_mask[orig_samples[:,0]] = True
        # store in mask the SPs to be returned
        return_mask = propagated_sps_mask.copy()

        # for each category, propagate samples available
        u_cats = np.unique(orig_samples[:,1])
        for cat in u_cats:
            orig_samples_in_cat = orig_samples[orig_samples[:,1] == cat,0]  # (n_samples_in_cat)
            curr_sample_ids = orig_samples_in_cat

            for iter_idx in range(n_iter):
                # get adjacent SPs
                adj_sp_ids, edge_weights = self._get_all_adjacent_sps_and_weights(curr_sample_ids)

                # set propageted SPs & SPs to be returned
                propagated_sps_mask[curr_sample_ids] = True
                return_mask[curr_sample_ids] = True

                # filter adjacent SPs with low score
                adj_sp_ids = adj_sp_ids[edge_weights >= min_prop_score]

                # set SPs that are not original but already have an assigned different category label to invalid
                adj_unorig_sp_ids = adj_sp_ids[~orig_sps_mask[adj_sp_ids]]  # adjacent & not original, int
                adj_unorig_prev_cats = all_sp_labels[adj_unorig_sp_ids]
                invalid_sp_ids = adj_unorig_sp_ids[(adj_unorig_prev_cats >= 0) & (adj_unorig_prev_cats != cat)]   # invalid, int
                valid_sps_mask[invalid_sp_ids] = False
                propagated_sps_mask[invalid_sp_ids] = True   # do not propagate invalid SPs further

                # set labels
                all_sp_labels[adj_sp_ids] = cat

                # filter SPs which have already been propagated
                adj_sp_ids = adj_sp_ids[~propagated_sps_mask[adj_sp_ids]]

                curr_sample_ids = adj_sp_ids
                # break if nothing to propagate
                if curr_sample_ids.shape[0] == 0:
                    break

        # get valid return SP IDs (includes original input)
        assert np.all(all_sp_labels[return_mask & valid_sps_mask] >= 0)   # assert valid labels
        return_samples = np.where(return_mask & valid_sps_mask)[0]
        return_sample_labels = all_sp_labels[return_samples]

        # construct return array: use resize to repeat sp IDs of less frequent labels
        u_return_sample_cats, c_return_sample_cats = np.unique(return_sample_labels, return_counts=True)
        n_max_cats = np.amax(c_return_sample_cats)
        ret_samples_arr = np.empty((u_return_sample_cats.shape[0], n_max_cats, 2), dtype=np.int32)
        for cat_idx in range(u_return_sample_cats.shape[0]):
            return_samples_in_cat = return_samples[return_sample_labels == u_return_sample_cats[cat_idx]]
            ret_samples_arr[cat_idx,:,0] = np.resize(return_samples_in_cat, ret_samples_arr.shape[1])
            ret_samples_arr[cat_idx,:,1] = u_return_sample_cats[cat_idx]

        return ret_samples_arr.reshape(-1, 2)













