
#
# Main runner script. RLseg2 implementation #1
#
#   @author Viktor Varga
#
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import sys
sys.path.append('..')

import os
import numpy as np
import pickle
import functools

from data_if import DataIF
from segmentation_labeling import SegLabeling

from label_estimation.basic_label_estimation import BasicLabelEstimation
from label_estimation.augmented_label_estimation import AugmentedLabelEstimation
from label_estimation.mrf_label_estimation import MRFLabelEstimation
from label_estimation.logreg_label_model import LogRegLabelModel
from simple_benchmark_task_selection import SimpleBenchmarkTaskSelection

from annotator import InteractiveAnnotator
import tkinter as tk

import util.viz as Viz
import config as Config


if Config.LABEL_MODEL_METHOD == 'leo':
    from label_estimation.leo_label_model import LEOLabelModel   # only import tensorflow if used
if Config.LABEL_ESTIMATION_ALGORITHM == 'gnn_bin':
    from label_estimation.gnn_label_estimation import GNNLabelEstimation   # only import pytorch if used
    from label_estimation.graphs.graph_datagen import GraphDatagen, GraphTrainingDataset, collate_training_samples
    from torch.utils.data import DataLoader


if __name__ == '__main__':

    print("CONFIG --->")
    print({var: Config.__dict__[var] for var in dir(Config) if not var.startswith("__")})
    print("<--- CONFIG")

    # LOAD DATA, INIT SegLabeling

    data_if = DataIF()
    segs = {}
    seg_labs = {}
    all_vidnames = set(data_if.get_train_vids() + data_if.get_validation_vids() + data_if.get_test_vids())
    for vidname in all_vidnames:
        segs[vidname] = data_if.get_seg_obj(vidname)
        seg_labs[vidname] = SegLabeling(segs[vidname])
    max_n_labels = max([seg_labs[vidname].get_n_labels() for vidname in all_vidnames])

    # LOAD IMAGE DATA IF NECESSARY

    if Config.FOLDER_RENDER_PREDICTIONS:
        img_data = DataIF.load_img_data(data_if.get_test_vids())

    print("Loaded videos & data:", str(len(data_if.get_train_vids())), "train,",\
                                   str(len(data_if.get_validation_vids())), "validation,",\
                                   str(len(data_if.get_test_vids())), "test videos.")

    # INIT LabelModel

    assert Config.LABEL_MODEL_METHOD in ['logreg', 'leo']
    if Config.LABEL_MODEL_METHOD == 'logreg':
        lab_model = LogRegLabelModel()
    elif Config.LABEL_MODEL_METHOD == 'leo':
        lab_model = LEOLabelModel()

    # PRETRAIN LabelModel

    
    tr_vids = data_if.get_train_vids()
    val_vids = data_if.get_validation_vids()
    feature_range = slice(None) if Config.FIRST_N_FEATURES_TO_USE is None else slice(None, Config.FIRST_N_FEATURES_TO_USE)
    xss_train = [seg_labs[vidname].seg.sp_chans['fvecs'][..., feature_range] for vidname in tr_vids]
    yss_train = [seg_labs[vidname].get_true_labels() for vidname in tr_vids]
    xss_val = [seg_labs[vidname].seg.sp_chans['fvecs'][..., feature_range] for vidname in val_vids]
    yss_val = [seg_labs[vidname].get_true_labels() for vidname in val_vids]
    n_features = xss_train[0].shape[-1]
    lab_model.reset(n_features=n_features, n_cats=None)
    lab_model.pretrain_mutliple_label_setups(xss_train, yss_train, xss_val, yss_val)

    # INIT LabelEstimation (gnn only), PRETRAIN LabelEstimation

    if Config.LABEL_ESTIMATION_ALGORITHM == 'gnn_bin':

        p_dim = max_n_labels if Config.GNNBIN_NATIVE_MULTICLASS else None
        lab_est = GNNLabelEstimation('fvecs', lab_model, Config.FIRST_N_FEATURES_TO_USE, multiclass_n_cats=p_dim)

        if Config.GNNBIN_PRETRAIN:
            tr_segs = {vidname: segs[vidname] for vidname in tr_vids}
            val_segs = {vidname: segs[vidname] for vidname in val_vids}
            tr_seglabs = {vidname: seg_labs[vidname] for vidname in tr_vids}
            val_seglabs = {vidname: seg_labs[vidname] for vidname in val_vids}
            assert Config.LABEL_MODEL_METHOD == 'logreg', "Multiprocessing is only implemented with logreg label model currently"
            lab_model_init_fn = lambda: LogRegLabelModel()
            tr_dataset = GraphTrainingDataset(tr_segs, tr_seglabs, lab_model_init_fn, 'fvecs', Config.GNNBIN_N_WORKERS, p_dim)
            val_dataset = GraphTrainingDataset(val_segs, val_seglabs, lab_model_init_fn, 'fvecs', Config.GNNBIN_N_WORKERS, p_dim)
            collate_fn = functools.partial(collate_training_samples, multiclass=Config.GNNBIN_NATIVE_MULTICLASS)
            tr_dataloader = DataLoader(tr_dataset, batch_size=Config.GNNBIN_BATCH_SIZE, num_workers=Config.GNNBIN_N_WORKERS, \
                                                                    collate_fn=collate_fn, pin_memory=False)
            val_dataloader = DataLoader(val_dataset, batch_size=Config.GNNBIN_BATCH_SIZE, num_workers=Config.GNNBIN_N_WORKERS, \
                                                                    collate_fn=collate_fn, pin_memory=False)
        else:
            tr_dataloader, val_dataloader = None, None
        lab_est.pretrain_graphs(tr_dataloader, val_dataloader)

    # INIT TASK SELECTION: create and save benchmark .pkl with info textfile if it has not been created yet

    benchmark_fpath = os.path.join(Config.BENCHMARK_TASK_SELECTION_FOLDER, Config.BENCHMARK_TASK_SELECTION_FNAME_NOEXT + '.pkl')
    if not os.path.isfile(benchmark_fpath):
        benchmark_n_sps_revealed_dict = {key: Config.BENCHMARK_TASK_SELECTION_N_REPEATS for key in [1, 2, 5, 10, 20, 50, 200, 500]}
        test_seg_labs = {vidname: seg_labs[vidname] for vidname in data_if.get_test_vids()}
        task_selection = SimpleBenchmarkTaskSelection(benchmark_n_sps_revealed_dict, test_seg_labs)
        os.makedirs(Config.BENCHMARK_TASK_SELECTION_FOLDER, exist_ok=True)
        with open(benchmark_fpath, 'wb') as f:
            pickle.dump(task_selection, f)
        benchmark_info_fpath = os.path.join(Config.BENCHMARK_TASK_SELECTION_FOLDER, Config.BENCHMARK_TASK_SELECTION_FNAME_NOEXT + '.txt')
        with open(benchmark_info_fpath, 'w') as f:
            f.write(task_selection._to_string_full_info())

    # LOAD TASK SELECTION

    with open(benchmark_fpath, 'rb') as f:
        task_selection = pickle.load(f)
    print("Task selection loaded, benchmark videos: ", list(task_selection.get_vidnames_set()))
    assert task_selection.get_vidnames_set() <= set(data_if.get_test_vids()),\
                                     "Task selection instance requires additional videos to be loaded."

    # EVALUATING: iterate test videos

    allvid_mean_accs = []
    allvid_min_accs = []
    allvid_max_accs = []
    for curr_vidname in data_if.get_test_vids():

        print("    Video:", curr_vidname)

        curr_seg = segs[curr_vidname]
        curr_seg_lab = seg_labs[curr_vidname]

        # INIT LABEL ESTIMATION per video

        assert Config.LABEL_ESTIMATION_ALGORITHM in ['basic', 'aug', 'mrf', 'gnn_bin']
        if Config.LABEL_ESTIMATION_ALGORITHM == 'basic':
            lab_est = BasicLabelEstimation(curr_seg, 'fvecs', curr_seg_lab.n_labels, lab_model, Config.FIRST_N_FEATURES_TO_USE)
        elif Config.LABEL_ESTIMATION_ALGORITHM == 'mrf':
            lab_est = MRFLabelEstimation(curr_seg, 'fvecs', curr_seg_lab.n_labels, lab_model, Config.FIRST_N_FEATURES_TO_USE)
        elif Config.LABEL_ESTIMATION_ALGORITHM == 'aug':
            lab_est = AugmentedLabelEstimation(curr_seg, 'fvecs', curr_seg_lab.n_labels, lab_model, Config.FIRST_N_FEATURES_TO_USE)
        elif Config.LABEL_ESTIMATION_ALGORITHM == 'gnn_bin':
            lab_est.set_prediction_video(curr_seg, curr_seg_lab.n_labels)
        curr_seg_lab.set_label_estimation(lab_est)


        # EVALUATION & VISUALIZATION

        print("Evaluation & visualization phase: ")
        if Config.FOLDER_RENDER_PREDICTIONS is not None:
            os.makedirs(Config.FOLDER_RENDER_PREDICTIONS, exist_ok=True)

        task_selection.reset(curr_vidname)
        accs = []
        for task in task_selection:
            curr_seg_lab.reset()
            curr_seg_lab.add_true_user_annot(sp_ids=task['sp_ids'])
            curr_seg_lab.update_predictions()
            acc = curr_seg_lab.evaluate_predictions('mean_j_sp')
            print("    Task", task['name'], ":", acc)
            accs.append(acc)

            # rendering predictions
            if Config.FOLDER_RENDER_PREDICTIONS is not None:
                vidfname = str(lab_est) + '_' + curr_vidname + '_' + task['name_fname'] + '.avi'
                fpath_out = os.path.join(Config.FOLDER_RENDER_PREDICTIONS, vidfname)
                sp_is_gt_mask = curr_seg_lab.user_labels >= 0
                Viz.render_predictions_video(fpath_out, curr_seg_lab.pred_labels, sp_is_gt_mask, img_data[curr_vidname], curr_seg_lab.seg)

        n_repeats = task_selection.get_n_repeats()
        assert n_repeats is not None
        accs_arr = np.array(accs).reshape((-1, n_repeats))
        mean_accs_arr = np.mean(accs_arr, axis=-1)
        min_accs_arr = np.amin(accs_arr, axis=-1)
        max_accs_arr = np.amax(accs_arr, axis=-1)
        print("\nMean accs: ", mean_accs_arr)
        print("\nMax accs: ", max_accs_arr)
        print("\nMin accs: ", min_accs_arr)
        allvid_mean_accs.append(mean_accs_arr)
        allvid_min_accs.append(min_accs_arr)
        allvid_max_accs.append(max_accs_arr)

    # compute mean of per video mean/min/max accs
    print("\nMean of pervid mean accs: ", np.mean(np.stack(allvid_mean_accs, axis=0), axis=0))
    print("\nMean of pervid max accs: ", np.mean(np.stack(allvid_max_accs, axis=0), axis=0))
    print("\nMean of pervid min accs: ", np.mean(np.stack(allvid_min_accs, axis=0), axis=0))

    # plotting test results (TEMP)
    
    '''
    ref_means = [0.389, 0.497, 0.691, 0.788, 0.849, 0.894, 0.923, 0.943]
    ref_maxs = [0.506, 0.582, 0.776, 0.818, 0.867, 0.898, 0.927, 0.946]
    curr_means = np.mean(np.stack(allvid_mean_accs, axis=0), axis=0)
    curr_maxs = np.mean(np.stack(allvid_max_accs, axis=0), axis=0)
    plt_labels = ['1', '2', '5', '10', '20', '50', '200', '500']*2
    ref_concat = np.array(ref_means + ref_maxs)
    curr_concat = np.concatenate([curr_means, curr_maxs])

    plt_x = np.arange(len(ref_concat))  # the label locations
    width = 0.35  # the width of the bars

    fig, ax = plt.subplots()
    plt_rects1 = ax.bar(plt_x - width/2, ref_concat, width, label='Ref')
    plt_rects2 = ax.bar(plt_x + width/2, curr_concat, width, label='Curr')

    ax.set_ylabel('mean IoU')
    ax.set_xticks(plt_x)
    ax.set_xticklabels(plt_labels)
    ax.legend()
    fig.tight_layout()
    plt.savefig('results.png')
    '''
    
    # DESTRUCTORS
    lab_model.destroy()
