
#
# Configuration module. RLseg2 implementation #1
#
#   @author Viktor Varga
#

# getting all fields of this module: {var: Config.__dict__[var] for var in dir(Config) if not var.startswith("__")}

# vi USAGE:
#    command mode: i - insert mode, u - undo, x - del char, hjkl - moving, o - nl below, ZZ - save and quit, :w<ENTER> - save, :q!<ENTER> - quit wo. saving
#    insert mode: esc - command mode

# main RUNNER

FOLDER_RENDER_PREDICTIONS = None   # '/home/vavsaai/git/rlseg2/temp_results/rendered_preds/'  # None to disable

FIRST_N_FEATURES_TO_USE = None   # int or None (fvec sizes: 16+24+32+96+320, total: 488)

LABEL_MODEL_METHOD = 'logreg'          # any of ['logreg', 'leo']
LABEL_ESTIMATION_ALGORITHM = 'gnn_bin'   # any of ['basic', 'aug', 'mrf', 'gnn_bin']

# main RUNNER: benchmark task selection

BENCHMARK_TASK_SELECTION_FOLDER = '/home/vavsaai/databases/DAVIS/rlseg2_data/benchmark/'
BENCHMARK_TASK_SELECTION_FNAME_NOEXT = 'benchmark_test15_17aug2020_rep10_slich' # 'benchmark_train30_22jul2020' # 'benchmark_test15_9jul2020_ccs' # 'benchmark_bear_boat_soapbox_2rep' # 'benchmark_test15_16aug2020_slich'  # 'benchmark_test15_17aug2020_rep10_slich'
BENCHMARK_TASK_SELECTION_N_REPEATS = 10    # ignored if loading a previously generated benchmark


# GNN Binary, training/validation data generation

DATAGENTR_N_NODES_REVEALED_PER_CAT_MIN = 100.   # n_nodes (per category) revealed in training/validation samples:
DATAGENTR_N_NODES_REVEALED_PER_CAT_MAX = 100.  #     n_nodes_revealed = ..._expbase ** uniform_rnd(..._min, ..._max)
DATAGENTR_N_NODES_REVEALED_PER_CAT_EXPBASE = 2.
DATAGENTR_SIMULATE_LABEL_MODEL = False      # replace label model prediction node feature with noisy ground truth labels
DATAGENTR_DISABLE_LABEL_MODEL = False   # replace label model prediction node feature with uniform random noise
DATAGENTR_SIMULATE_PRED_ALPHA = 1.5    # param of beta distribution, orig: 1.5
DATAGENTR_SIMULATE_PRED_BETA = 10.     # param of beta distribution, orig: 10

# GNN Binary LabelEstimation

GNNBIN_PRETRAIN = False     # if False, loads checkpoint
GNNBIN_CHECKPOINT_DIR = '/home/vavsaai/git/rlseg2/temp_results/gated_gcn_binary_pretrain_checkpoints/checkpoint/'
GNNBIN_CHECKPOINT_NAME_TO_LOAD = 'run36_ep160.pkl'
GNNBIN_BATCH_SIZE = 1
GNNBIN_N_WORKERS = 0        # set 0 to disable multiprocessing; set >=1 to enable multiprocessing and specify n. of worker processes

# GNN hyperparameters:

GNNBIN_NATIVE_MULTICLASS = True
GNNBIN_MULTICLASS_LOSS = 'miou'   # in ['ce', 'weighted_ce', 'miou', 'mse', 'lovasz']
GNNBIN_MULTICLASS_W_MLP = False     # if False, w_i, w_j values are produced by a single linear layer, if True, a 2-layer MLP is used
GNNBIN_SHARED_PARAMS = False
GNNBIN_NO_EDGE_REPR = False
GNNBIN_HIDDEN_DIM = 20
GNNBIN_N_LAYERS = 12
GNNBIN_DROPOUT = .1
GNNBIN_INFEAT_DROPOUT = .0
GNNBIN_READOUT = "mean"
GNNBIN_GRAPH_NORM = True
GNNBIN_BATCH_NORM = True
GNNBIN_RESIDUAL = True
GNNBIN_EDGE_DROPOUT = False
GNNBIN_EDGE_DROPOUT_MIN_DEG = 2
GNNBIN_EDGE_DROPOUT_RATIO = 0.25

GNNBIN_N_EPOCHS = 161
GNNBIN_N_EPOCHS_CHECKPOINT_FREQ = 20
GNNBIN_INIT_LR = 5e-3
GNNBIN_MIN_LR = 1e-5
GNNBIN_WEIGHT_DECAY = 0.0
GNNBIN_LR_REDUCE_FACTOR = 0.6
GNNBIN_LR_SCHEDULE_PATIENCE = 20
GNNBIN_TR_EPOCH_SIZE = 32  # 32
GNNBIN_VAL_EPOCH_SIZE = 8  # 8

