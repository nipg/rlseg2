
# 
# RLseg2 implementation #1, utils
#
# @author Viktor Varga
#

import numpy as np
import cv2
import skimage.transform

import sys
sys.path.append('..')

import util.imutil as ImUtil
import util.util as Util

# Dynamic loading for big, (possibly many-channel) images

class DynamicFeatureMapLoader:
    '''
    Class mimics a 4D ndarray of full resolution multi channel images. Shape: (n_frames, size_y, size_x, n_ch)
    Each full frame is stored as a set of downscaled images which are resized on demand
             and concatenated along the channel axis.
    Only one image (with all channels) can be queried at once.

    Member fields:
        ds_ims: dict{str - layer_name: ndarray(n_imgs, ?sy, ?sx, ?n_ch) of float32}
        shape: tuple(4) of ints; the target shape
        ndim: int; length of self.shape
        dtype: ndarray.dtype; the target data type

        smooth_resize: bool
    '''

    def __init__(self, ds_ims, target_size_yx, smooth_resize=False):
        assert len(target_size_yx) == 2

        # calculate target shape
        assert all([im.ndim == 4 for im in list(ds_ims.values())])
        n_ch = sum([im.shape[3] for im in list(ds_ims.values())])
        n_fr_set = set([im.shape[0] for im in list(ds_ims.values())])
        assert len(n_fr_set) == 1, "Frame count of images must match."
        n_frames = list(n_fr_set)[0]
        self.shape = (n_frames,) + target_size_yx + (n_ch,)
        self.ndim = len(self.shape)

        # set target dtype
        dtype_set = set([im.dtype for im in list(ds_ims.values())])
        assert len(dtype_set) == 1, "Datatype of images must match."
        self.dtype = list(dtype_set)[0]

        self.ds_ims = ds_ims
        self.smooth_resize = smooth_resize

    def __getitem__(self, idxs):
        if type(idxs) is int:
            idxs = (idxs,)
        assert len(idxs) <= 4
        fr_idx = idxs[0]
        assert type(fr_idx) is int, "No slicing is supported along axis#0."
        resize_order = 1 if self.smooth_resize else 0
        resized_im = [ImUtil.fast_resize_nearest(self.ds_ims[ln][fr_idx], self.shape[1:3]) for ln in self.ds_ims.keys()]
        resized_im = np.concatenate(resized_im, axis=-1)   # (size_y, size_x, n_ch)
        return resized_im[idxs[1:]]

    def __len__(self):
        return self.shape[0]

    def astype(self, dtype):
        self.dtype = dtype
        return self

# end of DynamicFeatureMapLoader class

def create_h_lvl_sv_seg(seg_lvl0, to_level, hierarchy):
    '''
    Creates level h SV segmentation by merging level 0 SVs following the 'hierarchy' array.
    Parameters:
        seg_lvl0: ndarray(n_frames, size_y, size_x) of uint32
        to_level: int; the target level (or h)
        hierarchy: ndarray(n_total_hier_segs,) of uint32; for each node, the ID of its parent.
                            IDs of level#i should be in range [sum_{j=0..i-1}(n_seg_lvl_j), sum_{j=0..i}(n_seg_lvl_j))
    Returns:
        seg_lvl_h: ndarray(n_frames, size_y, size_x) of uint32; IDs are left as they are in the 'hierarchy' array
    '''
    assert to_level > 0
    n_segs_lvl0 = np.amin(hierarchy)
                    # smallest parent is the ID of the smallest lvl1 seg == number of lvl0 segs (starting from ID 0)
    curr_seg_ids = np.arange(n_segs_lvl0)
    p_seg_ids = hierarchy[curr_seg_ids]
    from_seg_id = curr_seg_ids

    for lvl_idx in range(1,to_level):
        curr_seg_ids = p_seg_ids
        p_seg_ids = hierarchy[curr_seg_ids]

    assert Util.is_sorted(from_seg_id)
    seg_lvl_h = p_seg_ids[seg_lvl0].astype(np.uint32)
    return seg_lvl_h

def merge_iterators_random_select(gens):
    '''
    Generator.
    Merges multiple iterators by drawing from a randomly selected one on each query.
    Can handle finite iterators.
    '''
    yielding = list(range(len(gens)))   # idx of iterators that are not exhausted
    while len(yielding) > 0:
        gen_idx = np.random.choice(yielding)
        try:
            sample = next(gens[gen_idx])
        except StopIteration:
            gens.remove(gen_idx)
            continue
        yield sample

def get_graph_route_lengths(edges, source_nodes, directed=True, n_nodes=None):
    '''
    Computes minimum distance from each node to the closest source node.
    Returns distance frequencies of all nodes.
    Parameters:
        edges: ndarray(n_edges, 2:[node_from, node_to]) of int
        source_nodes(n_source_nodes,) of int
        directed: bool; if False, the reverse of edges are also considered
        n_nodes: None OR int; if not specified, number of nodes is inferred from other input arrays using max ID available
    Returns:
        route_lens: ndarray(n_nodes,) of int; idx#i designates the length of the shortest path from node#i to any source_node
    '''
    assert edges.shape[1:] == (2,)
    assert np.all(edges[:,0] != edges[:,1])
    assert np.all(edges >= 0)
    if not directed:
        edges = np.concatenate([edges, edges[:,::-1]], axis=0)
    if n_nodes is None:
        n_nodes = max(np.amax(edges)+1, np.amax(source_nodes)+1)

    # init
    dists = np.full((n_nodes,), dtype=np.float32, fill_value=np.inf)
    #edges_unused = np.ones(edges.shape[:1], dtype=np.bool_)
    edges_rem = edges
    active_nodes = np.unique(source_nodes)
    curr_dist = 0
    dists[active_nodes] = curr_dist

    # iterate
    while True:
        curr_dist += 1
        #print("--- ITER: ", curr_dist)

        #print("erem: ", edges_rem)
        #edges_rem_to_follow_mask = np.isin(edges_rem[:, 0], active_nodes)    # get remaining edges which start from active nodes
        # faster alternative for line above: 'active_nodes' array is sorted
        edges_rem_to_follow_mask = Util.isin_sorted(edges_rem[:, 0], active_nodes, check_sorted=True)
        #print("erem_mask: ", edges_rem_to_follow_mask)

        active_nodes = edges_rem[edges_rem_to_follow_mask, 1]                 # get new active nodes
        #print("active_nodes: ", active_nodes)
        edges_rem = edges_rem[~edges_rem_to_follow_mask,:]                    # remove used edges from 'edges_rem' array
        #print("erem: ", edges_rem)
        #print("dists: ", dists)
        active_nodes = active_nodes[dists[active_nodes] == np.inf]             # remove active nodes that have already been reached
        #print("active_nodes_f: ", active_nodes)

        if active_nodes.shape[0] == 0:
            break

        active_nodes = np.unique(active_nodes)                                # remove duplicates from active nodes
        dists[active_nodes] = curr_dist                                       # set distance for newly found active nodes

    return dists

