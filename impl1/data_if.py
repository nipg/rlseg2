
# 
# RLseg2 implementation #1, data interface class
# * Creates Segmentation objects for each video
# * Saves/loads Segmentation objects into/from cache
#
#   @author Viktor Varga
#

import sys
sys.path.append('..')

from segmentation import Segmentation

import os
import cv2
import h5py
import numpy as np
import pickle
import time

import impl_util as ImplUtil
from impl_util import DynamicFeatureMapLoader
import util.imutil as ImUtil


class DataIF():
    
    '''
    Member fields:
        # RAW DATA DICTS - only temp variables, only a single key is used at once, other items are deleted to save memory
            img_data_bgr, img_Data_lab, gt_annot_raw_data, optflow_fw_raw_data, optflow_bw_raw_data: dict{vidname - str: ?}
            occl_fw_raw_data, occl_bw_raw_data, seg_raw_data, feature_raw_data: dict{vidname - str: ?}

        # PROCESSED DATA
            seg_objs: Segmentation object

    '''
    
    LOAD_IMAGE_CHANNELS = False   # if True, loads BGR image channels
    DATASET_TO_LOAD = 'davis2017'  # 'davis2016', 'davis2017' or 'segtrack1'
    SEG_SOURCE = 'slich'  # 'ccs1024', 'ccs2048', 'slich'
    IMFEATURES_MODELKEY = 'MobileNetV2'

    DOWNSCALE_SIZES_YX = [(4, 7), (16, 28), (32, 56), (64, 112)]

    # paths and videos for each dataset

    if DATASET_TO_LOAD == 'davis2016':
        IM_FOLDER = '/home/vavsaai/databases/DAVIS/DAVIS/JPEGImages/480p/'
        if SEG_SOURCE == 'ccs1024':
            DATA_FOLDER_SEG_H5 = '/home/vavsaai/databases/DAVIS/rl_seg_data/davis_ccs_processed/fn2_1024/'
        elif SEG_SOURCE == 'ccs2048':
            DATA_FOLDER_SEG_H5 = '/home/vavsaai/databases/DAVIS/rl_seg_data/davis_ccs_processed/fn2_2048/'
        elif SEG_SOURCE == 'slich':
            assert False, "Not generated yet."
        DATA_FOLDER_GT_ANNOT = '/home/vavsaai/databases/DAVIS/rl_seg_data/davis_annot480p/'
        DATA_FOLDER_OPTFLOWS = '/home/vavsaai/databases/DAVIS/rl_seg_data/davis_optflows/'
        DATA_FOLDER_IMFEATURES = '/home/vavsaai/databases/DAVIS/rlseg2_data/davis2016_imfeatures/'
        CACHE_FOLDER = '/home/vavsaai/databases/DAVIS/rlseg2_data/cache/'
        ANNOT_FILENAME_PREFIX = 'annot480p_'
        ANNOT_H5_KEY = 'davis_annot_480p'

        # orig DAVIS 2016 split (50 vids: 30 tr (last 10 as validation), 20 test)
        
        # TR_SET_VIDNAMES = ['bear', 'bmx-bumps', 'boat', 'breakdance-flare', 'bus', 'car-turn', 'dance-jump',\
        #                   'dog-agility', 'drift-turn', 'elephant', 'flamingo', 'hike', 'hockey', 'horsejump-low',\
        #                   'kite-walk', 'lucia', 'mallard-fly', 'mallard-water', 'motocross-bumps', 'motorbike']
        # VAL_SET_VIDNAMES = ['paragliding', 'rhino', 'rollerblade', 'scooter-gray', 'soccerball', 'stroller', 'surf',\
        #                   'swing', 'tennis', 'train']
        # TEST_SET_VIDNAMES = ['blackswan', 'bmx-trees', 'breakdance', 'camel', 'car-roundabout', 'car-shadow', 'cows',\
        #                      'dance-twirl', 'dog', 'drift-chicane', 'drift-straight', 'goat', 'horsejump-high',\
        #                      'kite-surf', 'libby', 'motocross-jump', 'paragliding-launch', 'parkour', 'scooter-black',\
        #                      'soapbox']

        TR_SET_VIDNAMES = ['breakdance-flare']
        VAL_SET_VIDNAMES = ['rollerblade']
        TEST_SET_VIDNAMES = ['breakdance']

        # at least 75 frames: 15 tr + 10 test vids
        # TR_SET_VIDNAMES = ['bear', 'bmx-bumps', 'boat', 'bus', 'car-turn',\
        #                    'elephant', 'flamingo', 'hike', 'hockey', \
        #                    'kite-walk', 'mallard-water',\
        #                    'rhino', 'scooter-gray', 'stroller',\
        #                    'train']
        # TEST_SET_VIDNAMES = ['bmx-trees', 'breakdance', 'camel', 'car-roundabout', 'cows',\
        #                      'dance-twirl', 'goat', \
        #                      'paragliding-launch', 'parkour', \
        #                      'soapbox']

    elif DATASET_TO_LOAD == 'segtrack1':
        IM_FOLDER = '/home/vavsaai/databases/SegTrack/SegTrack_201111_resized_ims/segtrack_png_resized/'
        if SEG_SOURCE == 'ccs1024':
            DATA_FOLDER_SEG_H5 = '/home/vavsaai/databases/SegTrack/rl_seg_data/segtrack1_ccs_processed/sv_segtrack1_fn2_1024/'
        elif SEG_SOURCE == 'ccs2048':
            DATA_FOLDER_SEG_H5 = '/home/vavsaai/databases/SegTrack/rl_seg_data/segtrack1_ccs_processed/sv_segtrack1_fn2_2048/'
        elif SEG_SOURCE == 'slich':
            assert False, "Not generated yet."
        DATA_FOLDER_GT_ANNOT = '/home/vavsaai/databases/SegTrack/rl_seg_data/segtrack1_annot/'
        DATA_FOLDER_OPTFLOWS = '/home/vavsaai/databases/SegTrack/rl_seg_data/segtrack1_optflows/'
        DATA_FOLDER_IMFEATURES = '/home/vavsaai/databases/DAVIS/rlseg2_data/segtrack1_imfeatures/'
        CACHE_FOLDER = '/home/vavsaai/databases/SegTrack/rlseg2_data/cache/'
        ANNOT_FILENAME_PREFIX = 'annot_'
        ANNOT_H5_KEY = 'segtrack_annot'

        # orig SegTrack v1: 6 videos (used as test only)
        
        TR_SET_VIDNAMES = ['birdfall2', 'cheetah', 'girl', 'monkeydog', 'parachute', 'penguin']
        VAL_SET_VIDNAMES = ['birdfall2', 'cheetah', 'girl', 'monkeydog', 'parachute', 'penguin']
        TEST_SET_VIDNAMES = ['birdfall2', 'cheetah', 'girl', 'monkeydog', 'parachute', 'penguin']
        # TR_SET_VIDNAMES = ['penguin']
        # VAL_SET_VIDNAMES = ['penguin']
        # TEST_SET_VIDNAMES = ['penguin']

    elif DATASET_TO_LOAD == 'davis2017':
        IM_FOLDER = '/home/vavsaai/databases/DAVIS/DAVIS2017/DAVIS/JPEGImages/480p_unisize/'
        if SEG_SOURCE == 'ccs1024':
            DATA_FOLDER_SEG_H5 = '/home/vavsaai/databases/DAVIS/rl_seg_data/davis2017_ccs_processed/fn2_1024/'
        elif SEG_SOURCE == 'ccs2048':
            DATA_FOLDER_SEG_H5 = '/home/vavsaai/databases/DAVIS/rl_seg_data/davis2017_ccs_processed/fn2_2048/'
        elif SEG_SOURCE == 'slich':
            DATA_FOLDER_SEG_H5 = '/home/vavsaai/databases/DAVIS/rlseg2_data/davis2017_slich/'
        DATA_FOLDER_GT_ANNOT = '/home/vavsaai/databases/DAVIS/rl_seg_data/davis2017_annot480p/'
        DATA_FOLDER_OPTFLOWS = '/home/vavsaai/databases/DAVIS/rl_seg_data/davis2017_optflows/'
        DATA_FOLDER_IMFEATURES = '/home/vavsaai/databases/DAVIS/rlseg2_data/davis2017_imfeatures/'
        CACHE_FOLDER = '/home/vavsaai/databases/DAVIS/rlseg2_data/cache2017/'
        ANNOT_FILENAME_PREFIX = 'annot480p_'
        ANNOT_H5_KEY = 'davis2017_annot_480p'

        # orig DAVIS 2017 split (90 vids: 60 tr (last 15 as validation), 30 test)
        
        # TR_SET_VIDNAMES = ['bear', 'bmx-bumps', 'boat', 'boxing-fisheye', 'breakdance-flare', 'bus', 'car-turn',\
        #                    'cat-girl', 'classic-car', 'color-run', 'crossing', 'dance-jump', 'dancing', 'disc-jockey',\
        #                    'dog-agility', 'dog-gooses', 'dogs-scale', 'drift-turn', 'drone', 'elephant', 'flamingo',\
        #                    'hike', 'hockey', 'horsejump-low', 'kid-football', 'kite-walk', 'koala', 'lady-running',\
        #                    'lindy-hop', 'longboard', 'lucia', 'mallard-fly', 'mallard-water', 'miami-surf',\
        #                    'motocross-bumps', 'motorbike', 'night-race', 'paragliding', 'planes-water', 'rallye',\
        #                    'rhino', 'rollerblade', 'schoolgirls', 'scooter-board', 'scooter-gray']
        # VAL_SET_VIDNAMES = ['sheep',\
        #                    'skate-park', 'snowboard', 'soccerball', 'stroller', 'stunt', 'surf', 'swing', 'tennis',\
        #                    'tractor-sand', 'train', 'tuk-tuk', 'upside-down', 'varanus-cage', 'walking']
        # TEST_SET_VIDNAMES = ['bike-packing', 'blackswan', 'bmx-trees', 'breakdance', 'camel', 'car-roundabout',\
        #                      'car-shadow', 'cows', 'dance-twirl', 'dog', 'dogs-jump', 'drift-chicane', 'drift-straight',\
        #                      'goat', 'gold-fish', 'horsejump-high', 'india', 'judo', 'kite-surf', 'lab-coat', 'libby',\
        #                      'loading', 'mbike-trick', 'motocross-jump', 'paragliding-launch', 'parkour', 'pigs',\
        #                      'scooter-black', 'shooting', 'soapbox']

        # full tr/val set (reduced test set) DAVIS 2017 split (75 vids: 45 tr, 15 val, 15 test)
        
        # TR_SET_VIDNAMES = ['bear', 'bmx-bumps', 'boat', 'boxing-fisheye', 'breakdance-flare', 'bus', 'car-turn',\
        #                    'cat-girl', 'classic-car', 'color-run', 'crossing', 'dance-jump', 'dancing', 'disc-jockey',\
        #                    'dog-agility', 'dog-gooses', 'dogs-scale', 'drift-turn', 'drone', 'elephant', 'flamingo',\
        #                    'hike', 'hockey', 'horsejump-low', 'kid-football', 'kite-walk', 'koala', 'lady-running',\
        #                    'lindy-hop', 'longboard', 'lucia', 'mallard-fly', 'mallard-water', 'miami-surf',\
        #                    'motocross-bumps', 'motorbike', 'night-race', 'paragliding', 'planes-water', 'rallye',\
        #                    'rhino', 'rollerblade', 'schoolgirls', 'scooter-board', 'scooter-gray']
        # VAL_SET_VIDNAMES = ['sheep',\
        #                    'skate-park', 'snowboard', 'soccerball', 'stroller', 'stunt', 'surf', 'swing', 'tennis',\
        #                    'tractor-sand', 'train', 'tuk-tuk', 'upside-down', 'varanus-cage', 'walking']
        # TEST_SET_VIDNAMES = ['bike-packing', 'blackswan', 'bmx-trees', 'breakdance', 'camel', 'car-roundabout',\
        #                      'car-shadow', 'cows', 'dance-twirl', 'dog', 'dogs-jump', 'drift-chicane', 'drift-straight',\
        #                      'goat', 'gold-fish']

        # reduced DAVIS 2017 split (55 vids: 30 tr, 10 val, 15 test)
        
        TR_SET_VIDNAMES = ['bmx-bumps', 'boat', 'boxing-fisheye', 'breakdance-flare', 'bus', 'car-turn',\
                           'cat-girl', 'classic-car', 'color-run', 'crossing', 'dance-jump', 'dancing', 'disc-jockey',\
                           'dog-agility', 'dog-gooses', 'dogs-scale', 'drift-turn', 'drone', 'elephant', 'flamingo',\
                           'hike', 'hockey', 'horsejump-low', 'kid-football', 'kite-walk', 'koala', 'lady-running',\
                           'lindy-hop', 'longboard', 'lucia']
        VAL_SET_VIDNAMES = ['sheep',\
                           'skate-park', 'snowboard', 'soccerball', 'stroller', 'stunt', 'surf', 'swing', 'tennis',\
                           'tractor-sand']
        TEST_SET_VIDNAMES = ['bike-packing', 'blackswan', 'bmx-trees', 'breakdance', 'camel', 'car-roundabout',\
                             'car-shadow', 'cows', 'dance-twirl', 'dog', 'dogs-jump', 'drift-chicane', 'drift-straight',\
                             'goat', 'gold-fish']

        # TR_SET_VIDNAMES = ['boat']
        # VAL_SET_VIDNAMES = ['sheep']
        # TEST_SET_VIDNAMES = ['dance-twirl']  # ['boat','soapbox', 'bear']  # ['dance-twirl']

        # TEST ONLY
        # TEST_SET_VIDNAMES = ['soapbox', 'scooter-black', 'dance-twirl', 'libby', 'drift-chicane', 'parkour', 'horsejump-high',\
        #                    'drift-straight', 'breakdance', 'india',\
        #                    'loading']  # over 40fr
        # TEST_SET_VIDNAMES = ['soapbox']

    if SEG_SOURCE in ['ccs1024', 'ccs2048']:
        SEG_H5_PREFIX = 'ccs'
        SEG_H_LVL_TO_USE = 7   # if modified, the cache must be recreated, 0 by default
    elif SEG_SOURCE == 'slich':
        SEG_H5_PREFIX = 'slich128_4_512'
        SEG_H_LVL_TO_USE = 0


    def __init__(self):

        # raw
        self.img_data_bgr = {}  # only filled if LOAD_IMAGE_CHANNELS is True
        self.img_data_lab = {}
        self.gt_annot_raw_data = {}
        self.optflow_fw_raw_data = {}
        self.optflow_bw_raw_data = {}
        self.occl_fw_raw_data = {}
        self.occl_bw_raw_data = {}
        self.seg_raw_data = {}
        self.feature_raw_data = {}

        # processed
        self.seg_objs = {}

        # assert disjoint training and test set
        assert len(set(DataIF.TR_SET_VIDNAMES) & set(DataIF.VAL_SET_VIDNAMES) & set(DataIF.TEST_SET_VIDNAMES)) == 0

        # create cache folder if it does not exist
        os.makedirs(DataIF.CACHE_FOLDER, exist_ok=True)

        # load data from cache
        vidnames_to_load = set(DataIF.TR_SET_VIDNAMES + DataIF.VAL_SET_VIDNAMES + DataIF.TEST_SET_VIDNAMES)
        print("DataIF: loading " + str(len(vidnames_to_load)) + " videos...")
        for vidname in vidnames_to_load:
            fpath = os.path.join(DataIF.CACHE_FOLDER, \
                            DataIF.DATASET_TO_LOAD + '_' + DataIF.SEG_SOURCE + '_' + vidname + '.pkl')
            if os.path.isfile(fpath):
                # load image data if needed
                if DataIF.LOAD_IMAGE_CHANNELS:
                    dict_imbgr = DataIF.load_img_data([vidname], return_bgr=True, return_lab=False)
                    self.img_data_bgr[vidname] = dict_imbgr[vidname]

                self._load_from_cache(vidname, fpath)
            else:
                print("DataIF: video '", vidname, "' was not found in cache, creating now... timestamp:", time.time())
                DataIF.LOAD_IMAGE_CHANNELS = True
                self._create_cache_file(vidname, fpath)
        print("DataIF: done loading data from cache.")
        #

    def get_train_vids(self):
        return DataIF.TR_SET_VIDNAMES[:]

    def get_validation_vids(self):
        return DataIF.VAL_SET_VIDNAMES[:]

    def get_test_vids(self):
        return DataIF.TEST_SET_VIDNAMES[:]

    def get_vidnames_in_split(self, split_name):
        if split_name == 'train':
            return self.get_train_vids()
        elif split_name == 'test':
            return self.get_test_vids()
        else:
            assert False

    def get_seg_obj(self, vidname):
        return self.seg_objs[vidname]


    def _load_from_cache(self, vidname, cache_fpath):
        '''
        Loads preprocessed data from cache.
        '''
        assert vidname in cache_fpath
        assert os.path.isfile(cache_fpath)
        assert vidname not in self.seg_objs.keys()

        pkl_file = open(cache_fpath, 'rb')
        pkl_dict = pickle.load(pkl_file)
        pkl_file.close()
        pkl_seg_obj = pkl_dict['seg_obj']
        self.seg_objs[vidname] = pkl_seg_obj

    def _create_cache_file(self, vidname, cache_fpath):
        '''
        Preprocecesses raw data and saves it to a cache file per video.
        Sets self.img_data_bgr, self.img_data_lab.
        '''
        assert vidname in cache_fpath
        assert not os.path.isfile(cache_fpath)
        assert vidname not in self.seg_objs.keys()

        if DataIF.SEG_H_LVL_TO_USE > 0:
            print("DataIF: Converting segmentation to level#" + str(DataIF.SEG_H_LVL_TO_USE) + ".")

        # load image data
        print("DataIF: loading image channels...")
        dict_imbgr, dict_imlab = DataIF.load_img_data([vidname], return_bgr=True, return_lab=True)
        self.img_data_bgr[vidname] = dict_imbgr[vidname]
        self.img_data_lab[vidname] = dict_imlab[vidname]
        print("DataIF: Done loading image channels.")

        # load other data and segmentation
        self._load_raw_data(vidname)
        self._init_seg_objs(vidname)

        pkl_file = open(cache_fpath, 'wb')
        pkl_dict = {}
        pkl_dict['seg_obj'] = self.seg_objs[vidname]
        pkl_data = pickle.dump(pkl_dict, pkl_file, protocol=2)
        pkl_file.close()

    def _load_raw_data(self, vidname):
        '''
        Loads raw data from files.
        Sets members:
            gt_annot_raw_data, optflow_fw_raw_data, optflow_bw_raw_data, occl_fw_raw_data, occl_bw_raw_data,
            seg_raw_data
        '''
        assert vidname not in self.gt_annot_raw_data.keys()

        # load GT annots
        davis_annot_h5_path = os.path.join(DataIF.DATA_FOLDER_GT_ANNOT, DataIF.ANNOT_FILENAME_PREFIX + vidname + '.h5')
        h5f = h5py.File(davis_annot_h5_path, 'r')
        self.gt_annot_raw_data[vidname] = h5f[DataIF.ANNOT_H5_KEY][:].astype(np.uint8)
        h5f.close()

        # load optflow and occlusion data
        flows_h5_path = os.path.join(DataIF.DATA_FOLDER_OPTFLOWS, 'flownet2_' + vidname + '.h5')
        h5f = h5py.File(flows_h5_path, 'r')
        self.optflow_fw_raw_data[vidname] = h5f['flows'][:].astype(np.float16)
        self.optflow_bw_raw_data[vidname] = h5f['inv_flows'][:].astype(np.float16)
        self.occl_fw_raw_data[vidname] = h5f['occls'][:].astype(np.bool_)
        self.occl_bw_raw_data[vidname] = h5f['inv_occls'][:].astype(np.bool_)
        h5f.close()

        # load segmentation algorithm outputs: if needed, convert segmentation to higher level
        seg_h5_path = os.path.join(DataIF.DATA_FOLDER_SEG_H5, DataIF.SEG_H5_PREFIX + '_' + vidname + '.h5')
        h5f = h5py.File(seg_h5_path, 'r')
        seg_arr = h5f['lvl0_seg'][:].astype(np.uint32)
        if DataIF.SEG_H_LVL_TO_USE > 0:
            hierarchy = h5f['hierarchy'][:].astype(np.uint32)
            seg_arr = ImplUtil.create_h_lvl_sv_seg(seg_arr, DataIF.SEG_H_LVL_TO_USE, hierarchy)
            orig_shape = seg_arr.shape
            _, seg_arr = np.unique(seg_arr, return_inverse=True)
            seg_arr = seg_arr.reshape(orig_shape)
        self.seg_raw_data[vidname] = seg_arr
        h5f.close()

        # load feature vector heatmap images
        features_pkl_path = os.path.join(DataIF.DATA_FOLDER_IMFEATURES, \
                                                    'features_' + DataIF.IMFEATURES_MODELKEY + '_' + vidname + '.pkl')
        with open(features_pkl_path, 'rb') as f:
            # for each vid a dict: 
            # {'expanded_conv_project_BN': nd(82, 240, 427, 16) of fl16,
            #  'block_2_add': nd(82, 120, 213, 24) of fl16,
            #  'block_5_add': nd(82, 60, 106, 32) of fl16,
            #  'block_12_add': nd(82, 30, 53, 96) of fl16,
            #  'block_16_project_BN': nd(82, 15, 26, 320) of fl16,
            #  'out_relu': nd(82, 15, 26, 1280) of fl16}

            self.feature_raw_data[vidname] = pickle.load(f)
            del self.feature_raw_data[vidname]['out_relu']   # dropping 'out_relu'


    def _init_seg_objs(self, vidname):
        '''
        Sets self.seg_objs.
        '''
        assert vidname not in self.seg_objs.keys()

        seg_obj = Segmentation(self.seg_raw_data[vidname], DataIF.DOWNSCALE_SIZES_YX)

        print("    SEG INIT DONE", time.time())
        # add image channels to seg obj
        of_fw = np.pad(self.optflow_fw_raw_data[vidname], ((0,1),(0,0),(0,0),(0,0)),\
                                                         mode='constant', constant_values=0.)
        of_bw = np.pad(self.optflow_bw_raw_data[vidname], ((1,0),(0,0),(0,0),(0,0)),\
                                                         mode='constant', constant_values=0.)
        occl_fw = np.pad(self.occl_fw_raw_data[vidname], ((0,1),(0,0),(0,0)), mode='constant', constant_values=False)
        occl_bw = np.pad(self.occl_bw_raw_data[vidname], ((1,0),(0,0),(0,0)), mode='constant', constant_values=False)
        seg_obj.add_im("of_fw", of_fw)
        seg_obj.add_im("of_bw", of_bw)
        seg_obj.add_im("occl_fw", occl_fw[..., None])
        seg_obj.add_im("occl_bw", occl_bw[..., None])
        feature_loader = DynamicFeatureMapLoader(self.feature_raw_data[vidname], of_fw.shape[1:3], smooth_resize=False)
        seg_obj.add_im("feature_maps", feature_loader)

        # add GT annot as image channel
        seg_obj.add_im("gt_raw", self.gt_annot_raw_data[vidname][..., None])

        # add image channels if needed (for MRF baseline)
        seg_obj.add_im("im_bgr", self.img_data_bgr[vidname])
        seg_obj.add_im("im_lab", self.img_data_lab[vidname])

        # add border image channels
        border_img = np.zeros(self.img_data_bgr[vidname].shape[:3] + (1,), dtype=np.bool_)
        border_img[:,0,:] = 1
        border_img[:,-1,:] = 1
        border_img[:,:,0] = 1
        border_img[:,:,-1] = 1
        seg_obj.add_im("border", border_img)

        # add image -> SP reduce functions
        
        seg_obj.add_imsp_reduce_funcs("mean_of_fw", "of_fw", ImUtil.imsp_reduce_mean, func_named_args={})
        seg_obj.add_imsp_reduce_funcs("mean_of_bw", "of_bw", ImUtil.imsp_reduce_mean, func_named_args={})
        seg_obj.add_imsp_reduce_funcs("std_of_fw", "of_fw", ImUtil.imsp_reduce_std, func_named_args={})
        seg_obj.add_imsp_reduce_funcs("std_of_bw", "of_bw", ImUtil.imsp_reduce_std, func_named_args={})
        seg_obj.add_imsp_reduce_funcs("mean_occl_fw", "occl_fw", ImUtil.imsp_reduce_mean, func_named_args={})
        seg_obj.add_imsp_reduce_funcs("mean_occl_bw", "occl_bw", ImUtil.imsp_reduce_mean, func_named_args={})
        
        seg_obj.add_imsp_reduce_funcs("gt_rounded", "gt_raw", ImUtil.imsp_reduce_most_frequent_item, func_named_args={})
        seg_obj.add_imsp_reduce_funcs("fvecs", "feature_maps", ImUtil.imsp_reduce_mean, func_named_args={})
        
        seg_obj.add_imsp_reduce_funcs("mean_im_lab", "im_lab", ImUtil.imsp_reduce_mean, func_named_args={})
        seg_obj.add_imsp_reduce_funcs("std_im_lab", "im_lab", ImUtil.imsp_reduce_std, func_named_args={})
        #seg_obj.add_imsp_reduce_funcs("im_hist_lab", "im_lab", ImUtil.imsp_image_histograms, \
        #                                func_named_args={'hist_len_per_ch':20, 'chs_to_keep':[1,2]})

        seg_obj.add_imsp_reduce_funcs("border_sp", "border", ImUtil.imsp_reduce_any, func_named_args={})
        
        
        print("    SEG COMP SPCHAN", time.time())
        # execute (image -> SP) reduce funcs
        seg_obj.compute_seg_chans()

        ds_named_param_smooth = {'interp_smooth': True}
        ds_named_param_nearest = {'interp_smooth': False}
        
        seg_obj.add_im_downscale_funcs("of_fw_ds", "of_fw", ImUtil.imdownscale, func_named_args=ds_named_param_smooth)
        seg_obj.add_im_downscale_funcs("of_bw_ds", "of_bw", ImUtil.imdownscale, func_named_args=ds_named_param_smooth)
        seg_obj.add_im_downscale_funcs("occl_fw_ds", "occl_fw", ImUtil.imdownscale, func_named_args=ds_named_param_smooth)
        seg_obj.add_im_downscale_funcs("occl_bw_ds", "occl_bw", ImUtil.imdownscale, func_named_args=ds_named_param_smooth)
        seg_obj.add_im_downscale_funcs("gt_raw_ds", "gt_raw", ImUtil.imdownscale, func_named_args=ds_named_param_nearest)
        

        print("    SEG COMP DS", time.time())
        # execute image -> downscaled image funcs
        seg_obj.compute_downscaled_im_chans()

        print("    SEG EDGES", time.time())
        # add seg edge channels
        spatial_edges = ImUtil.find_spatial_neighbors(seg_obj.sp_seg)
        temporal_edges = ImUtil.find_temporal_neighbors(seg_obj.sp_seg)
        print("    SEG EDGES2", time.time())
        # symmetric flow edges
        flow_edges_mrf, flow_weights_mrf = ImUtil.find_flow_edges_and_weights(seg_obj.sp_seg,\
                        seg_obj.ims['of_fw'][:-1,:,:,:], seg_obj.ims['of_bw'][1:,:,:,:], seg_obj.sp_sizes)
        # assymetric, directed flow edges
        flow_edges_gnn, edge_features_gnn = ImUtil.get_flow_edge_features(seg_obj.sp_seg,\
                         seg_obj.ims['of_fw'][:-1,:,:,:], seg_obj.ims['of_bw'][1:,:,:,:], sp_sizes=seg_obj.sp_sizes)
        print("    SEG EDGES3", time.time())
        seg_obj.add_sp_edges_unidir('spatial_edges', spatial_edges, np.ones((spatial_edges.shape[0], 1)))
        seg_obj.add_sp_edges_unidir('temporal_edges', temporal_edges, np.ones((temporal_edges.shape[0], 1)))
        seg_obj.add_sp_edges_unidir('flow_edges_mrf', flow_edges_mrf, flow_weights_mrf[:,None])
        seg_obj.add_sp_edges_bidir('flow_edges_gnn', flow_edges_gnn, edge_features_gnn)
        all_edges_gnn = ImUtil.merge_edge_lists_only([spatial_edges, flow_edges_gnn])
        lab_dists_l2 = ImUtil.pairwise_l2dist(seg_obj.sp_chans['mean_im_lab'][all_edges_gnn[:,0],:],\
                                              seg_obj.sp_chans['mean_im_lab'][all_edges_gnn[:,1],:])
        of_fw_mean_diff = ImUtil.pairwise_optflow_diff_features(seg_obj.sp_chans['mean_of_fw'][all_edges_gnn[:,0],:],\
                                              seg_obj.sp_chans['mean_of_fw'][all_edges_gnn[:,1],:])
        of_bw_mean_diff = ImUtil.pairwise_optflow_diff_features(seg_obj.sp_chans['mean_of_bw'][all_edges_gnn[:,0],:],\
                                              seg_obj.sp_chans['mean_of_bw'][all_edges_gnn[:,1],:])
        seg_obj.add_sp_edges_unidir('lab_dist', all_edges_gnn, lab_dists_l2)
        seg_obj.add_sp_edges_unidir('of_fw_diff', all_edges_gnn, of_fw_mean_diff)
        seg_obj.add_sp_edges_unidir('of_bw_diff', all_edges_gnn, of_bw_mean_diff)
        print("    SEG EDGES DONE", time.time())

        #
        self.seg_objs[vidname] = seg_obj

        # delete temp data
        del self.optflow_fw_raw_data[vidname]
        del self.optflow_bw_raw_data[vidname]
        del self.occl_fw_raw_data[vidname]
        del self.occl_bw_raw_data[vidname]
        del self.seg_raw_data[vidname]
        del self.feature_raw_data[vidname]

    @staticmethod
    def load_img_data(vidnames_to_load, return_bgr=True, return_lab=False):
      '''
      Parameters:
          vidnames_to_load: list of str
          return_bgr: bool; whether to return 'img_data_bgr'
          return_lab: bool; whether to return 'img_data_lab'
      Returns:
          (OPTIONAL) img_data_bgr: dict{str - vidname: ndarray(n_imgs, 480, 854, 3:BGR) of uint8}
          (OPTIONAL) img_data_lab: dict{str - vidname: ndarray(n_imgs, 480, 854, 3:LAB) of uint8}
      '''
      assert return_bgr or return_lab
      img_data_bgr = {}
      img_data_lab = {}
      for vidname in vidnames_to_load:
          foldername = os.path.join(DataIF.IM_FOLDER, vidname)
          n_imgs = len(os.listdir(foldername))
          ims_bgr = []
          ims_lab = []
          for im_idx in range(n_imgs):
              fpath = os.path.join(foldername, str(im_idx).zfill(5) + ".jpg")
              assert os.path.isfile(fpath)
              im = cv2.imread(fpath, cv2.IMREAD_COLOR)
              im_bgr = cv2.imread(fpath, cv2.IMREAD_COLOR)
              ims_bgr.append(im_bgr)
              if return_lab:
                  im_lab = cv2.cvtColor(im_bgr, cv2.COLOR_BGR2LAB)
                  ims_lab.append(im_lab)

          if return_bgr:
              ims_bgr = np.stack(ims_bgr, axis=0)
              assert ims_bgr.shape == (n_imgs, 480, 854, 3)
              assert ims_bgr.dtype == np.uint8
              img_data_bgr[vidname] = ims_bgr
          if return_lab:
              ims_lab = np.stack(ims_lab, axis=0)
              assert ims_lab.shape == (n_imgs, 480, 854, 3)
              assert ims_lab.dtype == np.uint8
              img_data_lab[vidname] = ims_lab

      if return_bgr and return_lab:
          return img_data_bgr, img_data_lab
      elif return_bgr:
          return img_data_bgr
      elif return_lab:
          return img_data_lab
