
#
# Main runner script. RLseg2 implementation #1
#
#   @author Viktor Varga
#

import sys
sys.path.append('..')

import os
import numpy as np
import pickle

from data_if import DataIF
from segmentation_labeling import SegLabeling

from label_estimation.basic_label_estimation import BasicLabelEstimation
from label_estimation.augmented_label_estimation import AugmentedLabelEstimation
from label_estimation.mrf_label_estimation import MRFLabelEstimation
from label_estimation.logreg_label_model import LogRegLabelModel
from simple_benchmark_task_selection import SimpleBenchmarkTaskSelection

from annotator import InteractiveAnnotator
import tkinter as tk

import util.viz as Viz

FIRST_N_FEATURES_TO_USE = None   # int or None (fvec sizes: 16+24+32+96+320, total: 488)

LABEL_MODEL_METHOD = 'logreg'          # any of ['logreg', 'leo']
LABEL_ESTIMATION_ALGORITHM = 'mrf'   # any of ['basic', 'aug', 'mrf']

if LABEL_MODEL_METHOD == 'leo':
    from label_estimation.leo_label_model import LEOLabelModel   # only import tensorflow if used


if __name__ == '__main__':

    # LOAD DATA, INIT SegLabeling

    data_if = DataIF()
    segs = {}
    seg_labs = {}
    all_vidnames = set(data_if.get_train_vids() + data_if.get_validation_vids() + data_if.get_test_vids())
    for vidname in all_vidnames:
        segs[vidname] = data_if.get_seg_obj(vidname)
        seg_labs[vidname] = SegLabeling(segs[vidname])

    # LOAD IMAGE DATA
    img_data = DataIF.load_img_data(data_if.get_test_vids())

    print("Loaded videos & data:", str(len(data_if.get_train_vids())), "train,",\
                                   str(len(data_if.get_validation_vids())), "validation,",\
                                   str(len(data_if.get_test_vids())), "test videos.")

    # INIT LabelModel

    assert LABEL_MODEL_METHOD in ['logreg', 'leo']
    if LABEL_MODEL_METHOD == 'logreg':
        lab_model = LogRegLabelModel()
    elif LABEL_MODEL_METHOD == 'leo':
        lab_model = LEOLabelModel()

    #curr_vidname = 'car-shadow'
    #curr_vidname = 'dance-twirl'
    curr_vidname = 'bear'
    curr_seg = segs[curr_vidname]
    curr_seg_lab = seg_labs[curr_vidname]

    # INIT LABEL ESTIMATION

    assert LABEL_ESTIMATION_ALGORITHM in ['basic', 'aug', 'mrf']
    if LABEL_ESTIMATION_ALGORITHM == 'basic':
        lab_est = BasicLabelEstimation(curr_seg, 'fvecs', curr_seg_lab.n_labels, lab_model, FIRST_N_FEATURES_TO_USE)
    elif LABEL_ESTIMATION_ALGORITHM == 'mrf':
        lab_est = MRFLabelEstimation(curr_seg, 'fvecs', curr_seg_lab.n_labels, lab_model, FIRST_N_FEATURES_TO_USE)
    elif LABEL_ESTIMATION_ALGORITHM == 'aug':
        lab_est = AugmentedLabelEstimation(curr_seg, 'fvecs', curr_seg_lab.n_labels, lab_model, FIRST_N_FEATURES_TO_USE)
    curr_seg_lab.set_label_estimation(lab_est)

    # PRETRAIN MODELS

    tr_vids = data_if.get_train_vids()
    val_vids = data_if.get_validation_vids()
    feature_range = slice(None) if FIRST_N_FEATURES_TO_USE is None else slice(None, FIRST_N_FEATURES_TO_USE)
    xss_train = [seg_labs[vidname].seg.sp_chans['fvecs'][..., feature_range] for vidname in tr_vids]
    yss_train = [seg_labs[vidname].get_true_labels() for vidname in tr_vids]
    xss_val = [seg_labs[vidname].seg.sp_chans['fvecs'][..., feature_range] for vidname in val_vids]
    yss_val = [seg_labs[vidname].get_true_labels() for vidname in val_vids]
    n_features = xss_train[0].shape[-1]
    lab_model.reset(n_features=n_features, n_cats=None)
    lab_model.pretrain_mutliple_label_setups(xss_train, yss_train, xss_val, yss_val)

    # INIT Interactive annotator

    assert curr_vidname in data_if.get_test_vids()

    ims_displayed = img_data[curr_vidname]
    root_widget = tk.Tk()
    annotator = InteractiveAnnotator(root_widget, ims_displayed, curr_seg_lab, curr_vidname)
    root_widget.mainloop()    # takes over control of the main thread


    # DESTRUCTORS
    lab_model.destroy()
