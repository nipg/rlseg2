
import numpy as np

import tkinter as tk
from PIL import Image, ImageTk

import util.viz as Viz

class InteractiveAnnotator:

    '''
    Parameters:
        master: Tk; parent GUI item
        canvas: Tk.Canvas class for displaying image layers and text info
        vidname: str

        (BASE IMG ARRAYS, SEG_LABELING)
        bg_ims: ndarray(n_frs, size_y, size_x, n_ch=3 [BGR]) of uint8; original color images
        bg_ims_gray: ndarray(n_frs, size_y, size_x) of uint8; original grayscale images
        seg_lab: SegmentationLabeling instance

        (DISPLAYED PHOTO IMAGES)
        photo_ims_rgb: list(n_frs) of tk.PhotoImage, RBG mode (3 channel uint8); CONSTANT
        photo_ims_gray: list(n_frs) of tk.PhotoImage, L mode (1 channel uint8); CONSTANT
        photo_ims_true_labs: list(n_frs) of tk.PhotoImage, RGB mode (3 channel uint8); true labs + gray img; CONSTANT
        photo_ims_sp_edges: list(n_frs) of tk.PhotoImage, RGBA mode (4 channel uint8); CONSTANT

        photo_ims_pred_labs: list(n_inputs+1) of list(n_frs) of tk.PhotoImage, 
                                                RGB mode (3 channel uint8); pred labs + gray img; MODIFIED ON UPDATE

        (DISPLAY STATE)
        curr_fr_idx: int
        bg_display_mode: str; any of ['bg_rgb', 'bg_gray+true', 'bg_gray+pred']
        fg_boundaries_toggle: if True, the SP boundaries are shown
        infobox_left_corner_toggle: if True, the infobox is shown in the upper left corner,
                                                                     otherwise in the upper right corner

        
        pending_user_clicks: list(n_clicks_pending) of tuple(y, x)
        pred_metrics: dict{'metric_name': list(n_inputs) of float}; initial state (all -1 labels) is not considered.


    '''

    LABEL_COLORS_RGB = np.array([[128,128,128], [255,0,0], [0,255,0], [0,0,255],\
                                                [255,140,0], [0,255,255], [255,0,255],\
                                                [192,0,192]], dtype=np.uint8)
    BOUNDARY_COLOR_RGB = (255,0,255)

    def __init__(self, master, bg_ims, seg_lab, vidname):
        '''
        Parameters:
            master: Tk instance, parent GUI item
            bg_ims: ndarray(n_frs, size_y, size_x, n_ch=3 [BGR]) of uint8
            seg_lab: SegmentationLabeling instance
            vidname: str
        '''
        self.master = master
        master.title("Interactive Annotator Window")
        self.seg_lab = seg_lab
        self.vidname = vidname

        # create constant images
        assert bg_ims.shape[3:] == (3,)
        assert bg_ims.dtype == np.uint8
        self.bg_ims = bg_ims[...,::-1]  # BGR -> RGB
        self.curr_fr_idx = 0
        self.bg_display_mode = 'bg_gray+pred'
        self.fg_boundaries_toggle = False
        self._create_constant_imgs()

        self.photo_ims_pred_labs = []
        self.pending_user_clicks = []

        # create info textbox
        self.infobox_left_corner_toggle = False

        # create canvas
        self.canvas = tk.Canvas(self.master, width=854, height=480)
        self.canvas.pack()

        # bind keypress functions, see Event key details: https://anzeljg.github.io/rin2/book2/2405/docs/tkinter/key-names.html
        self.master.bind("<Left>", self.on_keypress_left)
        self.master.bind("<Right>", self.on_keypress_right)
        self.master.bind("<Prior>", self.on_keypress_pageup)   # PageUp
        self.master.bind("<Next>", self.on_keypress_pagedown)   # PageDown
        self.master.bind("<Home>", self.on_keypress_home)
        self.master.bind("<End>", self.on_keypress_end)
        self.master.bind("<Key-1>", self.on_keypress_1)
        self.master.bind("<Key-2>", self.on_keypress_2)
        self.master.bind("<Key-3>", self.on_keypress_3)
        self.master.bind("<Key-4>", self.on_keypress_4)
        self.master.bind("<Key-9>", self.on_keypress_9)
        self.master.bind("<Key-r>", self.on_keypress_r)
        self.master.bind("<Key-a>", self.on_keypress_a)
        self.master.bind("<Key-d>", self.on_keypress_d)
        self.master.bind("<Key-h>", self.on_keypress_h)
        self.master.bind("<Key-space>", self.on_keypress_space)
        self.master.bind("<BackSpace>", self.on_keypress_backspace)
        self.canvas.bind("<Button-1>", self.on_click_left)  # click only registered in top overlay

        self._reset_user_inputs()
        self._generate_new_prediction_image()

        # draw images
        self._redraw_bg_img()
        self._redraw_fg_edge_img()
        self._redraw_info_textbox()

    def _create_constant_imgs(self):
        '''
        Sets self.bg_ims_gray,
            self.photo_ims_rgb, self.photo_ims_gray, self.photo_ims_true_labs, self.photo_ims_sp_edges.
        '''
        self.bg_ims_gray = np.mean(self.bg_ims, axis=-1).astype(np.uint8)

        self.photo_ims_rgb = []
        self.photo_ims_gray = []
        self.photo_ims_true_labs = []
        self.photo_ims_sp_edges = []

        sp_seg = self.seg_lab.seg.sp_seg
        true_labels = self.seg_lab.get_true_labels()

        for fr_idx in range(sp_seg.shape[0]):

            # create colored & grayscale background photo image
            self.photo_ims_rgb.append(ImageTk.PhotoImage(Image.fromarray(self.bg_ims[fr_idx])))
            self.photo_ims_gray.append(ImageTk.PhotoImage(Image.fromarray(self.bg_ims_gray[fr_idx])))

            # create gray bg + true label image
            offset, end_offset = self.seg_lab.seg.get_sp_id_range_in_frame(fr_idx)
            seg_im = sp_seg[fr_idx]-offset
            seg_labels = true_labels[offset:end_offset]
            im_labs = Viz.render_segmentation_labels_RGB(seg_im, seg_labels, InteractiveAnnotator.LABEL_COLORS_RGB, \
                             color_alpha=.5, bg=self.bg_ims_gray[fr_idx], seg_is_gt=None, color_alpha_gt=1.)

            self.photo_ims_true_labs.append(ImageTk.PhotoImage(Image.fromarray(im_labs)))

            # create SP edges overlay
            im_edges = Viz.render_segmentation_edges_BGRA(sp_seg[fr_idx],\
                                             InteractiveAnnotator.BOUNDARY_COLOR_RGB, boundary_alpha=1.)
            im_edges[:,:,3] = 80    # overwriting alpha: PIL.ImageTk.PhotoImage class extremely slow with complicated alpha channels
            self.photo_ims_sp_edges.append(ImageTk.PhotoImage(Image.fromarray(im_edges, 'RGBA')))
        
        #

    def _generate_new_prediction_image(self):
        '''
        Adds rendered images of current state to self.photo_ims_pred_labs.
        '''
        photo_ims_pred = []
        sp_seg = self.seg_lab.seg.sp_seg
        pred_labels = self.seg_lab.pred_labels
        user_labels_mask = self.seg_lab.get_user_input_mask()

        for fr_idx in range(sp_seg.shape[0]):
            offset, end_offset = self.seg_lab.seg.get_sp_id_range_in_frame(fr_idx)
            seg_im = sp_seg[fr_idx]-offset
            seg_labels = pred_labels[offset:end_offset]
            gt_mask = user_labels_mask[offset:end_offset]
            im_labs = Viz.render_segmentation_labels_RGB(seg_im, seg_labels, InteractiveAnnotator.LABEL_COLORS_RGB, \
                             color_alpha=.5, bg=self.bg_ims_gray[fr_idx], seg_is_gt=gt_mask, color_alpha_gt=1.)

            photo_ims_pred.append(ImageTk.PhotoImage(Image.fromarray(im_labs)))

        self.photo_ims_pred_labs.append(photo_ims_pred)

    def _generate_info_text(self):
        '''
        Returns:
            text: str
        '''
        text = []
        text.append("Video name: " + str(self.vidname) + ",   frame# " + str(self.curr_fr_idx))
        text.append("Iter# " + str(len(self.photo_ims_pred_labs)-1) +\
                    "  (redo avail to iter# " + str(len(self.photo_ims_pred_labs)-1) + ")")
        n_annots_per_cat = self.seg_lab.get_n_user_annots_per_category()
        n_annots_per_cat_str = ', '.join([str(item) for item in n_annots_per_cat])
        text.append("User clicks: " + str(sum(n_annots_per_cat)) + " + " + str(len(self.pending_user_clicks)) +\
                    " pending ")
        text.append("      (per cat: " + n_annots_per_cat_str + ")")
        if len(self.photo_ims_pred_labs) <= 1:
            metrics_text = '-'
        else:
            curr_metrics_dict = {mkey: str(self.pred_metrics[mkey][-1])[:5] for mkey in self.pred_metrics.keys()}
            metrics_text = str(curr_metrics_dict)
        text.append("Metrics: " + metrics_text)
        return '\n'.join(text)


    # QUERIES

    def _get_bg_photoimg(self, fr_idx):
        '''
        Parameters:
            fr_idx: int
        Returns:
            ImageTk.PhotoImage: the color or grayscale background image at the specified frame
        '''
        assert 0 <= fr_idx < len(self.photo_ims_rgb)
        assert self.bg_display_mode in ['bg_rgb', 'bg_gray+true', 'bg_gray+pred']
        if self.bg_display_mode == 'bg_rgb':
            return self.photo_ims_rgb[fr_idx]
        elif self.bg_display_mode == 'bg_gray+true':
            return self.photo_ims_true_labs[fr_idx]
        elif self.bg_display_mode == 'bg_gray+pred':
            return self.photo_ims_pred_labs[-1][fr_idx]

    def _get_fg_edgeimg(self, fr_idx):
        '''
        Parameters:
            fr_idx: int
        Returns:
            None OR ImageTk.PhotoImage: the RGBA label overlay image at the specified frame (None if no overlay is shown)
        '''
        assert 0 <= fr_idx < len(self.photo_ims_true_labs)
        if self.fg_boundaries_toggle:
            return self.photo_ims_sp_edges[fr_idx]
        else:
            return None


    # CONTROL

    def _reset_user_inputs(self):
        self.pending_user_clicks.clear()
        self.photo_ims_pred_labs.clear()
        self.pred_metrics = {'sp_accuracy': [], 'mean_j_sp':[]}
        self.seg_lab.reset()

    def _undo(self):
        # TODO
        pass

    def _redo(self):
        # TODO
        pass

    def _redraw_bg_img(self):
        '''
        Redraws background image. Deletes drawn user click circles.
        '''
        if len(self.canvas.find_withtag('click')) > 0:
            self.canvas.delete('click')
        self.canvas.delete('bg')
        self.canvas.create_image((0,0), image=self._get_bg_photoimg(self.curr_fr_idx), anchor='nw', tags=('bg',))
        self.canvas.tag_lower('bg')

    def _redraw_fg_edge_img(self):
        '''
        Redraws SP edge overlay.
        '''
        if len(self.canvas.find_withtag('fg-edge')) > 0:
            self.canvas.delete('fg-edge')
        edge_im = self._get_fg_edgeimg(self.curr_fr_idx)
        if edge_im is None:
            return
        self.canvas.create_image((0,0), image=edge_im, anchor='nw', tags=('fg-edge',))
        self.canvas.tag_raise('fg-edge')
        # raise infobox Z level
        self.canvas.tag_raise('infobox')

    def _redraw_info_textbox(self):
        '''
        Redraws info textbox.
        '''
        self.canvas.delete('infobox')
        if self.infobox_left_corner_toggle:
            infobox_pos = (20,20)
        else:
            infobox_pos = (420,20)
        info_text = self._generate_info_text()
        FONT = ("Courier", 12, 'bold')
        self.canvas.create_text(infobox_pos, text=info_text, fill='#00FF00', anchor='nw', \
                                width=400, font=FONT, tags=('infobox',))

    def _redraw_pending_clicks(self):
        n_clicks_drawn = len(self.canvas.find_withtag('click'))

        # delete drawn clicks if pending clicks list was cleared
        if n_clicks_drawn > len(self.pending_user_clicks):
            self.canvas.delete('click')
            n_clicks_drawn = 0
        elif n_clicks_drawn == len(self.pending_user_clicks):
            # do nothing if number of pending clicks matches with drawn
            return

        # draw reamining clicks if more clicks are pending then the number of drawn clicks
        n_clicks_to_draw = len(self.pending_user_clicks) - n_clicks_drawn
        RADIUS = 5
        for click_revidx in range(-n_clicks_to_draw, 0):
            pos_y, pos_x = self.pending_user_clicks[click_revidx]
            self.canvas.create_oval(pos_x-RADIUS, pos_y-RADIUS, pos_x+RADIUS, pos_y+RADIUS, \
                                        fill='magenta', tags=('click',))

        # raise infobox Z level
        self.canvas.tag_raise('infobox')



    # ON KEY EVENTS

    def on_keypress_left(self, event):
        if self.curr_fr_idx > 0:
            self.pending_user_clicks.clear()
        self.curr_fr_idx = max(self.curr_fr_idx-1, 0)
        self._redraw_bg_img()
        self._redraw_fg_edge_img()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_right(self, event):
        if self.curr_fr_idx < len(self.photo_ims_rgb)-1:
            self.pending_user_clicks.clear()
        self.curr_fr_idx = min(self.curr_fr_idx+1, len(self.photo_ims_rgb)-1)
        self._redraw_bg_img()
        self._redraw_fg_edge_img()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_pageup(self, event):
        if self.curr_fr_idx > 0:
            self.pending_user_clicks.clear()
        self.curr_fr_idx = max(self.curr_fr_idx-5, 0)
        self._redraw_bg_img()
        self._redraw_fg_edge_img()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_pagedown(self, event):
        if self.curr_fr_idx < len(self.photo_ims_rgb)-1:
            self.pending_user_clicks.clear()
        self.curr_fr_idx = min(self.curr_fr_idx+5, len(self.photo_ims_rgb)-1)
        self._redraw_bg_img()
        self._redraw_fg_edge_img()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_home(self, event):
        if self.curr_fr_idx > 0:
            self.pending_user_clicks.clear()
        self.curr_fr_idx = 0
        self._redraw_bg_img()
        self._redraw_fg_edge_img()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_end(self, event):
        if self.curr_fr_idx < len(self.photo_ims_rgb)-1:
            self.pending_user_clicks.clear()
        self.curr_fr_idx = len(self.photo_ims_rgb)-1
        self._redraw_bg_img()
        self._redraw_fg_edge_img()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_1(self, event):
        self.bg_display_mode = 'bg_rgb'
        self._redraw_bg_img()
        self.canvas.update()

    def on_keypress_2(self, event):
        self.bg_display_mode = 'bg_gray+true'
        self._redraw_bg_img()
        self.canvas.update()

    def on_keypress_3(self, event):
        self.bg_display_mode = 'bg_gray+pred'
        self._redraw_bg_img()
        self.canvas.update()

    def on_keypress_4(self, event):
        self.fg_boundaries_toggle = not self.fg_boundaries_toggle
        self._redraw_fg_edge_img()
        self.canvas.update()

    def on_keypress_9(self, event):
        self.infobox_left_corner_toggle = not self.infobox_left_corner_toggle
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_r(self, event):
        self._reset_user_inputs()
        self._redraw_bg_img()
        self._redraw_fg_edge_img()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_a(self, event):
        print("a")

    def on_keypress_d(self, event):
        print("d")

    def on_keypress_h(self, event):
        print("h")

    def on_keypress_space(self, event):
        ''' 
        Prediction is updated and evaluated, predicted segmentation is rendered and shown.
        '''
        coords = np.array(self.pending_user_clicks)
        assert coords.shape[1:] == (2,)
        print("Sent:", coords)
        sp_ids = self.seg_lab.seg.get_sp_ids_from_coords(self.curr_fr_idx, coords)
        print(sp_ids.shape)
        sp_ids = np.array(sp_ids, dtype=np.int32).reshape(-1)
        self.seg_lab.add_true_user_annot(sp_ids)
        self.seg_lab.update_predictions()
        self.pending_user_clicks.clear()

        # get metrics
        for metric_name in self.pred_metrics.keys():
            metric_val = self.seg_lab.evaluate_predictions(metric_name)
            self.pred_metrics[metric_name].append(metric_val)

        # render new prediction and redraw display
        self._generate_new_prediction_image()
        self._redraw_bg_img()
        self._redraw_info_textbox()
        self.canvas.update()


    def on_keypress_backspace(self, event):
        ''' 
        The 'self.pending_user_clicks' list is cleared: unsubmitted clicks are dropped.
        '''
        self.pending_user_clicks.clear()
        self._redraw_bg_img()
        self._redraw_info_textbox()
        self.canvas.update()


    # ON MOUSE EVENTS

    def on_click_left(self, event):
        '''
        Adds user input to the 'self.pending_user_clicks' list.
        If there were any states in REDO, they are deleted and 'self.undo_idx' is set to the end
            of the user input list.
        '''
        coords = (event.y, event.x)
        self.pending_user_clicks.append(coords)
        self._redraw_pending_clicks()
        self._redraw_info_textbox()
        self.canvas.update()




