
#
# Computes IoU limits for hierarchical SP/SV segmentation: 
#    GT mask is rounded to SP/SV segmentations at each hierarchical level and compared to the original mask (IoU).
# Support for segmentation without hierarchy
# Saves results into text file in seg data folder
#

import sys
sys.path.append('..')
import util.util as Util

import numpy as np
import os
import h5py
import cv2

import itertools

SV_METHOD = 'slich2'
DATASET = 'davis2017'

VERBOSE = False

if DATASET == 'davis':
    if SV_METHOD == 'ccs':
        H5_ARCH_PREFIX = 'ccs_'
        HIERSEG_FOLDER = '/home/vavsaai/databases/DAVIS/rl_seg_data/davis_ccs_processed/fn2_2048/'
    elif SV_METHOD == 'gbh':
        H5_ARCH_PREFIX = 'gbh_'
        HIERSEG_FOLDER = '/home/vavsaai/databases/DAVIS/rl_seg_data/davis_gbh_processed/'
    elif SV_METHOD == 'slic512':
        assert False, "Not available"

    ANNOT_DATA = '/home/vavsaai/databases/DAVIS/rl_seg_data/davis_annot480p/'
    ANNOT_H5_PREFIX = 'annot480p_'
    ANNOT_KEY = 'davis_annot_480p'

elif DATASET == 'segtrack1':
    if SV_METHOD == 'ccs':
        H5_ARCH_PREFIX = 'ccs_'
        HIERSEG_FOLDER = '/home/vavsaai/databases/SegTrack/rl_seg_data/segtrack1_ccs_processed/sv_segtrack1_fn2_4096/'
    elif SV_METHOD == 'gbh':
        H5_ARCH_PREFIX = 'gbh_'
        HIERSEG_FOLDER = '/home/vavsaai/databases/SegTrack/rl_seg_data/segtrack1_gbh_processed/'
    elif SV_METHOD == 'slic512':
        assert False, "Not available"

    ANNOT_DATA = '/home/vavsaai/databases/SegTrack/rl_seg_data/segtrack1_annot/'
    ANNOT_H5_PREFIX = 'annot_'
    ANNOT_KEY = 'segtrack_annot'

elif DATASET == 'davis2017':
    if SV_METHOD == 'ccs':
        H5_ARCH_PREFIX = 'ccs_'
        HIERSEG_FOLDER = '/home/vavsaai/databases/DAVIS/rl_seg_data/davis2017_ccs_processed/fn2_2048/'
    elif SV_METHOD == 'gbh':
        assert False, "Not available"
    elif SV_METHOD == 'slic512':
        H5_ARCH_PREFIX = 'slic512_'
        HIERSEG_FOLDER = '/home/vavsaai/databases/DAVIS/rlseg2_data/davis2017_slic/'
    elif SV_METHOD == 'slich':
        #H5_ARCH_PREFIX = 'slich128_4_512_'
        H5_ARCH_PREFIX = 'slich256_4_1024_'
        #HIERSEG_FOLDER = '/home/vavsaai/databases/DAVIS/rlseg2_data/davis2017_slich/'
        HIERSEG_FOLDER = '/home/vavsaai/databases/DAVIS/rlseg2_data/davis2017_slich_256_4_1024_max2/'
    elif SV_METHOD == 'slich2':
        H5_ARCH_PREFIX = 'slich2_'
        #HIERSEG_FOLDER = '/home/vavsaai/databases/DAVIS/rlseg2_data/davis2017_slich2_128_4_512_max2/'
        #HIERSEG_FOLDER = '/home/vavsaai/databases/DAVIS/rlseg2_data/davis2017_slich2_256_4_1024_max2/'
        #HIERSEG_FOLDER = '/home/vavsaai/databases/DAVIS/rlseg2_data/davis2017_slichgt_256_4_1024_max2/'
        #HIERSEG_FOLDER = '/home/vavsaai/databases/DAVIS/rlseg2_data/davis2017_slichgt_256_4_800_max2_zero/'
        HIERSEG_FOLDER = '/home/vavsaai/databases/DAVIS/rlseg2_data/davis2017_slichgt_256_4_800_max3_zero/'


    ANNOT_DATA = '/home/vavsaai/databases/DAVIS/rl_seg_data/davis2017_annot480p/'
    ANNOT_H5_PREFIX = 'annot480p_'
    ANNOT_KEY = 'davis2017_annot_480p'

def getVideoList():
    '''
    Returns:
        list of str;
    '''
    EXT = '.h5'
    assert os.path.isdir(HIERSEG_FOLDER)
    filelist = os.listdir(HIERSEG_FOLDER)
    filelist = filter(lambda fname: fname.startswith(H5_ARCH_PREFIX) and fname.endswith(EXT), filelist)
    filelist = [fname[len(H5_ARCH_PREFIX):-len(EXT)] for fname in filelist]
    return filelist

def measure(seg, annot, n_labels):
    '''
    Parameters:
        seg: ndarray(?) of uint?
        annot: ndarray(?) of uint8
        n_labels: int
    Returns:
        iou: float or None if mask is all False
    '''
    assert seg.shape == annot.shape
    assert n_labels >= 2
    ious = []
    for lab in range(1, n_labels):
        seg_u, seg_c = np.unique(seg, return_counts=True)
        mask = annot == lab
        mseg = seg[mask]
        mseg_u, mseg_c = np.unique(mseg, return_counts=True)
        assert np.all(np.isin(mseg_u, seg_u))
        mseg_idxs_in_seg = np.searchsorted(seg_u, mseg_u)

        sizes = seg_c.astype(np.float32)   # (n_segs,)
        msizes = np.zeros_like(sizes)      # (n_segs,)
        msizes[mseg_idxs_in_seg] = mseg_c
        seg_in_mask = (msizes > 0.) & (msizes > sizes*0.5)  # masking (n_segs,)
        seg_ids_in_mask = seg_u[seg_in_mask]

        rounded_mask = np.isin(seg, seg_ids_in_mask)
        intersection = float(np.count_nonzero(rounded_mask & mask))
        union = float(np.count_nonzero(rounded_mask | mask))
        iou = intersection/union if union > 0 else 1.
        ious.append(iou)

    return np.mean(ious)


if __name__ == '__main__':
    
    vidnames = getVideoList()

    # TEMP
    #vidnames = ['paragliding-launch']

    sv_ious = {}
    sp_ious = {}
    n_h_svs = {}
    for vidname in vidnames:

        print(vidname)

        # load segmentation & hierarchy
        seg_h5_path = os.path.join(HIERSEG_FOLDER, H5_ARCH_PREFIX + vidname + '.h5')
        h5f = h5py.File(seg_h5_path, 'r')
        seg_lvl0 = h5f['lvl0_seg'][:].astype(np.uint32)
        hier_arr = h5f['hierarchy'][:].astype(np.uint32) if 'hierarchy' in h5f.keys() else None
        h5f.close()

        # load DAVIS annot masks
        annot_h5_path = os.path.join(ANNOT_DATA, ANNOT_H5_PREFIX + vidname + '.h5')
        h5f = h5py.File(annot_h5_path, 'r')
        annot = h5f[ANNOT_KEY][:].astype(np.uint8)
        annot_u = np.unique(annot)
        n_labels = len(annot_u)
        assert n_labels == np.amax(annot)+1
        h5f.close()

        assert seg_lvl0.shape == annot.shape

        h_lvl = 0
        if hier_arr is None:
            n_lvl0_svs = np.amax(seg_lvl0)+1
        else:
            n_lvl0_svs = np.amin(hier_arr)
        n_h_svs[vidname] = [n_lvl0_svs]
        lvl0_idxs = np.arange(n_lvl0_svs, dtype=seg_lvl0.dtype)
        p_arr = lvl0_idxs
        h_seg = seg_lvl0

        sv_ious_vid = {}
        sp_ious_vid = {}
        sv_ious[vidname] = sv_ious_vid
        sp_ious[vidname] = sp_ious_vid
        while True:
            print("    h_lvl: " + str(h_lvl))

            # compute SV iou
            sv_iou = measure(h_seg, annot, n_labels)
            if VERBOSE:
                print("sv iou: " + str(sv_iou))
            sv_ious_vid[h_lvl] = sv_iou

            # compute SP ious
            sp_ious_vid[h_lvl] = []
            for fr_idx in range(h_seg.shape[0]):
                sp_iou = measure(h_seg[fr_idx], annot[fr_idx], n_labels)
                sp_ious_vid[h_lvl].append(sp_iou)

            if VERBOSE:
                print("sp ious: " + str(sp_ious_vid[h_lvl]))

            # step to next hierarchic level
            if (hier_arr is None) or (p_arr[0] >= hier_arr.shape[0]):
                break
            h_lvl += 1
            p_arr = hier_arr[p_arr]
            h_seg = Util.replace(seg_lvl0, lvl0_idxs, p_arr)
            n_h_svs[vidname].append(np.unique(p_arr).shape[0])

    # write mean ious to file
    max_h_lvl = h_lvl
    result_fpath = os.path.join(HIERSEG_FOLDER, 'iou_limits.txt')
    result_f = open(result_fpath, 'w')
    result_f.write('Hierarchical SP/SV IoU limits to ' + DATASET + ' binary masks:')

    result_f.write('\nMean SVs: ')
    for h_lvl in range(max_h_lvl+1):
        mean_n_h_sv = np.mean([float(n_h_sv_vid[h_lvl]) for n_h_sv_vid in n_h_svs.values()])
        result_f.write('\n    lvl#' + str(h_lvl) + " (n_sv: " + str(mean_n_h_sv) + "): " +\
                       str(np.mean([sv_ious_vid[h_lvl] for sv_ious_vid in sv_ious.values()])))
    result_f.write('\nMean SPs: ')
    for h_lvl in range(max_h_lvl+1):
        mean_n_h_sv = np.mean([float(n_h_sv_vid[h_lvl]) for n_h_sv_vid in n_h_svs.values()])
        result_f.write('\n    lvl#' + str(h_lvl) + " (n_sv: " + str(mean_n_h_sv) + "): " +\
                       str(np.mean(list(itertools.chain(*[sp_ious_vid[h_lvl] for sp_ious_vid in sp_ious.values()])))))

    result_f.write('\n\nSVs for each vid: ')
    for vidname in vidnames:
        result_f.write('\n    ' + vidname + ':')
        for h_lvl in range(max_h_lvl+1):
            result_f.write('\n        lvl#' + str(h_lvl) + " (n_sv: " + str(n_h_svs[vidname][h_lvl]) + "): " +\
                           str(sv_ious[vidname][h_lvl]))
    result_f.write('\n\nSPs for each vid: ')
    for vidname in vidnames:
        result_f.write('\n    ' + vidname + ':')
        for h_lvl in range(max_h_lvl+1):
            result_f.write('\n        lvl#' + str(h_lvl) + " (n_sv: " + str(n_h_svs[vidname][h_lvl]) + "): " +\
                           str(np.mean(sp_ious[vidname][h_lvl])))
    result_f.close()



