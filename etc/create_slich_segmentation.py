
# Hierarchical SLIC
#   iteratively taking segments with highest variance in optflow and splitting them with SLIC to multiple parts
#   SLIC is only used on color images, optflow is only used in determining the next seg to split


import sys
sys.path.append('..')

import numpy as np
import cv2
import os
import h5py
from skimage.segmentation import slic, mark_boundaries
from skimage.measure import regionprops
from queue import PriorityQueue
import util.util as Util
import util.viz as Viz

DATASET = 'davis2017'  # 'davis', 'davis2017', 'segtrack1', 'segtrack2', 'buffaloxiph'

if DATASET == 'davis':
    IMGS_FOLDER = '/home/vavsaai/databases/DAVIS/DAVIS/JPEGImages/480p/'
    OPTFLOW_FOLDER = '/home/vavsaai/databases/DAVIS/rl_seg_data/davis_optflows/'
    OUTPUT_FOLDER = '/home/vavsaai/databases/DAVIS/rl_seg_data/davis_slich/'
    IMSIZE_YX = (480,854)
if DATASET == 'davis2017':
    IMGS_FOLDER = '/home/vavsaai/databases/DAVIS/DAVIS2017/DAVIS/JPEGImages/480p_unisize/'
    OPTFLOW_FOLDER = '/home/vavsaai/databases/DAVIS/rl_seg_data/davis2017_optflows/'
    OUTPUT_FOLDER = '/home/vavsaai/databases/DAVIS/rlseg2_data/davis2017_slich_256_4_1024_max2/'
    IMSIZE_YX = (480,854)
elif DATASET == 'segtrack1':
    IMGS_FOLDER = '/home/vavsaai/databases/SegTrack/SegTrack_201111_resized_ims/segtrack_png_resized/'
    OPTFLOW_FOLDER = '/home/vavsaai/databases/SegTrack/rl_seg_data/segtrack1_optflows/'
    OUTPUT_FOLDER = '/home/vavsaai/databases/SegTrack/rl_seg_data/segtrack1_slich/'
    IMSIZE_YX = (352,414)
elif DATASET == 'segtrack2':
    IMGS_FOLDER = '/home/vavsaai/databases/SegTrack2/SegTrackv2/JPEGImages/'
    OPTFLOW_FOLDER = '/home/vavsaai/databases/SegTrack2/segtrack2_optflows/'
    OUTPUT_FOLDER = '/home/vavsaai/databases/SegTrack2/segtrack2_slich/'
    IMSIZE_YX = (360,640)
elif DATASET == 'buffaloxiph':
    IMGS_FOLDER = '/home/vavsaai/databases/BuffaloXiph/BuffaloXiph/PNGImages/'
    OPTFLOW_FOLDER = '/home/vavsaai/databases/BuffaloXiph/BuffaloXiph/buffaloxiph_optflows/'
    OUTPUT_FOLDER = '/home/vavsaai/databases/BuffaloXiph/BuffaloXiph/buffaloxiph_slich/'
    IMSIZE_YX = (288,352)

N_BASE_SEGMENTS = 256
N_NEW_SEGMENTS_AT_SPLIT = 4   # SLIC sometimes fails to split into 2 segments
N_TARGET_SEGMENTS = 1024

MAX_SEGMENT_LEVEL = 2   # number of times a segment can be split
LEVEL_PRIORITY_MULTIPLIER = 0.6   # should be <= 1

SLIC_COMPACTNESS = 15    # default: 10

CREATE_VIDEO = True

def getVideoList(single_vidname=None):
    '''
    Parameters:
        single_vidname: str or None; if specified the name of a single video must be given; otherwise all videos are loaded
    Returns:
        list of tuple(str - video folder name, list of str - fnames in folder, sorted)
    '''
    if single_vidname is None:
        assert os.path.isdir(IMGS_FOLDER)
        folderlist = os.listdir(IMGS_FOLDER)
        folderlist = list(filter(lambda folder: os.path.isdir(os.path.join(IMGS_FOLDER, folder)), folderlist))
    else:
        folderlist = [single_vidname]
    fnames = [sorted(os.listdir(os.path.join(IMGS_FOLDER, folder))) for folder in folderlist]
    return zip(folderlist, fnames)

def _cart2polar(x, y):
    """
    Elementwise Cartesian2Polar for arrays. x and y should be of the same size.
    Parameters:
        x, y: ndarray(...); cartesian coordinate arrays
    Returns:
        r: ndarray(...); radius
        phi: ndarray(...); angle in radians, -pi..pi
    """
    r = np.sqrt(np.square(x) + np.square(y))
    phi = np.arctan2(y, x)
    return r, phi

def create_flowviz_img(flow, brightness_mul=10.):
    """
    Parameters:
        flow: ndarray(size_y, size_x, 2:[y,x]) of float32; output of optical flow algorithms
        brightness_mul: float;
    Returns:
        flowviz: ndarray(size_y, size_x, 3) of uint8
    """
    assert flow.ndim == 3
    assert flow.shape[2] == 2
    MAX_HUE = 179.
    flowviz = np.empty((flow.shape[0], flow.shape[1], 3), dtype=np.uint32)
    flowviz.fill(255)
    r, phi = _cart2polar(flow[:, :, 1], flow[:, :, 0])
    flowviz[:, :, 0] = ((phi + np.pi) / (2. * np.pi) * MAX_HUE).astype(np.uint32)
    flowviz[:, :, 2] = (r * brightness_mul).astype(np.uint32)
    flowviz[:, :, 1:] = np.clip(flowviz[:, :, 1:], 0, 255)
    flowviz[:, :, 0] = np.clip(flowviz[:, :, 0], 0, int(MAX_HUE))
    flowviz = flowviz.astype(np.uint8)
    flowviz = cv2.cvtColor(flowviz, cv2.COLOR_HSV2BGR)
    return flowviz

def compute_segment_priority(seg_bbox_tlbr, mask_in_bbox, of_fw, of_bw):
    '''
    Computes priority of a segment. Priority is LOWEST for segments which should be split FIRST.
    Parameters:
        seg_bbox_tlbr: tuple(4) of ints: (min_y, min_x, max_y, max_x)
        mask_in_bbox: ndarray(bbox_sy, bbox_sx) of bool_
        of_fw: None OR ndarray(sy, sx, 2:[dy, dx]) of fl32
        of_bw: None OR ndarray(sy, sx, 2:[dy, dx]) of fl32
    Returns:
        priority: float (negative number)
    '''
    assert len(seg_bbox_tlbr) == 4
    assert mask_in_bbox.ndim == 2
    assert of_fw.shape == of_bw.shape
    assert of_fw.shape[2:] == (2,)
    of_fw_vecs = of_fw[seg_bbox_tlbr[0]:seg_bbox_tlbr[2], seg_bbox_tlbr[1]:seg_bbox_tlbr[3], :][mask_in_bbox, :]   # (n_vecs, 2)
    of_bw_vecs = of_bw[seg_bbox_tlbr[0]:seg_bbox_tlbr[2], seg_bbox_tlbr[1]:seg_bbox_tlbr[3], :][mask_in_bbox, :]   # (n_vecs, 2)
    of_vecs = np.stack([of_fw_vecs, of_bw_vecs], axis=0)   # (2, n_vecs, 2)
    of_vecs = Util.to_logscale_bidir(of_vecs, base=2., min_exp=-3., max_exp=8.)   # between 0 and 1
    std_vecs = np.std(of_vecs, axis=1)   # (2, 2)
    priority = -np.mean(std_vecs)
    # print("--------> PR:  ", priority)
    # print("          of_fw_min:  ", np.amin(of_fw_vecs))
    # print("          of_bw_min:  ", np.amin(of_bw_vecs))
    # print("          of_fw_max:  ", np.amax(of_fw_vecs))
    # print("          of_bw_max:  ", np.amax(of_bw_vecs))
    # print("          of_fw_mean: ", np.mean(of_fw_vecs))
    # print("          of_bw_mean: ", np.mean(of_bw_vecs))
    # print("          of_fw_stdy:  ", std_vecs[0,0])
    # print("          of_bw_stdy:  ", std_vecs[1,0])
    # print("          of_fw_stdx:  ", std_vecs[0,1])
    # print("          of_bw_stdx:  ", std_vecs[1,1])
    # print("          priority  :  ", priority)
    return priority

def run_slich_on_frame(im, of_fw, of_bw):
    '''
    Parameters:
        im: ndarray(sy, sx, 3:bgr) of uint8
        of_fw: None OR ndarray(sy, sx, 2:[dy, dx]) of fl32
        of_bw: None OR ndarray(sy, sx, 2:[dy, dx]) of fl32
    Returns:
        seg: ndarray(sy, sx) of i32; segmentation with IDs starting from 0
        end_offset: int; max(segs)+1
    '''
    assert im.dtype == np.uint8
    im_fl32 = cv2.cvtColor(im, cv2.COLOR_BGR2RGB).astype(np.float32)/255.  # bgr -> rgb float [0,1]
    queue = PriorityQueue()
    queue_len = 0
    curr_max_id = 1
    #print("--> FRAME ENTRY")
    
    # create base segmentation, rprops
    rgb_segments = slic(im_fl32, n_segments=N_BASE_SEGMENTS, compactness=SLIC_COMPACTNESS, sigma=0, multichannel=True, start_label=1)
    rprops = regionprops(rgb_segments)   # labels are starting from 1 (start_label was specified in slic)
    #print("    base seg: ", len(rprops))
    for rprop in rprops:
        mask_in_bbox = rprop.image
        bbox_tlbr = rprop.bbox
        priority = compute_segment_priority(bbox_tlbr, mask_in_bbox, of_fw, of_bw)
        queue_item = (priority, (curr_max_id, mask_in_bbox, bbox_tlbr, 0))
        queue.put(queue_item)
        curr_max_id += 1
        queue_len += 1

    # iterate queue: select segment with lowest priority, split it with SLIC and add the result child segments to the queue
    n_iter = 0
    while queue_len < N_TARGET_SEGMENTS:
        priority_old, (_, mask_in_bbox, bbox_tlbr, seg_level) = queue.get(block=False)

        # run SLIC on segment
        split_segs = slic(im_fl32[bbox_tlbr[0]:bbox_tlbr[2], bbox_tlbr[1]:bbox_tlbr[3], :],\
                                     n_segments=N_NEW_SEGMENTS_AT_SPLIT, compactness=SLIC_COMPACTNESS,\
                                     sigma=0, multichannel=True, start_label=1, mask=mask_in_bbox)
        if not np.any(split_segs):
            # if slic did not find any segments (weird error?) then set area with mask as a single segment with ID == 1
            split_segs[mask_in_bbox] = 1
        rprops = regionprops(split_segs)   # labels are starting from 1 (start_label was specified in slic)
        #print("        ext by: ", len(rprops))
        assert len(rprops) >= 1
        for rprop in rprops:
            mask_in_bbox = rprop.image
            bboxin_t, bboxin_l, bboxin_b, bboxin_r = rprop.bbox
            bbox_tlbr2 = (bboxin_t+bbox_tlbr[0], bboxin_l+bbox_tlbr[1], bboxin_b+bbox_tlbr[0], bboxin_r+bbox_tlbr[1])

            if seg_level+1 >= MAX_SEGMENT_LEVEL:
                priority = 0.   # set to maximum priority if finest granularity reached (all valid priorities are negative)
            else:
                priority = compute_segment_priority(bbox_tlbr2, mask_in_bbox, of_fw, of_bw)*(LEVEL_PRIORITY_MULTIPLIER**(seg_level+1))
            #print("    PR: ", priority_old, "->", priority)
            queue_item = (priority, (curr_max_id, mask_in_bbox, bbox_tlbr2, seg_level+1))
            queue.put(queue_item)
            curr_max_id += 1
            queue_len += 1

        n_iter += 1
        if n_iter > 10*N_TARGET_SEGMENTS:
            assert False, "Maximum number of iterations reached, most of the SLIC splittings failed."

    # generate final segmentation with consecutive IDs (starting from 0)
    seg = np.full(im.shape[:2], dtype=np.int32, fill_value=-1)
    for queue_item in queue.queue:
        (_, (id, mask_in_bbox, bbox_tlbr, _)) = queue_item
        seg[bbox_tlbr[0]:bbox_tlbr[2], bbox_tlbr[1]:bbox_tlbr[3]][mask_in_bbox] = id
    assert np.all(seg) >= 0
    u_seg, inv_seg = np.unique(seg, return_inverse=True)
    return inv_seg.reshape(seg.shape), u_seg.shape[0]


def run_slich_on_video(foldername, fnames):
    
    print("Working on folder: " + str(foldername))
    imfolder = os.path.join(IMGS_FOLDER, foldername)

    # load optflow
    flows_h5_path = os.path.join(OPTFLOW_FOLDER, 'flownet2_' + foldername + '.h5')
    h5f = h5py.File(flows_h5_path, 'r')
    optflow_fw = h5f['flows'][:].astype(np.float32)          # (n_frames-1, sy, sx, 2:[dy, dx]) of fl32
    optflow_bw = h5f['inv_flows'][:].astype(np.float32)      # (n_frames-1, sy, sx, 2:[dy, dx]) of fl32
    h5f.close()

    # open videowriter stream
    if CREATE_VIDEO:
        vid_out_path = os.path.join(OUTPUT_FOLDER, 'viz_slich_' + str(foldername) + '.avi')
        vr_fourcc = cv2.VideoWriter_fourcc(*'MJPG')     # use this codec with avi
        vr_fps = 25.  # vid_capture.get(cv2.CAP_PROP_FPS)
        vr_frSize_xy = (IMSIZE_YX[1], IMSIZE_YX[0])
        vid_writer = cv2.VideoWriter(vid_out_path, fourcc=vr_fourcc, fps=vr_fps, frameSize=vr_frSize_xy)
        assert vid_writer.isOpened(), "Unable to open video file for writing: " + vid_out_path

    segs = []
    offset = 0
    for fr_idx, fname in enumerate(fnames):
        im_fpath = os.path.join(IMGS_FOLDER, foldername, fname)
        im = cv2.imread(im_fpath, cv2.IMREAD_COLOR)
        assert im is not None
        assert im.shape == IMSIZE_YX + (3,)

        if fr_idx % 10 == 0:
            print("    at frame#" + str(fr_idx))
            # if fr_idx == 10:
            #     break

        # run SLICh on frame
        of_fw = np.zeros_like(optflow_fw[0]) if fr_idx == len(fnames)-1 else optflow_fw[fr_idx]
        of_bw = np.zeros_like(optflow_bw[0]) if fr_idx == 0 else optflow_bw[fr_idx-1]
        seg, end_offset = run_slich_on_frame(im, of_fw, of_bw)

        if CREATE_VIDEO:
            #im = create_flowviz_img(of_fw)
            Viz.render_segmentation_edges_BGR(im, seg, boundary_color_rgb=(255,255,0))
            vid_writer.write(im)

        segs.append(seg + offset)
        offset += end_offset

    segs = np.stack(segs, axis=0).astype(np.int32)   # (n_ims, sy, sx)

    arch_out_fpath = os.path.join(OUTPUT_FOLDER, 'slich' + str(N_BASE_SEGMENTS) + '_' +\
                 str(N_NEW_SEGMENTS_AT_SPLIT) + '_' + str(N_TARGET_SEGMENTS) + '_' + str(foldername) + '.h5')
    h5f = h5py.File(arch_out_fpath, 'w')
    h5f.create_dataset("lvl0_seg", data=segs, compression='gzip')
    h5f.close()
        
    if CREATE_VIDEO:
        vid_writer.release()

        
if __name__ == '__main__':

    os.makedirs(OUTPUT_FOLDER, exist_ok=True)

    SINGLE_VIDNAME = None       # 'bear', 'color-run'
    vidlist = getVideoList(SINGLE_VIDNAME)    # (folder_name, fnames list)
        
    for foldername, fnames in vidlist:
        run_slich_on_video(foldername, fnames)
        