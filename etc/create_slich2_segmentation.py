
# Hierarchical SLIC v2
#   splitting segments with highest optflow laplacian intensities
#   SLIC is only used on color images, optflow is only used in determining the segs to split


import sys
sys.path.append('..')

import numpy as np
import cv2
import os
import h5py
from skimage.segmentation import slic, mark_boundaries
from skimage.measure import regionprops
import util.util as Util
import util.viz as Viz


N_BASE_SEGMENTS = 256
N_NEW_SEGMENTS_AT_SPLIT = 4   # SLIC sometimes fails to split into 2 segments
N_TARGET_SEGMENTS = 1024

N_SPLITS_MAX = 2   # number of times a segment can be split

SPLIT_LOWER_THRESHOLD = [0.75, 0.9]     # the lower intensity thresholds for splitting the segments at each split level
SPLIT_MAX_SEG_SPLIT_RATIO = [0.5, 0.5] # the maximum ratio of new segments split at each split level

SLIC_COMPACTNESS = 15    # default: 10
CREATE_VIDEO = True

DATASET = 'davis2017'  # 'davis', 'davis2017', 'segtrack1', 'segtrack2', 'buffaloxiph'
fname_param_tag = str(N_BASE_SEGMENTS) + '_' + \
                str(N_NEW_SEGMENTS_AT_SPLIT) + '_' + str(N_TARGET_SEGMENTS) + '_max' + str(N_SPLITS_MAX)

if DATASET == 'davis':
    IMGS_FOLDER = '/home/vavsaai/databases/DAVIS/DAVIS/JPEGImages/480p/'
    #OPTFLOW_FOLDER = '/home/vavsaai/databases/DAVIS/rl_seg_data/davis_optflows/'
    #OUTPUT_FOLDER = '/home/vavsaai/databases/DAVIS/rl_seg_data/davis_slich2_' + fname_param_tag + '/'
    IMSIZE_YX = (480,854)
if DATASET == 'davis2017':
    IMGS_FOLDER = '/home/vavsaai/databases/DAVIS/DAVIS2017/DAVIS/JPEGImages/480p_unisize/'
    OPTFLOW_FOLDER = '/home/vavsaai/databases/DAVIS/rlseg2_data/davis2017_optflows_fixedoccl/'
    OUTPUT_FOLDER = '/home/vavsaai/databases/DAVIS/rlseg2_data/davis2017_slich2_' + fname_param_tag + '/'
    IMSIZE_YX = (480,854)
elif DATASET == 'segtrack1':
    IMGS_FOLDER = '/home/vavsaai/databases/SegTrack/SegTrack_201111_resized_ims/segtrack_png_resized/'
    OPTFLOW_FOLDER = '/home/vavsaai/databases/SegTrack/rl_seg_data/segtrack1_optflows/'
    OUTPUT_FOLDER = '/home/vavsaai/databases/SegTrack/rl_seg_data/segtrack1_slich2_' + fname_param_tag + '/'
    IMSIZE_YX = (352,414)
elif DATASET == 'segtrack2':
    IMGS_FOLDER = '/home/vavsaai/databases/SegTrack2/SegTrackv2/JPEGImages/'
    OPTFLOW_FOLDER = '/home/vavsaai/databases/SegTrack2/segtrack2_optflows/'
    OUTPUT_FOLDER = '/home/vavsaai/databases/SegTrack2/segtrack2_slich2_' + fname_param_tag + '/'
    IMSIZE_YX = (360,640)
elif DATASET == 'buffaloxiph':
    IMGS_FOLDER = '/home/vavsaai/databases/BuffaloXiph/BuffaloXiph/PNGImages/'
    OPTFLOW_FOLDER = '/home/vavsaai/databases/BuffaloXiph/BuffaloXiph/buffaloxiph_optflows/'
    OUTPUT_FOLDER = '/home/vavsaai/databases/BuffaloXiph/BuffaloXiph/buffaloxiph_slich2_' + fname_param_tag + '/'
    IMSIZE_YX = (288,352)


def getVideoList(single_vidname=None):
    '''
    Parameters:
        single_vidname: str or None; if specified the name of a single video must be given; otherwise all videos are loaded
    Returns:
        list of tuple(str - video folder name, list of str - fnames in folder, sorted)
    '''
    if single_vidname is None:
        assert os.path.isdir(IMGS_FOLDER)
        folderlist = os.listdir(IMGS_FOLDER)
        folderlist = list(filter(lambda folder: os.path.isdir(os.path.join(IMGS_FOLDER, folder)), folderlist))
    else:
        folderlist = [single_vidname]
    fnames = [sorted(os.listdir(os.path.join(IMGS_FOLDER, folder))) for folder in folderlist]
    return zip(folderlist, fnames)

def create_border_intensity_images(optflow_fw, optflow_bw):
    '''
    Returns a border intensity image sequence with values in [0,255].
    Thickened border areas should have a value of close to 255.
    Parameters:
        optflow_fw, optflow_bw: ndarray(n_frames-1, sy, sx, 2:[dy, dx]) of float32
    Returns:
        border_intensities: ndarray(n_frames, sy, sx) of uint8;
    '''
    # getting laplacian of optical flows, then taking length of laplacian xy vectors
    optflow_lapl_comp = np.zeros((optflow_fw.shape[0]+1,) + optflow_fw.shape[1:-1], dtype=np.float32)
    for fr_idx in range(optflow_fw.shape[0]):
        optflow_fw_lapl = cv2.Laplacian(optflow_fw[fr_idx], ddepth=cv2.CV_32F, ksize=1)
        optflow_bw_lapl = cv2.Laplacian(optflow_bw[fr_idx], ddepth=cv2.CV_32F, ksize=1)
        optflow_lapl_comp[fr_idx] += np.linalg.norm(optflow_fw_lapl, ord=2, axis=-1)
        optflow_lapl_comp[fr_idx+1] += np.linalg.norm(optflow_bw_lapl, ord=2, axis=-1)

    # scaling laplacians to have 90th-percentile at 1., then clipping values outside [0,1],
    #    finally uint8 conversion needed to apply image transforms (e.g. dilate)
    optflow_lapl_comp_p90 = np.percentile(optflow_lapl_comp, 90)
    optflow_lapl_comp /= optflow_lapl_comp_p90
    optflow_lapl_comp = np.clip(optflow_lapl_comp*255., 0., 255.).astype(np.uint8)
    KERNEL_CIRC_RADIUS = 7

    border_intensities = []
    for fr_idx in range(optflow_lapl_comp.shape[0]):
        of_lapl_im = (optflow_lapl_comp[fr_idx])
        of_lapl_im = cv2.GaussianBlur(of_lapl_im, (11,11), 0)
        circ_kernel = np.zeros((2*KERNEL_CIRC_RADIUS+1, 2*KERNEL_CIRC_RADIUS+1), np.uint8)
        cv2.circle(circ_kernel, (KERNEL_CIRC_RADIUS, KERNEL_CIRC_RADIUS), radius=KERNEL_CIRC_RADIUS, color=255, thickness=-1)
        of_lapl_im = cv2.dilate(of_lapl_im, circ_kernel)
        of_lapl_im[:KERNEL_CIRC_RADIUS+5,:] = 0
        of_lapl_im[-(KERNEL_CIRC_RADIUS+5):,:] = 0
        of_lapl_im[:,:KERNEL_CIRC_RADIUS+5] = 0
        of_lapl_im[:,-(KERNEL_CIRC_RADIUS+5):] = 0
        border_intensities.append(of_lapl_im)

    border_intensities = np.stack(border_intensities, axis=0)
    return border_intensities

def run_slich_on_frame(im_rgb_fl32, border_intensity):
    '''
    Returns a segmentation of the frame.
    Parameters:
        im_rgb_fl32: ndarray(sy, sx, 3:rgb) of float32; in [0,1] range
        border_intensity: ndarray(sy, sx) of uint8;
    Returns:
        seg: ndarray(sy, sx) of i32; segmentation with IDs starting from 0
        end_offset: int; max(segs)+1
    '''
    assert im_rgb_fl32.dtype == np.float32
    assert np.amax(im_rgb_fl32) <= 1.
    assert im_rgb_fl32.ndim == 3
    assert im_rgb_fl32.shape == border_intensity.shape + (3,)
    assert N_SPLITS_MAX == len(SPLIT_LOWER_THRESHOLD) == len(SPLIT_MAX_SEG_SPLIT_RATIO)
    
    # run base level slic
    seg = slic(im_rgb_fl32, n_segments=N_BASE_SEGMENTS, compactness=SLIC_COMPACTNESS, sigma=0, multichannel=True, start_label=1)

    # get base level segment rprop data
    rprops = regionprops(seg)   # labels are starting from 1 (start_label was specified in slic)
    seg_data = {}
    new_seg_ids = []
    for rprop in rprops:
        mask_in_bbox = rprop.image
        bbox_tlbr = rprop.bbox
        seg_data[rprop.label] = (bbox_tlbr, mask_in_bbox)
        new_seg_ids.append(rprop.label)
    n_total_segs = [len(seg_data)]

    for split_lvl in range(N_SPLITS_MAX):

        # compute intensities for new segments
        curr_max_id = max(new_seg_ids) if len(new_seg_ids) > 0 else curr_max_id
        seg_intensities = np.empty((len(new_seg_ids), 2), dtype=np.int32)   # (n_segs, 2:[ID, value])
        for new_seg_idx in range(len(new_seg_ids)):
            new_seg_id = new_seg_ids[new_seg_idx]
            bbox_tlbr, mask_in_bbox = seg_data[new_seg_id]
            pixs = border_intensity[bbox_tlbr[0]:bbox_tlbr[2], bbox_tlbr[1]:bbox_tlbr[3]][mask_in_bbox]
            seg_intensities[new_seg_idx, 0] = new_seg_id
            seg_intensities[new_seg_idx, 1] = np.amax(pixs)

        # select segments to split, all conditions below must hold: 
        #   maximum N_TARGET_SEGMENTS total numer of segments EXPECTED after splitting
        #   maximum SPLIT_MAX_SEG_SPLIT_RATIO[split_lvl] ratio of segments selected with highest intensities
        #   split seg intensities must be above SPLIT_LOWER_THRESHOLD[split_lvl] and
        seg_intensities = seg_intensities[seg_intensities[:,1] >= SPLIT_LOWER_THRESHOLD[split_lvl]]
        n_max_segs_split = min(int(SPLIT_MAX_SEG_SPLIT_RATIO[split_lvl]*len(new_seg_ids)), \
                               (N_TARGET_SEGMENTS-len(seg_data))//N_NEW_SEGMENTS_AT_SPLIT)
        if seg_intensities.shape[0] > n_max_segs_split:
            seg_sorter = np.argsort(seg_intensities[:,1])[-n_max_segs_split:]
            seg_intensities = seg_intensities[seg_sorter,:]

        # split selected segments
        new_seg_ids = []
        for seg_id in seg_intensities[:,0]:
            bbox_tlbr, mask_in_bbox = seg_data[seg_id]
            del seg_data[seg_id]

            # run SLIC on segment
            split_segs = slic(im_rgb_fl32[bbox_tlbr[0]:bbox_tlbr[2], bbox_tlbr[1]:bbox_tlbr[3], :],\
                                         n_segments=N_NEW_SEGMENTS_AT_SPLIT, compactness=SLIC_COMPACTNESS,\
                                         sigma=0, multichannel=True, start_label=1, mask=mask_in_bbox)
            if not np.any(split_segs):
                # if slic did not find any segments (weird error?) then set area with mask as a single segment with ID == 1
                split_segs[mask_in_bbox] = 1
            rprops = regionprops(split_segs)   # labels are starting from 1 (start_label was specified in slic)
            assert len(rprops) >= 1
            for rprop in rprops:
                mask_in_bbox2 = rprop.image
                bbox_tlbr2 = (rprop.bbox[0]+bbox_tlbr[0], rprop.bbox[1]+bbox_tlbr[1], rprop.bbox[2]+bbox_tlbr[0], rprop.bbox[3]+bbox_tlbr[1])
                curr_max_id += 1
                assert curr_max_id not in seg_data.keys()
                seg_data[curr_max_id] = (bbox_tlbr2, mask_in_bbox2)
                new_seg_ids.append(curr_max_id)

        n_total_segs.append(len(seg_data))

    # write all segments into final segmentation image
    print("    n_segs in split levels:", n_total_segs)
    seg = np.full(im_rgb_fl32.shape[:2], dtype=np.int32, fill_value=-1)
    for seg_label, (bbox_tlbr, mask_in_bbox) in seg_data.items():
        seg[bbox_tlbr[0]:bbox_tlbr[2], bbox_tlbr[1]:bbox_tlbr[3]][mask_in_bbox] = seg_label
    assert np.all(seg >= 0)   # this was erroneous before (was before: np.all(seg) >= 0 )
    u_seg, inv_seg = np.unique(seg, return_inverse=True)
    return inv_seg.reshape(seg.shape), u_seg.shape[0]

def run_slich_on_video(foldername, fnames):
    
    print("Working on folder: " + str(foldername))
    imfolder = os.path.join(IMGS_FOLDER, foldername)

    # load optflow
    flows_h5_path = os.path.join(OPTFLOW_FOLDER, 'flownet2_' + foldername + '.h5')
    h5f = h5py.File(flows_h5_path, 'r')
    optflow_fw = h5f['flows'][:].astype(np.float32)          # (n_frames-1, sy, sx, 2:[dy, dx]) of fl32
    optflow_bw = h5f['inv_flows'][:].astype(np.float32)      # (n_frames-1, sy, sx, 2:[dy, dx]) of fl32
    border_intensities = create_border_intensity_images(optflow_fw, optflow_bw)   # (n_frames, sy, sx) of ui8
    h5f.close()

    # open videowriter stream
    if CREATE_VIDEO:
        vid_out_path = os.path.join(OUTPUT_FOLDER, 'viz_slich2_' + str(foldername) + '.avi')
        vr_fourcc = cv2.VideoWriter_fourcc(*'MJPG')     # use this codec with avi
        vr_fps = 25.  # vid_capture.get(cv2.CAP_PROP_FPS)
        vr_frSize_xy = (IMSIZE_YX[1], IMSIZE_YX[0])
        vid_writer = cv2.VideoWriter(vid_out_path, fourcc=vr_fourcc, fps=vr_fps, frameSize=vr_frSize_xy)
        assert vid_writer.isOpened(), "Unable to open video file for writing: " + vid_out_path

    segs = []
    offset = 0
    for fr_idx, fname in enumerate(fnames):
        im_fpath = os.path.join(IMGS_FOLDER, foldername, fname)
        im = cv2.imread(im_fpath, cv2.IMREAD_COLOR)
        assert im is not None
        assert im.shape == IMSIZE_YX + (3,)

        if fr_idx % 10 == 0:
            print("    at frame#" + str(fr_idx))
            # if fr_idx == 10:
            #     break

        # run SLICh on frame
        im_rgb_fl32 = cv2.cvtColor(im, cv2.COLOR_BGR2RGB).astype(np.float32)/255.  # bgr -> rgb float [0,1]
        seg, end_offset = run_slich_on_frame(im_rgb_fl32, border_intensities[fr_idx])

        if CREATE_VIDEO:
            Viz.render_segmentation_edges_BGR(im, seg, boundary_color_rgb=(255,255,0))
            vid_writer.write(im)

        segs.append(seg + offset)
        offset += end_offset

    segs = np.stack(segs, axis=0).astype(np.int32)   # (n_ims, sy, sx)

    arch_out_fpath = os.path.join(OUTPUT_FOLDER, 'slich2_' + str(foldername) + '.h5')
    h5f = h5py.File(arch_out_fpath, 'w')
    h5f.create_dataset("lvl0_seg", data=segs, compression='gzip')
    h5f.close()
        
    if CREATE_VIDEO:
        vid_writer.release()

if __name__ == '__main__':

    
    os.makedirs(OUTPUT_FOLDER, exist_ok=True)

    SINGLE_VIDNAME = None       # 'bear', 'color-run'
    vidlist = getVideoList(SINGLE_VIDNAME)    # (folder_name, fnames list)
        
    for foldername, fnames in vidlist:
        run_slich_on_video(foldername, fnames)
    

    # TEMP - developing method with visualizations
    '''
    #vidname = 'schoolgirls'
    #vidname = 'bear'
    #vidname = 'dogs-jump'
    #vidname = 'crossing'
    vidname = 'drift-straight'
    N_HISTOGRAM_BINS = 20

    # load optflow & occl
    flows_h5_path = os.path.join(OPTFLOW_FOLDER, 'flownet2_' + vidname + '.h5')
    h5f = h5py.File(flows_h5_path, 'r')
    optflow_fw = h5f['flows'][:].astype(np.float32)          # (n_frames-1, sy, sx, 2:[dy, dx]) of fl32
    optflow_bw = h5f['inv_flows'][:].astype(np.float32)      # (n_frames-1, sy, sx, 2:[dy, dx]) of fl32
    occl_fw = h5f['occls'][:].astype(np.bool_)          # (n_frames-1, sy, sx) of bool
    occl_bw = h5f['inv_occls'][:].astype(np.bool_)      # (n_frames-1, sy, sx) of bool
    # occl_comp = np.zeros((occl_bw.shape[0]+1,) + occl_bw.shape[1:], dtype=np.bool_)
    # occl_comp[:-1] |= occl_fw
    # occl_comp[1:] |= occl_bw
    optflow_lapl_comp = np.zeros((optflow_fw.shape[0]+1,) + optflow_fw.shape[1:-1], dtype=np.float32)
    h5f.close()

    for fr_idx in range(optflow_fw.shape[0]):
        optflow_fw_lapl = cv2.Laplacian(optflow_fw[fr_idx], ddepth=cv2.CV_32F, ksize=1)
        optflow_bw_lapl = cv2.Laplacian(optflow_bw[fr_idx], ddepth=cv2.CV_32F, ksize=1)
        optflow_lapl_comp[fr_idx] += np.linalg.norm(np.fabs(optflow_fw_lapl), ord=2, axis=-1)
        optflow_lapl_comp[fr_idx+1] += np.linalg.norm(np.fabs(optflow_bw_lapl), ord=2, axis=-1)

    optflow_lapl_comp_p90 = np.percentile(optflow_lapl_comp, 90)
    optflow_lapl_comp /= optflow_lapl_comp_p90
    optflow_lapl_comp = np.clip(optflow_lapl_comp, 0., 1.)

    # create video
    vr_fourcc = cv2.VideoWriter_fourcc(*'MJPG')     # use this codec with avi
    vr_fps = 25.  # vid_capture.get(cv2.CAP_PROP_FPS)
    vr_frSize_xy = (854,480)
    vid_writer = cv2.VideoWriter('of_laplace_' + vidname + '.avi', fourcc=vr_fourcc, fps=vr_fps, frameSize=vr_frSize_xy)
    assert vid_writer.isOpened(), "Unable to open video file for writing: " + vid_out_path

    for fr_idx in range(optflow_fw.shape[0]):
        fname = str(fr_idx).zfill(5) + '.jpg'
        im_fpath = os.path.join(IMGS_FOLDER, vidname, fname)
        im = cv2.imread(im_fpath, cv2.IMREAD_COLOR)
        im_fl32 = cv2.cvtColor(im, cv2.COLOR_BGR2RGB).astype(np.float32)/255.  # bgr -> rgb float [0,1]
        seg = slic(im_fl32, n_segments=256, compactness=SLIC_COMPACTNESS, sigma=0, multichannel=True, start_label=1)

        of_lapl_im = (optflow_lapl_comp[fr_idx]*255.).astype(np.uint8)
        of_lapl_im = cv2.GaussianBlur(of_lapl_im, (11,11), 0)
        CIRC_RADIUS = 7
        circ_kernel = np.zeros((2*CIRC_RADIUS+1, 2*CIRC_RADIUS+1),np.uint8)
        cv2.circle(circ_kernel, (CIRC_RADIUS,CIRC_RADIUS), radius=CIRC_RADIUS, color=255, thickness=-1)
        of_lapl_im = cv2.dilate(of_lapl_im, circ_kernel)
        of_lapl_im[:CIRC_RADIUS+5,:] = 0
        of_lapl_im[-(CIRC_RADIUS+5):,:] = 0
        of_lapl_im[:,:CIRC_RADIUS+5] = 0
        of_lapl_im[:,-(CIRC_RADIUS+5):] = 0
        # threshold
        of_lapl_im[of_lapl_im < 192] = 0

        rprops = regionprops(seg)   # labels are starting from 1 (start_label was specified in slic)
        data = np.empty((len(rprops),), dtype=np.float32)
        for rprop in rprops:
            mask_in_bbox = rprop.image
            bbox_tlbr = rprop.bbox
            #occl_comp_pixs = occl_comp[fr_idx, bbox_tlbr[0]:bbox_tlbr[2], bbox_tlbr[1]:bbox_tlbr[3]][mask_in_bbox]
            #data[rprop.label-1, 0] = np.mean(occl_comp_pixs, dtype=np.float64)
            #data[rprop.label-1, 1] = np.std(occl_comp_pixs, dtype=np.float64)
            pixs = of_lapl_im[bbox_tlbr[0]:bbox_tlbr[2], bbox_tlbr[1]:bbox_tlbr[3]][mask_in_bbox]
            data[rprop.label-1] = np.amax(pixs)

        hist_comp = np.bincount(np.round(data/255.*N_HISTOGRAM_BINS).astype(np.int32))
        print("frame#" + str(fr_idx) + ":", hist_comp)

        #cv2.imwrite('of_lapl' + str(fr_idx) + '.png', of_lapl_im)
        vid_writer.write(np.broadcast_to(of_lapl_im[:,:,None], of_lapl_im.shape + (3,)))

    vid_writer.release()
    '''
