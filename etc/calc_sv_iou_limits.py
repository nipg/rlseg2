
#
# Computes IoU limits for SP segmentation (hierarchy and SVs are not supported in this implementation): 
#    GT mask is rounded to SP segmentation and compared to the original mask (mean-IoU, background ignored).
# Support for segmentation without hierarchy
# Saves results into text file in seg data folder
#

import sys
sys.path.append('..')
import util.util as Util

import numpy as np
import os
import h5py
import cv2

import itertools

SV_METHOD = 'slich_gt100'
DATASET = 'davis2017'

VERBOSE = False

if DATASET == 'davis':
    if SV_METHOD == 'ccs':
        H5_ARCH_PREFIX = 'ccs_'
        SEG_FOLDER = '/home/vavsaai/databases/DAVIS/rl_seg_data/davis_ccs_processed/fn2_2048/'
    elif SV_METHOD == 'gbh':
        H5_ARCH_PREFIX = 'gbh_'
        SEG_FOLDER = '/home/vavsaai/databases/DAVIS/rl_seg_data/davis_gbh_processed/'
    elif SV_METHOD == 'slic512':
        assert False, "Not available"

    ANNOT_DATA = '/home/vavsaai/databases/DAVIS/rl_seg_data/davis_annot480p/'
    ANNOT_H5_PREFIX = 'annot480p_'
    ANNOT_KEY = 'davis_annot_480p'

elif DATASET == 'segtrack1':
    if SV_METHOD == 'ccs':
        H5_ARCH_PREFIX = 'ccs_'
        SEG_FOLDER = '/home/vavsaai/databases/SegTrack/rl_seg_data/segtrack1_ccs_processed/sv_segtrack1_fn2_4096/'
    elif SV_METHOD == 'gbh':
        H5_ARCH_PREFIX = 'gbh_'
        SEG_FOLDER = '/home/vavsaai/databases/SegTrack/rl_seg_data/segtrack1_gbh_processed/'
    elif SV_METHOD == 'slic512':
        assert False, "Not available"

    ANNOT_DATA = '/home/vavsaai/databases/SegTrack/rl_seg_data/segtrack1_annot/'
    ANNOT_H5_PREFIX = 'annot_'
    ANNOT_KEY = 'segtrack_annot'

elif DATASET == 'davis2017':
    if SV_METHOD == 'ccs':
        H5_ARCH_PREFIX = 'ccs_'
        SEG_FOLDER = '/home/vavsaai/databases/DAVIS/rl_seg_data/davis2017_ccs_processed/fn2_2048/'
    elif SV_METHOD == 'gbh':
        assert False, "Not available"
    elif SV_METHOD == 'slic512':
        H5_ARCH_PREFIX = 'slic512_'
        SEG_FOLDER = '/home/vavsaai/databases/DAVIS/rlseg2_data/davis2017_slic/'
    elif SV_METHOD == 'slich':
        #H5_ARCH_PREFIX = 'slich128_4_512_'
        H5_ARCH_PREFIX = 'slich256_4_1024_'
        #SEG_FOLDER = '/home/vavsaai/databases/DAVIS/rlseg2_data/davis2017_slich/'
        SEG_FOLDER = '/home/vavsaai/databases/DAVIS/rlseg2_data/davis2017_slich_256_4_1024_max2/'
    elif SV_METHOD == 'slich2':
        H5_ARCH_PREFIX = 'slich2_'
        #SEG_FOLDER = '/home/vavsaai/databases/DAVIS/rlseg2_data/davis2017_slich2_128_4_512_max2/'
        #SEG_FOLDER = '/home/vavsaai/databases/DAVIS/rlseg2_data/davis2017_slich2_256_4_1024_max2/'
        #SEG_FOLDER = '/home/vavsaai/databases/DAVIS/rlseg2_data/davis2017_slichgt_256_4_1024_max2_thresholds075_09/'
        #SEG_FOLDER = '/home/vavsaai/databases/DAVIS/rlseg2_data/davis2017_slichgt_256_4_1024_max2/'
        #SEG_FOLDER = '/home/vavsaai/databases/DAVIS/rlseg2_data/davis2017_slichgt_256_4_800_max2_zero/'
        #SEG_FOLDER = '/home/vavsaai/databases/DAVIS/rlseg2_data/davis2017_slichgt_256_4_800_max3_zero_thresholds_all05/'
        SEG_FOLDER = '/home/vavsaai/databases/DAVIS/rlseg2_data/davis2017_slichgt_256_4_800_max3_zero/'
    elif SV_METHOD == 'slich3':
        H5_ARCH_PREFIX = 'slich3_'
        SEG_FOLDER = '/home/vavsaai/databases/DAVIS/rlseg2_data/davis2017_slich3_opt1_canny200/'
    elif SV_METHOD == 'slich_gt100':
        H5_ARCH_PREFIX = 'slich_gt100_'
        SEG_FOLDER = '/home/vavsaai/databases/DAVIS/rlseg2_data/davis2017_slich_gt100/'


    ANNOT_DATA = '/home/vavsaai/databases/DAVIS/rl_seg_data/davis2017_annot480p/'
    ANNOT_H5_PREFIX = 'annot480p_'
    ANNOT_KEY = 'davis2017_annot_480p'

    TEST_SET = ['bike-packing', 'blackswan', 'bmx-trees', 'breakdance', 'camel', 'car-roundabout',\
                 'car-shadow', 'cows', 'dance-twirl', 'dog', 'dogs-jump', 'drift-chicane', 'drift-straight',\
                 'goat', 'gold-fish', 'horsejump-high', 'india', 'judo', 'kite-surf', 'lab-coat', 'libby',\
                 'loading', 'mbike-trick', 'motocross-jump', 'paragliding-launch', 'parkour', 'pigs',\
                 'scooter-black', 'shooting', 'soapbox']

def getVideoList():
    '''
    Returns:
        list of str;
    '''
    EXT = '.h5'
    assert os.path.isdir(SEG_FOLDER)
    filelist = os.listdir(SEG_FOLDER)
    filelist = filter(lambda fname: fname.startswith(H5_ARCH_PREFIX) and fname.endswith(EXT), filelist)
    filelist = [fname[len(H5_ARCH_PREFIX):-len(EXT)] for fname in filelist]
    return filelist

def measure(seg, annot, n_labels):
    '''
    Parameters:
        seg: ndarray(?) of uint?
        annot: ndarray(?) of uint8
        n_labels: int
    Returns:
        iou: float or None if mask is all False
        n_segments: int
    '''
    assert seg.shape == annot.shape
    assert n_labels >= 2
    ious = []

    # get majority labels for each seg (bg can be majority as well)
    seg_u, seg_inv = np.unique(seg, return_inverse=True)
    seg_inv = seg_inv.reshape(seg.shape)
    n_segs = len(seg_u)
    annot_im_onehot = np.zeros((n_labels,) + annot.shape, dtype=np.bool_)
    seg_label_counts = np.zeros((n_segs, n_labels), dtype=np.int32)   # (n_segs, n_labels)

    for lab in range(0, n_labels):
        annot_im_onehot[lab] = annot == lab
        mseg = seg[annot_im_onehot[lab]]
        mseg_u, mseg_c = np.unique(mseg, return_counts=True)
        assert np.all(np.isin(mseg_u, seg_u))
        mseg_idxs_in_seg = np.searchsorted(seg_u, mseg_u)
        seg_label_counts[mseg_idxs_in_seg, lab] += mseg_c
    assert np.all(np.sum(seg_label_counts, axis=1) > 0)
    majority_labs = np.argmax(seg_label_counts, axis=1)

    # generate rounded annot image
    annot_rounded = majority_labs[seg_inv]

    # compute ious for each fg label
    for lab in range(1, n_labels):
        rounded_mask = annot_rounded == lab
        intersection = float(np.count_nonzero(rounded_mask & annot_im_onehot[lab]))
        union = float(np.count_nonzero(rounded_mask | annot_im_onehot[lab]))
        iou = intersection/union if union > 0 else 1.
        ious.append(iou)

    return np.mean(ious), n_segs


if __name__ == '__main__':
    
    vidnames = getVideoList()

    # TEMP
    #vidnames = ['paragliding-launch', 'bear']
    #TEST_SET = ['bear']

    fr_ious = {}
    fr_n_segs = {}
    for vidname in vidnames:

        print(vidname)
        # load segmentation & hierarchy
        seg_h5_path = os.path.join(SEG_FOLDER, H5_ARCH_PREFIX + vidname + '.h5')
        h5f = h5py.File(seg_h5_path, 'r')
        seg = h5f['lvl0_seg'][:].astype(np.int32)
        h5f.close()

        # load DAVIS annot masks
        annot_h5_path = os.path.join(ANNOT_DATA, ANNOT_H5_PREFIX + vidname + '.h5')
        h5f = h5py.File(annot_h5_path, 'r')
        annot = h5f[ANNOT_KEY][:].astype(np.uint8)
        annot_u = np.unique(annot)
        n_labels = len(annot_u)
        assert n_labels == np.amax(annot)+1
        h5f.close()
        
        fr_ious[vidname] = []
        fr_n_segs[vidname] = []
        assert seg.shape == annot.shape
        for fr_idx in range(seg.shape[0]):
            sp_iou, n_segs = measure(seg[fr_idx], annot[fr_idx], n_labels)
            fr_ious[vidname].append(sp_iou)
            fr_n_segs[vidname].append(n_segs)
    #

    # write iou stats to file
    result_fpath = os.path.join(SEG_FOLDER, 'iou_limits2.txt')
    result_f = open(result_fpath, 'w')
    result_f.write('IoU limits on ' + DATASET + ', correctly labeled segments vs pixelwise true annotations:\n')

    for vidset in [vidnames, TEST_SET]:
        if vidset is vidnames:
            result_f.write('\n ------ ALL VIDEOS ------\n')
        elif vidset is TEST_SET:
            result_f.write('\n\n ------ TEST SET ONLY ------\n')

        #   n_segments
        mean_vid_n_sps = int(np.mean([sum(fr_n_segs[vidname]) for vidname in vidset]))
        max_vid_n_sps = int(np.amax([sum(fr_n_segs[vidname]) for vidname in vidset]))
        min_vid_n_sps = int(np.amin([sum(fr_n_segs[vidname]) for vidname in vidset]))
        result_f.write('\nn_segments per video: ')
        result_f.write('\n    Mean: ' + str(mean_vid_n_sps))
        result_f.write('\n    Max:  ' + str(max_vid_n_sps))
        result_f.write('\n    Min:  ' + str(min_vid_n_sps))

        #   IoU stats per frame
        mean_ious = np.mean(list(itertools.chain(*[fr_ious[vidname] for vidname in vidset])))
        result_f.write('\n\nmIoU (fg only), per frame: ')
        result_f.write('\n    Mean: ' + str(mean_ious))

        #   IoU stats per video, per frame
        mean_vid_mean_ious = np.mean([np.mean(fr_ious[vidname]) for vidname in vidset])
        mean_vid_min_ious = np.mean([np.amin(fr_ious[vidname]) for vidname in vidset])
        mean_vid_max_ious = np.mean([np.amax(fr_ious[vidname]) for vidname in vidset])
        result_f.write('\n\nmIoU (fg only), per video, per frame: ')
        result_f.write('\n    Mean of means: ' + str(mean_vid_mean_ious))
        result_f.write('\n    Mean of mins:  ' + str(mean_vid_min_ious))
        result_f.write('\n    Mean of maxs:  ' + str(mean_vid_max_ious))

        #   IoU stats per video, per frame, weighted with number of frames in videos
        weighted_mean_vid_mean_ious = np.average([np.mean(fr_ious[vidname]) for vidname in vidset], weights=[len(fr_ious[vidname]) for vidname in vidset])
        weighted_mean_vid_min_ious = np.average([np.amin(fr_ious[vidname]) for vidname in vidset], weights=[len(fr_ious[vidname]) for vidname in vidset])
        weighted_mean_vid_max_ious = np.average([np.amax(fr_ious[vidname]) for vidname in vidset], weights=[len(fr_ious[vidname]) for vidname in vidset])
        result_f.write('\n\nmIoU (fg only), per video, per frame, WEIGHTED by n_frames of videos: ')
        result_f.write('\n    Mean of means: ' + str(weighted_mean_vid_mean_ious))
        result_f.write('\n    Mean of mins:  ' + str(weighted_mean_vid_min_ious))
        result_f.write('\n    Mean of maxs:  ' + str(weighted_mean_vid_max_ious))

    # per video
    result_f.write('\n\n  -------------------------------- PER VIDEO details --------------------------------\n')
    for vidname in vidnames:
        result_f.write('\nVIDEO ' + vidname + ': ')
        result_f.write('\n    n_segs            : ' + str(sum(fr_n_segs[vidname])))
        result_f.write('\n    mean IoU (fg only): ' + str(np.mean(fr_ious[vidname])))
        result_f.write('\n    min IoU (fg only) : ' + str(np.amin(fr_ious[vidname])))

    result_f.close()
