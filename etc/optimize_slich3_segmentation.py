
# Parallel hyperparameter optimization of SLICH3 segmentation procedure
#


import sys
import os
import time

import numpy as np
import h5py
from mango import Tuner, scheduler
from mango.domain.distribution import loguniform
from scipy.stats import uniform

from create_slich3_segmentation import SLICH3

#ANNOT_DATA = '/home/vavsaai/databases/DAVIS/rl_seg_data/davis2017_annot480p/'
ANNOT_DATA = '/media/ssd/vavsaai/databases/DAVIS/rl_seg_data/davis2017_annot480p/'
ANNOT_H5_PREFIX = 'annot480p_'
ANNOT_KEY = 'davis2017_annot_480p'

TEST_VIDS_1_BIKE_PACKING = ['bike-packing']
TEST_VIDS_1_BLACKSWAN = ['blackswan']
TEST_VIDS_1_DANCE_TWIRL = ['dance-twirl']
TEST_VIDS_1_GOLD_FISH = ['gold-fish']
TEST_VIDS_1_HORSEJUMP_HIGH = ['horsejump-high']
TEST_VIDS_1_INDIA = ['india']
TEST_VIDS_1_BREAKDANCE = ['breakdance']
TEST_VIDS_1_SCOOTER_BLACK = ['scooter-black']
TEST_VIDS_5 = ['bike-packing', 'blackswan', 'horsejump-high', 'india', 'scooter-black']
ALL_TEST_VIDS = ['bike-packing', 'blackswan', 'bmx-trees', 'breakdance', 'camel', 'car-roundabout',\
                 'car-shadow', 'cows', 'dance-twirl', 'dog', 'dogs-jump', 'drift-chicane', 'drift-straight',\
                 'goat', 'gold-fish', 'horsejump-high', 'india', 'judo', 'kite-surf', 'lab-coat', 'libby',\
                 'loading', 'mbike-trick', 'motocross-jump', 'paragliding-launch', 'parkour', 'pigs',\
                 'scooter-black', 'shooting', 'soapbox']

def load_annot_data(vidname):
    '''
    Parameters:
        vidname: str
    Returns:
        gt_annot: ndarray(n_frames, sy, sx) of ui8
        n_labels: int
    '''
    annot_h5_path = os.path.join(ANNOT_DATA, ANNOT_H5_PREFIX + vidname + '.h5')
    h5f = h5py.File(annot_h5_path, 'r')
    annot = h5f[ANNOT_KEY][:].astype(np.uint8)
    annot_u = np.unique(annot)
    n_labels = len(annot_u)
    assert n_labels == np.amax(annot)+1
    h5f.close()
    return annot, n_labels

def create_final_param_dict(params_dict_from_opt):
    params_dict = params_dict_from_opt.copy()
    params_dict['N_SPLITS_MAX'] = 2
    del params_dict['_2_MAX_REMAINING_SEG_SPLIT_RATIO']
    del params_dict['_2_MIN_INTENSITY_VAL_TO_SPLIT']
    params_dict['MAX_REMAINING_SEG_SPLIT_RATIO'] = [params_dict_from_opt['_2_MAX_REMAINING_SEG_SPLIT_RATIO']]
    params_dict['MIN_INTENSITY_VAL_TO_SPLIT'] = [params_dict_from_opt['_2_MIN_INTENSITY_VAL_TO_SPLIT']]
    return params_dict

# EVALUATE SEGMENTATION

def _measure_iou(seg, annot, n_labels):
    '''
    Returns Mean-IoU (Mean Jaccard-index), foreground only.
    Parameters:
        seg: ndarray(?) of uint?
        annot: ndarray(?) of uint8
        n_labels: int
    Returns:
        iou: float or None if mask is all False
        n_segments: int
    '''
    assert seg.shape == annot.shape
    assert n_labels >= 2
    ious = []

    # get majority labels for each seg (bg can be majority as well)
    seg_u, seg_inv = np.unique(seg, return_inverse=True)
    seg_inv = seg_inv.reshape(seg.shape)
    n_segs = len(seg_u)
    annot_im_onehot = np.zeros((n_labels,) + annot.shape, dtype=np.bool_)
    seg_label_counts = np.zeros((n_segs, n_labels), dtype=np.int32)   # (n_segs, n_labels)

    for lab in range(0, n_labels):
        annot_im_onehot[lab] = annot == lab
        mseg = seg[annot_im_onehot[lab]]
        mseg_u, mseg_c = np.unique(mseg, return_counts=True)
        assert np.all(np.isin(mseg_u, seg_u))
        mseg_idxs_in_seg = np.searchsorted(seg_u, mseg_u)
        seg_label_counts[mseg_idxs_in_seg, lab] += mseg_c
    assert np.all(np.sum(seg_label_counts, axis=1) > 0)
    majority_labs = np.argmax(seg_label_counts, axis=1)

    # generate rounded annot image
    annot_rounded = majority_labs[seg_inv]

    # compute ious for each fg label
    for lab in range(1, n_labels):
        rounded_mask = annot_rounded == lab
        intersection = float(np.count_nonzero(rounded_mask & annot_im_onehot[lab]))
        union = float(np.count_nonzero(rounded_mask | annot_im_onehot[lab]))
        iou = intersection/union if union > 0 else 1.
        ious.append(iou)

    return np.mean(ious), n_segs

def measure_iou_video(vidname, seg, annot, n_labels):
    '''
    Returns Mean-IoU per frame for a video.
    Parameters:
        vidname: str
        seg: ndarray(n_fr, sy, sx) of uint?
        annot: ndarray(n_fr, sy, sx) of uint8
        n_labels: int
    Returns:
        mean_iou_per_frame: float
        min_iou: float
        n_total_segs: int
    '''
    assert seg.ndim == 3
    #assert seg.shape == annot.shape   # TODO enable assert later
    assert n_labels >= 2
    fr_ious = []
    n_total_segs = 0
    for fr_idx in range(seg.shape[0]):
        fr_iou, n_segs_in_fr = _measure_iou(seg[fr_idx], annot[fr_idx], n_labels)
        if fr_iou is None:
            fr_iou = 1.
            print("Warning! None returned by _measure_iou() for video", vidname)
        fr_ious.append(fr_iou)
        n_total_segs += n_segs_in_fr
    return np.mean(fr_ious), np.amin(fr_ious), n_total_segs


# OPTIMIZE

# TODO
#   check if soft intensity from optflow diff is correct
#   handle variable length lists if more than 2 splits are supported

def objective(params_dict_from_opt):
    '''
    Evaluates the result of a given parameter config.
    Parameters:
        params_dict_from_opt: dict{str: ?}
    Returns:
        (float); the value of the objective function in the given parameter config
    '''
    VERBOSE = True
    RENDR_VIDEOS = True
    N_VID_SEG_LIMIT = 100000
    METRIC = 'mean&min'   # 'mean' or 'mean&min'
    VIDNAMES = ALL_TEST_VIDS  # ALL_TEST_VIDS, TEST_VIDS_5, TEST_VIDS_1_INDIA
    #
    params_dict = create_final_param_dict(params_dict_from_opt)
    instance_id = np.random.randint(2**32)
    if VERBOSE:
        print(">>> OPT ENTRY ", instance_id, "T:", time.time(), ">>>", params_dict)
    slich_obj = SLICH3(params_dict)
    vid_mean_ious = []
    vid_min_ious = []
    n_frames = []
    for vidname in VIDNAMES:
        annot, n_labels = load_annot_data(vidname)
        seg, n_segs = slich_obj.segment_video(vidname, False, RENDR_VIDEOS, RENDR_VIDEOS, RENDR_VIDEOS, first_few_frames_only=False)
        # try:
        #     seg, n_segs = slich_obj.segment_video(vidname, False, RENDR_VIDEOS, RENDR_VIDEOS, RENDR_VIDEOS, first_few_frames_only=False)
        # except AssertionError as error:
        #     print("    ----> ", instance_id, vidname, "- ABORT - assertion error")
        #     return 0.
        vid_mean_iou, vid_min_iou, n_vid_segs = measure_iou_video(vidname, seg, annot, n_labels)
        vid_mean_ious.append(vid_mean_iou)
        vid_min_ious.append(vid_min_iou)
        n_frames.append(seg.shape[0])
        if n_vid_segs > N_VID_SEG_LIMIT:
            if VERBOSE:
                print("    ----> ", instance_id, vidname, "- ABORT - exceeded seg limit:", n_vid_segs)
            return 0.
        if VERBOSE:
            print("    ---->", instance_id, vidname, "- metrics: mean", vid_mean_iou, "min", vid_min_iou, "n_seg", n_vid_segs)
    # evaluate
    if VERBOSE:
        print("  --> ", instance_id , "SET METRIC: means", np.mean(vid_mean_ious), "mins", np.mean(vid_min_ious), \
              "means&mins", (np.mean(vid_mean_ious)+np.mean(vid_min_ious))*0.5, \
              "Wmeans", np.average(vid_mean_ious, weights=n_frames), \
              "Wmins", np.average(vid_min_ious, weights=n_frames))
    if METRIC == 'mean':
        return np.mean(vid_mean_ious)
    elif METRIC == 'mean&min':
        return (np.mean(vid_mean_ious)+np.mean(vid_min_ious))*0.5
    else:
        assert False

@scheduler.serial
def objective_serial(**params_dict_from_opt):
    return objective(params_dict_from_opt)

@scheduler.parallel(n_jobs=15)
def objective_parallel(**params_dict_from_opt):
    return objective(params_dict_from_opt)



if __name__ == '__main__':

    default_params = {
                   'N_BASE_SEGMENTS_PER_FR': 256,
                   'N_NEW_SEGMENTS_AT_SPLIT': 4,
                   'N_K_TARGET_SEGMENTS_TOTAL': 50,
                   'SLIC_COMPACTNESS': 10.,  # logarithmic in range [0.001, 1000.]
                   'SLIC_SIGMA': 0,
                   'SLIC_MIN_SIZE_FACTOR': 0.5,      # in [0.2, 0.8]
                   'SLIC_MAX_SIZE_FACTOR': 3.,    # in [1.25, 5]
                   'MIN_SEGMENT_SIZE': 30,
                   'CANNY_HIGH': 200,
                   'CANNY_LOW_HIGH_RATE': 2.,    # in [1.2, 6.]
                   'SOFT_INTENSITY_FROM_CANNY': True,
                   'SOFT_INTENSITY_SMOOTHING_SIGMA': 7.,    # in [0., 12.]
                   'FILL_BORDER_DIR_START_TOWARDS_BOTTOMRIGHT': True,
                   'ENABLE_SPLIT_WITH_BORDER': True,
                   '_2_MAX_REMAINING_SEG_SPLIT_RATIO': 0.5,    # in [0.2, 1.0], N_SPLITS_MAX fixed to 2
                   '_2_MIN_INTENSITY_VAL_TO_SPLIT': 0.75,    # in [0.3, 1.0], N_SPLITS_MAX fixed to 2
                   'RELATIVE_INTENSITY_PERCENTILE': 90    # in [50., 100.]
                   }

    params1 = {
                   'N_BASE_SEGMENTS_PER_FR': 449,
                   'N_NEW_SEGMENTS_AT_SPLIT': 4,
                   'N_K_TARGET_SEGMENTS_TOTAL': 66,
                   'SLIC_COMPACTNESS': 0.01,  # logarithmic in range [0.001, 1000.]
                   'SLIC_SIGMA': 0,
                   'SLIC_MIN_SIZE_FACTOR': 0.268,      # in [0.2, 0.8]
                   'SLIC_MAX_SIZE_FACTOR': 4.34,    # in [1.25, 5]
                   'MIN_SEGMENT_SIZE': 30,
                   'CANNY_HIGH': 538,
                   'CANNY_LOW_HIGH_RATE': 5.37,    # in [1.2, 6.]
                   'SOFT_INTENSITY_FROM_CANNY': True,
                   'SOFT_INTENSITY_SMOOTHING_SIGMA': 8.81,    # in [0., 12.]
                   'FILL_BORDER_DIR_START_TOWARDS_BOTTOMRIGHT': False,
                   'ENABLE_SPLIT_WITH_BORDER': True,
                   '_2_MAX_REMAINING_SEG_SPLIT_RATIO': 0.3824,    # in [0.2, 1.0], N_SPLITS_MAX fixed to 2
                   '_2_MIN_INTENSITY_VAL_TO_SPLIT': 0.4396,    # in [0.3, 1.0], N_SPLITS_MAX fixed to 2
                   'RELATIVE_INTENSITY_PERCENTILE': 99.7702    # in [50., 100.]
                   }

    params1_edit = {
                   'N_BASE_SEGMENTS_PER_FR': 449,
                   'N_NEW_SEGMENTS_AT_SPLIT': 4,
                   'N_K_TARGET_SEGMENTS_TOTAL': 66,
                   'SLIC_COMPACTNESS': 0.01,  # logarithmic in range [0.001, 1000.]
                   'SLIC_SIGMA': 0,
                   'SLIC_MIN_SIZE_FACTOR': 0.268,      # in [0.2, 0.8]
                   'SLIC_MAX_SIZE_FACTOR': 4.34,    # in [1.25, 5]
                   'MIN_SEGMENT_SIZE': 30,
                   'CANNY_HIGH': 200,
                   'CANNY_LOW_HIGH_RATE': 5.37,    # in [1.2, 6.]
                   'SOFT_INTENSITY_FROM_CANNY': True,
                   'SOFT_INTENSITY_SMOOTHING_SIGMA': 8.81,    # in [0., 12.]
                   'FILL_BORDER_DIR_START_TOWARDS_BOTTOMRIGHT': False,
                   'ENABLE_SPLIT_WITH_BORDER': True,
                   '_2_MAX_REMAINING_SEG_SPLIT_RATIO': 0.3824,    # in [0.2, 1.0], N_SPLITS_MAX fixed to 2
                   '_2_MIN_INTENSITY_VAL_TO_SPLIT': 0.4396,    # in [0.3, 1.0], N_SPLITS_MAX fixed to 2
                   'RELATIVE_INTENSITY_PERCENTILE': 99.7702    # in [50., 100.]
                   }

    params2 = {'N_BASE_SEGMENTS_PER_FR': 509, 'N_NEW_SEGMENTS_AT_SPLIT': 4, 'N_K_TARGET_SEGMENTS_TOTAL': 69, \
    'SLIC_COMPACTNESS': 0.0018871341254125577, 'SLIC_SIGMA': 1, 'SLIC_MIN_SIZE_FACTOR': 0.26211107467463923, \
    'SLIC_MAX_SIZE_FACTOR': 3.2031949692315207, 'MIN_SEGMENT_SIZE': 42, 'CANNY_HIGH': 264, \
    'CANNY_LOW_HIGH_RATE': 3.981637063335916, 'SOFT_INTENSITY_FROM_CANNY': False, \
    'SOFT_INTENSITY_SMOOTHING_SIGMA': 5.690339422756248, 'FILL_BORDER_DIR_START_TOWARDS_BOTTOMRIGHT': False, \
    'ENABLE_SPLIT_WITH_BORDER': False, 'RELATIVE_INTENSITY_PERCENTILE': 72.21073943372988, \
    '_2_MAX_REMAINING_SEG_SPLIT_RATIO': 0.9084844565676358, '_2_MIN_INTENSITY_VAL_TO_SPLIT': 0.993436597693051}

    params2_edit = {'N_BASE_SEGMENTS_PER_FR': 509, 'N_NEW_SEGMENTS_AT_SPLIT': 4, 'N_K_TARGET_SEGMENTS_TOTAL': 69, \
    'SLIC_COMPACTNESS': 0.0018871341254125577, 'SLIC_SIGMA': 1, 'SLIC_MIN_SIZE_FACTOR': 0.26211107467463923, \
    'SLIC_MAX_SIZE_FACTOR': 3.2031949692315207, 'MIN_SEGMENT_SIZE': 25, 'CANNY_HIGH': 264, \
    'CANNY_LOW_HIGH_RATE': 3.981637063335916, 'SOFT_INTENSITY_FROM_CANNY': False, \
    'SOFT_INTENSITY_SMOOTHING_SIGMA': 5.690339422756248, 'FILL_BORDER_DIR_START_TOWARDS_BOTTOMRIGHT': False, \
    'ENABLE_SPLIT_WITH_BORDER': True, 'RELATIVE_INTENSITY_PERCENTILE': 72.21073943372988, \
    '_2_MAX_REMAINING_SEG_SPLIT_RATIO': 0.9084844565676358, '_2_MIN_INTENSITY_VAL_TO_SPLIT': 0.993436597693051}

    #print("DEFAULT PARAM CONFIG: ")
    #objective(default_params)
    print("PARAM CONFIG#1 edit: ")
    objective(params1_edit)
    assert False
    print("PARAM CONFIG#2: ")
    objective(params2)

    param_space = {
                   'N_BASE_SEGMENTS_PER_FR': range(64,513),
                   'N_NEW_SEGMENTS_AT_SPLIT': range(2,5),
                   'N_K_TARGET_SEGMENTS_TOTAL': range(20, 70),
                   'SLIC_COMPACTNESS': loguniform(-3, 6),  # logarithmic in range [0.001, 1000.]
                   'SLIC_SIGMA': range(0,7),
                   'SLIC_MIN_SIZE_FACTOR': uniform(0.2, 0.6),      # in [0.2, 0.8]
                   'SLIC_MAX_SIZE_FACTOR': uniform(1.25, 3.75),    # in [1.25, 5]
                   'MIN_SEGMENT_SIZE': range(5, 60),
                   'CANNY_HIGH': range(60, 600),
                   'CANNY_LOW_HIGH_RATE': uniform(1.2, 4.8),    # in [1.2, 6.]
                   'SOFT_INTENSITY_FROM_CANNY': [False, True],
                   'SOFT_INTENSITY_SMOOTHING_SIGMA': uniform(0., 12.),    # in [0., 12.]
                   'FILL_BORDER_DIR_START_TOWARDS_BOTTOMRIGHT': [False, True],
                   'ENABLE_SPLIT_WITH_BORDER': [False, True],
                   '_2_MAX_REMAINING_SEG_SPLIT_RATIO': uniform(0.2, 0.8),    # in [0.2, 1.0], N_SPLITS_MAX fixed to 2
                   '_2_MIN_INTENSITY_VAL_TO_SPLIT': uniform(0.3, 0.7),    # in [0.3, 1.0], N_SPLITS_MAX fixed to 2
                   'RELATIVE_INTENSITY_PERCENTILE': uniform(50., 50.)    # in [50., 100.]
                   }

    tuner_config_dict = {'num_iteration': 30, 'domain_size': 10000, 'initial_random': 15}  # default: num_it: 20, dom_size: 5000, init_rand: 1

    tuner = Tuner(param_space, objective_parallel, tuner_config_dict)
    results = tuner.maximize()



