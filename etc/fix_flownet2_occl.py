
# Script to improve Flownet2 occlusion archives without rerunning FlowNet2

import sys
sys.path.append('/home/vavsaai/git/nipgutils/src/')
import nipgutils.visualization as Viz

import numpy as np
import cv2
import os
import h5py

OPTFLOW_H5_SOURCE_FOLDER = '/home/vavsaai/databases/DAVIS/rl_seg_data/davis2017_optflows/'
TARGET_FOLDER = '/home/vavsaai/databases/DAVIS/rlseg2_data/davis2017_optflows_fixedoccl/'

RECREATE_VIDEO = True

def get_video_list():
    '''
    Returns:
        list of str; video names
    '''
    assert os.path.isdir(OPTFLOW_H5_SOURCE_FOLDER)
    vidlist = os.listdir(OPTFLOW_H5_SOURCE_FOLDER)
    vidlist = [fname[9:-3] for fname in vidlist if (fname[:9] == 'flownet2_') and (fname[-3:] == '.h5')]
    return vidlist

def load_optflow_occlusion_data(vidname):
    '''
    Parameters:
        vidname: str
    Returns:
        flow_fw, flow_bw: ndarray(n_frs-1, sy, sx, 2:[dy, dx]) of fl32
        occl_fw, occl_bw: ndarray(n_frs-1, sy, sx) of bool_
    '''
    flows_h5_path = os.path.join(OPTFLOW_H5_SOURCE_FOLDER, 'flownet2_' + vidname + '.h5')
    h5f = h5py.File(flows_h5_path, 'r')
    flow_fw = h5f['flows'][:].astype(np.float32)
    flow_bw = h5f['inv_flows'][:].astype(np.float32)
    occl_fw = h5f['occls'][:].astype(np.bool_)
    occl_bw = h5f['inv_occls'][:].astype(np.bool_)
    h5f.close()
    return flow_fw, flow_bw, occl_fw, occl_bw

def compute_occlusions(flow_fw, flow_bw):
    '''
    Parameters:
        flow_fw, flow_bw: ndarray(n_frs-1, sy, sx, 2:[dy, dx]) of fl32
    Returns:
        occl_fw, occl_bw: ndarray(n_frs-1, sy, sx) of bool_
    '''
    assert flow_fw.shape == flow_bw.shape
    assert flow_fw.shape[3:] == (2,)
    NOISE_REDUCTION_BLOB_RADIUS = 0.1
    imsize = np.asarray(flow_fw.shape[1:3], dtype=np.int32)

    occl_fw = np.empty(flow_fw.shape[:3], dtype=np.bool_)
    occl_bw = occl_fw.copy()
    for fr_idx in range(flow_fw.shape[0]):

        vecs_to_2d_dict = {}
        occl_masks_dict = {}
        for direction in ['fw', 'bw']:
            flow_fwd_im = flow_fw[fr_idx] if direction == 'fw' else flow_bw[fr_idx]
            flow_fbd_im = flow_bw[fr_idx] if direction == 'fw' else flow_fw[fr_idx]

            vecs_to = np.zeros_like(flow_fwd_im)    # (box_y, box_x, 2)
            indices = np.indices(flow_fwd_im.shape[:2], dtype=np.float32)
            vecs_to[:,:,0] = indices[0]
            vecs_to[:,:,1] = indices[1]
            vecs_to += flow_fwd_im
            vecs_to_2d_dict[direction] = vecs_to  # should not modify 'vecs_to' array after this line
            vecs_to = vecs_to[None,:,:,:]
            vecs_to = np.tile(vecs_to, (5,1,1,1))
            vecs_to[1] += np.asarray([NOISE_REDUCTION_BLOB_RADIUS,NOISE_REDUCTION_BLOB_RADIUS], dtype=np.float32)
            vecs_to[2] += np.asarray([-NOISE_REDUCTION_BLOB_RADIUS,NOISE_REDUCTION_BLOB_RADIUS], dtype=np.float32)
            vecs_to[3] += np.asarray([NOISE_REDUCTION_BLOB_RADIUS,-NOISE_REDUCTION_BLOB_RADIUS], dtype=np.float32)
            vecs_to[4] += np.asarray([-NOISE_REDUCTION_BLOB_RADIUS,-NOISE_REDUCTION_BLOB_RADIUS], dtype=np.float32)
            vecs_to = vecs_to.reshape((-1, 2))    # (box_y*box_x(*5), 2)
            vecs_to = np.around(vecs_to).astype(np.int32)
            valid_inds = np.all(vecs_to >= 0, axis=1) & np.all(vecs_to < imsize, axis=1)    # (box_y*box_x,)
            vecs_to = vecs_to[valid_inds]
            
            # occlusion mask
            mask = np.ones(flow_fwd_im.shape[:2], dtype=np.bool_)
            mask[vecs_to[:,0], vecs_to[:,1]] = 0
            occl_masks_dict[direction] = mask

        # remove thick True border from mask: set those pixels zero in the mask from which inverse optflow vectors point out of frame
        for direction in ['fw', 'bw']:
            inv_direction = 'bw' if direction == 'fw' else 'fw'
            vecs_to_inv = vecs_to_2d_dict[inv_direction]
            invalid_mask = np.any(vecs_to_inv < 0, axis=-1) | np.any(vecs_to_inv >= imsize-1, axis=-1)
            mask = occl_masks_dict[direction]
            mask[invalid_mask] = 0

        occl_fw[fr_idx,:,:] = occl_masks_dict['fw']
        occl_bw[fr_idx,:,:] = occl_masks_dict['bw']

    return occl_fw, occl_bw

def save_new_archives(vidname, flow_fw, flow_bw, occl_fw, occl_bw):
    arch_out_fpath = os.path.join(TARGET_FOLDER, 'flownet2_' + str(vidname) + '.h5')
    of_h5 = h5py.File(arch_out_fpath, 'w')
    of_h5.create_dataset("offset_yx", data=np.array([0., 0.], dtype=np.float32))
    of_h5.create_dataset("orig_vidsize_yx", data=np.array((480, 854), dtype=np.float32))
    of_h5.create_dataset("flow_run_size_yx", data=np.array((512, 896), dtype=np.float32))  # copied from run_optflow_occl script
    of_h5.create_dataset('flows', data=flow_fw, compression="gzip")
    of_h5.create_dataset('inv_flows', data=flow_bw, compression="gzip")
    of_h5.create_dataset('occls', data=occl_fw, compression="gzip")
    of_h5.create_dataset('inv_occls', data=occl_bw, compression="gzip")
    of_h5.close()

def save_video(vidname, flow_fw, flow_bw, occl_fw, occl_bw):
    vid_out_path = os.path.join(TARGET_FOLDER, 'viz_flow_' + str(vidname) + '.avi')
    #vr_fourcc = cv2.VideoWriter_fourcc(*'H264')     # use this codec with avi
    vr_fourcc = cv2.VideoWriter_fourcc(*'MJPG')     # use this codec with mp4
    vr_fps = 25.  # vid_capture.get(cv2.CAP_PROP_FPS)
    vr_frSize_xy = (flow_fw.shape[2]*2, flow_fw.shape[1]*2)
    vid_writer = cv2.VideoWriter(vid_out_path, fourcc=vr_fourcc, fps=vr_fps, frameSize=vr_frSize_xy)
    assert vid_writer.isOpened(), "Unable to open video file for writing: " + vid_out_path

    for fr_idx in range(flow_fw.shape[0]):
        fig1 = Viz.create_flow_box_visualization(flow_fw[fr_idx], brightness_mul=40.)
        fig2 = Viz.create_flow_box_visualization(flow_bw[fr_idx], brightness_mul=40.)
        fig3 = Viz.visualize_bit_mask(occl_fw[fr_idx])
        fig4 = Viz.visualize_bit_mask(occl_bw[fr_idx])

        fig_top = np.concatenate([fig1, fig3], axis=1)
        fig_bottom = np.concatenate([fig2, fig4], axis=1)
        fig = np.concatenate([fig_top, fig_bottom], axis=0)
        
        vid_writer.write(fig)

    vid_writer.release()


if __name__ == '__main__':

    vidnames = get_video_list()    # (folder_name, fnames list)
    os.makedirs(TARGET_FOLDER, exist_ok=True)
        
    for vidname in vidnames:
        print("Working on", vidname)
        flow_fw, flow_bw, _, _ = load_optflow_occlusion_data(vidname)
        occl_fw, occl_bw = compute_occlusions(flow_fw, flow_bw)
        save_new_archives(vidname, flow_fw, flow_bw, occl_fw, occl_bw)

        if RECREATE_VIDEO:
            save_video(vidname, flow_fw, flow_bw, occl_fw, occl_bw)
        