
import sys
import numpy as np
import cv2
import os
import h5py
from skimage.segmentation import slic, mark_boundaries

DATASET = 'davis2017'  # 'davis', 'davis2017', 'segtrack1', 'segtrack2', 'buffaloxiph'

if DATASET == 'davis':
    IMGS_FOLDER = '/home/vavsaai/databases/DAVIS/DAVIS/JPEGImages/480p/'
    OUTPUT_FOLDER = '/home/vavsaai/databases/DAVIS/rl_seg_data/davis_slic/'
    IMSIZE_YX = (480,854)
if DATASET == 'davis2017':
    IMGS_FOLDER = '/home/vavsaai/databases/DAVIS/DAVIS2017/DAVIS/JPEGImages/480p_unisize/'
    OUTPUT_FOLDER = '/home/vavsaai/databases/DAVIS/rlseg2_data/davis2017_slic/'
    IMSIZE_YX = (480,854)
elif DATASET == 'segtrack1':
    IMGS_FOLDER = '/home/vavsaai/databases/SegTrack/SegTrack_201111_resized_ims/segtrack_png_resized/'
    OUTPUT_FOLDER = '/home/vavsaai/databases/SegTrack/rl_seg_data/segtrack1_slic/'
    IMSIZE_YX = (352,414)
elif DATASET == 'segtrack2':
    IMGS_FOLDER = '/home/vavsaai/databases/SegTrack2/SegTrackv2/JPEGImages/'
    OUTPUT_FOLDER = '/home/vavsaai/databases/SegTrack2/segtrack2_slic/'
    IMSIZE_YX = (360,640)
elif DATASET == 'buffaloxiph':
    IMGS_FOLDER = '/home/vavsaai/databases/BuffaloXiph/BuffaloXiph/PNGImages/'
    OUTPUT_FOLDER = '/home/vavsaai/databases/BuffaloXiph/BuffaloXiph/buffaloxiph_slic/'
    IMSIZE_YX = (288,352)

N_SLIC_SEGMENTS = 512
SLIC_COMPACTNESS = 15    # default: 10

def getVideoList():
    '''
    Returns:
        list of tuple(str - video folder name, list of str - fnames in folder, sorted)
    '''
    assert os.path.isdir(IMGS_FOLDER)
    folderlist = os.listdir(IMGS_FOLDER)
    folderlist = list(filter(lambda folder: os.path.isdir(os.path.join(IMGS_FOLDER, folder)), folderlist))
    fnames = [sorted(os.listdir(os.path.join(IMGS_FOLDER, folder))) for folder in folderlist]
    return zip(folderlist, fnames)

def run_slic(foldername, fnames):
    
    print("Working on folder: " + str(foldername))
    imfolder = os.path.join(IMGS_FOLDER, foldername)
        
    segs = []
    offset = 0
    for fname in fnames:
        im_fpath = os.path.join(IMGS_FOLDER, foldername, fname)
        im = cv2.imread(im_fpath, cv2.IMREAD_COLOR)
        assert im is not None
        assert im.shape == (480,854,3)
        im_fl32 = cv2.cvtColor(im, cv2.COLOR_BGR2RGB).astype(np.float32)/255.  # bgr -> rgb float [0,1]
        
        rgb_segments = slic(im_fl32, n_segments=N_SLIC_SEGMENTS, compactness=SLIC_COMPACTNESS,\
                                 sigma=0, multichannel=True)
        u_seg = np.unique(rgb_segments)
        max_seg = np.amax(u_seg)
        assert u_seg.shape[0] == max_seg+1
        segs.append(rgb_segments + offset)
        offset += max_seg+1

    segs = np.stack(segs, axis=0).astype(np.int32)   # (n_ims, sy, sx)

    arch_out_fpath = os.path.join(OUTPUT_FOLDER, 'slic' + str(N_SLIC_SEGMENTS) + '_' + str(foldername) + '.h5')
    h5f = h5py.File(arch_out_fpath, 'w')
    h5f.create_dataset("lvl0_seg", data=segs, compression='gzip')
    h5f.close()
        

        
if __name__ == '__main__':

    os.makedirs(OUTPUT_FOLDER, exist_ok=True)

    vidlist = getVideoList()    # (folder_name, fnames list)
        
    for foldername, fnames in vidlist:
        run_slic(foldername, fnames)
        