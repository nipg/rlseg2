
# Hierarchical SLIC using ground-truth labeling for near 100% accuracy segmentation
#   Two-stage split:
#       1) splitting hierarchically where soft intensities are high (in decreasing order of intensities, random noise can be introduced)
#               soft intensitiies are created by smoothing the exact edge image derived from the GT labeling
#       2) splitting result segmentation with exact edges from GT labeling and removing too small segments
#   using slic-zero algorithm to split segments
#


import sys
sys.path.append('..')

import numpy as np
import cv2
import os
import h5py
from skimage.segmentation import slic, mark_boundaries
from skimage.measure import label, regionprops
import util.util as Util
import util.imutil as ImUtil
import util.viz as Viz
from scipy.ndimage.filters import gaussian_filter

# BELOW: hyperparam config from optimize_slich3_segmentation.py, opt#1 config with canny_high=200
N_BASE_SEGMENTS_PER_FR = 256
N_K_TARGET_SEGMENTS_TOTAL = 60
N_NEW_SEGMENTS_AT_SPLIT = 4
SLIC_COMPACTNESS = 0.01
FILL_BORDER_DIR_START_TOWARDS_BOTTOMRIGHT = False
SOFT_INTENSITY_SMOOTHING_SIGMA = 7.
SOFT_INTENSITY_RANDOM_FACTOR = 0.5     # multiplying soft intensities by values randomly generated in range [1., 1.+SOFT_INTENSITY_RANDOM_FACTOR]
N_SPLITS_MAX = 2
MAX_REMAINING_SEG_SPLIT_RATIO = [0.5]
MIN_INTENSITY_VAL_TO_SPLIT = [0.5]
MIN_SEGMENT_SIZE = 5

CREATE_VIDEO = True
CREATE_ARCHIVE = True

DATASET = 'davis2017'

if DATASET == 'davis2017':
    #IMGS_FOLDER = '/home/vavsaai/databases/DAVIS/DAVIS2017/DAVIS/JPEGImages/480p_unisize/'
    IMGS_FOLDER = '/media/ssd/vavsaai/databases/DAVIS/DAVIS2017/DAVIS/JPEGImages/480p_unisize/'
    GT_LABELS_FOLDER = '/media/ssd/vavsaai/databases/DAVIS/rl_seg_data/davis2017_annot480p/'
    OUTPUT_FOLDER = '/home/vavsaai/databases/DAVIS/rlseg2_data/davis2017_slich_gt100/'
    IMSIZE_YX = (480,854)
else:
    assert False, "Missing database info"

def get_video_list():
    '''
    Returns:
        list of str; video names
    '''
    assert os.path.isdir(GT_LABELS_FOLDER)
    vidlist = os.listdir(GT_LABELS_FOLDER)
    vidlist = [fname[10:-3] for fname in vidlist if (fname[:10] == 'annot480p_') and (fname[-3:] == '.h5')]
    return vidlist

def _load_img_data(vidname, n_frames):
    '''
    Parameters:
        vidname: str
        n_frames: int
    Returns:
        ims: ndarray(n_frames, sy, sx, 3:bgr) of ui8
    '''
    ims = []
    for fr_idx in range(n_frames):
        fname = str(fr_idx).zfill(5) + ".jpg"
        im_fpath = os.path.join(IMGS_FOLDER, vidname, fname)
        im = cv2.imread(im_fpath, cv2.IMREAD_COLOR)
        assert im is not None
        assert im.shape == IMSIZE_YX + (3,)
        ims.append(im)
    ims = np.stack(ims, axis=0)
    return ims

def _load_annot_data(vidname):
    '''
    Parameters:
        vidname: str
    Returns:
        annot_arr: ndarray(n_frames, sy, sx) of i32
    '''
    annot_h5_path = os.path.join(GT_LABELS_FOLDER, 'annot480p_' + vidname + '.h5')
    h5f = h5py.File(annot_h5_path, 'r')
    annot_arr = h5f['davis2017_annot_480p'][:].astype(np.int32)          # (n_frames, sy, sx) of i32
    h5f.close()
    return annot_arr


def _downscale_im_by_int_factor(im, ds_factor, ds_op):
    '''
    Downscales an image by an integer factor. Remainder cols/rows are dropped.
    Parameters:
        im: ndarray(sy, sx, ...) of ?
        ds_factor: int; downscale factor
        ds_op: Callable; the downscale operation - must have an "axis" parameter; compatible with np.mean(), np.any()
    Returns:
        im_ds: ndarray(dsy, dsx, ...) of ?; where dsy = sy // ds_factor, ...
    '''
    ds_size_yx = (im.shape[0]//ds_factor, im.shape[1]//ds_factor)
    im_size_yx_div = (ds_size_yx[0]*ds_factor, ds_size_yx[1]*ds_factor)
    im = im[:im_size_yx_div[0], :im_size_yx_div[1]]
    im = im.reshape((ds_size_yx[0], ds_factor, ds_size_yx[1], ds_factor) + im.shape[2:])
    im_ds = ds_op(im, axis=(1,3)).astype(im.dtype, copy=False)
    return im_ds

def _upscale_im_by_int_factor(im_ds, us_factor, target_size):
    '''
    Upscales an image by an integer factor by repeating items. Pads the upscaled image to "target_size" with zeros.
    Parameters:
        im_ds: ndarray(dsy, dsx, ...) of ?
        us_factor: int; upscale factor
        target_size: tuple(2); size of the returned image (sy, sx) dims only; padded cols/rows are set to zero.
    Returns:
        im: ndarray(sy, sx, ...) of ?;
    '''
    assert len(target_size) == 2
    im = np.empty(target_size + im_ds.shape[2:], dtype=im_ds.dtype)
    us_unpadded_size_yx = (im_ds.shape[0]*us_factor, im_ds.shape[1]*us_factor)
    #pad_widths = (target_size[0] - us_unpadded_size_yx[0], target_size[1] - us_unpadded_size_yx[1])
    im[us_unpadded_size_yx[0]:, :us_unpadded_size_yx[1]] = 0   # zeroing padded parts in three assignments
    im[:us_unpadded_size_yx[0], us_unpadded_size_yx[1]:] = 0
    im[us_unpadded_size_yx[0]:, us_unpadded_size_yx[1]:] = 0
    im_ds = np.broadcast_to(im_ds[:,None,:,None], (im_ds.shape[0], us_factor, im_ds.shape[1], us_factor) + im_ds.shape[2:])
    im[:us_unpadded_size_yx[0], :us_unpadded_size_yx[1]] = im_ds.reshape(us_unpadded_size_yx + im_ds.shape[4:])
    return im


def _create_soft_intensity_img_from_hard_edges(hard_edges):
    '''
    Returns the smoothed version of the given hard edge image.
    Parameters:
        hard_edges: ndarray(n_frames, sy, sx) of uint8; values in {0,1}
    Returns:
        soft_intensity: ndarray(n_frames, sy, sx) of float32;
    '''
    assert hard_edges.ndim == 3
    soft_intensity = hard_edges.astype(np.float32)*255.
    
    # smooth
    DOWNSCALE_FACTOR = 8
    for fr_idx in range(soft_intensity.shape[0]):
        soft_int_ds = _downscale_im_by_int_factor(soft_intensity[fr_idx], ds_factor=DOWNSCALE_FACTOR, ds_op=np.amax)
        soft_int_ds = gaussian_filter(soft_int_ds, sigma=SOFT_INTENSITY_SMOOTHING_SIGMA)
        soft_intensity[fr_idx] = _upscale_im_by_int_factor(soft_int_ds, us_factor=DOWNSCALE_FACTOR, \
                                                        target_size=soft_intensity.shape[1:3])
    return soft_intensity

def _create_hard_edge_img_from_annots(annot_arr):
    '''
    Parameters:
        annot_arr: ndarray(n_frames, sy, sx) of i32
    Returns:
        hard_edges: ndarray(n_frames, sy, sx) of uint8; values in {0,1}
    '''
    hard_edges = np.zeros_like(annot_arr, dtype=np.bool_)
    hard_edges[:,1:,:] |= annot_arr[:,1:,:] != annot_arr[:,:-1,:]
    hard_edges[:,:,1:] |= annot_arr[:,:,1:] != annot_arr[:,:,:-1]
    hard_edges = hard_edges.astype(np.uint8)*255
    return hard_edges

def _fill_borders_with_adj_pixels(seg, border_val, n_iter_limit=20):
    '''
    Replaces pixels of 'border_val' values with approximately closest different values in image.
    Parameters:
        (MODIFIED) seg: ndarray(sy, sx) of int
        border_val: int; value to replace
        n_iter_limit: int
    '''
    assert seg.ndim == 2
    imsize_yx = np.array(seg.shape, dtype=np.int32)
    border_idxs = np.argwhere(seg == border_val)  # (n_border_pix, 2)
    for iter_idx in range(n_iter_limit):
        if FILL_BORDER_DIR_START_TOWARDS_BOTTOMRIGHT is True:
            delta_dirs = [[0, -1], [-1, 0], [0,1], [1,0]]
        else:
            delta_dirs = [[0,1], [1,0], [0, -1], [-1, 0]]

        for delta_yx in delta_dirs:
            if border_idxs.shape[0] == 0:
                break
            read_idxs = border_idxs + delta_yx
            readmask = np.all(read_idxs >= 0, axis=1) & np.all(read_idxs < imsize_yx, axis=1)
            read_idxs = read_idxs[readmask,:]
            write_idxs = border_idxs[readmask,:]
            seg[write_idxs[:,0], write_idxs[:,1]] = seg[read_idxs[:,0], read_idxs[:,1]]
            remain_mask = seg[border_idxs[:,0], border_idxs[:,1]] == border_val
            border_idxs = border_idxs[remain_mask,:]
        if border_idxs.shape[0] == 0:
            break
        assert iter_idx != n_iter_limit-1, "Failed to remove all borders in given number of iterations."
    assert not np.any (seg == border_val)
    #


def _save_seg_archive(seg_fpath, seg):
    '''
    Saves segmentation to a .h5 archive.
    Parameters:
        seg_fpath: str
        seg: ndarray(n_frames, sy, sx) of i32; video segmentation with IDs starting from 0
    '''
    assert seg.ndim == 3
    h5f = h5py.File(seg_fpath, 'w')
    h5f.create_dataset("lvl0_seg", data=seg, compression='gzip')
    h5f.close()
    
def _create_seg_video(vid_fpath, ims, seg):
    '''
    Renders segmentation over images and writes it to a video file.
    Parameters:
        vid_fpath: str
        ims: ndarray(n_frames, sy, sx, 3:rgb) of ui8
        seg: ndarray(n_frames, sy, sx) of i32; video segmentation with IDs starting from 0
    '''
    assert ims.shape[3:] == (3,)
    assert seg.shape == ims.shape[:3]
    vr_fourcc = cv2.VideoWriter_fourcc(*'MJPG')     # use this codec with avi
    vr_fps = 25.  # vid_capture.get(cv2.CAP_PROP_FPS)
    vr_frSize_xy = (ims.shape[2], ims.shape[1])
    vid_writer = cv2.VideoWriter(vid_fpath, fourcc=vr_fourcc, fps=vr_fps, frameSize=vr_frSize_xy)
    assert vid_writer.isOpened(), "Unable to open video file for writing: " + vid_out_path

    for fr_idx in range(ims.shape[0]):
        im = ims[fr_idx,:,:,:].copy()
        Viz.render_segmentation_edges_BGR(im, seg[fr_idx,:,:], boundary_color_rgb=(255,255,0))
        vid_writer.write(im)

    vid_writer.release()

def _run_slich_on_frame(im_rgb_fl32, soft_intensity, hard_edges, n_target_segs):
    '''
    Returns a segmentation of the frame.
    Parameters:
        im_rgb_fl32: ndarray(sy, sx, 3:rgb) of fl32; in [0,1] range - MUST BE RGB !!! (bgr might not be good, see SLIC details)
        soft_intensity: ndarray(sy, sx) of fl32;
        hard_edges: ndarray(sy, sx) of bool;
        n_target_segs: int
    Returns:
        seg: ndarray(sy, sx) of i32; segmentation with IDs starting from 0
        end_offset: int; max(segs)+1
    '''
    assert im_rgb_fl32.dtype == np.float32
    assert np.amax(im_rgb_fl32) <= 1.
    assert im_rgb_fl32.ndim == 3
    assert im_rgb_fl32.shape == soft_intensity.shape + (3,) == hard_edges.shape + (3,)
    assert hard_edges.dtype == np.bool_
    assert N_SPLITS_MAX == len(MAX_REMAINING_SEG_SPLIT_RATIO)+1 == len(MIN_INTENSITY_VAL_TO_SPLIT)+1
    
    # PHASE#0: run base level segmentation

    seg = slic(im_rgb_fl32, n_segments=N_BASE_SEGMENTS_PER_FR, compactness=SLIC_COMPACTNESS, sigma=0, multichannel=True, \
                convert2lab=True, min_size_factor=0.5, max_size_factor=3.0, slic_zero=True, start_label=1)
    unwritten_mask = seg <= 0
    if np.any(unwritten_mask):
        c_missed_pixels = np.count_nonzero(unwritten_mask)
        print(">>> WARNING! BASE level SLIC missed", c_missed_pixels, "pixels.")
        _fill_borders_with_adj_pixels(seg, border_val=0)
        assert np.all(seg > 0)

    # PHASE#1: split with 'soft_intensity'

    # get base level segment rprop data
    rprops = regionprops(seg)   # labels are starting from 1 (start_label was specified in slic)
    seg_data = {}   # {seg_id - int: tuple(bbox - tuple(4) of int, ndarray(bb_h, bb_w))}
    new_seg_ids = []
    for rprop in rprops:
        mask_in_bbox = rprop.image
        bbox_tlbr = rprop.bbox
        seg_data[rprop.label] = (bbox_tlbr, mask_in_bbox)
        new_seg_ids.append(rprop.label)
    n_total_segs = [len(seg_data)]

    for split_lvl in range(N_SPLITS_MAX):

        # compute intensities for new segments
        curr_max_id = max(new_seg_ids) if len(new_seg_ids) > 0 else curr_max_id
        seg_intensities = np.empty((len(new_seg_ids), 2), dtype=np.int32)   # (n_segs, 2:[ID, value])
        for new_seg_idx in range(len(new_seg_ids)):
            new_seg_id = new_seg_ids[new_seg_idx]
            bbox_tlbr, mask_in_bbox = seg_data[new_seg_id]
            pixs = soft_intensity[bbox_tlbr[0]:bbox_tlbr[2], bbox_tlbr[1]:bbox_tlbr[3]][mask_in_bbox]
            seg_intensities[new_seg_idx, 0] = new_seg_id
            rnd_mul = 1. + np.random.rand()*SOFT_INTENSITY_RANDOM_FACTOR
            seg_intensities[new_seg_idx, 1] = np.amax(pixs) * rnd_mul

        # select segments to split, all conditions below must hold: 
        #   1) maximum 'n_target_segs' total numer of segments EXPECTED after splitting
        #   2) maximum MAX_REMAINING_SEG_SPLIT_RATIO[split_lvl] ratio of the remaining segments selected with highest intensities
        #         for highest level, this is 1.0
        #   3) split seg intensities must be above MIN_INTENSITY_VAL_TO_SPLIT[split_lvl]
        min_intensity_val_to_split = MIN_INTENSITY_VAL_TO_SPLIT[split_lvl] \
                                                if split_lvl < N_SPLITS_MAX-1 else 0.    # condition 3)
        max_remaining_seg_split_ratio = MAX_REMAINING_SEG_SPLIT_RATIO[split_lvl] \
                                                if split_lvl < N_SPLITS_MAX-1 else 1.    # condition 2)
        n_max_segs_to_split = int(seg_intensities.shape[0]*max_remaining_seg_split_ratio)        # condition 2)
        n_max_remaining_splits = int((n_target_segs - len(seg_data))/N_NEW_SEGMENTS_AT_SPLIT)    # condition 1)
        n_max_segs_to_split = min(n_max_segs_to_split, n_max_remaining_splits)

        seg_intensities = seg_intensities[seg_intensities[:,1] >= min_intensity_val_to_split]
        if seg_intensities.shape[0] > n_max_segs_to_split:
            seg_sorter = np.argsort(seg_intensities[:,1])[-n_max_segs_to_split:]
            seg_intensities = seg_intensities[seg_sorter,:]

        # split selected segments
        new_seg_ids = []
        for seg_id in seg_intensities[:,0]:
            bbox_tlbr, mask_in_bbox = seg_data[seg_id]
            del seg_data[seg_id]

            # run SLIC on segment
            im_rgb_fl32_bbox = im_rgb_fl32[bbox_tlbr[0]:bbox_tlbr[2], bbox_tlbr[1]:bbox_tlbr[3], :]
            split_segs = slic(im_rgb_fl32_bbox, n_segments=N_NEW_SEGMENTS_AT_SPLIT, \
                                compactness=SLIC_COMPACTNESS, sigma=0, multichannel=True, convert2lab=True, min_size_factor=0.5, \
                                max_size_factor=3.0, slic_zero=True, start_label=1, mask=mask_in_bbox)

            unwritten_mask = (split_segs <= 0) & mask_in_bbox
            if np.any(unwritten_mask):
                if np.all(split_segs[mask_in_bbox] == 0):
                    # if slic did not find any segments then set area with mask as a single segment with ID == 1
                    #   happens more rarely if slic_zero is enabled
                    split_segs[mask_in_bbox] = 1
                    print(">>> WARNING! Split failed.")
                else:
                    # not all, but some pixels were left as bg within the mask area; happens in a smaller scale with slic_zero
                    # assign label 1 to these pixels (they are merged with other segs in phase#2 if too small)
                    split_segs[unwritten_mask] = 1
                    c_missed_pixels = np.count_nonzero(unwritten_mask)
                    if c_missed_pixels >= 50:
                        print(">>> WARNING! SLIC missed", c_missed_pixels, "pixels.")
                
            rprops = regionprops(split_segs)   # labels are starting from 1 (start_label was specified in slic)
            assert len(rprops) >= 1
            for rprop in rprops:
                mask_in_bbox2 = rprop.image
                bbox_tlbr2 = (rprop.bbox[0]+bbox_tlbr[0], rprop.bbox[1]+bbox_tlbr[1], rprop.bbox[2]+bbox_tlbr[0], rprop.bbox[3]+bbox_tlbr[1])
                curr_max_id += 1
                assert curr_max_id not in seg_data.keys()
                seg_data[curr_max_id] = (bbox_tlbr2, mask_in_bbox2)
                new_seg_ids.append(curr_max_id)

        n_total_segs.append(len(seg_data))

    # write all segments into final segmentation image
    seg = np.full(im_rgb_fl32.shape[:2], dtype=np.int32, fill_value=-1)
    for seg_label, (bbox_tlbr, mask_in_bbox) in seg_data.items():
        seg[bbox_tlbr[0]:bbox_tlbr[2], bbox_tlbr[1]:bbox_tlbr[3]][mask_in_bbox] = seg_label

    # PHASE#2: split with 'hard_edges'
    #   write border into seg image with 0 (background) value
    #   skimage.label, then rprops, find small segments, get segment adjacencies and their counts
    #   overwrite small seg with non-small most frequent adj seg id
    #       if no such seg id, overwrite with 0 value
    #   fill 0 values iteratively with nonzero adj pixels, (start with appropriate direction matching canny)
    seg += 1   # 0 is ignored as background in skimage.measure.label()
    assert np.all(seg > 0)   # ensure all pixels are written with valid segment values
    seg[hard_edges] = 0   # border value: 0 (background)
    seg = label(seg, background=0, return_num=False, connectivity=1).astype(np.int32)   # diagonals must not count as adjacent
    small_segs = {}
    rprops = regionprops(seg)
    for rprop in rprops:
        if rprop.area < MIN_SEGMENT_SIZE:
            small_segs[rprop.label] = (rprop.bbox, rprop.image)
    assert 0 not in small_segs.keys()
    u_edges, c_edges = ImUtil.get_adj_graph_edge_list_fast(seg, ignore_axes=[], return_counts=True)
    nonzero_edges_mask = np.all(u_edges != 0, axis=1)  # filter edges from/to background
    u_edges = u_edges[nonzero_edges_mask]
    c_edges = c_edges[nonzero_edges_mask]
    for small_seg, (bbox_tlbr, mask_in_bbox) in small_segs.items():
        edge_idxs_with_seg = np.where(np.any(u_edges == small_seg, axis=1))[0]
        if edge_idxs_with_seg.shape[0] == 0:   # if small seg only adj to background, overwrite seg with background value
            seg[bbox_tlbr[0]:bbox_tlbr[2], bbox_tlbr[1]:bbox_tlbr[3]][mask_in_bbox] = 0
        else:
            max_edge_idx_with_seg = np.argmax(c_edges[edge_idxs_with_seg])  # otherwise overwrite seg with most frequent non-bg adj value
            max_edge_with_seg = u_edges[edge_idxs_with_seg[max_edge_idx_with_seg]]
            seg_to_join_idx1 = 1 if max_edge_with_seg[0] == small_seg else 0
            assert (max_edge_with_seg[1-seg_to_join_idx1] == small_seg) and (max_edge_with_seg[seg_to_join_idx1] != small_seg)
            seg[bbox_tlbr[0]:bbox_tlbr[2], bbox_tlbr[1]:bbox_tlbr[3]][mask_in_bbox] = max_edge_with_seg[seg_to_join_idx1]
    _fill_borders_with_adj_pixels(seg, border_val=0)  # replace border pixels with nearby pixel values

    u_seg, inv_seg = np.unique(seg, return_inverse=True)
    #print("    n_segs in split levels:", n_total_segs, "final:", u_seg.shape[0])
    return inv_seg.reshape(seg.shape), u_seg.shape[0]

def _run_slich_on_video(ims_rgb_fl32, annots, n_target_segs_per_fr):
    '''
    Creates SLICH segmentation on a video.
    Parameters:
        ims_rgb_fl32: ndarray(n_frames, sy, sx, 3:rgb) of fl32; in [0,1] range - MUST BE RGB !!! (bgr might not be good, see SLIC details)
        annots: ndarray(n_frames, sy, sx) of i32
        n_target_segs_per_fr: int
    Returns:
        seg: ndarray(n_frames, sy, sx) of i32; video segmentation with IDs starting from 0
        n_segs: int;
    '''
    assert ims_rgb_fl32.shape[3:] == (3,)
    assert ims_rgb_fl32.shape[:3] == annots.shape
    seg_end_offset = 0
    seg = np.full(ims_rgb_fl32.shape[:3], dtype=np.int32, fill_value=-1)
    hard_edges = _create_hard_edge_img_from_annots(annots)
    soft_intensity = _create_soft_intensity_img_from_hard_edges(hard_edges)

    for fr_idx in range(ims_rgb_fl32.shape[0]):
        seg_fr, seg_fr_end_offset = _run_slich_on_frame(ims_rgb_fl32[fr_idx], soft_intensity[fr_idx], \
                                                                    hard_edges[fr_idx].astype(np.bool_), n_target_segs_per_fr)
        seg[fr_idx,:,:] = seg_fr + seg_fr_end_offset
        seg_end_offset += seg_fr_end_offset
    return seg, seg_end_offset


def segment_video( vidname, save_to_archive=False, save_result_video=False, first_few_frames_only=False):
    '''
    Creates segmentation on a video.
    Parameters:
        vidname: str
        save_to_archive, save_result_video, save_soft_video, save_hard_video: bool
    Returns:
        seg: ndarray(n_frames, sy, sx) of i32; video segmentation with IDs starting from 0
        n_segs: int
    '''
    annots = _load_annot_data(vidname)
    n_orig_frames = annots.shape[0]
    n_frames = n_orig_frames
    if first_few_frames_only is True:
        n_frames = 5
        annots = annots[:n_frames]
    ims = _load_img_data(vidname, n_frames)
    ims_rgb_fl32 = ims[..., ::-1].astype(np.float32)/255.  # bgr ui8  in [0,255] -> rgb fl32 in [0,1]

    target_segs_per_fr = int(N_K_TARGET_SEGMENTS_TOTAL*1000 / n_orig_frames)
    print("Working on video '" + vidname + "', " + str(target_segs_per_fr) + " target segments per frame (before canny splitting)")
    seg, n_segs = _run_slich_on_video(ims_rgb_fl32, annots, target_segs_per_fr)

    if save_to_archive is True:
        seg_fpath = os.path.join(OUTPUT_FOLDER, 'slich_gt100_' + str(vidname) + '.h5')
        _save_seg_archive(seg_fpath, seg)

    if save_result_video is True:
        vid_fpath = os.path.join(OUTPUT_FOLDER, 'viz_slich_gt100_' + str(vidname) + '.avi')
        _create_seg_video(vid_fpath, ims, seg)

    return seg, n_segs


if __name__ == '__main__':

    assert N_K_TARGET_SEGMENTS_TOTAL < 70
    os.makedirs(OUTPUT_FOLDER, exist_ok=True)

    vidnames = get_video_list()
    #vidnames = ['bear']  # , 'india'
    #vidnames = ['india']  # , 'india'
    #vidnames = ['mallard-water', 'dancing']  # , 'india'

    for vidname in vidnames:
        segment_video(vidname, save_to_archive=CREATE_ARCHIVE, save_result_video=CREATE_VIDEO, \
                                first_few_frames_only=False)

