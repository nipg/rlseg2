
#
# Dummy implementation for SeedPropagation. RLseg2 implementation #2
#
#   @author Viktor Varga
#

class DummySeedPropagation:

    def propagate(self, vidname, fr_idxs, points, labels):
        '''
        "Dummy porpagation" of seed points. Returns the input arrays.
        Parameters:
            vidname: str
            fr_idxs: ndarray(n_seed_annots,) of int32
            points: ndarray(n_seed_annots, 2:[py, px]) of int32
            labels: ndarray(n_seed_annots,) of int32
        Returns:
            prop_points: dict{<empty>}; original points are not stored
        '''
        return dict()
