
#
# LabelModel using LEO, see https://github.com/deepmind/leo
#   Paper: Rusu et al., "Meta-Learning with Latent Embedding Optimization", ICLR 2019
#   Installation (exact version requirements must be fulfilled):
#       Requirements:
#          tensorflow==1.13.1 (cpu version)
#          tensorflow-probability==0.5.0
#          dm-sonnet==1.29 (install after tensorflow installations, needs reinstall if tf changes)
#          absl-py==0.9.0
#       clone repo, download and unpack embeddings
#       python runner.py --data_path=<PATH to unpacked "embeddings" folder>
#


import os
import logging
import time
from absl import flags
from collections import OrderedDict
import functools
import tensorflow as tf
tf.logging.set_verbosity(tf.logging.INFO)

import numpy as np

from label_estimation.abstract_label_model import LabelModel

import label_estimation.leo.config as leo_config
import label_estimation.leo.model as leo_model
import label_estimation.leo.utils as leo_utils


LEO_PRETRAIN_CHECKPOINT_PATH = '/home/vavsaai/git/rlseg2/temp_results/leo_pretrain_checkpoints/'
LEO_PRETRAIN_CHECKPOINT_FOLDER = '_best_checkpoint_tr30v15_20ex_72feat_083valacc'
LEO_PRETRAIN_CHECKPOINT_INTERVAL_N_STEPS = 5000

class LEOLabelModel(LabelModel):

    '''
    Member fields:
        n_features: int; input shape to model is (n_features,)
        n_cats: int; number of categories to classify into

        tf_session_pred: tf.Session(); used for prediction only
        tf_input_placeholders: tuple(6) of tf.placeholder; tf placeholders to be used as input; 
                                    see format in 'self._leo_construct_prediction_input()'
        tf_pred_tensor: tf.Tensor; tf tensor with the predictions

        batch_size_inner_train: int; the batch size (per category) for a single task
        batch_size_inner_val_pretrain, batch_size_inner_val_pred: int; the batch size (per category) for a single task
        batch_size_meta_train, batch_size_meta_val: int; the meta-batch size for pretraining (number of tasks in a batch)
        n_leo_cats: int; number of categories in the LEO model

        data_fitted: tuple(xs: ndarray(n_sps, n_features) of fl32,
                           ys: ndarray(n_sps) of i32);

    '''

    def __init__(self, wrapped_model=None):
        FLAGS = flags.FLAGS
        FLAGS([""])
        self.n_features = None
        self.n_cats = None
        assert wrapped_model is None, "This is not a wrapper model."

        self.tf_session_pred = None
        self.tf_input_placeholders = None
        self.tf_pred_tensor = None
        self.data_fitted = None

        outer_model_config = leo_config.get_outer_model_config()
        self.batch_size_inner_train = outer_model_config["num_tr_examples_per_class"]
        self.batch_size_inner_val_pretrain = outer_model_config["num_val_examples_per_class"]
        self.batch_size_inner_val_pred = 4096
        self.batch_size_meta_train = outer_model_config["metatrain_batch_size"]
        self.batch_size_meta_val = outer_model_config["metavalid_batch_size"]
        self.n_leo_cats = outer_model_config["num_classes"]
        assert self.n_leo_cats == 2


    def destroy(self):
        if self.tf_session_pred is not None:
            self.tf_session_pred.close()
            self.tf_session_pred = None

    def __str__(self):
        return "LEO"

    def reset(self, n_features, n_cats=None):
        '''
        Clear fitted data and modify number of categories.
        '''
        assert (self.n_features is None) or (self.n_features == n_features)
        self.n_features = n_features
        self.n_cats = n_cats
        self.data_fitted = None

    def pretrain_mutliple_label_setups(self, xss_train=None, yss_train=None, xss_val=None, yss_val=None):
        '''
        Pretrain with multiple batches of independent segment feature vectors and corresponding labels.
        OR
        Loads a pretraining checkpoint for the LEO model if available.
        Each batch uses a different label category set. 
        Parameters:
            xss_train: None OR list(n_train_batches) of ndarray(n_segs, n_features) of float
            yss_train: None OR list(n_train_batches) of ndarray(n_segs) of int32
            xss_val: None OR list(n_val_batches) of ndarray(n_segs, n_features) of float
            yss_val: None OR list(n_val_batches) of ndarray(n_segs) of int32;
                IF checkpoint is available, None can be given, as parameters are ignored in this case
        '''
        checkpoint_dir = os.path.join(LEO_PRETRAIN_CHECKPOINT_PATH, LEO_PRETRAIN_CHECKPOINT_FOLDER)
        if os.path.isdir(checkpoint_dir):
            # load pretrained checkpoint if available
            print("LeoLabelModel.pretrain_mutliple_label_setups(): Pretraining skipped," + \
                              " since " + checkpoint_dir + " folder exists. Loading checkpoint...")
        else:
            # pretrain if checkpoint is not available
            print("LeoLabelModel.pretrain_mutliple_label_setups(): Starting pretraining...")
            pretraining_data = (xss_train, yss_train, xss_val, yss_val)
            assert all([ls is not None for ls in pretraining_data])
            assert all([len(ls) > 0 for ls in pretraining_data])
            assert xss_train[0].shape[-1] == self.n_features and xss_val[0].shape[-1] == self.n_features
            self._pretrain_leo_model(pretraining_data)
            # TEMP: quit after pretraining
            self.destroy()
            assert False, "LEOLabelModel: Run script again to use predict mode"

        self._load_pretrained_leo_model(checkpoint_dir)


    def fit(self, xs, ys, verbose=False):
        '''
        Parameters:
            xs: ndarray(n_sps, n_features) of float32; the feature vectors for each SP
            ys: ndarray(n_sps) of int32; the true labels of each SP
        Sets self.data_fitted,
            format: tuple(xs: ndarray(n_sps, n_features) of fl32,
                          ys: ndarray(n_sps) of i32)
        '''
        assert xs.shape[1:] == (self.n_features,)
        assert np.amax(ys) < self.n_cats
        self.data_fitted = (xs, ys)


    def predict(self, xs, return_probs=False, verbose=False):
        '''
        Parameters:
            xs: ndarray(n_sps, n_features) of float32; the feature vectors for each SP
            return_probs: bool;
        Returns:
            ys_pred: ndarray(n_sps, n_cat) of fl32 (IF return_probs == True)
                     ndarray(n_sps,) of i32        (IF return_probs == False)

        '''
        assert self.tf_session_pred is not None
        assert self.tf_input_placeholders is not None
        assert self.data_fitted is not None
        assert xs.shape[1:] == (self.n_features,)
        xs_fit, ys_fit = self.data_fitted   # (n_sps, n_features) fl32, (n_sps) i32

        # tf input placeholders & dummy inputs
        tr_input, tr_output, tr_info, val_input, val_output, val_info = self.tf_input_placeholders
        tr_info_dummy = np.zeros((1, self.n_leo_cats, self.batch_size_inner_train), dtype='U0')
        val_output_dummy = np.zeros((1, self.n_leo_cats, self.batch_size_inner_val_pred, 1), dtype=np.int32)
        val_info_dummy = np.zeros((1, self.n_leo_cats, self.batch_size_inner_val_pred), dtype='U0')

        # convert 'self.n_cats'-way classification into a set of binary classification problems
        fit_cat_idxs = {cat: np.where(ys_fit == cat)[0] for cat in range(self.n_cats)}
        n_pred_samples = xs.shape[0]

        assert self.n_leo_cats == 2
        batch_size_flat = self.n_leo_cats*self.batch_size_inner_val_pred
        pred_probs = np.zeros((n_pred_samples, self.n_cats), dtype=np.float32)  # storing prob of categories
        xs_to_pred = np.zeros((batch_size_flat, xs.shape[1]), dtype=np.float32)

        # iterate each category-pair
        cat_pairs = [(cat1, cat2) for cat1 in range(self.n_cats) for cat2 in range(cat1+1, self.n_cats)]
        for cat1, cat2 in cat_pairs:

            # if either cat1 or cat2 is not present in fitted data, skip this pair
            if (fit_cat_idxs[cat1].shape[0] == 0) or (fit_cat_idxs[cat2].shape[0] == 0):
                continue

            # create training data
            cat1_fit_idxs = np.resize(fit_cat_idxs[cat1], (self.batch_size_inner_train,))
            cat2_fit_idxs = np.resize(fit_cat_idxs[cat2], (self.batch_size_inner_train,))
            fit_idxs_binary = np.stack([cat1_fit_idxs, cat2_fit_idxs], axis=0)   # (n_leo_cats:2, n_fit_samples)
            fit_xs_binary = xs_fit[fit_idxs_binary,:]  # (n_leo_cats:2, n_fit_samples, n_features)
            fit_ys_binary = ys_fit[fit_idxs_binary]    # (n_leo_cats:2, n_fit_samples)
            assert np.all(fit_ys_binary == np.array([cat1, cat2])[:,None])  # check label consistency
            fit_ys_binary[:,:] = [[0],[1]]

            # split xs to predict into minibatches, use same xs_fit, ys_fit for each predict session
            for minibatch_offset in range(0, n_pred_samples, batch_size_flat):
                minibatch_end_offset = min(n_pred_samples, minibatch_offset + batch_size_flat)
                xs_to_pred[:minibatch_end_offset-minibatch_offset] = xs[minibatch_offset:minibatch_end_offset, :]
                                                                        # handling last, incomplete minibatch as well

                # tf run, see placeholder shapes in self._leo_construct_prediction_input()
                fit_xs_binary_reshaped = fit_xs_binary[None, ...]          # (1, n_leo_cats:2, n_tr_samples, n_features)
                fit_ys_binary_reshaped = fit_ys_binary[None, ..., None]    # (1, n_leo_cats:2, n_tr_samples, 1)
                xs_to_pred_reshaped = xs_to_pred.reshape((1, self.n_leo_cats, self.batch_size_inner_val_pred, self.n_features))
                                                                  # (1, n_leo_cats:2, n_val_samples, n_features)

                feed_dict = {tr_input: fit_xs_binary_reshaped.astype(np.float32, copy=False),
                             tr_output: fit_ys_binary_reshaped.astype(np.int32, copy=False), tr_info: tr_info_dummy,
                             val_input: xs_to_pred_reshaped.astype(np.float32, copy=False),
                             val_output: val_output_dummy, val_info: val_info_dummy}
                ys_pred = self.tf_session_pred.run(self.tf_pred_tensor, feed_dict=feed_dict)

                assert ys_pred.shape == (1, self.n_leo_cats, self.batch_size_inner_val_pred, self.n_leo_cats)
                ys_pred = ys_pred.reshape((batch_size_flat, self.n_leo_cats))

                # store predicted probabilites
                '''
                # option1: counting binary argmaxs
                minibatch_maxcat_idxs = np.argmax(ys_pred[:minibatch_end_offset-minibatch_offset, :], axis=-1)
                minibatch_maxcats = np.where(minibatch_maxcat_idxs, cat2, cat1)
                pred_probs[np.arange(minibatch_offset, minibatch_end_offset), minibatch_maxcats] += 1
                '''
                # option2: summing binary preds
                pred_probs[minibatch_offset:minibatch_end_offset, cat1] +=\
                 ys_pred[:minibatch_end_offset-minibatch_offset, 0]
                pred_probs[minibatch_offset:minibatch_end_offset, cat2] +=\
                 ys_pred[:minibatch_end_offset-minibatch_offset, 1]

                #pred_probs[minibatch_offset:minibatch_end_offset,:] = ys_pred[:minibatch_end_offset-minibatch_offset, :]

        #pred_probs = np.argmax(minibatch_maxcat_idxs, axis=-1)
        pred_probs = np.argmax(pred_probs, axis=-1)

        if return_probs:
            #return all_cat_pred_probs
            assert False, "Probability calculation is not implemented."
        else:
            #return np.argmax(all_cat_pred_probs, axis=0)
            return pred_probs.astype(np.int32)


    def evaluate(self, xs, ys, return_preds=False, verbose=False):
        '''
        Parameters:
            xs: ndarray(n_sps, n_features) of float32; the feature vectors for each SP
            ys: ndarray(n_sps) of int32; the true labels of each SP
            return_preds: bool
        Returns:
            acc: float; accuracy of predictions (ratio of correctly predicted datapoints)
            (OPTIONAL if return_preds is True) ys_pred: ndarray(n_sps,) of i32
        '''
        assert np.amax(ys) < self.n_cats
        ys_pred = self.predict(xs, return_probs=False, verbose=verbose)
        acc = np.count_nonzero(ys_pred == ys)/float(ys.shape[0])
        if return_preds:
            return acc, ys_pred
        else:
            return acc


    # PRIVATE

    def _pretrain_leo_model(self, pretraining_data):
        '''
        Pretrains LEO model and saves model to checkpoint.
        Pretrain with multiple batches of independent segment feature vectors and corresponding labels.
        Each batch uses a different label category set. 
        Parameters:
            pretraining_data: tuple(
                xss_train: list(n_train_batches) of ndarray(n_segs, n_features) of float32
                yss_train: list(n_train_batches) of ndarray(n_segs) of int32
                xss_val: list(n_val_batches) of ndarray(n_segs, n_features) of float32
                yss_val: list(n_val_batches) of ndarray(n_segs) of int32
            )
        '''
        assert self.tf_session_pred is None

        # pretraining session and graph construction
        outer_model_config = leo_config.get_outer_model_config()
        tf.logging.info("outer_model_config: {}".format(outer_model_config))
        (train_op, global_step, metatrain_accuracy, metavalid_accuracy, _) = \
                                      self._leo_construct_graph(outer_model_config, pretraining_data)

        num_steps_limit = outer_model_config["num_steps_limit"]
        best_metavalid_accuracy = 0.
        with tf.train.MonitoredTrainingSession(checkpoint_dir=LEO_PRETRAIN_CHECKPOINT_PATH,
                                               save_summaries_steps=LEO_PRETRAIN_CHECKPOINT_INTERVAL_N_STEPS,
                                               log_step_count_steps=LEO_PRETRAIN_CHECKPOINT_INTERVAL_N_STEPS,
                                               save_checkpoint_steps=LEO_PRETRAIN_CHECKPOINT_INTERVAL_N_STEPS,
                                               summary_dir=LEO_PRETRAIN_CHECKPOINT_PATH) as sess:
            global_step_ev = sess.run(global_step)
            while global_step_ev < num_steps_limit:
                if global_step_ev % LEO_PRETRAIN_CHECKPOINT_INTERVAL_N_STEPS == 0:
                    # Just after saving checkpoint, calculate accuracy 10 times and save
                    # the best checkpoint for early stopping.
                    metavalid_accuracy_ev = leo_utils.evaluate_and_average(sess, metavalid_accuracy, 10)
                    tf.logging.info("Time: {}".format(time.time()))
                    tf.logging.info("Step: {} meta-valid accuracy: {}".format(global_step_ev, metavalid_accuracy_ev))

                    if metavalid_accuracy_ev > best_metavalid_accuracy:
                        leo_utils.copy_checkpoint(LEO_PRETRAIN_CHECKPOINT_PATH, global_step_ev, metavalid_accuracy_ev)
                        best_metavalid_accuracy = metavalid_accuracy_ev
  
                _, global_step_ev, metatrain_accuracy_ev = sess.run([train_op, global_step, metatrain_accuracy])
                if global_step_ev % (LEO_PRETRAIN_CHECKPOINT_INTERVAL_N_STEPS // 2) == 0:
                    tf.logging.info("Time: {}".format(time.time()))
                    tf.logging.info("Step: {} meta-train accuracy: {}".format(global_step_ev, metatrain_accuracy_ev))


    def _load_pretrained_leo_model(self, checkpoint_dir):
        '''
        Restores the tf.train.MonitoredTrainingSession for prediction.
        Sets self.tf_session_pred, self.tf_pred_tensor. 
        '''
        assert self.tf_session_pred is None
        assert self.tf_pred_tensor is None

        # create tf graph
        outer_model_config = leo_config.get_outer_model_config()
        _, _, _, _, self.tf_pred_tensor = self._leo_construct_graph(outer_model_config, pretraining_data=None)

        leo_utils.use_as_best_checkpoint(checkpoint_dir, LEO_PRETRAIN_CHECKPOINT_PATH)
        self.tf_session_pred = tf.train.MonitoredTrainingSession(checkpoint_dir=checkpoint_dir,
                                                                 save_summaries_steps=None,
                                                                 log_step_count_steps=None,
                                                                 save_checkpoint_steps=None,
                                                                 summary_dir=checkpoint_dir)


    # PRIVATE: modified code from the original LEO runner.py - Tensorflow graph construction

    def _leo_construct_graph(self, outer_model_config, pretraining_data=None):
        '''
        Constructs the optimization graph.
        - PRETRAINING MODE: pass 'pretraining_data'
        - PREDICTION MODE: pass None as 'pretraining_data'
        Parameters:
            pretraining_data: None OR tuple(
                xss_train: list(n_train_batches) of ndarray(n_segs, n_features) of float32
                yss_train: list(n_train_batches) of ndarray(n_segs) of int32
                xss_val: list(n_val_batches) of ndarray(n_segs, n_features) of float32
                yss_val: list(n_val_batches) of ndarray(n_segs) of int32
            );
        Returns:
            tf Tensors: train_op, global_step, metatrain_accuracy, metavalid_accuracy, all_preds
        '''
        inner_model_config = leo_config.get_inner_model_config()
        tf.logging.info("inner_model_config: {}".format(inner_model_config))
        leo = leo_model.LEO(inner_model_config, use_64bits_dtype=False)

        # define input tensors
        if pretraining_data is None:
            # PREDICTION MODE
            metatrain_batch = self._leo_construct_prediction_input(self.batch_size_inner_train,\
                                                        self.batch_size_inner_val_pred)
        else:
            # PRETRAINING MODE
            metatrain_batch = self._leo_construct_pretrain_batch(self.batch_size_meta_train, "train",\
                self.batch_size_inner_train, self.batch_size_inner_val_pretrain, pretraining_data)

        # construct training graph subparts: training loss/acc, gradients for clipping
        metatrain_loss, metatrain_accuracy, _ = self._leo_construct_loss_and_accuracy(leo, metatrain_batch, True)

        metatrain_gradients, metatrain_variables = leo.grads_and_vars(metatrain_loss)

        # Avoids NaNs in summaries. TODO delete?
        metatrain_loss = tf.cond(tf.is_nan(metatrain_loss),
                                 lambda: tf.zeros_like(metatrain_loss),
                                 lambda: metatrain_loss)

        # construct training graph subparts: clip gradients
        metatrain_gradients = self._leo_clip_gradients(metatrain_gradients, \
            outer_model_config["gradient_threshold"], outer_model_config["gradient_norm_threshold"])

        # construct training graph subparts: training summary, optimizer, global step
        self._leo_construct_training_summaries(metatrain_loss, metatrain_accuracy,
                                      metatrain_gradients, metatrain_variables)
        optimizer = tf.train.AdamOptimizer(learning_rate=outer_model_config["outer_lr"])
        global_step = tf.train.get_or_create_global_step()
        train_op = optimizer.apply_gradients(list(zip(metatrain_gradients, metatrain_variables)), global_step)

        # construct validation graph subparts
        data_config = leo_config.get_data_config()
        tf.logging.info("data_config: {}".format(data_config))

        # define input tensors for validation
        if pretraining_data is None:
            # PREDICTION MODE
            metavalid_batch = self._leo_construct_prediction_input(self.batch_size_inner_train,\
                                                        self.batch_size_inner_val_pred)
            assert self.tf_input_placeholders is None
            self.tf_input_placeholders = metavalid_batch
        else:
            # PRETRAINING MODE
            metavalid_batch = self._leo_construct_pretrain_batch(self.batch_size_meta_val, "val",\
                self.batch_size_inner_train, self.batch_size_inner_val_pretrain, pretraining_data)

        # construct validation graph subparts: loss, acc, predictions
        metavalid_loss, metavalid_accuracy, all_preds = self._leo_construct_loss_and_accuracy(leo, metavalid_batch, False)
        self._leo_construct_validation_summaries(metavalid_loss, metavalid_accuracy)

        # metatest part was removed

        return (train_op, global_step, metatrain_accuracy, metavalid_accuracy, all_preds)


    def _leo_clip_gradients(self, gradients, gradient_threshold, gradient_norm_threshold):
        """Clips gradients by value and then by norm."""
        if gradient_threshold > 0:
            gradients = [
                tf.clip_by_value(g, -gradient_threshold, gradient_threshold)
                for g in gradients
            ]
        if gradient_norm_threshold > 0:
            gradients = [
                tf.clip_by_norm(g, gradient_norm_threshold) for g in gradients
            ]
        return gradients

    # TODO remove
    def _leo_construct_validation_summaries(self, metavalid_loss, metavalid_accuracy):
        tf.summary.scalar("metavalid_loss", metavalid_loss)
        tf.summary.scalar("metavalid_valid_accuracy", metavalid_accuracy)
        # The summaries are passed implicitly by TensorFlow.

    # TODO remove
    def _leo_construct_training_summaries(self, metatrain_loss, metatrain_accuracy,
                                      model_grads, model_vars):
        tf.summary.scalar("metatrain_loss", metatrain_loss)
        tf.summary.scalar("metatrain_valid_accuracy", metatrain_accuracy)
        for g, v in zip(model_grads, model_vars):
            histogram_name = v.name.split(":")[0]
            tf.summary.histogram(histogram_name, v)
            histogram_name = "gradient/{}".format(histogram_name)
            tf.summary.histogram(histogram_name, g)

    def _leo_construct_pretrain_batch(self, batch_size, split, num_tr_examples_per_class,
                                  num_val_examples_per_class, pretraining_data):
        '''
        Constructs training data interface to Tensorflow graph.
        Paramters:
            batch_size: int
            split: str
            num_tr_examples_per_class, num_val_examples_per_class: int
            pretraining_data: tuple(
                xss_train: list(n_train_batches) of ndarray(n_segs, n_features) of float32
                yss_train: list(n_train_batches) of ndarray(n_segs) of int32
                xss_val: list(n_val_batches) of ndarray(n_segs, n_features) of float32
                yss_val: list(n_val_batches) of ndarray(n_segs) of int32
            );
        Returns:
            task_batch: [
                tf.Tensor 'shuffle_batch_2:0' shape=(meta_batch_size, n_categories, n_samples, n_features) of fl32,
                tf.Tensor 'shuffle_batch_2:1' shape=(meta_batch_size, n_categories, n_samples, 1) of i32,
                tf.Tensor 'shuffle_batch_2:2' shape=(meta_batch_size, n_categories, n_samples, ) of str,
                tf.Tensor 'shuffle_batch_2:3' shape=(meta_batch_size, n_categories, n_samples, n_features) of fl32,
                tf.Tensor 'shuffle_batch_2:4' shape=(meta_batch_size, n_categories, n_samples, 1) of i32,
                tf.Tensor 'shuffle_batch_2:5' shape=(meta_batch_size, n_categories, n_samples, ) of str
            ] (multithreaded)
        '''
        assert split in ["train", "val"]
        xss_train, yss_train, xss_val, yss_val = pretraining_data
        xss_split = xss_train if split == 'train' else xss_val
        yss_split = yss_train if split == 'train' else yss_val
        n_samples = num_tr_examples_per_class + num_val_examples_per_class

        # NESTED FUNC
        def _generate_pretrain_batch_npy():
            '''
            Generates a pretraining random dataset which represents a single binary labeling session.
            Using from outside:
                n_samples: int
                xss_split: list(n_batches) of ndarray(n_segs, n_features) of float32
                yss_split: list(n_batches) of ndarray(n_segs) of int32
            Returns:
                xs: ndarray(n_categories=2, n_samples, n_features) of fl32
                ys: ndarray(n_categories=2, n_samples, 1) of int32
                infos: ndarray(n_categories=2, n_samples) of <U? (unicode type)
            '''
            assert len(xss_split) == len(yss_split)
            batch_idx = np.random.randint(len(xss_split))
            xss = xss_split[batch_idx]
            yss = yss_split[batch_idx]
            n_labels = np.amax(yss)+1
            assert n_labels > 1
            cats = np.random.choice(n_labels, size=2, replace=False)
            cat_all_sample_idxs = [np.where(yss == cat)[0] for cat in cats]   # list(2) of idx arrays
            cat_sample_idxs = [np.random.choice(idx_arr, n_samples) for idx_arr in cat_all_sample_idxs]
            cat_sample_idxs = np.stack(cat_sample_idxs, axis=0)   # (n_categories=2, n_samples)
            xs = xss[cat_sample_idxs,:].astype(np.float32, copy=False)
            ys = yss[cat_sample_idxs,None].astype(np.int32, copy=False)
            ys_m0 = ys == cats[0]
            assert np.all(ys[~ys_m0] == cats[1])
            ys[:] = 1
            ys[ys_m0] = 0
            infos = [['b' + str(batch_idx) + 'c' + str(cats[cat_idx]) + 's' + str(cat_sample_idxs[cat_idx, sample_idx])\
                                         for sample_idx in range(cat_sample_idxs.shape[1])] for cat_idx in range(len(cats))]
            infos = np.array(infos, dtype='U12')   # TODO replace this and line above with line below if no info is needed
            #infos = np.zeros_like(ys[:,:,0], dtype='U0')
            assert xs.shape == (2, n_samples, xss_split[0].shape[-1])  # TODO temp, can remove
            assert ys.shape == (2, n_samples, 1)  # TODO temp, can remove
            assert infos.shape == (2, n_samples)  # TODO temp, can remove
            # print("BATCH_GEN rlseg>>>")
            # print("    xs sh:", xs.shape)
            # print("    ys sh:", ys.shape)
            # print("    xs mean:", np.mean(xs))
            # print("    xs std:", np.std(xs))
            # print("    xs min:", np.amin(xs))
            # print("    xs max:", np.amax(xs))
            # print("    ys mean:", np.mean(ys))
            # print("    ys min:", np.amin(ys))
            # print("    ys max:", np.amax(ys))
            #print("  EXAMPLES>>>")
            #print("    xs:", xs[:,:5,:])
            #print("    ys:", ys[:,:5,0])
            return xs, ys, infos
        # NESTED FUNC END

        output_list = tf.py_func(_generate_pretrain_batch_npy, [],
                                 [tf.float32, tf.int32, tf.string])   # convert numpy arrays to tensors
        instance_input, instance_output, instance_info = output_list
        instance_input = tf.nn.l2_normalize(instance_input, axis=-1)  # normalize embeddings
        instance_info = tf.regex_replace(instance_info, "\x00*", "")  # remove null terminator if present from paths

        # USE DEBUG WITH single thread
        # tf.logging.info("input_batch: {} ".format(instance_input.shape))
        # tf.logging.info("output_batch: {} ".format(instance_output.shape))
        # tf.logging.info("info_batch: {} ".format(instance_info.shape))

        split_sizes = [num_tr_examples_per_class, num_val_examples_per_class]
        tr_input, val_input = tf.split(instance_input, split_sizes, axis=1)   # split training/validation
        tr_output, val_output = tf.split(instance_output, split_sizes, axis=1)
        tr_info, val_info = tf.split(instance_info, split_sizes, axis=1)
        
        # tf.logging.info("tr_output: {} ".format(tr_output))
        # tf.logging.info("val_output: {}".format(val_output))

        tr_output = tf.identity(tr_output)              # TODO do we need this? input & output have the same type
        val_output = tf.identity(val_output)

        single_problem_instance = (tr_input, tr_output, tr_info, val_input, val_output, val_info)

        tr_data_shape = (self.n_leo_cats, num_tr_examples_per_class)
        val_data_shape = (self.n_leo_cats, num_val_examples_per_class)
        num_threads = 10   # set to 1 if displaying debug info with tf.logging.info
        task_batch = tf.train.shuffle_batch(single_problem_instance, batch_size=batch_size,
                            capacity=1000, min_after_dequeue=0, enqueue_many=False,
                            shapes=[tr_data_shape + (self.n_features,), tr_data_shape + (1,), tr_data_shape,
                                    val_data_shape + (self.n_features,), val_data_shape + (1,), val_data_shape],
                            num_threads=num_threads)

        return task_batch

    def _leo_construct_prediction_input(self, n_tr_examples_per_class, n_val_examples_per_class):
        '''
        Constructs prediction data interface to Tensorflow graph.
        Paramters:
            n_tr_examples_per_class, n_val_examples_per_class: int
        Returns:
            pred_batch: [
                tf.Tensor(batch_size, num_classes, n_tr_samples, n_features) of fl32,
                tf.Tensor(batch_size, num_classes, n_tr_samples, 1) of i32,
                tf.Tensor(batch_size, num_classes, n_tr_samples) of str,
                tf.Tensor(batch_size, num_classes, n_val_samples, n_features) of fl32,
                tf.Tensor(batch_size, num_classes, n_val_samples, 1) of i32,
                tf.Tensor(batch_size, num_classes, n_val_samples) of str
            ]
        '''

        # TODO can we use None in the placeholders for 'n_samples'?

        tr_input = tf.placeholder(tf.float32, shape=(1, self.n_leo_cats, n_tr_examples_per_class, self.n_features))
        tr_output = tf.placeholder(tf.int32, shape=(1, self.n_leo_cats, n_tr_examples_per_class, 1))
        tr_info = tf.placeholder(tf.string, shape=(1, self.n_leo_cats, n_tr_examples_per_class))
        val_input = tf.placeholder(tf.float32, shape=(1, self.n_leo_cats, n_val_examples_per_class, self.n_features))
        val_output = tf.placeholder(tf.int32, shape=(1, self.n_leo_cats, n_val_examples_per_class, 1))
        val_info = tf.placeholder(tf.string, shape=(1, self.n_leo_cats, n_val_examples_per_class))

        tr_input = tf.nn.l2_normalize(tr_input, axis=-1)  # normalize embeddings
        val_input = tf.nn.l2_normalize(val_input, axis=-1)  # normalize embeddings
        pred_batch = (tr_input, tr_output, tr_info, val_input, val_output, val_info)
        print("PLACEHOLDERS: ", pred_batch)

        return pred_batch


    def _leo_construct_loss_and_accuracy(self, inner_model, inputs, is_meta_training):
        """Returns batched loss and accuracy of the model ran on the inputs."""
        call_fn = functools.partial(
            inner_model.__call__, is_meta_training=is_meta_training)
        per_instance_loss, per_instance_accuracy, all_preds = tf.map_fn(
            call_fn,
            inputs,
            dtype=(tf.float32, tf.float32, tf.float32),
            back_prop=is_meta_training)
        loss = tf.reduce_mean(per_instance_loss)
        accuracy = tf.reduce_mean(per_instance_accuracy)
        return loss, accuracy, all_preds
