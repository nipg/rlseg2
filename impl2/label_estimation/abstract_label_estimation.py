
#
# Abstract label estimation interface. RLseg2 implementation #2
#
#   @author Viktor Varga
#

import os
import numpy as np

class LabelEstimation:

    '''
    Member fields:

    '''

    def __init__(self, seg, n_labels):
        raise NotImplementedError

    def __str__(self):
        raise NotImplementedError

    def get_name(self):
        '''
        Returns:
            name: str
        '''
        return "UnknownLabelEstimation"

    def pretrain(self, xs_train, ys_train, xs_val, ys_val):
        '''
        Pretrain with independent segment feature vectors and corresponding labels.
        Paramters:
            xs_train: ndarray(n_segs, n_features) of float32
            ys_train: ndarray(n_segs) of int32
            xs_val: ndarray(n_val_segs, n_features) of float32
            ys_val: ndarray(n_val_segs) of int32
        '''
        pass

    def pretrain_mutliple_label_setups(self, xss_train, yss_train, xss_val, yss_val):
        '''
        Pretrain with multiple batches of independent segment feature vectors and corresponding labels.
        Each batch uses a different label category set. 
        Paramters:
            xss_train: list(n_train_batches) of ndarray(n_segs, n_features) of float32
            yss_train: list(n_train_batches) of ndarray(n_segs) of int32
            xss_val: list(n_val_batches) of ndarray(n_segs, n_features) of float32
            yss_val: list(n_val_batches) of ndarray(n_segs) of int32
        '''
        pass

    def pretrain_graphs(self, train_dataloader, val_dataloader):
        '''
        Pretrain with graphs and corresponding node label arrays.
        Paramters:
            train_dataloader: Pytorch DataLoader instance
            val_dataloader: Pytorch DataLoader instance
        '''
        pass

    def pretrain_unsupervised(self, xs):
        '''
        Pretrain with independent segment feature vectors in an unsupervised fashion.
        Paramters:
            xs: ndarray(n_segs, n_features) of float32
        '''
        pass

    def fit(self, user_annot):
        '''
        Parameters:
            user_annot: ndarray(n_segs_in_user_annot, 2:[SP id, label])
        '''
        raise NotImplementedError

    def predict_all(self, return_probs=False):
        '''
        Parameters:
            return_probs: bool
        Returns:
            ys_pred: ndarray(n_sps, n_cat) of fl32 (IF return_probs == True)
                     ndarray(n_sps,) of i32        (IF return_probs == False)
        '''
        raise NotImplementedError
