
#
# MRF label estimation. RLseg2 implementation #2
#   Segment labels are estimated and propagated with a graph based MRF.
#
#   @author Viktor Varga
#

import os
import numpy as np
from .abstract_label_estimation import LabelEstimation
import util.imutil as ImUtil

import sys
sys.path.append('/home/vavsaai/git/pyGCO/')
import gco
from gco import GCO

PAIRWISE_POT_WEIGHT = 1.
OPTFLOW_WEIGHT = 10.
MRF_N_ITER = 1

class MRFLabelEstimation(LabelEstimation):

    '''
    EVALUATION on video 'bear':
        basic sklearn logreg with l2reg == 2., tol == 1e-4, max_iter == 1000
        IoU acc for n_labels x [1, 2, 5, 10, 20, 50, 100, 200, 500] SP labels revealed
        unary: 0 OR 1, binary: 1 - eye, weights: 0, n_iter: 1
        all gco input data types are int32 OR all are float32 (same results), down_weight disabled
            [0.26504834 0.39907599 0.49096074 0.62110569 0.71185408 0.78572558 0.81698328 0.83959158 0.86297543]
        weight changes: 
            1: does not work (mostly estimating same label)
            0.01: - same as 0
            0.05: [0.26439649 0.40070682 0.49299998 0.6246729  0.71639192 0.79210843 0.82071933 0.8422547  0.86601925]
            0.1: [0.25589483 0.41783406 0.51678343 0.67632733 0.78185167 0.84491034 0.84294994 0.86049589 0.88589904]
            0.3: [0.24568574 0.43940165 0.56901091 0.74333807 0.81687255 0.8723275 0.86350075 0.87458926 0.8987337 ]
            0.5: [0.24269556 0.41833898 0.46040317 0.75098994 0.81480181 0.87374751 0.86665741 0.87407143 0.8958972 ]
        weight changes: beta*(1*spatial + 1*temporal + 10*optflow), beta ->
            0.05: [0.26122588 0.4104252  0.50575006 0.65033717 0.74388677 0.82570764 0.83853186 0.85810821 0.88462074]
            0.1: [0.25496616 0.42229538 0.52791201 0.6914158  0.79992117 0.85717599 0.85598513 0.87052651 0.89961657]
            0.2: [0.24670877 0.42800367 0.55057648 0.72257419 0.81451318 0.87189423 0.8646191  0.88062807 0.90775016]
            0.3: [0.24394788 0.44139292 0.5869625  0.74666811 0.81767309 0.88011842 0.86841496 0.88288392 0.90780185]
            0.4: [0.24055244 0.43073295 0.54187223 0.76196878 0.81500768 0.88156732 0.87283798 0.87930245 0.90763557]
            0.5: [0.24406634 0.42522242 0.5326401  0.76571449 0.81361393 0.88165743 0.87393285 0.88436567 0.90646084]
        unaries for GT SPs are set to zero (if matching) and 10. (if not matching), previous weight config, beta ->
            0.3: [0.24394788 0.44340227 0.58697894 0.74666811 0.81769225 0.88078772 0.86908211 0.88359397 0.91007593]
        use continuous unaries: clipped -np.log(ys_pred_probabilities)*rho (beta == 0.3); rho ->
            1: does not work (too strong smoothing)

    '''


    '''
    Member fields:
        seg: Segmentation instance
        label_model: LabelModel subclass instance

        seg_feature_key: str
        n_labmodel_features: int
        n_labels: int

        user_annots: ndarray(n_sps_annotated, 2:[SP ids, labels])

        graph_edges: ndarray(n_all_edges, 2:[ID_from, ID_to]) of int; only valid edges (no -1)
        graph_weights: ndarray(n_all_edges,) of int (integer is required for gco)
    '''

    def __init__(self, seg, seg_feature_key, n_labels, label_model, n_labmodel_features):
        self.seg = seg
        self.n_labels = n_labels
        self.seg_feature_key = seg_feature_key
        fvecs = self.seg.sp_chans[seg_feature_key]
        assert fvecs.ndim == 2
        assert n_labmodel_features <= fvecs.shape[1]
        self.n_labmodel_features = n_labmodel_features

        self.label_model = label_model
        self.label_model.reset(self.n_labmodel_features, self.n_labels)

        self.user_annots = None
        self._create_edge_lists()

    def __str__(self):
        return "MRF_" + str(self.label_model)

    def fit(self, user_annot):
        '''
        Parameters:
            user_annot: ndarray(n_labeled_segs, 2:[SP id, label])
        '''
        self.label_model.reset(self.n_labmodel_features, self.n_labels)
        self.user_annots = user_annot   # (n_train_sps, 2)
        print("INPUT ->", self.user_annots.shape)
        print("    -->", self.user_annots.T)
        train_xs = self.seg.sp_chans[self.seg_feature_key][self.user_annots[:,0], :self.n_labmodel_features]   # (n_train_sps, n_labmodel_features)
        self.label_model.fit(train_xs, self.user_annots[:,1])

    def predict_all(self, return_probs=False, verbose=True):
        '''
        Parameters:
            return_probs: bool
        Returns:
            ys_pred: ndarray(n_sps, n_cat) of fl32 (IF return_probs == True)
                     ndarray(n_sps,) of i32        (IF return_probs == False)
        '''
        # TEMP TOY EXAMPLE: 2 labels - 1,2,3 vs 0,4,5
        '''
        edge_list = np.array([[0,2], [1,2], [1,3], [2,5], [3,5], [4,5]])
        edge_weights = np.array([0., 0., 0., 0., 0., 0.])
        unary = np.array([[1,0.], [0.,1.], [0.,1.], [0.,1.], [1.,0.], [1.,0.]])  # (n_nodes=6, n_labels=2)
        pairwise = 1. - np.eye(2)
        ys_pred = self._cut_general_graph_mod(edge_list, edge_weights, unary, pairwise,\
                                              n_iter=100, verbose=verbose)
        print(ys_pred)
        assert False, "END"
        '''
        
        # TEMP TOY EXAMPLE END

        assert not return_probs, "MRF does not support probability prediction."
        xs_to_pred = self.seg.sp_chans[self.seg_feature_key][:,:self.n_labmodel_features]   # (n_sps, n_labmodel_features)
        ys_pred = self.label_model.predict(xs_to_pred, return_probs=True)
        assert np.amin(ys_pred) >= 0.
        assert np.amax(ys_pred) <= 1.

        # compute unary potentials, set GT SP potentials to inf and zero. 
        #ys_unary = -np.log(ys_pred)*10.
        #ys_unary = np.clip(ys_unary, 0., 10.).astype(np.int32).astype(np.float32)

        #ys_unary[self.user_annots[:,0], :] = np.inf
        #ys_unary[self.user_annots[:,0], self.user_annots[:,1]] = 0.  # (n_sps, n_labels)
        #ys_unary = np.clip(ys_unary, 0., 100.)

        # TEMP: only 0 and 1 unaries
        ys_unary = np.ones((ys_pred.shape[0], 2), dtype=np.float32)
        ys_unary[np.arange(ys_unary.shape[0]), np.argmax(ys_pred, axis=1)] = 0
        # TEMP END

        # set unary potentials for GT SPs
        ys_unary[self.user_annots[:,0], :] = 10.
        ys_unary[self.user_annots[:,0], self.user_annots[:,1]] = 0.  # (n_sps, n_labels)
        ys_unary = ys_unary.astype(np.float32)

        #pairwise_pot = (PAIRWISE_POT_WEIGHT*(1. - np.eye(ys_pred.shape[-1])))   # (n_labels, n_labels)
        pairwise_pot = (PAIRWISE_POT_WEIGHT*(1 - np.eye(2, dtype=np.float32)))  # (n_labels, n_labels)

        if verbose:
            print("    MRF input stats > Unary shape, mean, std, min, max: ",\
                         ys_unary.shape, np.mean(ys_unary, axis=0), np.std(ys_unary), np.amin(ys_unary), np.amax(ys_unary))
            print("    MRF input stats > Pairwise shape, mean, std, min, max: ",\
                         pairwise_pot.shape, np.mean(pairwise_pot), np.std(pairwise_pot),\
                         np.amin(pairwise_pot), np.amax(pairwise_pot))
            print("    MRF input stats > Weights shape, mean, std, min, max: ",\
                         self.graph_weights.shape, np.mean(self.graph_weights), np.std(self.graph_weights),\
                         np.amin(self.graph_weights), np.amax(self.graph_weights))

        #gw = np.full_like(self.graph_weights, dtype=np.float32, fill_value=0.5)
        ys_pred = self._cut_general_graph_mod(self.graph_edges, self.graph_weights*0.3, ys_unary, pairwise_pot,\
                                              n_iter=MRF_N_ITER, verbose=verbose)
        assert ys_pred.shape == (ys_unary.shape[0],)
        return ys_pred


    # PRIVATE

    def _create_edge_lists(self):
        '''
        Sets self.graph_edges, self.graph_weights.
        '''
        spatial_edges = self.seg.sp_edges['spatial_edges']
        temporal_edges = self.seg.sp_edges['temporal_edges']
        flow_edges = self.seg.sp_edges['flow_edges_mrf']
        flow_weights = self.seg.sp_edge_chans['flow_edges_mrf']

        merge_fun = lambda ews: ews[:,0] + ews[:,1] + OPTFLOW_WEIGHT*ews[:,2]
        graph_edges, graph_weights = ImUtil.merge_edge_lists([spatial_edges, temporal_edges, flow_edges],\
                           [np.ones((spatial_edges.shape[0],),  dtype=np.float32),\
                            np.ones((temporal_edges.shape[0],),  dtype=np.float32),\
                            flow_weights.reshape(-1)], np.zeros((3,), dtype=np.float32), fun_weight_merge=merge_fun)
        valid_edges = np.all(graph_edges >= 0, axis=1)   # (n_edges)
        self.graph_edges = graph_edges[valid_edges]
        self.graph_weights = graph_weights[valid_edges]


    def _cut_general_graph_mod(self, edges, edge_weights, unary_cost, pairwise_cost,
                      n_iter=-1, algorithm='expansion', verbose=False):
        #
        # Params, Returns: see gco.cut_general_graph()
        #
        _int_types = [np.int, np.intc, np.int32, np.int64, np.longlong]
        _float_types = [np.float, np.float32, np.float64, np.float128]

        energy_is_int = all(dt in _int_types for dt in [unary_cost.dtype, edge_weights.dtype, pairwise_cost.dtype])
        energy_is_float = all(dt in _float_types for dt in [unary_cost.dtype, edge_weights.dtype, pairwise_cost.dtype])
        assert energy_is_int or energy_is_float

        if verbose:
            print("    MRF > Dtype edge_weights: ", edge_weights.dtype)
            print("    MRF > Dtype unary_cost: ", unary_cost.dtype)
            print("    MRF > Dtype pairwise_cost: ", pairwise_cost.dtype)
            print("    MRF > Energy is float: ", energy_is_float)

        n_sites, n_labels = unary_cost.shape

        _SMALL_CONSTANT = 1e-10
        down_weight_factor = max(np.abs(unary_cost).max(),
                                 np.abs(edge_weights).max() *
                                 pairwise_cost.max()) + _SMALL_CONSTANT
        down_weight_factor = 1.
        if verbose:
            print("    MRF > Downweight factor: ", down_weight_factor)

        gc = GCO()
        gc.create_general_graph(n_sites, n_labels, energy_is_float)
        gc.set_data_cost(unary_cost / down_weight_factor)
        gc.set_all_neighbors(edges[:, 0], edges[:, 1],
                             edge_weights / down_weight_factor)
        if pairwise_cost is not None:
            gc.set_smooth_cost(pairwise_cost)

        # initialize labels
        # if init_labels is not None:
        #     for i in range(n_sites):
        #         gc.init_label_at_site(i, init_labels[i])

        if algorithm == 'expansion':
            gc.expansion(n_iter)
        else:
            gc.swap(n_iter)

        if verbose:
            print("    MRF > Unary energy: ", gc.compute_data_energy())
            print("    MRF > Binary energy: ", gc.compute_smooth_energy())

        labels = gc.get_labels()
        gc.destroy_graph()

        if verbose:
            u_labels, c_labels = np.unique(labels, return_counts=True)
            print("    MRF > Prediction labels & counts: ", u_labels, c_labels)

        return labels