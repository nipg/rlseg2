
#
# Abstract label model. RLseg2 implementation #1.
#
#   @author Viktor Varga
#


class LabelModel:

    '''
    Member fields:

    '''

    def __init__(self, wrapped_model=None):
        raise NotImplementedError

    def destroy(self):
        pass

    def __str__(self):
        raise NotImplementedError

    def reset(self, n_features, n_cats):
        '''
        Reinitialize model.
        Parameters:
            n_features: int; input shape to model is (n_features,)
            n_cats: int; number of categories to classify into
        '''
        raise NotImplementedError


    def pretrain_unsupervised(self, xss):
        '''
        May be called before training or evaluation, to use unsupervised techniques to adapt to dataset.
        Parameters:
            xss: ndarray(n_sps, n_features_total) of float32; the feature vectors for each SP
        '''
        pass

    def pretrain_mutliple_label_setups(self, xss_train, yss_train, xss_val, yss_val):
        '''
        Pretrain with multiple batches of independent segment feature vectors and corresponding labels.
        Each batch uses a different label category set. 
        Parameters:
            xss_train: list(n_train_batches) of ndarray(n_segs, n_features) of float32
            yss_train: list(n_train_batches) of ndarray(n_segs) of int32
            xss_val: list(n_val_batches) of ndarray(n_segs, n_features) of float32
            yss_val: list(n_val_batches) of ndarray(n_segs) of int32
        '''
        pass
        
    def fit(self, xs, ys, verbose=False):
        '''
        Parameters:
            xs: ndarray(n_sps, n_features_total) of float32; the feature vectors for each SP
            ys: ndarray(n_sps) of int32; the true labels of each SP
        '''
        raise NotImplementedError

    def predict(self, xs, return_probs=False, verbose=False):
        '''
        Parameters:
            xs: ndarray(n_sps, n_features_total) of float32; the feature vectors for each SP
            return_probs: bool;
        Returns:
            ys_pred: ndarray(n_sps, n_cat) of fl32 (IF return_probs == True)
                     ndarray(n_sps,) of i32        (IF return_probs == False)

        '''
        raise NotImplementedError

    def evaluate(self, xs, ys, return_preds=False, verbose=False):
        '''
        Parameters:
            xs: ndarray(n_sps, n_features_total) of float32; the feature vectors for each SP
            ys: ndarray(n_sps) of int32; the true labels of each SP
            return_preds: bool
        Returns:
            acc: float; accuracy of predictions (ratio of correctly predicted datapoints)
            (OPTIONAL if return_preds is True) ys_pred: ndarray(n_sps,) of i32
        '''
        raise NotImplementedError



