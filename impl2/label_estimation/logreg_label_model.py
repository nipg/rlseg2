
import numpy as np
from sklearn.linear_model import LogisticRegression

from .abstract_label_model import LabelModel

PARAM_MAX_ITER = 3000  # default is 100
PARAM_TOL = 1e-4  # default is 1e-4
PARAM_REGULARIZER_WEIGHT = 2.  # default is 1.

class LogRegLabelModel(LabelModel):

    '''
    Member fields:
        n_features: int; input shape to model is (n_features,)
        n_cats: int; number of categories to classify into

        model: sklearn.linear_model.LogisticRegression
    '''

    def __init__(self, wrapped_model=None):
        self.n_features = None
        self.n_cats = None
        assert wrapped_model is None
        print("TODO, review logreg solver; default is changed from liblinear to lbfgs (original setup: liblinear with 'ovr' multi_class arg)")

    def __str__(self):
        return "LogReg"

    def reset(self, n_features, n_cats):
        '''
        Reinitialize model.
        '''
        self.n_features = n_features
        self.n_cats = n_cats
        self.model = LogisticRegression(max_iter=PARAM_MAX_ITER, tol=PARAM_TOL, C=PARAM_REGULARIZER_WEIGHT, solver='liblinear', multi_class='ovr')


    def fit(self, xs, ys, verbose=False):
        '''
        Parameters:
            xs: ndarray(n_sps, n_features) of float32; the feature vectors for each SP
            ys: ndarray(n_sps) of int32; the true labels of each SP
        '''
        assert xs.shape[1:] == (self.n_features,)
        assert np.amax(ys) < self.n_cats
        self.model.fit(xs, ys)

    def predict(self, xs, return_probs=False, verbose=False):
        '''
        Parameters:
            xs: ndarray(n_sps, n_features) of float32; the feature vectors for each SP
            return_probs: bool;
        Returns:
            ys_pred: ndarray(n_sps, n_cat) of fl32 (IF return_probs == True)
                     ndarray(n_sps,) of i32        (IF return_probs == False)
        '''
        assert xs.shape[1:] == (self.n_features,)
        if return_probs:
            ys_pred = self.model.predict_proba(xs)
        else:
            ys_pred = self.model.predict(xs)
        return ys_pred

    def evaluate(self, xs, ys, return_preds=False, verbose=False):
        '''
        Parameters:
            xs: ndarray(n_sps, n_features) of float32; the feature vectors for each SP
            ys: ndarray(n_sps) of int32; the true labels of each SP
            return_preds: bool
        Returns:
            acc: float; accuracy of predictions (ratio of correctly predicted datapoints)
            (OPTIONAL if return_preds is True) ys_pred: ndarray(n_sps,) of i32
        '''
        assert np.amax(ys) < self.n_cats
        ys_pred = self.predict(xs, return_probs=False, verbose=verbose)
        acc = np.count_nonzero(ys_pred == ys)/float(ys.shape[0])
        if return_preds:
            return acc, ys_pred
        else:
            return acc





