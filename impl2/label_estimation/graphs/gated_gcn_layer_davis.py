import numpy as np

import torch
import torch.nn as nn
import torch.nn.functional as F

"""
    ResGatedGCN: Residual Gated Graph ConvNets
    An Experimental Study of Neural Networks for Variable Graphs (Xavier Bresson and Thomas Laurent, ICLR 2018)
    https://arxiv.org/pdf/1711.07553v2.pdf
"""

class GatedGCNLayer_custom_DAVIS(nn.Module):
    """
    MODIFIED:
        - Native multi-class support enabled by passing an int as 'p_dim'
        - 3 independent p vectors can be passed to the layer, p1 is propagated to output only
        - If p_node_features is enabled, the 'h' representation in each layer is concatenated with 
                the maximum item in p1,p2,p3 vectors and (p1,p2,p3)[..., 0] (the probability of the background labels)
                 -> 6 extra features.
    """
    def __init__(self, input_dim, output_dim, dropout, graph_norm, batch_norm, residual, norm_adj_att_scores, \
                       p_node_features, p_dim=None, p_rescale=True, w_mlp=False):
        '''
        Parameters:
            input_dim, output_dim: int
            dropout: float
            graph_norm, batch_norm, residual, norm_adj_att_scores, p_node_features: bool
        '''
        super().__init__()
        assert input_dim == output_dim  # assert condition was added
        self.in_channels = input_dim
        self.out_channels = output_dim
        self.h_channels = input_dim+6 if ((p_node_features is True) and (p_dim is not None)) else input_dim
        self.dropout = dropout
        self.graph_norm = graph_norm
        self.batch_norm = batch_norm
        self.residual = residual
        self.norm_adj_att_scores = norm_adj_att_scores
        self.p_node_features = p_node_features
        assert (p_dim is None) or (p_dim >= 2)
        self.p_dim = p_dim
        self.p_rescale = p_rescale
        self.w_mlp = w_mlp
        
        if input_dim != output_dim:
            self.residual = False
        
        self.A = nn.Linear(self.h_channels, output_dim, bias=True)
        self.B = nn.Linear(self.h_channels, output_dim, bias=True)
        self.D = nn.Linear(self.h_channels, output_dim, bias=True)
        self.E = nn.Linear(self.h_channels, output_dim, bias=True)
        self.bn_node_h = nn.BatchNorm1d(output_dim)
        self.C = nn.Linear(input_dim, output_dim, bias=True)
        self.bn_node_e = nn.BatchNorm1d(output_dim)
        if p_dim is not None:
            if self.w_mlp:
                self.q1 = MLP_w(self.h_channels, output_dim)
                self.q2 = MLP_w(self.h_channels, output_dim)
                self.q3 = MLP_w(self.h_channels, output_dim)
                self.z = MLP_w(self.h_channels, output_dim)
            else:
                self.q1 = nn.Linear(self.h_channels, 1, bias=True)
                self.q2 = nn.Linear(self.h_channels, 1, bias=True)
                self.q3 = nn.Linear(self.h_channels, 1, bias=True)
                self.z = nn.Linear(output_dim, 1, bias=True)

    def message_func(self, edges):
        Bh_j = edges.src['Bh']
        e_ij = edges.data['Ce'] +  edges.src['Dh'] + edges.dst['Eh'] # e_ij = Ce_ij + Dhi + Ehj
        edges.data['e'] = e_ij

        if self.p_dim is not None:
            p1_j = edges.src['p1']
            return {'Bh_j' : Bh_j, 'e_ij' : e_ij, 'p1_j' : p1_j}
        else:
            return {'Bh_j' : Bh_j, 'e_ij' : e_ij}

    def reduce_func(self, nodes):
        Ah_i = nodes.data['Ah']          # (n_nodes, output_dim)
        Bh_j = nodes.mailbox['Bh_j']     # (n_nodes, n_adj, output_dim)
        e = nodes.mailbox['e_ij']        # (n_nodes, n_adj, output_dim)
        sigma_ij = torch.sigmoid(e)             # sigma_ij = sigmoid(e_ij)
        #h = Ah_i + torch.mean( sigma_ij * Bh_j, dim=1 ) # hi = Ahi + mean_j alpha_ij * Bhj 
        sb_ij = sigma_ij * Bh_j     # (n_nodes, n_adj, output_dim)

        if self.p_dim is not None:
            p1_j = nodes.mailbox['p1_j']     # (n_nodes, n_adj, p_dim)
            w1_ij = self.z(sb_ij)     # (n_nodes, n_adj, 1)
            p1_i = nodes.data['p1']     # (n_nodes, p_dim)
            p2_i = nodes.data['p2']     # (n_nodes, p_dim)
            p3_i = nodes.data['p3']     # (n_nodes, p_dim)
            w1_i = nodes.data['q1h']     # (n_nodes, 1)
            w2_i = nodes.data['q2h']     # (n_nodes, 1)
            w3_i = nodes.data['q3h']     # (n_nodes, 1)

            if self.norm_adj_att_scores:
                w_ij = w_ij/w_ij.size(1)     # (n_nodes, n_adj, 1)

            # FIXED ARCHITECTURE
            ws = torch.cat([w1_ij, w1_i[:,None,:], w2_i[:,None,:], w3_i[:,None,:]], dim=1)     # (n_nodes, n_adj+3, 1)
            ps = torch.cat([p1_j, p1_i[:,None,:], p2_i[:,None,:], p3_i[:,None,:]], dim=1)     # (n_nodes, n_adj+3, p_dim)
            if self.p_rescale:
                ws = torch.softmax(ws, dim=1)   # sum == 1 along axis#1
            p1 = torch.sum(ws*ps, dim=1)     # (n_nodes, p_dim)

            # INCORRECT ARCHITECTURE mod
            # p1 = torch.sum(w1_ij*p1_j, dim=1) + w1_i*p1_i + w2_i*p2_i + w3_i*p3_i   # (n_nodes, p_dim)
            # if self.p_rescale:
            #     p1 = torch.softmax(p1, dim=-1)
                
            # END

            h = Ah_i + torch.sum( sb_ij, dim=1 ) / ( torch.sum( sigma_ij, dim=1 ) + 1e-6 )  # hi = Ahi + sum_j eta_ij/sum_j' eta_ij' * Bhj <= dense attention       
            return {'h' : h, 'p1': p1}
        else:
            h = Ah_i + torch.sum( sb_ij, dim=1 ) / ( torch.sum( sigma_ij, dim=1 ) + 1e-6 )  # hi = Ahi + sum_j eta_ij/sum_j' eta_ij' * Bhj <= dense attention       
            return {'h' : h}
    
    def forward(self, g, h, e, snorm_n, snorm_e, p1, p2, p3):

        h_in = h # for residual connection
        e_in = e # for residual connection

        if (self.p_node_features is True) and (self.p_dim is not None):
            h = torch.cat([h, torch.max(p1, dim=-1, keepdim=True)[0], torch.max(p2, dim=-1, keepdim=True)[0], torch.max(p3, dim=-1, keepdim=True)[0],
                              p1[..., [0]], p2[..., [0]], p3[..., [0]]], dim=-1)   # torch.max returns tuple
        
        g.ndata['h']  = h
        g.ndata['Ah'] = self.A(h)
        g.ndata['Bh'] = self.B(h)
        g.ndata['Dh'] = self.D(h)
        g.ndata['Eh'] = self.E(h)
        g.edata['e']  = e 
        g.edata['Ce'] = self.C(e)

        if self.p_dim is not None:
            g.ndata['q1h'] = self.q1(h)
            g.ndata['q2h'] = self.q2(h)
            g.ndata['q3h'] = self.q3(h)
            g.ndata['p1'] = p1
            g.ndata['p2'] = p2
            g.ndata['p3'] = p3

        g.update_all(self.message_func, self.reduce_func)
        
        h = g.ndata['h'] # result of graph convolution
        e = g.edata['e'] # result of graph convolution

        if self.p_dim is not None:
            p1 = g.ndata['p1']
        
        if self.graph_norm:
            h = h* snorm_n # normalize activation w.r.t. graph size
            e = e* snorm_e # normalize activation w.r.t. graph size
        
        if self.batch_norm:
            h = self.bn_node_h(h) # batch normalization
            e = self.bn_node_e(e) # batch normalization
        
        h = F.relu(h) # non-linear activation
        e = F.relu(e) # non-linear activation
        
        if self.residual:
            h = h_in + h # residual connection
            e = e_in + e # residual connection
        
        h = F.dropout(h, self.dropout, training=self.training)
        e = F.dropout(e, self.dropout, training=self.training)

        return h, e, p1
    
    def __repr__(self):
        return '{}(in_channels={}, out_channels={})'.format(self.__class__.__name__, self.in_channels, self.out_channels)
    #

class MLP_w(nn.Module):

    def __init__(self, input_dim, hidden_dim):
        super().__init__()
        self.FC_layers = nn.ModuleList([nn.Linear(input_dim, hidden_dim, bias=True),\
                                        nn.Linear(hidden_dim, 1, bias=True)])
        
    def forward(self, x):
        y = x
        for layer in self.FC_layers[:-1]:
            y = layer(y)
            y = F.relu(y)
        y = self.FC_layers[-1](y)
        return y

    #