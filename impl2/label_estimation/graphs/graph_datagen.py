
#
# Graph training data generator for the GNN label estimation, binary & multiclass GNN setup. RLseg2 implementation #2
#   Iterator. Generates training samples from a single graph by varying labels and labeled nodes.
#
#   @author Viktor Varga
#

import sys
sys.path.append('../..')
sys.path.append('../../..')
import os
import pickle
import math
import random
import time

import numpy as np
from sklearn.linear_model import LogisticRegression   # temp for safety assert
import dgl
import torch
from torch.utils.data import IterableDataset
from torch.utils.data import DataLoader

from segmentation import Segmentation
from segmentation_labeling import SegLabeling
import util.util as Util
import util.imutil as ImUtil
import config as Config

class GraphDatagen:

    '''
    A SINGLE GraphDatagen OBJECT PER VIDEO.

    TWO MODES (train/predict), TWO LABEL CONFIGS (binary/multiclass):
        - 'train' (binary, p_dim=None): 
            infinitely generates random training samples with random label partitioning and randomly revealed node labels
        - 'train' (multiclass, p_dim=n_max_cats): 
            infinitely generates random training samples with original multiclass labels and randomly revealed node labels
        - 'predict' (binary, p_dim=None):
            finite iteration of all one vs all node partitioning setups with previously given revealed nodes
            must reset and specify revealed nodes first with reset_prediction_iterator()
            IF Config.GNNBIN_BINARY_DOUBLE_PREDICTION_ROUND: 2*n_labels iterations, then iterator becomes exhausted
            OTHERWISE: n_labels iterations, then iterator becomes exhausted
        - 'predict' (multiclass, p_dim=n_max_cats):
            generation of a single sample with previously given revealed nodes
            must reset and specify revealed nodes first with reset_prediction_iterator()
            1 iteration, then iterator becomes exhausted


    Member fields:
        seg: Segmentation instance
        label_model: None OR LabelModel subclass instance
        labmodel_feature_key: str; the SP channel key to be used as input features for the label model
        n_labmodel_features: int; number of input features for the label model

        n_labels: int; number of categories in the video (independent of whether binary of multiclass mode is used)

        true_label_node_idxs: list(n_labels) of ndarray(n_nodes_with_label) of int32; node idxs for each label
        user_annots: ndarray(n_sps_annotated, 2:[SP ids, labels]); only used in 'predict' mode

        mode: str; 'train' or 'predict';
            'train' (binary) mode generates samples with random label partition; generator does not expire
            'train' (multiclass) mode generates samples with original multiclass labels; generator does not expire
            'predict' (binary) mode generates each possible one-vs-all category label partition; generator expires
            'predict' (multiclass) mode generates a single sample; generator expires
        p_dim: None or int; if None, binary label mode is used, otherwise p_dim specifies length of label vector in multiclass mode
                                    (can be greater than actual number of labels used in video)
        iter_idx: int; idx of previous sample generated

    '''

    def __init__(self, vidname, seg, n_labels, mode, label_model, labmodel_feature_key, p_dim=None):
        self.vidname = vidname  # debug info
        self.seg = seg
        assert n_labels >= 2
        self.n_labels = n_labels
        assert mode in ['train', 'predict']
        self.mode = mode
        self.user_annots = None
        self.iter_idx = -1
        assert (p_dim is None) or (p_dim >= self.n_labels)
        self.p_dim = p_dim

        # init label model
        self.labmodel_feature_key = labmodel_feature_key
        self.label_model = label_model
        self.n_labmodel_features = None
        if self.label_model is None:
            assert mode == 'train'
        else:
            fvecs = self.seg.sp_chans[labmodel_feature_key]  # Warning! do not save fvecs.shape[0], as n. of segments might change
            assert fvecs.ndim == 2
            self.n_labmodel_features = fvecs.shape[1]
            self.label_model.reset(self.n_labmodel_features, 2)
        #
        assert Config.GNNBIN_DAVIS_SESSION_IDX_NODE_FEATURE is False, "Only in 'davis' mode."


    def __iter__(self):
        return self

    def __next__(self):
        '''
        Returns:
            'train' MODE:
                (DGLGraph - the graph including the node/edge features,
                 ndarray(n_nodes) of int32 - the node labels for the graph)
            'predict' MODE:
                DGLGraph - the graph including the node/edge features
        '''
        self.iter_idx += 1
        if self.mode == 'train':
            if self.p_dim is None:
                return self._generate_binary_training_sample()
            else:
                return self._generate_multiclass_training_sample()
        else:
            if self.p_dim is None:
                g = self._generate_binary_prediction_sample()
            else:
                g = self._generate_multiclass_prediction_sample()
            if g is None:
                raise StopIteration
            return g

    def reset_prediction_iterator(self, user_annots):
        '''
        Reset iterator with new user annots given. Only for prediction mode.
        Parameters:
            user_annots: ndarray(n_sps_annotated, 2:[SP ids, labels])
        '''
        assert self.mode == 'predict'
        assert user_annots.shape[1:] == (2,)
        self.user_annots = user_annots
        self.iter_idx = -1

    # PRIVATE

    '''
    FEATURE LIST (shared - binary & multiclass):
    Constant node features:
        #0..1: mean occl fw,bw [0..1]
        #2..7: mean_im_lab, std_im_lab [0..1]
        #8: (log2 / 20.) node size
        #9: is image border node (border_sp) {0,1}
    Constant edge features (per direction):
        #0: lab_dist [approx. 0..1] (SAME IN BOTH DIRS)
        #1..6: of_fw_diff, of_bw_diff [[0,pi], any real number, [0,1], [0,pi], any real number, [0,1]] (SAME IN BOTH DIRS)
            [abs_angular_diff, min_mag, rel_mag]
        #7..10: flow_edges_gnn (VARIES BY DIR), all zero if not flow edge
        #11: is spatial edge {0,1} (SAME IN BOTH DIRS)
        #12: is flow edge {0,1} (SAME IN BOTH DIRS)

    FEATURE LIST (binary only):
    Dynamic node features:
        #10..11: true bg, fg annotation
        #12: label model fg/bg prediction probability [0,1] - 1 is fg, 0 is bg

    FEATURE LIST (multiclass only):
    Dynamic node features:
        #10: true annotation
    Multiclass label node features:
        #0..#p_dim-1: label model probabilities

    '''

    # PRIVATE

    def _prepare_constant_nodefs_arr(self):
        '''
        Returns a new nodefs array with constant node features written in it and place for dynamic features is left as zeros.
        Returns:
            nodefs: ndarray(n_nodes, 13 OR 11) of float32; 
                        shape[1] == 13 IF in 'binary' mode, otherwise shape[1] == 11
        '''
        n_node_features = 13 if self.p_dim is None else 11
        nodefs = np.zeros((self.seg.get_n_sps_total(), n_node_features), dtype=np.float32)
        nodefs[:,0] = self.seg.sp_chans['mean_occl_fw'][:,0]
        nodefs[:,1] = self.seg.sp_chans['mean_occl_bw'][:,0]
        nodefs[:,2:5] = self.seg.sp_chans['mean_im_lab'] / 255.
        nodefs[:,5:8] = self.seg.sp_chans['std_im_lab'] / 255.
        nodefs[:,8] = (np.log2(self.seg.get_seg_sizes())[:,None] / 20.)[:,0]
        nodefs[:,9] = self.seg.sp_chans['border_sp'][:,0]
        return nodefs

    def _prepare_constant_edgefs_arr(self):
        '''
        Returns a new edgefs array with constant edge features written in it and place for dynamic features is left as zeros.
        Returns:
            edges: ndarray(n_edges, 2:[from_ID, to_ID]) of int32; unique edges where ID_from < ID_to
            edgefs: ndarray(n_edges, 2:[from -> to, to -> from], 13) of float32;
        '''
        spatial_edges = self.seg.sp_edges['spatial_edges']
        flow_edges = self.seg.sp_edges['flow_edges_gnn']
        merger_els, merger_idxs = self.seg.edgelist_mergers['spatial+optflow_gnn']
        merger_idxs_part1_mask = merger_idxs < spatial_edges.shape[0]
        assert merger_els == ['spatial_edges', 'flow_edges_gnn']

        edges = np.concatenate([spatial_edges, flow_edges], axis=0)[merger_idxs,:]
        edgefs = np.zeros((edges.shape[0], 2, 13), dtype=np.float32)
        edgefs[:,:,0] = (self.seg.get_edge_features('mean_lab_dist', 'spatial+optflow_gnn', ensure_copy=False) / 255.)[:,:]
        edgefs[:,:,1:4] = self.seg.get_edge_features('mean_of_fw_dist', 'spatial+optflow_gnn', ensure_copy=False)[:,None,:]
        edgefs[:,:,4:7] = self.seg.get_edge_features('mean_of_bw_dist', 'spatial+optflow_gnn', ensure_copy=False)[:,None,:]
        edgefs[~merger_idxs_part1_mask,:,7:11] = self.seg.get_edge_features('flow_fs_gnn', 'flow_edges_gnn', ensure_copy=False)   # bidir
        edgefs[merger_idxs_part1_mask,:,11] = 1.
        edgefs[~merger_idxs_part1_mask,:,12] = 1.
        return edges, edgefs


    def _generate_binary_training_sample(self):
        '''
        Generates a random binary training sample.
        Returns:
            g: DGLGraph - the graph including the node/edge features
            true_labels_bin: ndarray(n_nodes) of fl32 - the node labels for the graph
        '''
        assert Config.DATAGENTR_SIMULATE_PRED_ALPHA > 1.
        assert Config.DATAGENTR_SIMULATE_PRED_BETA > Config.DATAGENTR_SIMULATE_PRED_ALPHA
        assert Config.DATAGENTR_SIMULATE_LABEL_MODEL or (self.label_model is not None)
        assert (not Config.DATAGENTR_DISABLE_LABEL_MODEL) or Config.DATAGENTR_SIMULATE_LABEL_MODEL

        # generate constant features
        nodefs = self._prepare_constant_nodefs_arr()
        edges, edgefs = self._prepare_constant_edgefs_arr()

        # randomly selecting revealed nodes from all over the video
        revealed_node_idxs, lab_sets = self._select_revealed_nodes(binary_labels=True)
        true_node_idxs0, true_node_idxs1 = revealed_node_idxs
        labelset0, labelset1 = lab_sets
        true_labels_orig = self.seg.sp_chans['gt_rounded'].reshape(-1).astype(np.int32)
        assert np.all(np.isin(true_labels_orig[true_node_idxs1], labelset1))   # TODO debug check, remove

        # assign dynamic node features #10..11 (true user annots)
        nodefs[true_node_idxs0, 10] = 1.
        nodefs[true_node_idxs1, 11] = 1.

        # generate true labels (multiclass -> binary conversion)
        label_replace_arr = np.zeros((self.n_labels,), dtype=np.int32)
        label_replace_arr[labelset1] = 1
        true_labels_bin = label_replace_arr[true_labels_orig].astype(np.float32)   # (n_nodes,) of int (0 or 1)

        # generate dynamic node feature #12 (label model prediction or its simulated version)
        if Config.DATAGENTR_SIMULATE_LABEL_MODEL:
            # simulate label model without actually using it - simulate predictions with noisy gt labels
            if Config.DATAGENTR_DISABLE_LABEL_MODEL:
                # disable label model (replace feature with uniform random noise)
                nodefs[:, 12] = np.random.rand(self.seg.get_n_sps_total())
            else:
                rnd_beta = np.random.beta(Config.DATAGENTR_SIMULATE_PRED_ALPHA, Config.DATAGENTR_SIMULATE_PRED_BETA,\
                                                                                         size=(self.seg.get_n_sps_total()))
                nodefs[:, 12] = np.fabs(true_labels_bin - rnd_beta)

        else:
            # train & predict with label model
            true_node_idxs = np.concatenate([true_node_idxs0, true_node_idxs1], axis=0)
            true_node_ys = np.zeros(true_node_idxs.shape[0], dtype=np.int32)
            true_node_ys[true_node_idxs0.shape[0]:] = 1
            self.label_model.reset(self.n_labmodel_features, 2)
            labmodel_train_xs = self.seg.sp_chans[self.labmodel_feature_key][true_node_idxs, :self.n_labmodel_features]   # (n_train_sps, n_features)
            self.label_model.fit(labmodel_train_xs, true_node_ys)
            #
            xs_to_pred = self.seg.sp_chans[self.labmodel_feature_key][:,:self.n_labmodel_features]   # (n_sps, n_features)
            ys_pred = self.label_model.predict(xs_to_pred, return_probs=True)   # (n_nodes, 2)
            assert ys_pred.shape == (xs_to_pred.shape[0], 2)
            assert np.amin(ys_pred) >= 0.
            assert np.amax(ys_pred) <= 1.
            nodefs[:, 12] = ys_pred[:,1]
        nodefs[true_node_idxs0, 12] = 0.
        nodefs[true_node_idxs1, 12] = 1.

        # convert bidir edges to unidir without copying: similar to concatenating efs[:,0,:] and efs[:,1,:]
        unidir_edges_from = edges.T.reshape(-1)
        unidir_edges_to = edges[:,::-1].T.reshape(-1)
        edgefs_directed = np.concatenate([edgefs[:,0,:], edgefs[:,1,:]], axis=0)

        # create DGL graph from self.edges and self.nodefs, self.edgefs copies
        g = dgl.DGLGraph((unidir_edges_from, unidir_edges_to))   # similar to concatenating e[:,0] and e[:,1]
        g.ndata['fs'] = nodefs
        g.edata['fs'] = edgefs_directed
        print("GEN mc vidname: ", self.vidname, " - n_nodes_revealed_per_cat: ", len(revealed_node_idxs[0]),\
                 " - node/edge_count: ", nodefs.shape[0], edges.shape[0]*2)

        return g, true_labels_bin

    def _generate_multiclass_training_sample(self):
        '''
        Generates a random binary training sample.
        Returns:
            g: DGLGraph - the graph including the node/edge features & input label distribution vector
            true_labels: ndarray(n_nodes,) of int32 - the true node category labels for the graph
            (DISABLED) cat_weights: ndarray(p_dim,) of float32 - the category weights to be used when balancing the loss
        '''
        assert not Config.DATAGENTR_SIMULATE_LABEL_MODEL, "Simulated label model is only implemented for the binary case."

        #t0 = time.time()

        # generate constant features
        nodefs = self._prepare_constant_nodefs_arr()
        edges, edgefs = self._prepare_constant_edgefs_arr()

        # select nodes where true labels are revealed
        true_labels_orig = self.seg.sp_chans['gt_rounded'].reshape(-1).astype(np.int32)
        revealed_node_idxs, _ = self._select_revealed_nodes(binary_labels=False)
        all_true_node_idxs = np.concatenate(revealed_node_idxs, axis=0)

        # fit label model to true nodes
        if isinstance(self.label_model, LogisticRegression):
            assert self.label_model.solver == 'lbfgs', "'lbfgs' solver in 'auto' multi_class mode will use CE loss, which is faster than OVR method"
        self.label_model.reset(self.n_labmodel_features, self.n_labels)
        labmodel_train_xs = self.seg.sp_chans[self.labmodel_feature_key][all_true_node_idxs, :self.n_labmodel_features]   # (n_train_sps, n_features)
        labmodel_train_ys = np.repeat(np.arange(self.n_labels), [len(revealed_node_idxs[lab]) for lab in range(self.n_labels)])
        assert labmodel_train_xs.shape[0] == labmodel_train_ys.shape[0]
        self.label_model.fit(labmodel_train_xs, labmodel_train_ys)  # up to 1 sec
        #t2 = time.time()

        # predict all node labels with trained label model
        xs_to_pred = self.seg.sp_chans[self.labmodel_feature_key][:,:self.n_labmodel_features]   # (n_sps, n_features)
        ys_pred = self.label_model.predict(xs_to_pred, return_probs=True)   # (n_nodes, n_labels)
        assert ys_pred.shape == (xs_to_pred.shape[0], self.n_labels)
        assert np.amin(ys_pred) >= 0.
        assert np.amax(ys_pred) <= 1.

        # add variable node feature: feature#10
        nodefs[all_true_node_idxs, 10] = 1.

        # generate input label probability vectors
        ys_pred_padded = np.pad(ys_pred, [(0,0), (0, self.p_dim-self.n_labels)])
        ys_pred_padded[all_true_node_idxs, :] = 0.
        ys_pred_padded[all_true_node_idxs, labmodel_train_ys] = 1.

        # convert bidir edges to unidir: similar to concatenating efs[:,0,:] and efs[:,1,:]
        unidir_edges_from = edges.T.reshape(-1)
        unidir_edges_to = edges[:,::-1].T.reshape(-1)
        edgefs_directed = np.concatenate([edgefs[:,0,:], edgefs[:,1,:]], axis=0)

        #t7 = time.time()
        # create DGL graph from self.edges and self.nodefs, self.edgefs copies
        g = dgl.DGLGraph((unidir_edges_from, unidir_edges_to))   # similar to concatenating e[:,0] and e[:,1]
        #t8 = time.time()
        g.ndata['fs'] = nodefs
        g.ndata['ps'] = ys_pred_padded.astype(np.float32)
        g.edata['fs'] = edgefs_directed
        print("GEN mc vidname: ", self.vidname, " - n_nodes revealed total: ", all_true_node_idxs.shape[0],\
                 " - node/edge_count: ", nodefs.shape[0], edges.shape[0]*2)
        #t9 = time.time()

        return g, true_labels_orig


    def _generate_binary_prediction_sample(self):
        '''
        Generates the next one-vs-all binary label partitioning.
        Returns:
            DGLGraph - the graph including the node/edge features
        '''
        assert self.user_annots is not None
        if ((Config.GNNBIN_BINARY_DOUBLE_PREDICTION_ROUND is True) and (self.iter_idx >= 2*self.n_labels)) or\
               ((Config.GNNBIN_BINARY_DOUBLE_PREDICTION_ROUND is False) and (self.iter_idx >= self.n_labels)):
            return None

        # generates n_labels OR 2*n_labels samples; 
        #   i=0..n_labels-1 where label#i -> 1, all others -> 0
        #   option: (i=n_labels..2*n_labels-1 where label#i -> 0, all others -> 1) IF Config.GNNBIN_BINARY_DOUBLE_PREDICTION_ROUND is True

        # generate constant features
        nodefs = self._prepare_constant_nodefs_arr()
        edges, edgefs = self._prepare_constant_edgefs_arr()

        # generate dynamic node features #10..11 (true user annots)
        curr_fg_label = self.iter_idx % self.n_labels
        fg_mask = self.user_annots[:,1] == curr_fg_label
        true_node_idxs1 = self.user_annots[fg_mask, 0]
        true_node_idxs0 = self.user_annots[~fg_mask, 0]
        true_node_idxs0, true_node_idxs1 = (true_node_idxs1, true_node_idxs0) if self.iter_idx >= self.n_labels\
                                                                                    else (true_node_idxs0, true_node_idxs1)
        nodefs[true_node_idxs0, 10] = 1.
        nodefs[true_node_idxs1, 11] = 1.

        # fit label model
        true_node_idxs = np.concatenate([true_node_idxs0, true_node_idxs1], axis=0)
        true_node_ys = np.zeros(true_node_idxs.shape[0], dtype=np.int32)
        true_node_ys[true_node_idxs0.shape[0]:] = 1
        self.label_model.reset(self.n_labmodel_features, 2)
        labmodel_train_xs = self.seg.sp_chans[self.labmodel_feature_key][true_node_idxs, :self.n_labmodel_features]   # (n_train_sps, n_features)
        self.label_model.fit(labmodel_train_xs, true_node_ys)
        
        # predict with label model -> node feature#12
        xs_to_pred = self.seg.sp_chans[self.labmodel_feature_key][:,:self.n_labmodel_features]   # (n_sps, n_features)
        ys_pred = self.label_model.predict(xs_to_pred, return_probs=True)   # (n_nodes, 2)
        assert ys_pred.shape == (xs_to_pred.shape[0], 2)
        assert np.amin(ys_pred) >= 0.
        assert np.amax(ys_pred) <= 1.
        nodefs[:, 12] = ys_pred[:,1]
        nodefs[true_node_idxs0, 12] = 0.
        nodefs[true_node_idxs1, 12] = 1.

        # create DGL graph from self.edges and self.nodefs, self.edgefs copies
        g = dgl.DGLGraph((edges.T.reshape(-1), edges[:,::-1].T.reshape(-1)))   # similar to concatenating e[:,0] and e[:,1]
        g.ndata['fs'] = nodefs
        edgefs_directed = edgefs.transpose((1,0,2)).reshape((-1, edgefs.shape[-1]))
                                                               # similar to concatenating efs[:,0,:] and efs[:,1,:]
        g.edata['fs'] = edgefs_directed
        return g

    def _generate_multiclass_prediction_sample(self):
        '''
        Generates a single sample (then, the iterator is exhausted).
        Returns:
            DGLGraph - the graph including the node/edge features
        '''
        assert self.user_annots is not None
        if self.iter_idx >= 1:
            return None

        # generate constant features
        nodefs = self._prepare_constant_nodefs_arr()
        edges, edgefs = self._prepare_constant_edgefs_arr()

        # fit label model to true nodes (user annots)
        if isinstance(self.label_model, LogisticRegression):
            assert self.label_model.solver == 'lbfgs', "'lbfgs' solver in 'auto' multi_class mode will use CE loss, which is faster than OVR method"
        true_node_idxs = self.user_annots[:,0]
        true_node_ys = self.user_annots[:,1]
        true_node_xs = self.seg.sp_chans[self.labmodel_feature_key][true_node_idxs, :self.n_labmodel_features]   # (n_train_sps, n_features)
        assert true_node_xs.shape[0] == true_node_ys.shape[0]
        self.label_model.fit(true_node_xs, true_node_ys)

        # predict all node labels with trained label model
        xs_to_pred = self.seg.sp_chans[self.labmodel_feature_key][:,:self.n_labmodel_features]   # (n_sps, n_features)
        ys_pred = self.label_model.predict(xs_to_pred, return_probs=True)   # (n_nodes, n_labels)
        assert ys_pred.shape == (xs_to_pred.shape[0], self.n_labels)
        assert np.amin(ys_pred) >= 0.
        assert np.amax(ys_pred) <= 1.

        # add variable node feature: feature#10
        nodefs[true_node_idxs, 10] = 1.

        # generate input label probability vectors
        ys_pred_padded = np.pad(ys_pred, [(0,0), (0, self.p_dim-self.n_labels)])
        ys_pred_padded[true_node_idxs, :] = 0.
        ys_pred_padded[true_node_idxs, true_node_ys] = 1.

        # generate graph & true label vectors
        g = dgl.DGLGraph((edges.T.reshape(-1), edges[:,::-1].T.reshape(-1)))   # similar to concatenating e[:,0] and e[:,1]
        g.ndata['fs'] = nodefs
        g.ndata['ps'] = ys_pred_padded.astype(np.float32)
        edgefs_directed = edgefs.transpose((1,0,2)).reshape((-1, edgefs.shape[-1]))
                                                               # similar to concatenating efs[:,0,:] and efs[:,1,:]
        g.edata['fs'] = edgefs_directed
        return g

    # HELPER: revealed node selection

    def _select_revealed_nodes(self, binary_labels):
        '''
        Parameters:
            binary_labels: bool; if True, partitions the label set randomly to bg & fg first
        Returns:
            revealed_node_idxs: list(n_labelsets) of ndarray(n_revealed_nodes_in_cat,) of int32
            lab_sets: list(n_labelsets) of ndarray(n_labels_in_set,) of int32
        '''
        n_revealed_nodes_per_cat = GraphDatagen.random_sample_n_nodes_revealed()   # int
        true_labels_orig = self.seg.sp_chans['gt_rounded'].reshape(-1).astype(np.int32)

        if binary_labels:
            # binary mode: partitioning label set to bg & fg and sampling from these two categories
            labelset0 = np.random.choice(self.n_labels, size=(np.random.randint(1, self.n_labels),), replace=False)
            labelset1 = np.setdiff1d(np.arange(self.n_labels), labelset0)
            lab_sets = [labelset0, labelset1]
        else:
            # multiclass mode: sampling from original labels per category
            lab_sets = list(np.arange(self.n_labels).reshape((self.n_labels,1)))

        revealed_node_idxs = []
        c_true_labs = []
        for lab_set in lab_sets:
            nodes_with_lab_mask = np.isin(true_labels_orig, lab_set)

            # optionally, select from only a few randomly chosen frames - except, if a label is not present at all in these frames
            if Config.DATAGENTR_LABELS_REVEALED_IN_N_FRAMES_MAX is not None:
                assert 1 <= Config.DATAGENTR_LABELS_REVEALED_IN_N_FRAMES_MAX <= 25
                selected_frames = np.random.choice(self.seg.get_n_frames(), size=(Config.DATAGENTR_LABELS_REVEALED_IN_N_FRAMES_MAX,))
                nodes_in_frames_mask = self.seg.get_sp_mask_for_fr_idxs(selected_frames)
                if np.count_nonzero(nodes_in_frames_mask & nodes_with_lab_mask) > 0:
                    nodes_with_lab_mask &= nodes_in_frames_mask

            node_idxs_with_lab = np.where(nodes_with_lab_mask)[0]         # (n_nodes_with_given_label,)
            node_idxs_with_lab_sel = np.random.choice(node_idxs_with_lab, size=(n_revealed_nodes_per_cat,))
            revealed_node_idxs.append(node_idxs_with_lab_sel)
            c_true_labs.append(node_idxs_with_lab.shape[0])

        assert all(c_true_lab > 0. for c_true_lab in c_true_labs)
        return revealed_node_idxs, lab_sets


    @staticmethod
    def random_sample_n_nodes_revealed():
        '''
        Generates a random number to be used as "number of revealed nodes" by using Config cosntants.
        Returns:
            n_nodes_revealed: int
        '''
        assert Config.DATAGENTR_N_NODES_REVEALED_PER_CAT_MIN <= Config.DATAGENTR_N_NODES_REVEALED_PER_CAT_MAX
        assert Config.DATAGENTR_N_NODES_REVEALED_PER_CAT_EXPBASE > 0.
        # if min == max, return them
        if Config.DATAGENTR_N_NODES_REVEALED_PER_CAT_MIN == Config.DATAGENTR_N_NODES_REVEALED_PER_CAT_MAX:
            n_nodes_revealed = int(Config.DATAGENTR_N_NODES_REVEALED_PER_CAT_MAX)
        else:
            logmin = math.log(Config.DATAGENTR_N_NODES_REVEALED_PER_CAT_MIN, Config.DATAGENTR_N_NODES_REVEALED_PER_CAT_EXPBASE)
            logmax = math.log(Config.DATAGENTR_N_NODES_REVEALED_PER_CAT_MAX, Config.DATAGENTR_N_NODES_REVEALED_PER_CAT_EXPBASE)
            logrnd = random.uniform(logmin, logmax)
            n_nodes_revealed = int(Config.DATAGENTR_N_NODES_REVEALED_PER_CAT_EXPBASE ** logrnd)
        assert 1 <= n_nodes_revealed <= 2000
        return n_nodes_revealed


    # END OF GraphDatagen class

class GraphTrainingDataset(IterableDataset):

    '''
    Training/validation dataset subclassing the PyTorch IterableDataset class to support parallel sample generation.
    A single instance should be initialized for training and another one for validation.

    Member fields:
        segs: dict{vidname - str: Segmentation}
        seg_labs: dict{vidname - str: SegLabeling}
        lab_model_init_fn: Callable, signature is (,) -> LabelModel
        n_workers: int; if 0, Multiprocessing is disabled; otherwise designates the number of parallel asynchronous workers

        datagens: dict{vidname - str: GraphDatagen}
        lab_models: list(n_workers) of LabelModel
        worker_datagen_names: list(n_workers) of list(n_datagen_in_worker) of strs - the lists of vidnames handled by each worker
    '''

    def __init__(self, segs, seg_labs, lab_model_init_fn, labmodel_feature_key, n_workers=0, p_dim=None):
        super(GraphTrainingDataset).__init__()
        self.segs = segs
        assert set(seg_labs.keys()) == set(segs.keys())
        self.seg_labs = seg_labs

        self.lab_model_init_fn = lab_model_init_fn
        assert n_workers >= 0
        assert n_workers <= len(self.segs.keys()), "The number of workers must be equal or less than the number of videos in the dataset."
        self.n_workers = n_workers
        self.labmodel_feature_key = labmodel_feature_key
        self.p_dim = p_dim

        # init label models: one instance for each worker
        self.lab_models = [lab_model_init_fn() for _ in range(self.get_n_threads())]

        # init data generators and split them up into bins (one bin for each worker)
        self.worker_datagen_names = np.array_split(list(self.segs.keys()), self.get_n_threads())
        self.worker_datagen_names = [list(charr) for charr in self.worker_datagen_names]
        datagen_lab_model_dict = {vidname: self.lab_models[worker_idx] for worker_idx, worker_vids in enumerate(self.worker_datagen_names) for vidname in worker_vids}

        self.datagens = {vidname: GraphDatagen(vidname, self.segs[vidname], self.seg_labs[vidname].n_labels,\
                                 'train', datagen_lab_model_dict[vidname], labmodel_feature_key, p_dim) \
                                                         for vidname in list(self.segs.keys())}

    def get_n_threads(self):
        return max(1, self.n_workers)

    def __iter__(self):
        worker_info = torch.utils.data.get_worker_info()
        assert ((worker_info is None) and (self.n_workers == 0)) or ((worker_info.num_workers == self.get_n_threads()))
        current_worker_idx = 0 if worker_info is None else worker_info.id
        vidnames_for_current_worker = self.worker_datagen_names[current_worker_idx]   # list of str
        datagens_for_current_worker = [self.datagens[vidname] for vidname in vidnames_for_current_worker]
        return self._merge_iterators_random_select(datagens_for_current_worker)

    def _merge_iterators_random_select(self, gens):
        '''
        Generator.
        Merges multiple iterators by drawing from a randomly selected one on each query.
        Can handle finite iterators.
        '''
        yielding = list(range(len(gens)))   # idx of iterators that are not exhausted
        while len(yielding) > 0:
            gen_idx = np.random.choice(yielding)
            try:
                sample = next(gens[gen_idx])
            except StopIteration:
                gens.remove(gen_idx)
                continue
            yield sample

    # END OF GraphTrainingDataset class

def collate_training_samples(batch, multiclass):
    '''
    Can be used as 'collate_fn' in a PyTorch DataLoader.
    Assembles batches from samples generated by training data generators.
    Additional parameters:
        multiclass: bool
    '''
    batch_gs, batch_ys, batch_snorm_n, batch_snorm_e = [], [], [], []
    for (g, node_ys) in batch:
        batch_gs.append(g)
        if not multiclass:
            node_ys = node_ys.reshape(node_ys.shape + (1,))   # binary case
        batch_ys.append(node_ys)
        n_nodes, n_edges = g.number_of_nodes(), g.number_of_edges()
        batch_snorm_n.append(torch.FloatTensor(n_nodes,1).fill_(1./float(n_nodes)))
        batch_snorm_e.append(torch.FloatTensor(n_edges,1).fill_(1./float(n_edges)))
    batch_snorm_n = torch.cat(batch_snorm_n, dim=0).sqrt()
    batch_snorm_e = torch.cat(batch_snorm_e, dim=0).sqrt()
    batch_ys = torch.tensor(np.concatenate(batch_ys, axis=0))
    batch_gs = dgl.batch(batch_gs)
    return batch_gs, batch_ys, batch_snorm_n, batch_snorm_e


