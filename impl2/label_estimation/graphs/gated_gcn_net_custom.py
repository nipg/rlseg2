
#
# Gated-GCN, float node & edge features, output is a single float for binary classification.
#    RLseg2 implementation #1
#
#   @author Viktor Varga, based on the benchmrking-gnn repo
#

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import sklearn.metrics # temp
import scipy.special   # temp

import dgl
import numpy as np

from label_estimation.graphs.gated_gcn_layer_multi import GatedGCNLayer_custom
from label_estimation.graphs.gated_gcn_layer_davis import GatedGCNLayer_custom_DAVIS
import config as Config

class GatedGCNNet_nodecl(nn.Module):

    '''
    Member fields:
        device, readout, graph_norm, batch_norm, residual: ?; net settings
        no_edge_repr, shared_params, n_classes, native_multiclass, davis_mode: ?; net settings
    '''
    
    def __init__(self, net_params):
        super().__init__()
        self.no_edge_repr = net_params['no_edge_repr']
        self.shared_params = net_params['shared_params']
        self.w_mlp = net_params['w_mlp']
        in_dim_edge = net_params['in_dim_edge']
        in_dim_node = net_params['in_dim_node']
        hidden_dim = net_params['hidden_dim']
        out_dim = net_params['out_dim']
        dropout = net_params['dropout']
        n_layers = net_params['L']
        self.device = net_params['device']
        self.readout = net_params['readout']
        self.graph_norm = net_params['graph_norm']
        self.batch_norm = net_params['batch_norm']
        self.residual = net_params['residual']
        self.norm_adj_att_scores = net_params['norm_adj_att_scores']
        self.p_node_features = net_params['p_node_features']
        self.n_classes = net_params['n_classes']
        self.native_multiclass = net_params['native_multiclass']
        self.davis_mode = net_params['davis_mode']


        # h input embedding
        self.embedding_h = nn.Linear(in_dim_node, hidden_dim)

        # e input embedding: if 'no_edge_repr' is True, edge representation is not propagated but estimated once with an MLP
        #                                           and its output is used as input edge representation in all GatedGCN layers
        #                    if 'no_edge_repr' is False, a simple linear layer produces the input edge embedding
        if self.no_edge_repr:
            self.embedding_e = MLPLayers(input_dim=in_dim_edge, output_dim=hidden_dim, hidden_dims=[hidden_dim, hidden_dim])
        else:
            self.embedding_e = nn.Linear(in_dim_edge, hidden_dim)

        # create shared parameters if needed
        if self.shared_params:
            assert not self.davis_mode
            shared_params_keys = ['A', 'B', 'D', 'E'] if self.no_edge_repr else ['A', 'B', 'C', 'D', 'E']
            shared_params_dict = {k: nn.Linear(hidden_dim, hidden_dim, bias=True) for k in shared_params_keys}
            if self.native_multiclass:
                shared_params_dict['q'] = nn.Linear(hidden_dim, 1, bias=True)
                shared_params_dict['z'] = nn.Linear(hidden_dim, 1, bias=True)
        else:
            shared_params_dict = {}

        p_dim = self.n_classes if self.native_multiclass else None
        # TODO: normalization for last layer is enabled with p_rescale=True, disabled with p_rescale=(layer_idx < n_layers-1)
        if self.davis_mode:
            self.layers = nn.ModuleList([ GatedGCNLayer_custom_DAVIS(hidden_dim, hidden_dim, dropout, self.graph_norm,
                            self.batch_norm, self.residual, norm_adj_att_scores=self.norm_adj_att_scores, \
                            p_node_features=self.p_node_features, p_dim=p_dim, p_rescale=(layer_idx < n_layers-1), \
                                                                                                  w_mlp=self.w_mlp)\
                                                                                 for layer_idx in range(n_layers) ]) 
        else:
            self.layers = nn.ModuleList([ GatedGCNLayer_custom(hidden_dim, hidden_dim, dropout, self.graph_norm,
                            self.batch_norm, self.residual, norm_adj_att_scores=self.norm_adj_att_scores, \
                            p_node_features=self.p_node_features, no_edge_repr=self.no_edge_repr, \
                            shared_params_dict=shared_params_dict, p_dim=p_dim, \
                            p_rescale=(layer_idx < n_layers-1), w_mlp=self.w_mlp)\
                                                                                 for layer_idx in range(n_layers) ]) 

        # h output readout (classifier) if not native multiclass
        if not self.native_multiclass:
            self.MLP_layer = MLPReadoutClassifier(input_dim=hidden_dim, n_classes=self.n_classes, L=2)
        
    def forward(self, g, h, e, snorm_n, snorm_e, p, p2, p3):
        h = self.embedding_h(h)
        e = self.embedding_e(e)

        # convnets
        for conv in self.layers:
            if self.davis_mode:
                h, e, p = conv(g, h, e, snorm_n, snorm_e, p, p2, p3)
            else:
                h, e, p = conv(g, h, e, snorm_n, snorm_e, p)
        
        # output
        if self.native_multiclass:
            g.ndata['p'] = p
            return p
        else:
            g.ndata['h'] = h
            h = self.MLP_layer(h)
            return h
        
    def loss(self, pred, label, cat_weights=None):
        # pred: (n_samples, 1) fl32 if binary, (n_samples, n_cats) of fl32 if multilabel
        # label: (n_samples, 1) fl32 if binary, (n_samples,) i32 if multilabel
        # cat_weights: None OR (n_batches, n_cats)
        if self.n_classes == 2:
            if Config.GNNBIN_BINARY_LOSS == 'bce':
                loss = nn.BCELoss()(pred, label)
            elif Config.GNNBIN_BINARY_LOSS == 'iou_bin':
                assert Config.GNNBIN_BATCH_SIZE == 1, "The handling of different semantic labels with same label "+\
                                                                    " ID inside a single batch is not implemented."
                loss = BinaryIOULoss()(pred, label)
            else:
                assert False
        else:
            if Config.GNNBIN_MULTICLASS_LOSS == 'ce':
                loss = nn.CrossEntropyLoss()(pred, label)
            elif Config.GNNBIN_MULTICLASS_LOSS == 'weighted_ce':
                assert cat_weights is not None
                assert Config.GNNBIN_BATCH_SIZE == 1, "Category weighting in CE loss is not implemented for "+\
                                                                    "batches containing multiple class configurations."
                cat_weights = cat_weights.reshape(-1)
                loss = nn.CrossEntropyLoss(weight=cat_weights)(pred, label)
            elif Config.GNNBIN_MULTICLASS_LOSS == 'miou':
                assert Config.GNNBIN_BATCH_SIZE == 1, "The handling of different semantic labels with same label "+\
                                                                    " ID inside a single batch is not implemented."
                loss = MeanIOULoss(apply_softmax=True, only_fg=False)(pred, label)
            elif Config.GNNBIN_MULTICLASS_LOSS == 'miou_fg_only':
                assert Config.GNNBIN_BATCH_SIZE == 1, "The handling of different semantic labels with same label "+\
                                                                    " ID inside a single batch is not implemented."
                loss = MeanIOULoss(apply_softmax=True, only_fg=True)(pred, label)
            elif Config.GNNBIN_MULTICLASS_LOSS == 'mse':
                loss = nn.MSELoss()(pred, label)
            elif Config.GNNBIN_MULTICLASS_LOSS == 'lovasz':
                loss = LovaszSoftmax()(pred, label)
            else:
                assert False
        return loss

#
    
class MLPReadoutClassifier(nn.Module):
    # Based on MLPReadout layer

    def __init__(self, input_dim, n_classes, L=2): #L=nb_hidden_layers
        super().__init__()
        assert n_classes >= 2
        self.n_classes = n_classes
        list_FC_layers = [ nn.Linear( input_dim//2**l , input_dim//2**(l+1) , bias=True ) for l in range(L) ]
        output_dim = 1 if self.n_classes == 2 else self.n_classes
        list_FC_layers.append(nn.Linear( input_dim//2**L , output_dim , bias=True ))
        self.FC_layers = nn.ModuleList(list_FC_layers)
        self.L = L
        
    def forward(self, x):
        y = x
        for l in range(self.L):
            y = self.FC_layers[l](y)
            y = F.relu(y)
        y = self.FC_layers[self.L](y)
        if self.n_classes == 2:
            y = torch.sigmoid(y)
        else:
            y = torch.softmax(y, dim=-1)
        return y

class MLPLayers(nn.Module):

    def __init__(self, input_dim, output_dim, hidden_dims=[]): #L=nb_hidden_layers
        super().__init__()
        hidden_dims.insert(0, input_dim)
        hidden_dims.append(output_dim)
        list_FC_layers = []
        for l_idx in range(len(hidden_dims)-1):
            list_FC_layers.append(nn.Linear( hidden_dims[l_idx] , hidden_dims[l_idx+1] , bias=True ))
        self.FC_layers = nn.ModuleList(list_FC_layers)
        
    def forward(self, x):
        y = x
        for layer in self.FC_layers[:-1]:
            y = layer(y)
            y = F.relu(y)
        y = self.FC_layers[-1](y)
        return y

    #

class BinaryIOULoss(nn.Module):

    # MeanIOULoss adapted to binary case, computing mean IoU from both bg and fg IoUs

    def __init__(self):
        super(BinaryIOULoss, self).__init__()

    def forward(self, pred, label):
        # pred: (n_samples, 1) of fl32
        # label: (n_samples, 1) of fl32

        pred = pred.reshape(-1)
        label = label.reshape(-1)
        pred_inv = 1. - pred
        label_inv = 1. - label

        # IoU fg
        inter_fg = pred * label
        union_fg = pred + label - inter_fg
        inter_fg = inter_fg.sum(dim=0)
        union_fg = union_fg.sum(dim=0)
        iou_fg = inter_fg / (union_fg + 1e-8)

        # IoU bg
        inter_bg = pred_inv * label_inv
        union_bg = pred_inv + label_inv - inter_bg
        inter_bg = inter_bg.sum(dim=0)
        union_bg = union_bg.sum(dim=0)
        iou_bg = inter_bg / (union_bg + 1e-8)

        return 1. - 0.5*(iou_fg + iou_bg)


class MeanIOULoss(nn.Module):

    # Rahman et al. 2016, "Optimizing Intersection-Over-Union in Deep Neural Networks for Image Segmentation"

    def __init__(self, apply_softmax=False, only_fg=False):
        super(MeanIOULoss, self).__init__()
        self.apply_softmax = apply_softmax
        self.only_fg = only_fg

    def forward(self, pred, label):
        # pred: (n_samples, n_cats) of fl32
        # label: (n_samples,) of i32

        # cat labels to one-hot
        n_samples, n_cats = pred.size()
        n_cats_present = label.detach().max()+1
        label_onehot = torch.zeros_like(pred).scatter_(\
                                            dim=1, \
                                            index=label.long().view(n_samples, 1), \
                                            src=torch.ones_like(pred))

        # apply softmax on predictions if needed
        if self.apply_softmax:
            pred = F.softmax(pred, dim=1)

        # if 'only_fg' enabled, do not compute IoU for category #0 (background)
        if self.only_fg:
            pred = pred[:,1:]
            label_onehot = label_onehot[:,1:]
            n_cats_present -= 1

        # intersection / union
        inter = pred * label_onehot
        union = pred + label_onehot - inter
        inter = inter.sum(dim=0)
        union = union.sum(dim=0)
        iou = inter / (union + 1e-8)
        iou = iou[:n_cats_present]
        return 1. - iou.mean()

class LovaszSoftmax(nn.Module):

    # Berman et al., 2017, "The Lovasz-Softmax loss: A tractable surrogate for the optimization of the intersection-over-union measure in neural networks"
    #   based on code: https://github.com/bermanmaxim/LovaszSoftmax/blob/master/pytorch/lovasz_losses.py

    def forward(self, pred, label):
        # pred: (n_samples, n_cats) of fl32
        # label: (n_samples,) of i32
        n_samples, n_cats = pred.size()
        n_cats_present = label.detach().max()+1
        losses = []
        for c in range(n_cats_present):
            fg = (label == c).float() # foreground for class c
            class_pred = pred[:, c]
            errors = (Variable(fg) - class_pred).abs()
            errors_sorted, perm = torch.sort(errors, 0, descending=True)
            perm = perm.data
            fg_sorted = fg[perm]
            losses.append(torch.dot(errors_sorted, Variable(LovaszSoftmax.lovasz_grad(fg_sorted))))
        return torch.mean(torch.stack(losses))

    @staticmethod
    def lovasz_grad(gt_sorted):
        """
        Computes gradient of the Lovasz extension w.r.t sorted errors
        See Alg. 1 in paper
        """
        p = len(gt_sorted)
        gts = gt_sorted.sum()
        intersection = gts - gt_sorted.float().cumsum(0)
        union = gts + (1 - gt_sorted).float().cumsum(0)
        jaccard = 1. - intersection / union
        if p > 1: # cover 1-pixel case
            jaccard[1:p] = jaccard[1:p] - jaccard[0:-1]
        return jaccard