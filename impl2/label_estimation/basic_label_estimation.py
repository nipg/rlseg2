
#
# Basic label estimation. RLseg2 implementation #2.
#   Segment labels are estimated independently (no graphical label propagation implemented).
#
#   @author Viktor Varga
#

import os
import numpy as np
from .abstract_label_estimation import LabelEstimation

class BasicLabelEstimation(LabelEstimation):

    '''
    Member fields:
        seg: Segmentation instance
        label_model: LabelModel subclass instance

        seg_feature_key: str
        n_labmodel_features: int
        n_labels: int
    '''

    def __init__(self, seg, seg_feature_key, n_labels, label_model, n_labmodel_features):
        self.seg = seg
        self.n_labels = n_labels
        self.seg_feature_key = seg_feature_key
        fvecs = self.seg.sp_chans[seg_feature_key]
        assert fvecs.ndim == 2
        assert n_labmodel_features <= fvecs.shape[1]
        self.n_labmodel_features = n_labmodel_features

        self.label_model = label_model
        self.label_model.reset(self.n_labmodel_features, self.n_labels)

    def __str__(self):
        return "Basic_" + str(self.label_model)

    def fit(self, user_annot):
        '''
        Parameters:
            user_annot: ndarray(n_labeled_segs, 2:[SP id, label])
        '''
        self.label_model.reset(self.n_labmodel_features, self.n_labels)
        train_xs = self.seg.sp_chans[self.seg_feature_key][user_annot[:,0],:self.n_labmodel_features]   # (n_train_sps, n_labmodel_features)
        self.label_model.fit(train_xs, user_annot[:,1])

    def predict_all(self, return_probs=False):
        '''
        Parameters:
            return_probs: bool
        Returns:
            ys_pred: ndarray(n_sps, n_cat) of fl32 (IF return_probs == True)
                     ndarray(n_sps,) of i32        (IF return_probs == False)
        '''
        xs_to_pred = self.seg.sp_chans[self.seg_feature_key][:,:self.n_labmodel_features]   # (n_sps, n_labmodel_features)
        ys_pred = self.label_model.predict(xs_to_pred, return_probs=return_probs)
        return ys_pred
