
# 
# RLseg2 implementation #2, data interface class
# * Factory for Segmentation objects for each video
# * Manages caching of initialized Segmentation objects
#
#   @author Viktor Varga
#

import sys
sys.path.append('..')

import os
import cv2
import h5py
import numpy as np
import pickle
import time

from segmentation import Segmentation
import segmentation_featuregen as SegFGen
from segmentation_featuregen import EdgeGenFs
from segmentation_featuregen import DynamicFeatureMapLoader
import util.imutil as ImUtil


class DataIF():
    
    '''
    Member fields:
        seg_objs: dict{vidname - str: Segmentation instance}
    '''

    DATASET_TO_LOAD = 'davis2017'  # 'davis2016', 'davis2017' or 'segtrack1'
    SEGMENTATION_ALGORITHM = 'pregen'  # 'ccs', 'slic_dynamic', 'pregen'

    if DATASET_TO_LOAD == 'davis2017':
        import datasets.DAVIS17 as Dataset
    else:
        assert False, "Not implemented."

    if SEGMENTATION_ALGORITHM == 'ccs':
        import segmentation_algorithm.ccs_segmentation_algorithm as SegAlg
    elif SEGMENTATION_ALGORITHM == 'slic_dynamic':
        import segmentation_algorithm.slic_dynamic_segmentation_algorithm as SegAlg
    elif SEGMENTATION_ALGORITHM == 'pregen':
        import segmentation_algorithm.pregen_segmentation_algorithm as SegAlg
    else:
        assert False, "Not implemented."

    # PUBLIC

    def __init__(self):
        os.makedirs(DataIF.Dataset.CACHE_FOLDER, exist_ok=True)
        self.seg_objs = {}

    def get_dataset_class(self):
        return DataIF.Dataset

    def get_seg_obj(self, vidname):
        return self.seg_objs[vidname]

    def load_videos(self, vidnames, load_data='cached'):
        '''
        Parameters:
            vidnames: list of str
            load_data: str; in ['cached', 'cached+bgr_im', 'cached+bgr_im+of', 'all']
                            if 'all', the data for all other videos are cleared from memory first; 
                                    this way, only a single video can be loaded at once
        Modifies members:
            seg_objs
        '''
        assert load_data in ['cached', 'cached+bgr_im', 'cached+bgr_im+of', 'all']
        assert (load_data != 'all') or (len(vidnames) == 1)
        print("DataIF: loading " + str(len(vidnames)) + " videos...")
        time0 = time.time()
        if load_data == 'all':
            self.seg_objs.clear()

        for vidname in vidnames:
            fpath = os.path.join(DataIF.Dataset.CACHE_FOLDER, DataIF.Dataset.ID + '_' + vidname + '.pkl')
            # create cache if not present
            if not os.path.isfile(fpath):
                print("DataIF: video '", vidname, "' was not found in cache, creating now... timestamp:", time.time())
                self._create_cache_file(vidname, fpath)

            # load cache
            self._load_from_cache(vidname, fpath)

            # load image data & add to seg object if necessary (for visualization or full eval)
            if load_data in ['cached+bgr_im', 'cached+bgr_im+of', 'all']:
                all_raw_data = {}
                all_raw_data['im_bgr'], all_raw_data['im_lab'] = DataIF.Dataset.get_img_data(vidname, return_bgr=True, return_lab=True)

                # load all raw data & add to seg object if necessary (for full eval)
                if load_data == 'all':
                    # load true annot
                    all_raw_data['gt_annot'] = DataIF.Dataset.get_true_annots(vidname)
                    # load optflow & occl
                    all_raw_data['of_fw'], all_raw_data['of_bw'],\
                    all_raw_data['occl_fw'], all_raw_data['occl_bw'] = DataIF.Dataset.get_optflow_occlusion_data(vidname)
                    # (segmentation image archive is not loaded again)
                    # load feature maps
                    all_raw_data['feature_maps'] = DataIF.Dataset.get_featuremap_data(vidname)
                    # add all images to Segmentation object
                    self._add_ims_to_seg_obj(self.seg_objs[vidname], all_raw_data)
                    self._add_funcs_to_seg_obj(self.seg_objs[vidname])
                else:
                    if load_data == 'cached+bgr_im+of':
                        all_raw_data['of_fw'], all_raw_data['of_bw'],\
                        all_raw_data['occl_fw'], all_raw_data['occl_bw'] = DataIF.Dataset.get_optflow_occlusion_data(vidname)
                        
                    self._add_ims_to_seg_obj(self.seg_objs[vidname], all_raw_data)
        #
        time1 = time.time()

        print("DataIF: Done loading data from cache. Time taken: " + str(round(time1-time0, 2)) + " seconds.")

    # PRIVATE

    def _load_from_cache(self, vidname, cache_fpath):
        '''
        Loads preprocessed data from cache.
        '''
        assert vidname in cache_fpath
        assert os.path.isfile(cache_fpath)
        assert vidname not in self.seg_objs.keys()

        pkl_file = open(cache_fpath, 'rb')
        pkl_dict = pickle.load(pkl_file)
        pkl_file.close()
        pkl_seg_obj = pkl_dict['seg_obj']
        pkl_seg_obj.set_seg_alg(DataIF.SegAlg)
        self.seg_objs[vidname] = pkl_seg_obj

    def _create_cache_file(self, vidname, cache_fpath):
        '''
        Preprocecesses raw data and saves it to a cache file per video.
        '''
        assert vidname in cache_fpath
        assert not os.path.isfile(cache_fpath)
        assert vidname not in self.seg_objs.keys()
        all_raw_data = {}

        # load image data
        img_data_bgr, img_data_lab = DataIF.Dataset.get_img_data(vidname, return_bgr=True, return_lab=True)
        all_raw_data['im_bgr'] = img_data_bgr
        all_raw_data['im_lab'] = img_data_lab

        # load true annot
        all_raw_data['gt_annot'] = DataIF.Dataset.get_true_annots(vidname)
        # load optflow & occl
        all_raw_data['of_fw'], all_raw_data['of_bw'],\
        all_raw_data['occl_fw'], all_raw_data['occl_bw'] = DataIF.Dataset.get_optflow_occlusion_data(vidname)
        # load segmentation
        all_raw_data['seg'] = DataIF.SegAlg.get_base_sp_segmentation(ims_bgr=all_raw_data['im_bgr'],\
                                        base_folder_path=DataIF.Dataset.get_seg_folder_path(DataIF.SEGMENTATION_ALGORITHM,
                                                n_segs=DataIF.SegAlg.get_n_base_segs_per_frame()), vidname=vidname)
        # load feature maps
        all_raw_data['feature_maps'] = DataIF.Dataset.get_featuremap_data(vidname)

        seg_obj = self._init_seg_obj(vidname, all_raw_data)

        pkl_file = open(cache_fpath, 'wb')
        pkl_dict = {}
        pkl_dict['seg_obj'] = seg_obj
        pkl_data = pickle.dump(pkl_dict, pkl_file, protocol=2)
        pkl_file.close()

    def _add_ims_to_seg_obj(self, seg_obj, all_raw_data):
        '''
        Parameters:
            (MODIFIED) seg_obj: Segmentation instance
            all_raw_data: {key - str: ?}
        '''
        # add image channels to seg obj
        if 'of_fw' in all_raw_data.keys():
            of_fw = np.pad(all_raw_data['of_fw'], ((0,1),(0,0),(0,0),(0,0)), mode='constant', constant_values=0.)
            seg_obj.add_im("of_fw", of_fw)
        if 'of_bw' in all_raw_data.keys():
            of_bw = np.pad(all_raw_data['of_bw'], ((1,0),(0,0),(0,0),(0,0)), mode='constant', constant_values=0.)
            seg_obj.add_im("of_bw", of_bw)
        if 'occl_fw' in all_raw_data.keys():
            occl_fw = np.pad(all_raw_data['occl_fw'], ((0,1),(0,0),(0,0)), mode='constant', constant_values=False)
            seg_obj.add_im("occl_fw", occl_fw[..., None])
        if 'occl_bw' in all_raw_data.keys():
            occl_bw = np.pad(all_raw_data['occl_bw'], ((1,0),(0,0),(0,0)), mode='constant', constant_values=False)
            seg_obj.add_im("occl_bw", occl_bw[..., None])
        if 'feature_maps' in all_raw_data.keys():
            feature_loader = DynamicFeatureMapLoader(all_raw_data['feature_maps'], of_fw.shape[1:3])
            seg_obj.add_im("feature_maps", feature_loader)

        # add GT annot as image channel (if loaded from cache, the seg obj already contains the raw GT annot images)
        if ('gt_annot' in all_raw_data.keys()) and ("gt_raw" not in seg_obj.ims.keys()):
            seg_obj.add_im("gt_raw", all_raw_data['gt_annot'][..., None])

        # add image channels
        if 'im_bgr' in all_raw_data.keys():
            seg_obj.add_im("im_bgr", all_raw_data['im_bgr'])
        if 'im_lab' in all_raw_data.keys():
            seg_obj.add_im("im_lab", all_raw_data['im_lab'])

        # add border image channels (not needed in most cases, only added if fmaps are also added)
        if 'feature_maps' in all_raw_data.keys():
            border_img = np.zeros(all_raw_data['im_bgr'].shape[:3] + (1,), dtype=np.bool_)
            border_img[:,0,:] = 1
            border_img[:,-1,:] = 1
            border_img[:,:,0] = 1
            border_img[:,:,-1] = 1
            seg_obj.add_im("border", border_img)
        #

    def _add_funcs_to_seg_obj(self, seg_obj):
        '''
        Parameters:
            (MODIFIED) seg_obj: Segmentation instance
        '''
        #   image -> SP reduce functions
        seg_obj.add_segfs_func('of_fw', 'mean_of_fw', SegFGen.reduce_mean)
        seg_obj.add_segfs_func('of_bw', 'mean_of_bw', SegFGen.reduce_mean)
        seg_obj.add_segfs_func('of_fw', 'std_of_fw', SegFGen.reduce_std)
        seg_obj.add_segfs_func('of_bw', 'std_of_bw', SegFGen.reduce_std)
        seg_obj.add_segfs_func('occl_fw', 'mean_occl_fw', SegFGen.reduce_mean)
        seg_obj.add_segfs_func('occl_bw', 'mean_occl_bw', SegFGen.reduce_mean)
        seg_obj.add_segfs_func('gt_raw', 'gt_rounded', SegFGen.reduce_most_frequent_item)
        seg_obj.add_segfs_func('feature_maps', 'fvecs', SegFGen.reduce_mean)
        seg_obj.add_segfs_func('im_lab', 'mean_im_lab', SegFGen.reduce_mean)
        seg_obj.add_segfs_func('im_lab', 'std_im_lab', SegFGen.reduce_std)
        seg_obj.add_segfs_func('border', 'border_sp', SegFGen.reduce_any)
        #seg_obj.add_segfs_func("im_lab", "im_hist_lab", SegFGen.reduce_to_image_histograms, hist_len_per_ch=20, chs_to_keep=[1,2])
        
        seg_obj.add_edgegen_func('spatial_edges', SegFGen.find_spatial_temporal_edges, edge_type='spatial')
        #seg_obj.add_edgegen_func('temporal_edges', SegFGen.find_spatial_temporal_edges, edge_type='temporal')

        # MRF flow edges and features (weights) + edgelist merger
        #edgegen_fs_mrf = EdgeGenFs(SegFGen.find_flow_edges_and_mrf_weights)
        #seg_obj.add_edgegen_func('flow_edges_mrf', edgegen_fs_mrf.edgegen)
        #seg_obj.add_edgefs_func('flow_fs_mrf', ['flow_edges_mrf'], edgegen_fs_mrf.edgefs)
        #seg_obj.add_edgelist_merger('spatial+optflow_mrf', ['spatial_edges', 'flow_edges_mrf'])

        # GNN flow edges and features + edgelist merger
        edgegen_fs_gnn = EdgeGenFs(SegFGen.find_flow_edges_and_gnn_features)
        seg_obj.add_edgegen_func('flow_edges_gnn', edgegen_fs_gnn.edgegen)
        seg_obj.add_edgefs_func('flow_fs_gnn', ['flow_edges_gnn'], edgegen_fs_gnn.edgefs)

        # add edgelist mergers only if needed
        if len(seg_obj.edgelist_mergers) == 0:
            seg_obj.add_edgelist_merger('spatial+optflow_gnn', ['spatial_edges', 'flow_edges_gnn'])

        seg_obj.add_edgefs_func('mean_lab_dist', ['spatial_edges', 'flow_edges_gnn'], SegFGen.edgefs_segdist_pairwise_L2dist,\
                                                                                                     segfs_name='mean_im_lab')
        seg_obj.add_edgefs_func('mean_of_fw_dist', ['spatial_edges', 'flow_edges_gnn'], SegFGen.edgefs_optflow_diff,\
                                                                                                     segfs_name='mean_of_fw')
        seg_obj.add_edgefs_func('mean_of_bw_dist', ['spatial_edges', 'flow_edges_gnn'], SegFGen.edgefs_optflow_diff,\
                                                                                                     segfs_name='mean_of_bw')
        #

    def _init_seg_obj(self, vidname, all_raw_data):
        '''
        Parameters:
            vidname: str
            all_raw_data: {key - str: ?}
        Returns:
            seg_obj: Segmentation instance
        '''
        seg_obj = Segmentation(all_raw_data['seg'])
        seg_obj.set_seg_alg(DataIF.SegAlg)

        print("    SEG INIT DONE", time.time())
        self._add_ims_to_seg_obj(seg_obj, all_raw_data)

        # define functions
        self._add_funcs_to_seg_obj(seg_obj)
        seg_obj.compute_all()

        return seg_obj
