
#
# Simple benchmark task selection class. RLseg2 implementation #1
#
#   @author Viktor Varga
#

import sys
sys.path.append('..')

import numpy as np

from segmentation import Segmentation
import util.util as Util

class SimpleBenchmarkTaskSelection:

    '''
    Creates a benchmark for a single video on initialization
    Benchmarks:
        Phase#1: reveal label of 'n_sps_revealed' randomly selected SPs fr each label from across video,
                     train model on them and evaluate on full dataset.
                 repeat experiment 'n_repeats' times
                 (when selecting SPs, SP idx repetitions may happen within a single experiment)

    Member fields:
        n_sps_revealed_dict: dict{n_sps_revealed - int: n_repeats - int}
        seg_labelings: dict{vidname - str: SegLabeling}; the SegLabeling instances to get segmentation data and true labels
            (NOT SERIALIZED)
        
        id: str; random generated ID of benchmark instance
        benchmark_data: dict{n_sps_revealed - int:
                                 dict{vidname - str:
                                         ndarray(n_repeats, n_labels, n_sps_revealed) of i32}} SP IDs

        # ITERATION
        iter_vidname: str
        iter_gen: Generator instance, generates tasks: dict{'name': str,
                                                            'name_fname': str,
                                                            'sp_ids': ndarray(n_sps_annotated,) - SP IDs}
    '''

    def __init__(self, n_sps_revealed_dict, seg_labelings):
        '''
        Parameters: <see member fields>
        '''
        self.iter_vidname = None
        self.n_sps_revealed_dict = n_sps_revealed_dict
        self.id = Util.generate_session_id()
        self.seg_labelings = seg_labelings
        self._generate_benchmarks()

    # CUSTOM SERIALIZATION: to avoid serializing shared objects

    def __getstate__(self):
        d = {}
        d['id'] = self.id
        d['n_sps_revealed_dict'] = self.n_sps_revealed_dict
        d['benchmark_data'] = self.benchmark_data

        # fields not serialized (None is stored)
        d['seg_labelings'] = None
        d['iter_vidname'] = None

        return d

    # the default __setstate__() is used

    def __iter__(self):
        self.reset()
        return self.iter_gen

    def __next__(self):
        return next(self.iter_gen)

    def get_vidnames_set(self):
        '''
        Returns set of str; the vidnames
        '''
        benchmark_dict =  next(iter(self.benchmark_data.values()))
        return set(benchmark_dict.keys())

    def get_n_repeats(self):
        '''
        Returns number of repeats if it is the same for each n_sps_revealed scenario. Otherwise returns None.
        Returns: int or None
        '''
        n_repeats = set(self.n_sps_revealed_dict.values())
        return list(n_repeats)[0] if len(n_repeats) == 1 else None

    def reset(self, vidname=None):
        '''
        Resets iteration. Optionally sets current video.
        Parameters:
            vidname: None OR str;
        '''
        self.iter_vidname = self.iter_vidname if vidname is None else vidname
        self.iter_gen = self.iter_generator()

    def iter_generator(self):
        '''
        GENERATOR which iterates through the benchmark tasks for the current video.
        '''
        for n_sps_revealed, dict1 in self.benchmark_data.items():
            arr = dict1[self.iter_vidname]
            for rep_idx in range(arr.shape[0]):
                task_name = '<revealed: ' + str(n_sps_revealed) + ', rep_idx: ' + str(rep_idx) + '>'
                task_name_fname = 'nsp' + str(n_sps_revealed) + '_rep' + str(rep_idx)
                yield {'name': task_name, 'name_fname': task_name_fname, 'sp_ids': arr[rep_idx,:,:].reshape(-1)}


    # PRIVATE

    def _to_string_full_info(self):
        content = ["SimpleBenchmarkTaskSelection instance, ID: " + str(self.id) +\
                  "\n    n_sps_revealed_dict: " + str(self.n_sps_revealed_dict) + "\n"]
        for n_sps_revealed in self.benchmark_data.keys():
            content.append("\n\nEXPERIMENT, n_sps_revealed: " + str(n_sps_revealed) + ": \n")
            for vidname in self.benchmark_data[n_sps_revealed].keys():
                content.append("  --> video " + str(vidname) + ": \n")
                for repeat_idx in range(self.benchmark_data[n_sps_revealed][vidname].shape[0]):
                    content.append("    --> repeat idx #" + str(repeat_idx) + ": \n")
                    for lab in range(self.benchmark_data[n_sps_revealed][vidname].shape[1]):
                        content.append("      --> label #" + str(lab) + ": \n")
                        arr = self.benchmark_data[n_sps_revealed][vidname][repeat_idx, lab]  # (n_sps,)
                        arr_fr_idxs = self.seg_labelings[vidname].seg.get_fr_idxs_from_sp_ids(arr)
                        sp_ids = [(arr[choice_idx], arr_fr_idxs[choice_idx]) for choice_idx in range(arr.shape[0])]
                        content.append("          IDs(sp_idx, fr_idx): " + str(sp_ids) + "\n")
        content = ''.join(content)
        return content

    def _generate_benchmarks(self):
        '''
        Sets self.benchmark_data, self.phase2_benchmark_data.
        '''
        self.benchmark_data = {}
        for n_sps_revealed, n_repeats in self.n_sps_revealed_dict.items():
            self.benchmark_data[n_sps_revealed] = {}

            for vidname in self.seg_labelings.keys():
                n_labels = self.seg_labelings[vidname].get_n_labels()
                out_array = np.empty((n_repeats, n_labels, n_sps_revealed), dtype=np.int32)
                yss = self.seg_labelings[vidname].get_true_seg_labels()                     # (n_sps)

                for repeat_idx in range(n_repeats):
                    for lab in range(n_labels):
                        sps_idxs_with_lab = np.where(yss == lab)[0]         # (n_sps_with_given_label)
                        assert sps_idxs_with_lab.shape[0] > 0
                        selected_sp_ids = np.random.choice(sps_idxs_with_lab, size=(n_sps_revealed,))
                        out_array[repeat_idx, lab, :] = np.sort(selected_sp_ids)
                
                self.benchmark_data[n_sps_revealed][vidname] = out_array
        #