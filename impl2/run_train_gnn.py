
#
# Runner script for training a GNN label estimation model. RLseg2 implementation #2
#
#   @author Viktor Varga
#

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import sys
sys.path.append('..')

import os
import numpy as np
import pickle
import functools

from data_if import DataIF
from segmentation import Segmentation
from segmentation_labeling import SegLabeling

from seed_propagation.basic_optflow_seed_propagation import BasicOptflowSeedPropagation
from label_estimation.logreg_label_model import LogRegLabelModel

import util.viz as Viz
import config as Config

from label_estimation.gnn_label_estimation import GNNLabelEstimation   # only import pytorch if used
if Config.DATAGENTR_TRAINING_MODE == 'simple':
    from label_estimation.graphs.graph_datagen import GraphTrainingDataset, collate_training_samples
    from torch.utils.data import DataLoader
elif Config.DATAGENTR_TRAINING_MODE == 'davis':
    if Config.GNNBIN_NATIVE_MULTICLASS is True:
        from label_estimation.graphs.graph_datagen_davis_multi import GraphTrainingDatasetDAVIS
    else:
        from label_estimation.graphs.graph_datagen_davis_bin import GraphTrainingDatasetDAVIS



if __name__ == '__main__':

    #SET_NAME = 'debug_train (3+1+1)'
    #SET_NAME = 'trainval_reduced (30+10+1)'
    #SET_NAME = 'reduced_test (45+15+15)'
    #SET_NAME = 'reduced (30+10+15)'
    SET_NAME = 'full_trainval (45+15+1)'

    #assert False, "SeedProp was disabled."
    # TODO !!! History input to net might have been disabled !!!
    # TODO !!! SeedProp might have been set to False !!!
    # TODO !!! DAVIS iterator shuffling might have been disabled !!!
    # TODO !!! DAVIS iterator parallelism might have been disabled !!!

    print("CONFIG --->")
    print({var: Config.__dict__[var] for var in dir(Config) if not var.startswith("__")})
    print("<--- CONFIG")

    # LOAD DATA, INIT SegLabeling
    data_if = DataIF()
    DatasetClass = data_if.get_dataset_class()
    train_vidnames = DatasetClass.get_video_set_vidnames(SET_NAME, 'train')
    val_vidnames = DatasetClass.get_video_set_vidnames(SET_NAME, 'val')
    test_vidnames = DatasetClass.get_video_set_vidnames(SET_NAME, 'test')
    all_vidnames = set(train_vidnames + val_vidnames + test_vidnames)
    load_data_protocol_dict = {'simple': 'cached', 'davis': 'cached+bgr_im+of'}
    data_if.load_videos(all_vidnames, load_data=load_data_protocol_dict[Config.DATAGENTR_TRAINING_MODE])

    segs = {vidname: data_if.get_seg_obj(vidname) for vidname in all_vidnames}
    seg_labs = {vidname: SegLabeling(vidname, segs[vidname], labeling_mode='seg') for vidname in all_vidnames}
    max_n_labels = max([seg_labs[vidname].get_n_labels() for vidname in all_vidnames])
    n_labmodel_features = list(segs.values())[0].sp_chans['fvecs'].shape[-1] if Config.FIRST_N_FEATURES_TO_USE is None\
                                                                         else Config.FIRST_N_FEATURES_TO_USE
    print("Loaded videos & data:", SET_NAME)

    # INIT LabelModel
    lab_model = LogRegLabelModel()

    # INIT SeedProp
    if (Config.DATAGENTR_TRAINING_MODE == 'davis') and (Config.DATAGENTR_DAVIS_USE_SEEDPROP is True):
        seedprop_alg = BasicOptflowSeedPropagation(segs)
    else:
        seedprop_alg = None

    # INIT LabelEstimation (gnn only), PRETRAIN LabelEstimation
    p_dim = max_n_labels if Config.GNNBIN_NATIVE_MULTICLASS else None
    lab_est = GNNLabelEstimation('fvecs', lab_model, n_labmodel_features, multiclass_n_cats=p_dim, seedprop_alg=seedprop_alg)

    tr_segs = {vidname: segs[vidname] for vidname in train_vidnames}
    val_segs = {vidname: segs[vidname] for vidname in val_vidnames}
    tr_seglabs = {vidname: seg_labs[vidname] for vidname in train_vidnames}
    val_seglabs = {vidname: seg_labs[vidname] for vidname in val_vidnames}
    assert Config.LABEL_MODEL_METHOD == 'logreg', "Multiprocessing is only implemented with logreg label model currently"
    lab_model_init_fn = lambda: LogRegLabelModel()

    if Config.DATAGENTR_TRAINING_MODE == 'simple':
        tr_dataset = GraphTrainingDataset(tr_segs, tr_seglabs, lab_model_init_fn, 'fvecs', Config.GNNBIN_N_WORKERS, p_dim)
        val_dataset = GraphTrainingDataset(val_segs, val_seglabs, lab_model_init_fn, 'fvecs', Config.GNNBIN_N_WORKERS, p_dim)
        collate_fn = functools.partial(collate_training_samples, multiclass=Config.GNNBIN_NATIVE_MULTICLASS)
        tr_iter = DataLoader(tr_dataset, batch_size=Config.GNNBIN_BATCH_SIZE, num_workers=Config.GNNBIN_N_WORKERS, \
                                                                collate_fn=collate_fn, pin_memory=False)
        val_iter = DataLoader(val_dataset, batch_size=Config.GNNBIN_BATCH_SIZE, num_workers=Config.GNNBIN_N_WORKERS, \
                                                                collate_fn=collate_fn, pin_memory=False)
    elif Config.DATAGENTR_TRAINING_MODE == 'davis':
        N_PARALLEL_DAVIS_TR_SESSIONS = 10
        tr_iter = GraphTrainingDatasetDAVIS(tr_seglabs, lab_model_init_fn, 'fvecs', N_PARALLEL_DAVIS_TR_SESSIONS, \
                                                                        'train0', seedprop_alg, p_dim)
        val_iter = GraphTrainingDatasetDAVIS(val_seglabs, lab_model_init_fn, 'fvecs', N_PARALLEL_DAVIS_TR_SESSIONS, \
                                                                        'train1', seedprop_alg, p_dim)

        # TODO train/val iterator
        
    lab_est.pretrain_graphs(tr_iter, val_iter)


