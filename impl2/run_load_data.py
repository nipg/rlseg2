
#
# Runner script for loading all Segmentation instances (and creating cache) in the given dataset
#    split without training or evaluating anything. RLseg2 implementation #2
#
#   @author Viktor Varga
#

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import sys
sys.path.append('..')

import os
import numpy as np
import pickle
import functools

from data_if import DataIF
from segmentation import Segmentation
from segmentation_labeling import SegLabeling

import config as Config

if __name__ == '__main__':

    #SET_NAME = 'debug_train (3+1+1)'
    #SET_NAME = 'reduced_test (45+15+15)'
    SET_NAME = 'full (45+15+30)'

    print("CONFIG --->")
    print({var: Config.__dict__[var] for var in dir(Config) if not var.startswith("__")})
    print("<--- CONFIG")

    # LOAD DATA, INIT SegLabeling
    data_if = DataIF()
    DatasetClass = data_if.get_dataset_class()
    train_vidnames = DatasetClass.get_video_set_vidnames(SET_NAME, 'train')
    val_vidnames = DatasetClass.get_video_set_vidnames(SET_NAME, 'val')
    test_vidnames = DatasetClass.get_video_set_vidnames(SET_NAME, 'test')
    all_vidnames = set(train_vidnames + val_vidnames + test_vidnames)
    data_if.load_videos(all_vidnames, load_data='cached')

    segs = {vidname: data_if.get_seg_obj(vidname) for vidname in all_vidnames}
    seg_labs = {vidname: SegLabeling(vidname, segs[vidname], labeling_mode='seg') for vidname in all_vidnames}
    max_n_labels = max([seg_labs[vidname].get_n_labels() for vidname in all_vidnames])
    n_labmodel_features = list(segs.values())[0].sp_chans['fvecs'].shape[-1] if Config.FIRST_N_FEATURES_TO_USE is None\
                                                                         else Config.FIRST_N_FEATURES_TO_USE
    print("Loaded videos & data:", SET_NAME)


