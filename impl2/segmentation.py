
#
# A SV/SP segmentation of a video clip and all corresponding data. RLseg2 implementation #2
#
#   @author Viktor Varga
#

import sys
sys.path.append('..')

import os
import numpy as np
import skimage.measure
import time
import itertools

import util.imutil as ImUtil


class Segmentation:

    '''
    Member fields:
        data_source: str; either 'cache' or 'init' based on whether 'self' object was
                     created by loading from cache or throug __init__ from raw data
                     (NOT SERIALIZED) 
        seg_alg: module (stateless); the segmentation algorithm

        sp_seg: ndarray(n_frames, size_y, size_x) of int32; SP IDs; !!! only the ID difference from prev frame end offset is stored
        sp_id_fr_end_offsets: ndarray(n_frames,) of int32; SP ID end offests
        sp_data: ndarray(n_segs, 5:[sp_size, bbox_tlbr]) of int32

        sp_chans: dict{name - str: ndarray(n_sps, n_sp_chans) of ?;}

        sp_edges: dict{name - str: ndarray(n_edges, 2:[from_SP_id, to_SP_id]) of ?};
                                             where from_SP_id < to_SP_id; SP graph undirected edge lists
        sp_edge_chans: dict{tuple(edgefs_name - str, edgelist_name - str): ndarray(n_edges, n_ch) OR ndarray(n_edges, 2:[fwd, bwd], n_ch) of ?};
                                             edge order matching self.sp_edges; edgefs_name matching self.edgefs_funcs keys
                                             edgelists cannot be merged edgelists
        edgelist_mergers: dict{merged_edgelist_name - str: tuple(edgelist_names - list of str, None OR ndarray(n_total_size,) of int32)}
                                    index array to select items into merged list from the concatenation of the source lists


        (NOT SERIALIZED)

        ims: dict{name - str: ndarray(n_frames, size_y, size_x, n_chans) of ?;}

        segfs_funcs: dict{segfs_outchan_name - str: tuple(im_inchan_namefunc - str, func - callable, **kwargs - dict)}
                                where 'func' signature is: 
                                    (ndarray(n_pixels, n_in_chans) of ?, **func_kwargs) -> (ndarray(n_out_chans,) of ?
        edgegen_funcs: dict{edgelist_name - str: tuple(func - callable, **kwargs - dict)}
                                where 'func' signature is: 
                                    (Segmentation CONST, seg_sets - None OR list(n_seg_exts) of ndarray(n_segs_in_ext,) of int32, **func_kwargs)
                                    -> (new_edges - ndarray(n_new_edges, 2) of int32)
        edgefs_funcs:  dict{edgefs_name - str: tuple(edgelist_names - list of str, func - callable, **kwargs - dict)}
                                where 'func' signature is: 
                                    (Segmentation CONST, edges - ndarray(n_edges, 2) of int32, **func_kwargs)
                                    -> (edge_fs - ndarray(n_edges, 2, n_outchans) of ? OR ndarray(n_edges, n_outchans) of ?)

    '''

    def __init__(self, sp_seg):
        self.data_source = 'init'
        self.seg_alg = None

        self._init_segs(sp_seg)

        self.sp_chans = {}
        self.sp_edges = {}
        self.sp_edge_chans = {}

        self.ims = {}
        self.segfs_funcs = {}
        self.edgegen_funcs = {}
        self.edgelist_mergers = {}
        self.edgefs_funcs = {}
        

    # CUSTOM SERIALIZATION: to avoid serializing functions

    def __getstate__(self):
        assert self.data_source == 'init', "Object loaded from cache cannot be serialized again"
        d = {}
        d['sp_seg'] = self.sp_seg
        d['sp_id_fr_end_offsets'] = self.sp_id_fr_end_offsets
        d['sp_data'] = self.sp_data
        d['sp_chans'] = self.sp_chans
        d['sp_edges'] = self.sp_edges
        d['sp_edge_chans'] = self.sp_edge_chans
        d['edgelist_mergers'] = self.edgelist_mergers

        # from images, only raw gt annotation is serialized
        d['ims'] = {'gt_raw': self.ims['gt_raw']}

        # fields not serialized
        d['seg_alg'] = None  # self.seg_alg
        d['segfs_funcs'] = None  # self.segfs_funcs
        d['edgegen_funcs'] = None  # self.edgegen_funcs
        d['edgefs_funcs'] = None  # self.edgefs_funcs

        return d

    def __setstate__(self, d):
        self.__dict__ = d
        self.data_source = 'cache'
        self.seg_alg = None
        self.segfs_funcs = {}
        self.edgegen_funcs = {}
        self.edgefs_funcs = {}

    def set_seg_alg(self, seg_alg):
        self.seg_alg = seg_alg

    # PUBLIC CONST

    def get_shape(self):
        return self.sp_seg.shape

    def get_n_frames(self):
        return self.get_shape()[0]

    def get_n_sps_total(self):
        return self.get_fr_sp_id_end_offset(self.get_n_frames()-1)

    def get_n_sps_in_frames(self):
        return np.insert(np.diff(self.sp_id_fr_end_offsets), 0, [self.sp_id_fr_end_offsets[0]])

    def get_fr_sp_id_offset(self, fr_idx):
        return 0 if fr_idx == 0 else self.sp_id_fr_end_offsets[fr_idx-1]

    def get_fr_sp_id_offsets(self, fr_idxs):
        sp_id_fr_offsets = np.concatenate([[0], self.sp_id_fr_end_offsets[:-1]])
        return sp_id_fr_offsets[fr_idxs].astype(np.int32)

    def get_fr_sp_id_end_offset(self, fr_idx):
        return self.sp_id_fr_end_offsets[fr_idx]

    def get_sp_mask_for_fr_idxs(self, fr_idxs):
        '''
        Returns a mask over all sp segments. True where segment is located in frame in 'fr_idxs'.
        Parameters:
            fr_idxs: array-like(n_frames,) of int
        Returns:
            sp_mask: ndarray(n_sps,) of bool_
        '''
        sp_mask = np.zeros((self.get_n_sps_total(),), dtype=np.bool_)
        for fr_idx in fr_idxs:
            offset, end_offset = self.get_fr_sp_id_offset(fr_idx), self.get_fr_sp_id_end_offset(fr_idx)
            sp_mask[offset:end_offset] = True
        return sp_mask

    def get_framewise_sp_ids(self, sp_ids_global):
        fr_idxs = self.get_fr_idxs_from_sp_ids(sp_ids_global)
        return sp_ids_global - self.get_fr_sp_id_offsets(fr_idxs)

    def get_seg_sizes(self):
        '''
        Returns:
            seg_sizes: ndarray(n_segs,) of int32
        '''
        return self.sp_data[:,0]

    def get_seg_region(self, bbox_stlebr=None, framewise_seg_ids=False, keep_frame_dim=False):
        '''
        Returns pixelwise global or framewise segment IDs in the given 3D bbox.
        Paramters:
            bbox_stlebr: None OR tuple(6:stlebr) of ints (any can be None)
            framewise_seg_ids: bool; if True, SP IDs are returned as in self.sp_seg, if False, the appropriate ID offset is added
            keep_frame_dim: bool; if False, does not keep framewise axis when returned bbox is 1 frame long in time
        Returns:
            ndarray(n_frames_in_bbox, sy, sx) of int32; the global SP IDs if framewise_seg_ids is False, otherwise framewise IDs
        '''
        seg = self.sp_seg if bbox_stlebr is None else \
                        self.sp_seg[bbox_stlebr[0]:bbox_stlebr[3], bbox_stlebr[1]:bbox_stlebr[4], bbox_stlebr[2]:bbox_stlebr[5]]
        if not framewise_seg_ids:
            fr_offset, fr_end_offset = (0, self.sp_seg.shape[0]) if bbox_stlebr is None else (bbox_stlebr[0], bbox_stlebr[3])
            seg = seg + self.get_fr_sp_id_offsets(np.arange(fr_offset, fr_end_offset))[:,None,None]   # DO NOT use +=
        assert seg.ndim == 3
        if (not keep_frame_dim) and (seg.shape[0] == 1):
            seg = seg[0,:,:]
        return seg

    def get_segs_bbox(self, sp_ids):
        '''
        Returns the 3D bbox enclosing the given segments (global IDs)
        Parameters:
            sp_ids: ndarray(n_segments,) of int32
        Returns:
            bbox_stlebr: tuple(6:[stlebr]) of ints; 3D bbox in order: startframe-top-left-endframe-bottom-right (ends exclusive)
        '''
        fr_idxs = self.get_fr_idxs_from_sp_ids(sp_ids)
        fr_start, fr_end = np.amin(fr_idxs), np.amax(fr_idxs)+1
        bboxes = self.sp_data[sp_ids, 1:]
        bbox_tl = np.amin(bboxes[:,:2], axis=0)
        bbox_br = np.amax(bboxes[:,2:], axis=0)
        assert len(bbox_tl) == len(bbox_br) == 2
        return (fr_start, bbox_tl[0], bbox_tl[1], fr_end, bbox_br[0], bbox_br[1])

    def get_fr_idxs_from_sp_ids(self, sp_ids):
        '''
        Paramters:
            sp_ids: ndarray(n_sps) of int; (global IDs)
        Returns:
            fr_idxs: ndarray(n_sps) of int
        '''
        assert np.amax(sp_ids) < self.sp_id_fr_end_offsets[-1]
        return np.digitize(sp_ids, self.sp_id_fr_end_offsets)

    def get_sp_ids_from_coords(self, fr_idxs, points_yx, framewise_seg_ids=False):
        '''
        Paramters:
            fr_idxs: array-like(n_points,)
            points_yx: array-like(n_points, 2:[y, x])
            framewise_seg_ids: bool; if True, SP IDs are returned as in self.sp_seg, if False, the appropriate ID offset is added
        Returns:
            sp_ids: ndarray(n_points,) of int
        '''
        fr_idxs = np.asarray(fr_idxs, dtype=np.int32)
        points_yx = np.asarray(points_yx, dtype=np.int32)
        assert points_yx.shape[1:] == (2,)
        assert fr_idxs.shape[0] == points_yx.shape[0]
        assert np.all(fr_idxs >= 0) and np.all(fr_idxs < self.sp_seg.shape[0])
        assert np.all(points_yx >= 0) and np.all(points_yx < self.sp_seg.shape[1:])
        ret = self.sp_seg[fr_idxs, points_yx[:,0], points_yx[:,1]]
        if not framewise_seg_ids:
            ret += self.get_fr_sp_id_offsets(fr_idxs)
        assert ret.shape == fr_idxs.shape
        return ret

    def get_edge_features(self, edgefs_name, edgelist_name, ensure_copy=False):
        '''
        Creates and returns the edge feature array with the given name and for the given edgelists. Mergers are supported.
        Paramters:
            edgefs_name: str
            edgelist_name: str; can be an merged_edgelist_name as well
            ensure_copy: bool
        Returns:
            edgefs_arr: ndarray(n_edges, n_edgefs) of ? OR ndarray(n_edges, 2:[fwd_dir, bwd_dir], n_edgefs) of ?
        '''
        if edgelist_name in self.sp_edges.keys():
            edgefs_arr = self.sp_edge_chans[(edgefs_name, edgelist_name)]
            if ensure_copy:
                edgefs_arr = edgefs_arr.copy()
        elif edgelist_name in self.edgelist_mergers.keys():
            el_names, merged_indices = self.edgelist_mergers[edgelist_name]
            assert merged_indices is not None, "Edgelist merger index array has not yet been computed."
            edgefs_arr = np.concatenate([self.sp_edge_chans[(edgefs_name, el_name)] for el_name in el_names], axis=0)
            edgefs_arr = edgefs_arr[merged_indices,:]
        else:
            assert False

        return edgefs_arr

    # PUBLIC NON-CONST

    def compute_all(self):
        '''
        Computes all segment and edge features in a freshly initialized Segmentation instance.
        Modifies members:
            sp_chans, sp_edges, sp_edge_chans, edgelist_mergers
        '''
        print("    SEG COMPUTE_ALL START", time.time())
        # compute all image -> SP (segfs) functions
        self.sp_chans = {}
        for segfs_outchan_name, (im_inchan_name, func, func_kwargs) in self.segfs_funcs.items():
            print("    SEG COMPUTE_ALL segfs", segfs_outchan_name, time.time())
            self.sp_chans[segfs_outchan_name] = self._exec_segfs_func(func, im_inchan_name, seg_ids=None, **func_kwargs)

        # generate edgelists
        self.sp_edges = {}
        for edgelist_name, (func, func_kwargs) in self.edgegen_funcs.items():
            print("    SEG COMPUTE_ALL edgelists", edgelist_name, time.time())
            el_arr = func(self, seg_sets=None, **func_kwargs)
            # TODO assert new_edges are unique (in funcs)
            assert np.all(el_arr[:,0] < el_arr[:,1])    # TODO debug check, remove
            assert np.unique(el_arr[:,0] * 2**32 + el_arr[:,1]).shape[0] == el_arr.shape[0]  # TODO debug only, REMOVE
            # TODO debug assert all edges are unique ???
            self.sp_edges[edgelist_name] = el_arr

        # generate edgefs
        self.sp_edge_chans = {}
        for edgefs_name, (edgelist_names, func, func_kwargs) in self.edgefs_funcs.items():
            # update all edgefs arrays the same way as edgelists were updated (delete old edges with mask, add new edges to end)
            for edgelist_name in edgelist_names:
                print("    SEG COMPUTE_ALL edgefs", edgefs_name, edgelist_name, time.time())
                edges_arr = self.sp_edges[edgelist_name]
                self.sp_edge_chans[(edgefs_name, edgelist_name)] = func(self, edges_arr, **func_kwargs)

        # compute edgelist mergers
        for merged_edgelist_name in self.edgelist_mergers.keys():
            print("    SEG COMPUTE_ALL merger", merged_edgelist_name, time.time())
            edgelist_names, _ = self.edgelist_mergers[merged_edgelist_name]
            els = [self.sp_edges[el_name] for el_name in edgelist_names]
            _, merger_indices = ImUtil.merge_edge_lists_only(els, return_index=True)
            self.edgelist_mergers[merged_edgelist_name] = edgelist_names, merger_indices
        #
        print("    SEG COMPUTE_ALL DONE", merged_edgelist_name, time.time())

    def split_segments(self, conflicting_seeds):
        '''
        Split multiple segments with sets of conflicting seeds, then updates all segment and edge features.
        Parameters:
            conflicting_seeds: list[tuple(sp_id - int, ndarray(n_seeds, 3:[py, px, label]) of ints)]; list ordered by 'sp_id's.
        Modifies members:
            sp_seg, sp_data, sp_id_fr_end_offsets, sp_chans, sp_edges, sp_edge_chans, edgelist_mergers
        '''
        n_segs_in_frames = np.copy(self.get_n_sps_in_frames())  # (n_frames,)
        sp_ext_ranges_framewise = {}   # dict{fr_idx - int: dict{sp_old_id - int: tuple(sp_fwise_id, sp_fwise_offs, sp_fwise_endoffs)}}
                                       # storing new framewise ID range of children segments for each parent SP

        # iterate and split conflicting segments, merge updates into self.sp_seg, compute and gather size and bbox data for new segs
        new_sp_data = {}
        for sp_id, conflicting_seeds_arr in conflicting_seeds:
            fr_idx, = self.get_fr_idxs_from_sp_ids([sp_id])
            sp_id_fwise, = self.get_framewise_sp_ids([sp_id])
            im_bgr = self.ims['im_bgr'][fr_idx]
            seg_bbox_tlbr = self.sp_data[sp_id, 1:5]
            seg_bbox_stlebr = (fr_idx, seg_bbox_tlbr[0], seg_bbox_tlbr[1], fr_idx+1, seg_bbox_tlbr[2], seg_bbox_tlbr[3])
            seg_mask_in_bbox = self.get_seg_region(seg_bbox_stlebr, framewise_seg_ids=True) == sp_id_fwise

            # convert label range to 0..n_lab-1
            seed_points = conflicting_seeds_arr.copy()
            _, seed_points[:,2] = np.unique(seed_points[:,2], return_inverse=True)
            new_seg_in_bbox, new_bboxes_tlbr, new_seg_sizes = self.seg_alg.split_seg_with_seeds(im_bgr,\
                                                                     seg_bbox_tlbr, seg_mask_in_bbox, seed_points)
            # merging new SPs into segmentation: new ID #0 is assigned parent (old) segment ID,
            #               others are assigned IDs from range n_segs_in_frame .. n_segs_in_frame + (n_new_labels-1)

            new_glob_bboxes_tlbr = new_bboxes_tlbr + seg_bbox_tlbr    # local to global bbox conversion
            seg_bbox0_view = new_seg_in_bbox[new_bboxes_tlbr[0,0]:new_bboxes_tlbr[0,2], new_bboxes_tlbr[0,1]:new_bboxes_tlbr[0,3]]
            seg0_mask = seg_bbox0_view == 0
            new_seg_in_bbox += n_segs_in_frames[fr_idx]-1  # assigning all SPs IDs starting from n_segs_in_frame (SP#0 is given the old ID)
            seg_bbox0_view[seg0_mask] = sp_id_fwise      # assigning SP#0 to old sp ID
            self.sp_seg[fr_idx, seg_bbox_tlbr[0]:seg_bbox_tlbr[2], seg_bbox_tlbr[1]:seg_bbox_tlbr[3]][seg_mask_in_bbox] =\
                                                                                new_seg_in_bbox[seg_mask_in_bbox]
            # add new segment data
            new_sp_data[(fr_idx, sp_id_fwise)] = (new_seg_sizes[0], new_glob_bboxes_tlbr[0,:])   # add SP#0 to sp_data (overwrite old SP)
            if fr_idx not in sp_ext_ranges_framewise:
                sp_ext_ranges_framewise[fr_idx] = {}
            sp_ext_ranges_framewise[fr_idx][sp_id] = (sp_id_fwise, n_segs_in_frames[fr_idx], n_segs_in_frames[fr_idx]+new_seg_sizes.shape[0]-1)

            for new_sp_idx in range(1, new_seg_sizes.shape[0]):
                new_sp_id_fwise = new_sp_idx + n_segs_in_frames[fr_idx]-1
                new_sp_data[(fr_idx, new_sp_id_fwise)] = (new_seg_sizes[new_sp_idx], new_glob_bboxes_tlbr[new_sp_idx,:])   # add other new segments to sp_data (overwrite old SP)
            n_segs_in_frames[fr_idx] += new_seg_sizes.shape[0]-1

        print("old end offsets: ", self.sp_id_fr_end_offsets)

        # create new frame sp ID end offsets, create sp_ext_ranges dict
        new_end_offsets = np.cumsum(n_segs_in_frames)
        sp_ext_ranges = {}   # dict{sp_new_id - int: tuple(sp_global_offset, sp_global_endoffset)}
        for fr_idx, fr_entry_dict in sp_ext_ranges_framewise.items():
            for old_sp_id, (sp_id_fwise, fwise_offset, fwise_end_offset) in fr_entry_dict.items():
                print("SP ext entry: ", old_sp_id, fr_idx, sp_id_fwise, fwise_offset, fwise_end_offset)
                fr_new_offset = 0 if fr_idx == 0 else new_end_offsets[fr_idx-1]
                sp_ext_ranges[sp_id_fwise+fr_new_offset] = (fr_new_offset+fwise_offset, fr_new_offset+fwise_end_offset)

        # create old ID -> new ID mapping
        old_new_sp_id_mapping = np.empty((self.get_n_sps_total(),), dtype=np.int32)  # mapping global sp IDs from old to new
        for fr_idx in range(self.get_n_frames()):
            old_offset, old_end_offset = self.get_fr_sp_id_offset(fr_idx), self.get_fr_sp_id_end_offset(fr_idx)
            new_offset = 0 if fr_idx == 0 else new_end_offsets[fr_idx-1]
            old_new_sp_id_mapping[old_offset:old_end_offset] = np.arange(new_offset, new_offset+(old_end_offset-old_offset))

        # update self.sp_data (containing segment sizes and bboxes)

        # TODO check whether there are no sp_channels which are not updated by any segfs func

        updated_sp_data = np.full((np.sum(n_segs_in_frames), 5), dtype=np.int32, fill_value=-1)   # TODO np.full for debug, later use np.empty
        updated_sp_data[old_new_sp_id_mapping] = self.sp_data
        print("XXX")
        print("updated data sh:", updated_sp_data.shape)
        print("new endoffsets: ", new_end_offsets)
        print(old_new_sp_id_mapping.shape)
        u_old_new_sp_id_mapping = np.unique(old_new_sp_id_mapping)
        print(u_old_new_sp_id_mapping.shape)
        print("OLD_NEW missing: ", np.setdiff1d(np.arange(updated_sp_data.shape[0]), u_old_new_sp_id_mapping))
        print(len(new_sp_data))
        for (fr_idx, sp_id_fwise), (new_seg_size, new_glob_bbox_tlbr) in new_sp_data.items():
            fr_new_offset = 0 if fr_idx == 0 else new_end_offsets[fr_idx-1]
            print("writing idx: ", fr_new_offset+sp_id_fwise, " --> ", new_seg_size, new_glob_bbox_tlbr)
            updated_sp_data[fr_new_offset+sp_id_fwise, 0] = new_seg_size
            updated_sp_data[fr_new_offset+sp_id_fwise, 1:] = new_glob_bbox_tlbr
        assert np.all(updated_sp_data >= 0)   # TODO debug, remove later and use np.empty to init array
        self.sp_id_fr_end_offsets = new_end_offsets
        self.sp_data = updated_sp_data

        # compute IM -> SP channel (segfs) functions in new segments, update self.sp_chans array
        all_sp_ids_to_update = list(sp_ext_ranges.keys()) +\
                                     list(itertools.chain.from_iterable([list(range(v1, v2)) for (v1, v2) in sp_ext_ranges.values()]))
        new_sp_chans = {}
        for sp_chan_name, sp_chan_arr in self.sp_chans.items():
            new_sp_chan_arr = np.full((np.sum(n_segs_in_frames), sp_chan_arr.shape[1]), dtype=sp_chan_arr.dtype,\
                                                         fill_value=np.nan)   # TODO np.full for debug, later use np.empty
            new_sp_chan_arr[old_new_sp_id_mapping,:] = sp_chan_arr     # copy old contents
            new_sp_chans[sp_chan_name] = new_sp_chan_arr

        for segfs_outchan_name, (im_inchan_name, func, func_kwargs) in self.segfs_funcs.items():
            new_sp_chan_arr = new_sp_chans[segfs_outchan_name]
            new_sp_chan_arr[all_sp_ids_to_update,:] = self._exec_segfs_func(func, im_inchan_name, all_sp_ids_to_update, **func_kwargs)
            assert not np.any(np.isnan(new_sp_chan_arr))  # TODO debug, remove
        self.sp_chans = new_sp_chans

        # update edge lists to new ID arrangement
        #   get affected edges and segments 
        #       affected segs: all adjacent segments to old split segments
        #       affected edges: edges between any two affected segs
        #   remove affected edges, find new edges by calling the EDGEGEN func for edgelist, add new edges

        all_split_sp_ids = np.array(list(sp_ext_ranges.keys()), dtype=np.int32)
        deleted_edgemask_dict = {}   # {edgelist_name - str: ndarray(n_old_edges) of bool_} masking old edgelist arrays
        n_new_edges_dict = {}   # {edgelist_name - str: n_new_edges - int}
        for edgelist_name, (func, func_kwargs) in self.edgegen_funcs.items():
            print("ELDEBUG name", edgelist_name)
            el_arr = self.sp_edges[edgelist_name]
            el_arr = old_new_sp_id_mapping[el_arr]  # map edges from old to new seg IDs
            assert np.all(el_arr[:,0] < el_arr[:,1])    # TODO debug check, remove
            all_affected_old_segs = el_arr[np.any(np.isin(el_arr, all_split_sp_ids), axis=1),:].reshape(-1) # all adjacent segs to 
            all_affected_old_segs = np.unique(all_affected_old_segs)
            all_affected_edges_mask = np.any(np.isin(el_arr, all_affected_old_segs), axis=1)
            deleted_edgemask_dict[edgelist_name] = all_affected_edges_mask
            print("DELmask added -> ", edgelist_name, el_arr.shape, all_affected_edges_mask.shape)
            all_affected_edges = el_arr[all_affected_edges_mask, :]
            print("ELDEBUG all split SP IDs", all_split_sp_ids)
            print("ELDEBUG affected edges", all_affected_edges)
            el_arr = el_arr[~all_affected_edges_mask,:]   # remove affected edges

            affected_seg_sets = []
            for split_sp_id in all_split_sp_ids:    # affected segment sets grouped by individual splits
                affected_segs = all_affected_edges[np.any(all_affected_edges == split_sp_id, axis=1),:].reshape(-1)
                children_offset, children_end_offset = sp_ext_ranges[split_sp_id]           # add children SP IDs too
                affected_segs = np.concatenate([affected_segs, np.arange(children_offset, children_end_offset)])
                affected_segs = np.unique(affected_segs)
                affected_seg_sets.append(affected_segs)
                print("ELDEBUG affected segset", affected_segs)
            new_edges = func(self, affected_seg_sets, **func_kwargs)
            print("ELDEBUG new edges", new_edges)
            assert np.all(np.isin(new_edges, np.concatenate(affected_seg_sets, axis=0)))   # TODO debug check
            assert np.all(new_edges[:,0] < new_edges[:,1])    # TODO debug check, remove
            print("ELDEBUG new edges already in", [newedge for newedge in new_edges if np.any(np.all(newedge == el_arr, axis=1))])
            el_arr = np.concatenate([el_arr, new_edges], axis=0)
            print("ELARR sh:", el_arr.shape)
            print("U ELARR sh:", np.unique(el_arr, axis=0).shape)
            assert np.unique(el_arr[:,0] * 2**32 + el_arr[:,1]).shape[0] == el_arr.shape[0]  # TODO debug only, REMOVE
            n_new_edges_dict[edgelist_name] = new_edges.shape[0]    # new edges are located in the end of edgelist arrays
            self.sp_edges[edgelist_name] = el_arr

        # update edgefs arrays
        for edgefs_name, (edgelist_names, func, func_kwargs) in self.edgefs_funcs.items():
            # update all edgefs arrays the same way as edgelists were updated (delete old edges with mask, add new edges to end)
            for elname in edgelist_names:
                edgefs_arr = self.sp_edge_chans[(edgefs_name, elname)]
                edgedel_mask = deleted_edgemask_dict[elname]
                print("DELmask queried -> ", elname, edgefs_arr.shape, edgedel_mask.shape)
                edgefs_arr = edgefs_arr[~edgedel_mask,:]
                new_edges = self.sp_edges[elname][-n_new_edges_dict[elname]:,:]
                new_edgefs = func(self, new_edges, **func_kwargs)
                edgefs_arr = np.concatenate([edgefs_arr, new_edgefs], axis=0)
                self.sp_edge_chans[(edgefs_name, elname)] = edgefs_arr

        # update edge mergers
        for merged_edgelist_name in self.edgelist_mergers.keys():
            edgelist_names, _ = self.edgelist_mergers[merged_edgelist_name]
            els = [self.sp_edges[el_name] for el_name in edgelist_names]
            _, merger_indices = ImUtil.merge_edge_lists_only(els, return_index=True)
            self.edgelist_mergers[merged_edgelist_name] = edgelist_names, merger_indices
        #


    def add_im(self, name, img):
        '''
        Adds an image channel.
        Parameters:
            name: str; the ID of the image channel
            img: ndarray(n_frames, size_y, size_x, n_ch) of ?;
        '''
        assert name not in self.ims.keys()
        assert img.shape[:3] == self.sp_seg.shape
        assert img.ndim == 4
        self.ims[name] = img

    def add_sp_channel(self, name, vals):
        '''
        Adds a seg channel.
        Parameters:
            name: str; the ID of the seg channel
            vals: ndarray(n_sps, n_sp_chans) of ?;
        '''
        assert False, "TODO remove"  # remove this or make sure it is not possible to add sp channels which are not updated during a segment splitting
        assert vals.ndim == 2
        assert vals.shape[0] == self.get_n_sps_total()
        assert name not in self.sp_chans.keys()
        for fr_idx in range(self.get_n_frames()):
            vals = values_per_frame[fr_idx]
            assert vals.ndim == 2
            self.sp_chans[name] = vals

    def add_segfs_func(self, im_inchan_name, segfs_outchan_name, func, **func_kwargs):
        '''
        Parameters:
            im_inchan_name: str
            segfs_outchan_name: str
            func: callable, with signature: (ndarray(n_pixels, n_in_chans) of ?, **func_kwargs) -> (ndarray(n_out_chans,) of ?)
            func_kwargs: named varargs to call 'func' with
        '''
        assert segfs_outchan_name not in self.segfs_funcs.keys()
        self.segfs_funcs[segfs_outchan_name] = (im_inchan_name, func, func_kwargs)

    def add_edgegen_func(self, edgelist_name, func, **func_kwargs):
        '''
        Parameters:
            edgelist_name: str
            func: callable, with signature: (Segmentation CONST, seg_sets - None OR list(n_seg_exts) of ndarray(n_segs_in_ext,) of int32, **func_kwargs)
                                            -> (new_edges - ndarray(n_new_edges, 2) of int32)
            func_kwargs: named varargs to call 'func' with
        '''
        assert edgelist_name not in self.edgegen_funcs.keys()
        assert edgelist_name not in self.edgelist_mergers.keys()
        self.edgegen_funcs[edgelist_name] = (func, func_kwargs)

    def add_edgelist_merger(self, merged_edgelist_name, edgelist_names):
        '''
        Parameters:
            merged_edgelist_name: str
            edgelist_names: list of str; edgelist names to merge
        '''
        assert merged_edgelist_name not in self.edgegen_funcs.keys()
        assert merged_edgelist_name not in self.edgelist_mergers.keys()
        assert len(edgelist_names) >= 2
        self.edgelist_mergers[merged_edgelist_name] = (edgelist_names, None)

    def add_edgefs_func(self, edgefs_name, edgelist_names, func, **func_kwargs):
        '''
        Parameters:
            edgefs_name: str
            edgelist_names: list of str
            func: callable, with signature: (Segmentation CONST, edges - ndarray(n_edges, 2) of int32, **func_kwargs)
                                            -> (edge_fs - ndarray(n_edges, 2, n_outchans) of ? OR ndarray(n_edges, n_outchans) of ?)
            func_kwargs: named varargs to call 'func' with
        '''
        assert edgefs_name not in self.edgefs_funcs.keys()
        self.edgefs_funcs[edgefs_name] = (edgelist_names, func, func_kwargs)

    # PRIVATE

    def _exec_segfs_func(self, func, im_inchan_name, seg_ids=None, **func_kwargs):
        '''
        Computes segfs (IM -> SP) funcs on the specified segments. Does not modify self.
        Parameters:
            func: Callable with signature: (ndarray(n_pixels, n_inchans), **func_kwargs) -> (ndarray(n_outchans,))
            im_inchan_name: str; input image channel
            seg_ids: None OR ndarray(n_segs,) of int32; the segment IDs for which to execute segfs functions; if None, computing all segments
            func_kwargs: named varargs; named parameters for 'func'.
        Returns:
            sp_chan_update: ndarray(n_segs, n_out_chans)
        '''
        if seg_ids is None:
            seg_ids = np.arange(self.get_n_sps_total())
        fr_idxs = self.get_fr_idxs_from_sp_ids(seg_ids)   # (n_segs, )
        bbox_tlbrs = self.sp_data[seg_ids, 1:]            # (n_segs, 4:tlbr)
        seg_ids_fwise = self.get_framewise_sp_ids(seg_ids)   # (n_segs, )
        ims_arr = self.ims[im_inchan_name]
        sp_chan_outs = []
        for seg_id_idx in range(len(seg_ids)):
            fr_idx = fr_idxs[seg_id_idx]
            seg_pixs, = ImUtil.get_segment_pixels_from_bbox_and_mask(ims_arr[fr_idx], self.sp_seg[fr_idx],\
                                                                     bbox_tlbrs[seg_id_idx], [seg_ids_fwise[seg_id_idx]])
            assert seg_pixs.shape[0] > 0
            sp_chan_outs.append(func(seg_pixs, **func_kwargs))

        sp_chan_update = np.stack(sp_chan_outs, axis=0)
        assert sp_chan_update.shape[:-1] == (len(seg_ids),)
        return sp_chan_update

    def _init_segs(self, sp_seg):
        '''
        Parameters:
            sp_seg: ndarray(n_frames, size_y, size_x) of int; SP IDs
        Sets members:
            sp_seg, n_sps_in_frame, sp_data
        '''
        assert sp_seg.ndim == 3
        n_frames = sp_seg.shape[0]
        assert np.all(sp_seg >= 0)
        self.sp_seg = np.empty_like(sp_seg, dtype=np.int32)
        sp_id_fr_end_offsets = []
        new_sp_data = {}

        sp_id_offset = 0
        for fr_idx in range(n_frames):
            # store framewise SP IDs in self.sp_seg instead of the global SP IDs
            u_sp_fr_seg, inv_sp_fr_seg, c_sp_fr_seg = np.unique(sp_seg[fr_idx], return_inverse=True, return_counts=True)
            inv_sp_fr_seg = inv_sp_fr_seg.reshape(sp_seg.shape[1:])
            self.sp_seg[fr_idx,:,:] = inv_sp_fr_seg

            # get bbox from regionprops
            rprops = skimage.measure.regionprops(self.sp_seg[fr_idx]+1, cache=True)
            for rprop in rprops:
                # store segment size and bbox in self.sp_data
                new_sp_data[rprop.label+sp_id_offset-1] = (c_sp_fr_seg[rprop.label-1], rprop.bbox)

            sp_id_offset += u_sp_fr_seg.shape[0]
            sp_id_fr_end_offsets.append(sp_id_offset)

        self.sp_id_fr_end_offsets = np.array(sp_id_fr_end_offsets, dtype=np.int32)
        self.sp_data = np.empty((self.sp_id_fr_end_offsets[-1], 5), dtype=np.int32)
        for sp_idx in range(self.sp_data.shape[0]):
            new_sp_data_rec = new_sp_data[sp_idx]
            self.sp_data[sp_idx, :] = new_sp_data_rec[0], *new_sp_data_rec[1]
        #

