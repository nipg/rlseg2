
#
# Configuration module. RLseg2 implementation #1
#
#   @author Viktor Varga
#

# getting all fields of this module: {var: Config.__dict__[var] for var in dir(Config) if not var.startswith("__")}

# vi USAGE:
#    command mode: i - insert mode, u - undo, x - del char, hjkl - moving, o - nl below, ZZ - save and quit, :w<ENTER> - save, :q!<ENTER> - quit wo. saving
#    insert mode: esc - command mode

# main RUNNER

FOLDER_RENDER_PREDICTIONS = None   # '/home/vavsaai/git/rlseg2/temp_results/rendered_preds/'  # None to disable

FIRST_N_FEATURES_TO_USE = None   # int or None (fvec sizes: 16+24+32+96+320, total: 488)

LABEL_MODEL_METHOD = 'logreg'          # any of ['logreg'], "leo" is not implemented for impl#2
LABEL_ESTIMATION_ALGORITHM = 'gnn'   # any of ['basic', 'mrf', 'gnn']

# main RUNNER: benchmark task selection

BENCHMARK_TASK_SELECTION_FOLDER = '/home/vavsaai/databases/DAVIS/rlseg2_data/benchmark/'
BENCHMARK_TASK_SELECTION_FNAME_NOEXT = 'benchmark15_3rep_10nov2020'   #'benchmark_temp_debug_train_3p1p1'
BENCHMARK_TASK_SELECTION_N_REPEATS = 3    # ignored if loading a previously generated benchmark


# GNN, training/validation data generation

DATAGENTR_TRAINING_MODE = 'davis'   # 'simple' or 'davis'
#   simple training mode
DATAGENTR_N_NODES_REVEALED_PER_CAT_MIN = 100.   # n_nodes (per category) revealed in training/validation samples:
DATAGENTR_N_NODES_REVEALED_PER_CAT_MAX = 100.  #     n_nodes_revealed = ..._expbase ** uniform_rnd(..._min, ..._max)
DATAGENTR_N_NODES_REVEALED_PER_CAT_EXPBASE = 2.
DATAGENTR_LABELS_REVEALED_IN_N_FRAMES_MAX = None    # nodes are only revealed from this many randomly selected frames (if None, from all frames)
#   davis training mode
DATAGENTR_DAVIS_N_INTERACTIONS = 6
DATAGENTR_DAVIS_USE_SEEDPROP = True
DATAGENTR_DAVIS_SESSION_FRAME_QUERY_POLICY = 'random_uniform'   # None OR in ['random_uniform', 'random_linear_distance_prob']

#   replace label model with simulation
DATAGENTR_SIMULATE_LABEL_MODEL = False      # replace label model prediction node feature with noisy ground truth labels
DATAGENTR_DISABLE_LABEL_MODEL = False   # replace label model prediction node feature with uniform random noise
DATAGENTR_SIMULATE_PRED_ALPHA = 1.5    # param of beta distribution, orig: 1.5
DATAGENTR_SIMULATE_PRED_BETA = 10.     # param of beta distribution, orig: 10

# GNN LabelEstimation

GNNBIN_CHECKPOINT_DIR = '/home/vavsaai/git/rlseg2/temp_results/gated_gcn_binary_pretrain_checkpoints/checkpoint/'
GNNBIN_CHECKPOINT_NAME_TO_LOAD = 'run121_ep160.pkl'   # 'run102_ep160.pkl'
GNNBIN_BATCH_SIZE = 1
GNNBIN_N_WORKERS = 0        # set 0 to disable multiprocessing; set >=1 to enable multiprocessing and specify n. of worker processes

# GNN hyperparameters:

GNNBIN_NATIVE_MULTICLASS = False   # only in 'simple' training mode
GNNBIN_BINARY_DOUBLE_PREDICTION_ROUND = False   # if in binary mode (native multiclass == False), if True, predicts multiclass with double one-v-rest rounds
GNNBIN_BINARY_LOSS = 'bce'   # in ['bce', 'iou_bin']
GNNBIN_MULTICLASS_LOSS = 'miou'   # in ['ce', 'miou', 'miou_fg_only', 'mse', 'lovasz']
GNNBIN_MULTICLASS_W_MLP = False     # if False, w_i, w_j values are produced by a single linear layer, if True, a 2-layer MLP is used
GNNBIN_SHARED_PARAMS = False
GNNBIN_NO_EDGE_REPR = False
GNNBIN_NORMALIZE_ADJ_ATTENTION_SCORES = False   # multiclass only
GNNBIN_P_NODE_FEATURES = False
GNNBIN_DAVIS_SESSION_IDX_NODE_FEATURE = False
GNNBIN_HIDDEN_DIM = 20
GNNBIN_N_LAYERS = 12
GNNBIN_DROPOUT = .1
GNNBIN_INFEAT_DROPOUT = .0
GNNBIN_READOUT = "mean"
GNNBIN_GRAPH_NORM = True
GNNBIN_BATCH_NORM = True
GNNBIN_RESIDUAL = True
GNNBIN_EDGE_DROPOUT = False
GNNBIN_EDGE_DROPOUT_MIN_DEG = 2
GNNBIN_EDGE_DROPOUT_RATIO = 0.25

GNNBIN_N_EPOCHS = 241
GNNBIN_N_EPOCHS_CHECKPOINT_FREQ = 20
GNNBIN_INIT_LR = 5e-3
GNNBIN_MIN_LR = 1e-5
GNNBIN_WEIGHT_DECAY = 0.0
GNNBIN_LR_REDUCE_FACTOR = 0.6
GNNBIN_LR_SCHEDULE_PATIENCE = 20
GNNBIN_TR_EPOCH_SIZE = 32  # 32
GNNBIN_VAL_EPOCH_SIZE = 8  # 8

