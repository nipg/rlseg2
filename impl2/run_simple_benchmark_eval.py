
#
# Runner script for evaluating a model configuration with a SimpleBenchmark benchmark. RLseg2 implementation #2
#
#   @author Viktor Varga
#

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import sys
sys.path.append('..')

import os
import numpy as np
import pickle
import functools

from data_if import DataIF
from segmentation import Segmentation
from segmentation_labeling import SegLabeling

from label_estimation.basic_label_estimation import BasicLabelEstimation
from label_estimation.mrf_label_estimation import MRFLabelEstimation
from label_estimation.logreg_label_model import LogRegLabelModel
from benchmark.simple_benchmark_task_selection import SimpleBenchmarkTaskSelection

import util.viz as Viz
import config as Config

if Config.LABEL_ESTIMATION_ALGORITHM == 'gnn':
    from label_estimation.gnn_label_estimation import GNNLabelEstimation   # only import pytorch if used
    from label_estimation.graphs.graph_datagen import GraphDatagen, GraphTrainingDataset, collate_training_samples
    from torch.utils.data import DataLoader
else:
    assert False, "Not implemented."


if __name__ == '__main__':
    
    #SET_NAME = 'debug_train (3+1+1)'
    #SET_NAME = 'reduced_test (45+15+15)'
    #SET_NAME = 'reduced (30+10+15)'
    #SET_NAME = 'full (45+15+30)'
    SET_NAME = 'test_only_reduced (1+1+15)'

    print("CONFIG --->")
    print({var: Config.__dict__[var] for var in dir(Config) if not var.startswith("__")})
    print("<--- CONFIG")

    # LOAD DATA, INIT SegLabeling, LOAD image data if necessary
    data_if = DataIF()
    DatasetClass = data_if.get_dataset_class()
    train_vidnames = DatasetClass.get_video_set_vidnames(SET_NAME, 'train')
    val_vidnames = DatasetClass.get_video_set_vidnames(SET_NAME, 'val')
    test_vidnames = DatasetClass.get_video_set_vidnames(SET_NAME, 'test')
    all_vidnames = set(train_vidnames + val_vidnames + test_vidnames)
    load_data_rule = 'cached+bgr_im' if Config.FOLDER_RENDER_PREDICTIONS else 'cached'
    data_if.load_videos(all_vidnames, load_data=load_data_rule)

    segs = {vidname: data_if.get_seg_obj(vidname) for vidname in all_vidnames}
    seg_labs = {vidname: SegLabeling(vidname, segs[vidname], labeling_mode='seg') for vidname in all_vidnames}
    max_n_labels = max([seg_labs[vidname].get_n_labels() for vidname in all_vidnames])

    print("Loaded videos & data:", SET_NAME)

    # INIT LabelModel
    assert Config.LABEL_MODEL_METHOD == 'logreg', "Other algorithms are not implemented for impl#2."
    lab_model = LogRegLabelModel()

    # LOAD TRAINED LABEL MODEL
    n_labmodel_features = list(segs.values())[0].sp_chans['fvecs'].shape[-1] if Config.FIRST_N_FEATURES_TO_USE is None\
                                                                         else Config.FIRST_N_FEATURES_TO_USE
    lab_model.reset(n_features=n_labmodel_features, n_cats=None)
    lab_model.pretrain_mutliple_label_setups(xss_train=None, yss_train=None, xss_val=None, yss_val=None)

    # INIT LabelEstimation (gnn only), load trained LabelEstimation model
    if Config.LABEL_ESTIMATION_ALGORITHM == 'gnn':
        assert Config.DATAGENTR_TRAINING_MODE == 'simple'
        p_dim = max_n_labels if Config.GNNBIN_NATIVE_MULTICLASS else None
        lab_est = GNNLabelEstimation('fvecs', lab_model, n_labmodel_features, multiclass_n_cats=p_dim)
        lab_est.pretrain_graphs(tr_iter=None, val_iter=None)   # loads pretrained model (since None is passed instead of dataloaders)

    # INIT TASK SELECTION: create and save benchmark .pkl with info textfile if it has not been created yet
    benchmark_fpath = os.path.join(Config.BENCHMARK_TASK_SELECTION_FOLDER, Config.BENCHMARK_TASK_SELECTION_FNAME_NOEXT + '.pkl')
    if not os.path.isfile(benchmark_fpath):
        benchmark_n_sps_revealed_dict = {key: Config.BENCHMARK_TASK_SELECTION_N_REPEATS for key in [1, 2, 5, 10, 20, 50, 200, 500]}
        test_seg_labs = {vidname: seg_labs[vidname] for vidname in test_vidnames}
        task_selection = SimpleBenchmarkTaskSelection(benchmark_n_sps_revealed_dict, test_seg_labs)
        os.makedirs(Config.BENCHMARK_TASK_SELECTION_FOLDER, exist_ok=True)
        with open(benchmark_fpath, 'wb') as f:
            pickle.dump(task_selection, f)
        benchmark_info_fpath = os.path.join(Config.BENCHMARK_TASK_SELECTION_FOLDER, Config.BENCHMARK_TASK_SELECTION_FNAME_NOEXT + '.txt')
        with open(benchmark_info_fpath, 'w') as f:
            f.write(task_selection._to_string_full_info())

    # LOAD TASK SELECTION

    with open(benchmark_fpath, 'rb') as f:
        task_selection = pickle.load(f)
    print("Task selection loaded, benchmark videos: ", list(task_selection.get_vidnames_set()))
    assert task_selection.get_vidnames_set() == set(test_vidnames),\
                                     "Safety check: Task selection instance video set must match test video set."

    # EVALUATING: iterate test videos

    allvid_mean_accs = []
    allvid_min_accs = []
    allvid_max_accs = []
    for curr_vidname in task_selection.get_vidnames_set():

        print("    Video:", curr_vidname)
        curr_seg = segs[curr_vidname]
        curr_seg_lab = seg_labs[curr_vidname]

        # INIT LABEL ESTIMATION per video

        assert Config.LABEL_ESTIMATION_ALGORITHM in ['basic', 'mrf', 'gnn']
        if Config.LABEL_ESTIMATION_ALGORITHM == 'basic':
            lab_est = BasicLabelEstimation(curr_seg, 'fvecs', curr_seg_lab.n_labels, lab_model, n_labmodel_features)
        elif Config.LABEL_ESTIMATION_ALGORITHM == 'mrf':
            lab_est = MRFLabelEstimation(curr_seg, 'fvecs', curr_seg_lab.n_labels, lab_model, n_labmodel_features)
        elif Config.LABEL_ESTIMATION_ALGORITHM == 'gnn':
            assert Config.DATAGENTR_TRAINING_MODE == 'simple'
            lab_est.set_prediction_video(curr_vidname, curr_seg, curr_seg_lab.n_labels)
        curr_seg_lab.set_label_estimation(lab_est)


        # EVALUATION & VISUALIZATION

        print("Evaluation & visualization phase: ")
        if Config.FOLDER_RENDER_PREDICTIONS is not None:
            os.makedirs(Config.FOLDER_RENDER_PREDICTIONS, exist_ok=True)

        task_selection.reset(curr_vidname)
        accs = []
        for task in task_selection:
            curr_seg_lab.reset()
            curr_seg_lab.add_true_user_seg_annot(sp_ids=task['sp_ids'])
            curr_seg_lab.update_predictions()
            acc = curr_seg_lab.evaluate_predictions('mean_j_sp')
            print("    Task", task['name'], ":", acc)
            accs.append(acc)

            # rendering predictions
            if Config.FOLDER_RENDER_PREDICTIONS is not None:
                vidfname = str(lab_est) + '_' + curr_vidname + '_' + task['name_fname'] + '.avi'
                fpath_out = os.path.join(Config.FOLDER_RENDER_PREDICTIONS, vidfname)
                sp_is_gt_mask = curr_seg_lab.user_labels >= 0
                Viz.render_predictions_video(fpath_out, curr_seg_lab.pred_labels, sp_is_gt_mask, img_data[curr_vidname], curr_seg_lab.seg)

        n_repeats = task_selection.get_n_repeats()
        assert n_repeats is not None
        accs_arr = np.array(accs).reshape((-1, n_repeats))
        mean_accs_arr = np.mean(accs_arr, axis=-1)
        min_accs_arr = np.amin(accs_arr, axis=-1)
        max_accs_arr = np.amax(accs_arr, axis=-1)
        print("\nMean accs: ", mean_accs_arr)
        print("\nMax accs: ", max_accs_arr)
        print("\nMin accs: ", min_accs_arr)
        allvid_mean_accs.append(mean_accs_arr)
        allvid_min_accs.append(min_accs_arr)
        allvid_max_accs.append(max_accs_arr)

    # compute mean of per video mean/min/max accs
    print("\nMean of pervid mean accs: ", np.mean(np.stack(allvid_mean_accs, axis=0), axis=0))
    print("\nMean of pervid max accs: ", np.mean(np.stack(allvid_max_accs, axis=0), axis=0))
    print("\nMean of pervid min accs: ", np.mean(np.stack(allvid_min_accs, axis=0), axis=0))

    # plotting test results (TEMP)
    
    '''
    ref_means = [0.389, 0.497, 0.691, 0.788, 0.849, 0.894, 0.923, 0.943]
    ref_maxs = [0.506, 0.582, 0.776, 0.818, 0.867, 0.898, 0.927, 0.946]
    curr_means = np.mean(np.stack(allvid_mean_accs, axis=0), axis=0)
    curr_maxs = np.mean(np.stack(allvid_max_accs, axis=0), axis=0)
    plt_labels = ['1', '2', '5', '10', '20', '50', '200', '500']*2
    ref_concat = np.array(ref_means + ref_maxs)
    curr_concat = np.concatenate([curr_means, curr_maxs])

    plt_x = np.arange(len(ref_concat))  # the label locations
    width = 0.35  # the width of the bars

    fig, ax = plt.subplots()
    plt_rects1 = ax.bar(plt_x - width/2, ref_concat, width, label='Ref')
    plt_rects2 = ax.bar(plt_x + width/2, curr_concat, width, label='Curr')

    ax.set_ylabel('mean IoU')
    ax.set_xticks(plt_x)
    ax.set_xticklabels(plt_labels)
    ax.legend()
    fig.tight_layout()
    plt.savefig('results.png')
    '''
    
    # DESTRUCTORS
    lab_model.destroy()
