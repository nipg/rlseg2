
#
# Feature generation code for impl#2
#

import numpy as np
import time

from segmentation import Segmentation
import util.util as Util
import util.imutil as ImUtil

def find_spatial_temporal_edges(seg_obj, seg_sets, edge_type):
    '''
    Searches for spatial edges connecting any two segments in the segment ID arrays in 'seg_sets' list.
    If 'seg_sets' is None, all segments are processed.
    Parameters:
        seg_obj: Segmentation instance
        seg_sets: None OR list(n_seg_exts) of ndarray(n_segs_in_ext,) of int32; 
                            the seg ID sets to update, or None to execute for all segments
        edge_type: str; either 'spatial' or 'temporal'
    Returns:
        new_edges: ndarray(n_new_edges, 2) of int32
    '''
    assert edge_type in ['spatial', 'temporal']
    ignore_axes = [0] if edge_type == 'spatial' else [1,2]
    if seg_sets is None:
        sp_seg = seg_obj.get_seg_region(bbox_stlebr=None, framewise_seg_ids=False)
        return ImUtil.get_adj_graph_edge_list_fast(sp_seg, ignore_axes=ignore_axes)
    else:
        new_edges = []
        for seg_id_set in seg_sets:
            bbox_stlebr = seg_obj.get_segs_bbox(seg_id_set)
            sp_seg = seg_obj.get_seg_region(bbox_stlebr=bbox_stlebr, framewise_seg_ids=False)
            el_arr = ImUtil.get_adj_graph_edge_list_fast(sp_seg, ignore_axes=ignore_axes)
            el_arr = el_arr[np.all(np.isin(el_arr, seg_id_set), axis=1),:]   # keep edges where both ends are in seg_id_set
            new_edges.append(el_arr)
        return ImUtil.merge_edge_lists_only(new_edges, return_index=False)

def edgefs_segdist_pairwise_L2dist(seg_obj, edges, segfs_name):
    '''
    Computes edge features by taking L2 distance of specified segment features for segments connected by the given edges.
    Parameters:
        seg_obj: Segmentation instance
        edges: ndarray(n_edges, 2) of int32
        segfs_name: str; the key name in 'Segmentation.sp_chans' dict
    Returns: 
        edgefs: ndarray(n_edges, 1) of ?
    '''
    fs1 = seg_obj.sp_chans[segfs_name][edges[:,0],:]   # (n_edges, n_infeatures)
    fs2 = seg_obj.sp_chans[segfs_name][edges[:,1],:]   # (n_edges, n_infeatures)
    assert fs1.ndim == fs2.ndim == 2
    return np.linalg.norm(fs1.astype(np.float32) - fs2.astype(np.float32), axis=-1, keepdims=True)

def edgefs_optflow_diff(seg_obj, edges, segfs_name):
    '''
    Computes difference features of two vectors: returns 3 values -> [abs_angular_diff, min_mag, rel_mag]
      abs_angular_diff: in [0,pi]; pi if vectors point to opposite dir, 0 if same dir
      min_mag: length of shorter vec; positive float
      rel_mag: shorter_vec_len/longer_vec_len, in range [0,1], 1 for similar lengths, 0 for infinitely different lengths
    Parameters:
        seg_obj: Segmentation instance
        edges: ndarray(n_edges, 2) of int32
        segfs_name: str; the key name in 'Segmentation.sp_chans' dict
    Returns: 
        edgefs: ndarray(n_edges, 3) of float32
    '''
    ofs1 = seg_obj.sp_chans[segfs_name][edges[:,0],:]   # (n_edges, 2:[dy, dx])
    ofs2 = seg_obj.sp_chans[segfs_name][edges[:,1],:]   # (n_edges, 2:[dy, dx])
    assert ofs1.shape[0] == ofs2.shape[0]
    assert ofs1.shape[1:] == ofs2.shape[1:] == (2,)
    EPS = 1e-9
    edgefs = np.empty((ofs1.shape[0], 3), dtype=np.float32)
    angle1 = np.arctan2(ofs1[:,0], ofs1[:,1])
    angle2 = np.arctan2(ofs2[:,0], ofs2[:,1])
    edgefs[:,0] = np.fabs((angle1 - angle2 + np.pi) % (2.*np.pi) - np.pi)
    len1 = np.linalg.norm(ofs1, axis=-1)
    len2 = np.linalg.norm(ofs2, axis=-1)
    edgefs[:,1] = np.minimum(len1, len2)
    max_len = np.maximum(len1, len2)
    edgefs[:,2] = edgefs[:,1]/(max_len + EPS)
    return edgefs

def find_flow_edges_and_mrf_weights(seg_obj, seg_sets):
    '''
    Searches for flow edges connecting any two segments in the segment ID arrays in 'seg_sets' list.
    If 'seg_sets' is None, all segments are processed.
    The returned weights can be used for label propagation with the MRF model.
    Parameters:
        seg_obj: Segmentation instance
        seg_sets: None OR list(n_seg_exts) of ndarray(n_segs_in_ext,) of int32; 
                            the seg ID sets to update, or None to execute for all segments
    Returns:
        new_edges: ndarray(n_new_edges, 2) of int32
        edgefs: ndarray(n_new_edges, 1) of float32
    '''
    sp_sizes = seg_obj.sp_data[:,0]
    seg_sets = [None] if seg_sets is None else seg_sets    # to run on all segments
    new_edges = []
    edgefs = []
    for seg_id_set in seg_sets:
        # get segmentation map and flow images (cropped if a bbox is given)
        if seg_id_set is None:
            sp_seg = seg_obj.get_seg_region(bbox_stlebr=None, framewise_seg_ids=False)
            flow_fwd = seg_obj.ims['of_fw'][:-1,:,:]
            flow_bwd = seg_obj.ims['of_bw'][1:,:,:]
        else:
            bbox_stlebr = seg_obj.get_segs_bbox(seg_id_set)
            assert bbox_stlebr[3] - bbox_stlebr[0] >= 2
            sp_seg = seg_obj.get_seg_region(bbox_stlebr=bbox_stlebr, framewise_seg_ids=False)
            flow_fwd = seg_obj.ims['of_fw'][bbox_stlebr[0]:bbox_stlebr[3]-1, bbox_stlebr[1]:bbox_stlebr[4], bbox_stlebr[2]:bbox_stlebr[5],:]
            flow_bwd = seg_obj.ims['of_bw'][bbox_stlebr[0]+1:bbox_stlebr[3], bbox_stlebr[1]:bbox_stlebr[4], bbox_stlebr[2]:bbox_stlebr[5],:]
        # 

        print("    SEG EDGES2 flow1", time.time())
        edges_fwd, dir_fwd, c_fwd = ImUtil.find_sps_associated_by_flow(sp_seg, flow_fwd, flow_dir_forward=True)
        print("    SEG EDGES2 flow2", time.time())
        edges_bwd, dir_bwd, c_bwd = ImUtil.find_sps_associated_by_flow(sp_seg, flow_bwd, flow_dir_forward=False)
        print("    SEG EDGES2 flow3, edges shape:", edges_fwd.shape, edges_bwd.shape, ", t:", time.time())
        assert np.all(dir_fwd[edges_fwd[:,0] >= 0])          # assert all valid fwd edges point to a higher SP ID
        assert not np.any(dir_fwd[edges_fwd[:,0] < 0])          # assert all invalid fwd edges are reversed
        assert not np.any(dir_bwd)      # assert all bwd edges point to a lower SP ID
        assert np.all(edges_fwd[:,0] < edges_fwd[:,1])   # TODO remove
        assert np.all(edges_bwd[:,0] < edges_bwd[:,1])   # TODO remove
        source_sizes_fwd = sp_sizes[edges_fwd[np.arange(dir_fwd.shape[0]), 1-dir_fwd]].astype(np.float32)
        source_sizes_bwd = sp_sizes[edges_bwd[np.arange(dir_bwd.shape[0]), 1-dir_bwd]].astype(np.float32)
                                            # source ID should never be negative (since edges originate from valid SPs)
        edge_w_fwd = c_fwd / source_sizes_fwd
        edge_w_bwd = c_bwd / source_sizes_bwd
        assert np.all(edge_w_fwd) <= 1.
        assert np.all(edge_w_bwd) <= 1.
        print("    SEG EDGES2 flow4", time.time())
        edges_all, weights = ImUtil.merge_edge_lists([edges_fwd, edges_bwd], [edge_w_fwd, edge_w_bwd],\
                                 missing_weights=np.array([0., 0.], dtype=np.float32),
                                 fun_weight_merge=lambda ws: np.amin(ws, axis=1))

        print("    SEG EDGES2 flow5", time.time())
        # only keep edges that have both ends in 'seg_id_set' (also, remove invalid edges with -1 ends)
        if seg_id_set is not None:
            edges_kept_mask = np.all(np.isin(edges_all, seg_id_set), axis=1)
            edges_all = edges_all[edges_kept_mask,:]
            weights = weights[edges_kept_mask, None]
        new_edges.append(edges_all)
        edgefs.append(weights)


    if len(new_edges) == 1:
        return new_edges[0], edgefs[0]
    else:
        new_edges, merge_idxs = ImUtil.merge_edge_lists_only(new_edges, return_index=True)
        edgefs = np.concatenate(edgefs, axis=0)
        edgefs = edgefs[merge_idxs,:]
        return new_edges, edgefs

def find_flow_edges_and_gnn_features(seg_obj, seg_sets):
    '''
    Searches for flow edges connecting any two segments in the segment ID arrays in 'seg_sets' list.
    If 'seg_sets' is None, all segments are processed.
    The returned weights can be used for label propagation with the MRF model.
    Parameters:
        seg_obj: Segmentation instance
        seg_sets: None OR list(n_seg_exts) of ndarray(n_segs_in_ext,) of int32; 
                            the seg ID sets to update, or None to execute for all segments
    Returns:
        new_edges: ndarray(n_new_edges, 2) of int32
        edgefs: ndarray(n_new_edges, 2:[ID_from -> ID_to, ID_to -> ID_from], n_features) of float32
                        FEATURES (per direction):
                            #0: ratio of ID_from OF vecs pointing to ID_to; (0,1]
                            #1: ratio of these (pointing to ID_to) vectors returning to ID_from with flow_bwd; (0,1]
                            #2-3: mean, std of distances of the returning bwd vectors (positive in #1) target points from the
                                        original starting points of fwd vectors
    '''
    sp_sizes = seg_obj.sp_data[:,0]
    seg_sets = [None] if seg_sets is None else seg_sets    # to run on all segments
    new_edges = []
    edgefs = []
    for seg_id_set in seg_sets:
        # get segmentation map and flow images (cropped if a bbox is given)
        if seg_id_set is None:
            sp_seg = seg_obj.get_seg_region(bbox_stlebr=None, framewise_seg_ids=False)
            flow_fwd = seg_obj.ims['of_fw'][:-1,:,:]
            flow_bwd = seg_obj.ims['of_bw'][1:,:,:]
        else:
            bbox_stlebr = seg_obj.get_segs_bbox(seg_id_set)
            assert bbox_stlebr[3] - bbox_stlebr[0] >= 2
            sp_seg = seg_obj.get_seg_region(bbox_stlebr=bbox_stlebr, framewise_seg_ids=False)
            flow_fwd = seg_obj.ims['of_fw'][bbox_stlebr[0]:bbox_stlebr[3]-1, bbox_stlebr[1]:bbox_stlebr[4], bbox_stlebr[2]:bbox_stlebr[5],:]
            flow_bwd = seg_obj.ims['of_bw'][bbox_stlebr[0]+1:bbox_stlebr[3], bbox_stlebr[1]:bbox_stlebr[4], bbox_stlebr[2]:bbox_stlebr[5],:]
        # 

        n_fr = sp_seg.shape[0]
        sy, sx = sp_seg.shape[1:3]
        assert flow_fwd.shape == flow_bwd.shape == (n_fr-1, sy, sx, 2)
        base_mgrid = np.mgrid[:sy, :sx].transpose((1,2,0))  # (sy, sx, 2)
        EPS = 1e-9
        if sp_sizes is None:
            _, sp_sizes = np.unique(sp_seg, return_counts=True)
            assert sp_sizes.shape[0] == np.amax(sp_seg)+1

        edges_fwd = []
        edges_bwd = []
        fs_fwd = []
        fs_bwd = []
        for direction in ["fwd", "bwd"]:
            edges_ls = edges_fwd if direction == "fwd" else edges_bwd
            fs_ls = fs_fwd if direction == "fwd" else fs_bwd

            for fr_idx in range(n_fr-1):
                # fwd/bwd is swapped when direction is backwards
                flow_to = flow_fwd[fr_idx] if direction == "fwd" else flow_bwd[fr_idx]
                flow_back = flow_bwd[fr_idx] if direction == "fwd" else flow_fwd[fr_idx]
                sp_seg_orig = sp_seg[fr_idx] if direction == "fwd" else sp_seg[fr_idx+1]
                sp_seg_target = sp_seg[fr_idx+1] if direction == "fwd" else sp_seg[fr_idx]

                #
                fwd_mgrid, fwdbwd_mgrid, invalid_fwd, invalid_fwdbwd = ImUtil.transform_dense_with_flow_fwdbwd(flow_to, flow_back)
                fwd_mgrid, fwdbwd_mgrid = fwd_mgrid.astype(np.int32), fwdbwd_mgrid.astype(np.int32)   # (sy, sx, 2) each
                fwdbwd_deltalen = np.linalg.norm(fwdbwd_mgrid - base_mgrid, axis=-1)   # (sy, sx)
                ids_fwd = sp_seg_target[fwd_mgrid[:,:,0], fwd_mgrid[:,:,1]]    # (sy, sx) i32
                ids_fwdbwd = sp_seg_orig[fwdbwd_mgrid[:,:,0], fwdbwd_mgrid[:,:,1]]    # (sy, sx) i32

                # feature#0, #1: count flow vectors for each edge and count how many of them return to the origin segment
                flow_vec_ids = np.stack([sp_seg_orig, ids_fwd], axis=-1).astype(np.int32, copy=False)   # (sy, sx, 2) i32
                flow_vec_ret_mask = sp_seg_orig == ids_fwdbwd                                  # (sy, sx) bool
                flow_vec_ids64 = Util.convert_multichannel_i32_to_i64(flow_vec_ids)       # (sy, sx) ui64
                u_edges64, inv_edges64, c_edges64 = np.unique(flow_vec_ids64, return_inverse=True, return_counts=True)
                returning_inv_edges64 = inv_edges64.reshape(flow_vec_ids64.shape)[flow_vec_ret_mask & (~invalid_fwdbwd)]
                c_edges_ret64 = np.bincount(returning_inv_edges64, minlength=u_edges64.shape[0])
                u_edges = Util.restore_multichannel_i32_from_i64(u_edges64)       # (n_edges, 2:[from, to]) i32
                edge_from_sizes = sp_sizes[u_edges[:,0]].astype(np.float32)
                f0 = c_edges64/edge_from_sizes
                f1 = c_edges_ret64/(c_edges64 + EPS)
                assert np.all(f1 <= 1.)

                # feature#2, #3: select returning edges and get their deltas: mean/std over them grouped by the edge IDs
                fwdbwd_deltalen_returning = fwdbwd_deltalen[flow_vec_ret_mask & (~invalid_fwdbwd)]
                f2 = Util.apply_func_with_groupby_manyIDs(fwdbwd_deltalen_returning, returning_inv_edges64, np.mean,\
                                                                             assume_arange_to=u_edges64.shape[0], empty_val=0)  
                f3 = Util.apply_func_with_groupby_manyIDs(fwdbwd_deltalen_returning, returning_inv_edges64, np.std,\
                                                                             assume_arange_to=u_edges64.shape[0], empty_val=0) 
                edges_ls.append(u_edges)
                fs_ls.append(np.stack([f0, f1, f2, f3], axis=-1))

        # merge fwd and bwd edges
        edges = np.concatenate(edges_fwd + edges_bwd, axis=0)
        fs = np.concatenate(fs_fwd + fs_bwd, axis=0)
        edges_reversed = edges[:,0] > edges[:,1]
        edges[edges_reversed,:] = edges[edges_reversed,::-1]
        edges64 = Util.convert_multichannel_i32_to_i64(edges)
        edges64_u, edges_inv = np.unique(edges64, return_inverse=True)
        edges_u = Util.restore_multichannel_i32_from_i64(edges64_u)
        edges_u_fs = np.zeros((edges_u.shape[0], 2, fs.shape[1]), dtype=fs.dtype)
        edges_u_fs[edges_inv, edges_reversed.astype(np.int32),:] = fs

        # only keep edges that have both ends in 'seg_id_set'
        if seg_id_set is not None:
            edges_kept_mask = np.all(np.isin(edges_u, seg_id_set), axis=1)
            edges_u = edges_u[edges_kept_mask,:]
            edges_u_fs = edges_u_fs[edges_kept_mask,:,:]
        new_edges.append(edges_u)
        edgefs.append(edges_u_fs)

    if len(new_edges) == 1:
        return new_edges[0], edgefs[0]
    else:
        new_edges, merge_idxs = ImUtil.merge_edge_lists_only(new_edges, return_index=True)
        edgefs = np.concatenate(edgefs, axis=0)
        edgefs = edgefs[merge_idxs,:]
        return new_edges, edgefs


class EdgeGenFs:

    '''
    A class to simulate individual edgegen and edgefs functions using a single callable. EdgeFs results are stored until queried.
    Member fields:
        edgegenfs_func: Callable with signature:
            (Segmentation CONST, seg_sets - None OR list(n_seg_exts) of ndarray(n_segs_in_ext,) of int32, **func_kwargs)
                                    -> (new_edges - ndarray(n_new_edges, 2) of int32,
                                        edge_fs - ndarray(n_edges, 2, n_outchans) of ? OR ndarray(n_edges, n_outchans) of ?)
        results: list of tuple(seg_sets - list of ndarray(n_segs,) of int32, edge_fs - see 'edgegenfs_func' signature for details)

    '''

    def __init__(self, edgegenfs_func):
        self.edgegenfs_func = edgegenfs_func
        self.results = []

    def edgegen(self, seg_obj, seg_sets, **func_kwargs):
        new_edges, edge_fs = self.edgegenfs_func(seg_obj, seg_sets, **func_kwargs)
        self.results.append((new_edges, edge_fs))
        print("APPEND: ", new_edges.shape)
        return new_edges

    def edgefs(self, seg_obj, edges, **func_kwargs):
        print("RETRIEVE: ", edges.shape)
        ret_idx = None
        for idx, (new_edges, edge_fs) in enumerate(self.results):
            if new_edges.shape != edges.shape:
                continue
            if not np.all(edges == new_edges):
                continue
            ret_idx = idx
            break

        assert ret_idx is not None, "Failed to locate previously stored entry."
        _, ret_edge_fs = self.results[ret_idx]
        self.results = self.results[:ret_idx] + self.results[ret_idx+1:]   # delete entry from list
        return ret_edge_fs
    #

#

def reduce_mean(arr2d):
    # ndarray(n_pixels, n_chans) -> ndarray(n_chans,)
    assert arr2d.ndim == 2
    return np.mean(arr2d.astype(np.float64), axis=0, dtype=np.float64).astype(np.float32)

def reduce_std(arr2d):
    # ndarray(n_pixels, n_chans) -> ndarray(n_chans,)
    assert arr2d.ndim == 2
    return np.std(arr2d.astype(np.float64), axis=0, dtype=np.float64).astype(np.float32)

def reduce_any(arr2d):
    # ndarray(n_pixels, n_chans) -> ndarray(n_chans,)
    assert (arr2d.ndim == 2) and (arr2d.dtype == np.bool_)
    return np.any(arr2d, axis=0)

def reduce_most_frequent_item(arr2d):
    # ndarray(n_pixels, n_chans) -> ndarray(1,)
    assert arr2d.shape[1:] == (1,)
    return np.array([np.argmax(np.bincount(arr2d.reshape(-1)))], dtype=arr2d.dtype)

def reduce_to_image_histograms(arr2d, hist_len_per_ch, chs_to_keep):
    # ndarray(n_pixels, n_chans) -> ndarray(hist_len_per_ch*len(chs_to_keep),)
    assert arr2d.ndim == 2
    assert len(chs_to_keep) >= 1
    chan_results = [np.histogram(arr2d[:,ch_idx], bins=hist_len_per_ch, range=(0,255)) for ch_idx in chs_to_keep]
    return np.concatenate(chan_results)


# Dynamic, loading for big, (possibly many-channel) images with a ndarray compatible interface

class DynamicFeatureMapLoader:
    '''
    Class mimics a 4D ndarray of full resolution multi channel images. Shape: (n_frames, size_y, size_x, n_ch)
    Each full frame is stored as a set of downscaled images which are resized on demand
             and concatenated along the channel axis.
    Only one image (with all channels) can be queried at once.
    Previously queried frame is stored as an ndarray in 'self.frame_cache' to speed up repeated queries.

    Member fields:
        ds_ims: dict{str - layer_name: ndarray(n_imgs, ?sy, ?sx, ?n_ch) of float32}
        shape: tuple(4) of ints; the target shape
        ndim: int; length of self.shape
        dtype: ndarray.dtype; the target data type

        frame_cache: dict{fr_idx - int: ndarray(size_y, size_x, n_ch) of float32}; 
                                    limited to maximum one key (either empty or stores a single frame),
                                        except if 'self.preload_allowed' is True
        preload_allowed: bool; if True, unlimited frames are allowed in the cache
    '''

    def __init__(self, ds_ims, target_size_yx):
        assert len(target_size_yx) == 2

        # calculate target shape
        assert all([im.ndim == 4 for im in list(ds_ims.values())])
        n_ch = sum([im.shape[3] for im in list(ds_ims.values())])
        n_fr_set = set([im.shape[0] for im in list(ds_ims.values())])
        assert len(n_fr_set) == 1, "Frame count of images must match."
        n_frames = list(n_fr_set)[0]
        self.shape = (n_frames,) + target_size_yx + (n_ch,)
        self.ndim = len(self.shape)

        # set target dtype
        dtype_set = set([im.dtype for im in list(ds_ims.values())])
        assert len(dtype_set) == 1, "Datatype of images must match."
        self.dtype = list(dtype_set)[0]

        self.preload_allowed = False
        self.ds_ims = ds_ims
        self.frame_cache = {}

    def __getitem__(self, idxs):
        if np.issubdtype(type(idxs), np.integer):
            idxs = (idxs,)
        assert len(idxs) <= 4
        fr_idx = idxs[0]
        assert np.issubdtype(type(fr_idx), np.integer), "No slicing is supported along axis#0."
        if fr_idx not in self.frame_cache.keys():
            print("        Fmaploader, get", idxs, " - cache miss")
            if not self.preload_allowed:
                self.frame_cache.clear()
            self.frame_cache[fr_idx] = self._create_frame(fr_idx)
        resized_im = self.frame_cache[fr_idx]
        return resized_im[idxs[1:]]

    def __len__(self):
        return self.shape[0]

    def astype(self, dtype):
        self.dtype = dtype
        return self

    def _create_frame(self, fr_idx):
        resized_im = [ImUtil.fast_resize_nearest(self.ds_ims[ln][fr_idx], self.shape[1:3]) for ln in self.ds_ims.keys()]
        resized_im = np.concatenate(resized_im, axis=-1)   # (size_y, size_x, n_ch)
        return resized_im

    def preload_all(self):
        self.preload_allowed = True
        for fr_idx in range(self.shape[0]):
            self.frame_cache[fr_idx] = self._create_frame(fr_idx)
