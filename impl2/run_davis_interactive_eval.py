
#
# Runner script for running the DAVIS Interactive benchmark. RLseg2 implementation #2
#
#   @author Viktor Varga
#


import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import sys
sys.path.append('..')

import os
import numpy as np
import pickle
import functools

from datasets import DAVIS17
from data_if import DataIF
from segmentation import Segmentation
from segmentation_labeling import SegLabeling
from scipy.ndimage.morphology import distance_transform_cdt

from label_estimation.basic_label_estimation import BasicLabelEstimation
from label_estimation.mrf_label_estimation import MRFLabelEstimation
from label_estimation.logreg_label_model import LogRegLabelModel
from benchmark.simple_benchmark_task_selection import SimpleBenchmarkTaskSelection
from seed_propagation.basic_optflow_seed_propagation import BasicOptflowSeedPropagation

import util.imutil as ImUtil
from datasets import davis_utils as DavisUtils
import util.util_paper as PaperUtils
import config as Config

if Config.LABEL_ESTIMATION_ALGORITHM == 'gnn':
    from label_estimation.gnn_label_estimation import GNNLabelEstimation   # only import pytorch if used
else:
    assert False, "Not implemented."

from davisinteractive.session import DavisInteractiveSession    # install from pip, see https://interactive.davischallenge.org

if __name__ == '__main__':
    
    LABELING_MODE = 'seed_nosplit' if Config.DATAGENTR_DAVIS_USE_SEEDPROP else 'seg'
    assert LABELING_MODE in ['seg', 'seed_nosplit', 'seed']

    SET_NAME = 'test_only (1+1+30)'
    #SET_NAME = 'debug_test (1+1+3)'

    RENDER_RESULTS_PAPER = True

    # !!!! Warning! p_dim fix might have been disabled (edited in 3 locations)

    print("CONFIG --->")
    print({var: Config.__dict__[var] for var in dir(Config) if not var.startswith("__")})
    print("<--- CONFIG")

    # LOAD DATA, INIT SegLabeling, LOAD image data if necessary
    data_if = DataIF()
    DatasetClass = data_if.get_dataset_class()
    assert DatasetClass == DAVIS17
    test_vidnames = DatasetClass.get_video_set_vidnames(SET_NAME, 'test')
    load_data_protocol_dict = {'seg': 'cached', 'seed_nosplit': 'cached+bgr_im+of'}
    data_if.load_videos(test_vidnames, load_data=load_data_protocol_dict[LABELING_MODE])

    segs = {vidname: data_if.get_seg_obj(vidname) for vidname in test_vidnames}
    seg_labs = {vidname: SegLabeling(vidname, segs[vidname], labeling_mode=LABELING_MODE) for vidname in test_vidnames}   # TODO, use seed mode
    max_n_labels = max([seg_labs[vidname].get_n_labels() for vidname in test_vidnames])

    print("Loaded videos & data:", SET_NAME)

    # INIT LabelModel
    assert Config.LABEL_MODEL_METHOD == 'logreg', "Other algorithms are not implemented for impl#2."
    lab_model = LogRegLabelModel()

    # INIT SEED PROPAGATION
    #   seedprop_alg = DummySeedPropagation() if LABELING_MODE == 'seed' else None
    seedprop_alg = BasicOptflowSeedPropagation(segs) if Config.DATAGENTR_DAVIS_USE_SEEDPROP is True else None

    # LOAD TRAINED LABEL MODEL
    n_labmodel_features = list(segs.values())[0].sp_chans['fvecs'].shape[-1] if Config.FIRST_N_FEATURES_TO_USE is None\
                                                                         else Config.FIRST_N_FEATURES_TO_USE
    lab_model.reset(n_features=n_labmodel_features, n_cats=None)
    lab_model.pretrain_mutliple_label_setups(xss_train=None, yss_train=None, xss_val=None, yss_val=None)

    # INIT LabelEstimation (gnn only), load trained LabelEstimation model
    if Config.LABEL_ESTIMATION_ALGORITHM == 'gnn':
        p_dim = max_n_labels if Config.GNNBIN_NATIVE_MULTICLASS else None
        lab_est = GNNLabelEstimation('fvecs', lab_model, n_labmodel_features, multiclass_n_cats=p_dim, seedprop_alg=seedprop_alg)
        lab_est.pretrain_graphs(tr_iter=None, val_iter=None)   # loads pretrained model (since None is passed instead of dataloaders)
    else:
        assert False, "Not implemented."


    # ---------------------------- DAVIS Interactive Challenge - benchmark interaction visualization ----------------------------

    DAVIS_report_dir = '../temp_results/davis_interactive_reports/'
    os.makedirs(DAVIS_report_dir, exist_ok=True)
    
    # Configuration used in the challenges
    DAVIS_max_nb_interactions = 8 # Maximum number of interactions 
    DAVIS_max_time_per_interaction = 30 # Maximum time per interaction per object (in seconds)

    # Total time available to interact with a sequence and an initial set of scribbles
    DAVIS_max_time = DAVIS_max_nb_interactions * DAVIS_max_time_per_interaction # Maximum time per object

    # Metric to optimize
    DAVIS_metric = 'J'
    assert DAVIS_metric in ['J', 'F', 'J_AND_F']

    DAVIS_frame_query_method = 'default'   # 'default', 'equidistant', 'choose_from_distant'

    with DavisInteractiveSession(host='localhost',
                            user_key=None,
                            davis_root=DAVIS17.ROOT_FOLDER,
                            subset='val',
                            shuffle=False,
                            max_time=DAVIS_max_time,
                            max_nb_interactions=DAVIS_max_nb_interactions,
                            metric_to_optimize=DAVIS_metric,
                            report_save_dir=DAVIS_report_dir) as sess:
                            #report_save_dir=None) as sess:

        curr_vidname = None
        pred_metrics = None
        davis_iter = sess.scribbles_iterator()

        # iterate DAVIS benchmark
        annotated_fr_idxs = []
        seq_idx = 0
        for vidname, scribble, is_new_sequence in davis_iter:

            print("--> DAVIS STEP: ", vidname, is_new_sequence)

            # on new video, load video data
            if vidname != curr_vidname:
                seq_idx = -1
                curr_vidname = vidname
                curr_seg_lab = seg_labs[vidname]
                assert is_new_sequence

                # init/reset label estimation, seedprop
                assert Config.LABEL_ESTIMATION_ALGORITHM == 'gnn', "Other methods are not implemented; if other than GNN, should reinstantiate lab_est here..."
                lab_est.set_prediction_video(vidname, curr_seg_lab.seg, curr_seg_lab.n_labels)
                curr_seg_lab.set_label_estimation(lab_est)
                if LABELING_MODE in ['seed_nosplit', 'seed']:
                    curr_seg_lab.set_seed_propagation(seedprop_alg)

                if RENDER_RESULTS_PAPER is True:
                    root_folder_path = '/home/vavsaai/git/rlseg2/temp_results/ijcnn21_qual/'
                    PaperUtils.save_imgs_with_label_overlay(root_folder_path, vidname, fname_prefix='gt_', \
                                label_ims=segs[vidname].ims['gt_raw'][:,:,:,0], bgr_ims=segs[vidname].ims['im_bgr'], \
                                bgr_saturation=.5, label_alpha=0.7, render_scale=1.)

            # on new sequence (same or new video, restarted labeling session)
            if is_new_sequence:
                prev_scribble_dict = None
                curr_scribble_dict = None
                annotated_fr_idxs = []
                if pred_metrics is not None:
                    print("    -> metrics for previous sequence:", pred_metrics)
                curr_seg_lab.reset()
                pred_metrics = {'sp_accuracy': [], 'mean_j_sp':[], 'mean_j_raw':[]}
                davis_state_prev_preds = None
                davis_state_seed_hist = []
                davis_state_seed_prop_hist = []
                seq_idx += 1

            # extract new scribbles
            prev_scribble_dict = curr_scribble_dict
            curr_scribble_dict = scribble
            curr_annot_fr_idx, new_scribbles = DavisUtils.get_new_scribbles(vidname, prev_scribble_dict, curr_scribble_dict, (480, 854))
            annotated_fr_idxs.append(curr_annot_fr_idx)
            print("      fr idx: ", curr_annot_fr_idx)

            if RENDER_RESULTS_PAPER is True:
                root_folder_path = '/home/vavsaai/git/rlseg2/temp_results/ijcnn21_qual/'
                step_idx = len(annotated_fr_idxs)
                PaperUtils.save_img_with_scribble_overlay(root_folder_path, vidname, \
                            fname_prefix='scrib' + str(seq_idx) + '_' + str(step_idx) + '_', \
                            scribbles=(curr_annot_fr_idx, new_scribbles), bgr_ims=segs[vidname].ims['im_bgr'], \
                            bgr_saturation=.5, scribble_width=3, render_scale=1.)

            # convert new scribbles to seeds, use different algorithm if first step in current sequence
            N_SEEDS_PER_CAT_INITIAL = 100   # TODO in davis mode this is ignored
            N_SEEDS_PER_CAT_LATER = 100

            n_seeds_per_cat = N_SEEDS_PER_CAT_INITIAL if is_new_sequence else N_SEEDS_PER_CAT_LATER
            curr_seed_points = DavisUtils.davis_scribbles2seeds_uniform(new_scribbles, (480, 854), n_seeds_per_cat, \
                                                                       generate_bg=is_new_sequence)  # (n_seeds, 3:[y, x, lab])

            # submit seeds to label estimation, generate predictions
            assert curr_seed_points.shape[1:] == (3,)
            fr_idxs = np.full(curr_seed_points.shape[0], dtype=np.int32, fill_value=curr_annot_fr_idx)
            coords, labels = curr_seed_points[:,:2], curr_seed_points[:,2]

            if Config.DATAGENTR_TRAINING_MODE == 'davis':
                # ugly hack to be able to keep using the seg_lab to store annotation info;
                #   in 'davis' mode, the seg_lab is not used for this purpose, but only to get true label info

                if Config.GNNBIN_NATIVE_MULTICLASS is False:
                    lab_est.reset_prediction_davis_state()
                lab_est.set_prediction_davis_state(curr_annot_fr_idx, new_scribbles, \
                                                davis_state_prev_preds, davis_state_seed_hist, davis_state_seed_prop_hist)
                davis_state_prev_preds = lab_est.predict_all(return_probs=True)
                davis_state_prev_preds_am = np.argmax(davis_state_prev_preds, axis=-1)

                # disable p_dim fix by uncommenting line below
                #davis_state_prev_preds = np.pad(davis_state_prev_preds, ((0,0), (0, max_n_labels-davis_state_prev_preds.shape[-1])))

                _, davis_state_seed_hist, davis_state_seed_prop_hist = lab_est.get_prediction_davis_state()
                seg_im = curr_seg_lab.seg.get_seg_region(bbox_stlebr=None, framewise_seg_ids=False)
                pred_label_im = davis_state_prev_preds_am[seg_im]
                # get metrics
                for metric_name in pred_metrics.keys():
                    if metric_name == 'mean_j_raw':
                        true_lab_im = curr_seg_lab.get_true_im_labels()
                        metric_val = ImUtil.compute_pixelwise_labeling_error(true_lab_im, pred_label_im, curr_seg_lab.n_labels)
                    else:
                        metric_val = ImUtil.compute_segmentation_labeling_error(curr_seg_lab.get_true_seg_labels(), \
                                        davis_state_prev_preds_am, curr_seg_lab.n_labels, metric_name, seg_sizes=curr_seg_lab.seg.get_seg_sizes())
                    pred_metrics[metric_name].append(metric_val)


            elif Config.DATAGENTR_TRAINING_MODE == 'simple':
                if LABELING_MODE == 'seg':
                    sp_ids = curr_seg_lab.seg.get_sp_ids_from_coords(fr_idxs, coords, framewise_seg_ids=False)
                    sp_ids = np.array(sp_ids, dtype=np.int32).reshape(-1)
                    # TODO unique, choose higher count if conflicting
                    curr_seg_lab.add_custom_user_seg_annot(sp_ids, labels)
                elif LABELING_MODE == 'seed_nosplit':
                    curr_seg_lab.add_custom_user_seed_annot(fr_idxs, coords, labels)
                elif LABELING_MODE == 'seed':
                    assert False, "TODO implement"

                curr_seg_lab.update_predictions()
                # get metrics
                for metric_name in pred_metrics.keys():
                    metric_val = curr_seg_lab.evaluate_predictions(metric_name)
                    pred_metrics[metric_name].append(metric_val)

            # produce frame query list
            assert DAVIS_frame_query_method in ['default', 'equidistant', 'choose_from_distant']
            if DAVIS_frame_query_method == 'default':
                frames_to_query = None
            elif DAVIS_frame_query_method == 'equidistant':
                query_ratios = [0.5, 0.25, 0.75, 0.125, 0.375, 0.625, 0.875, 0.]
                next_fr = int(query_ratios[len(annotated_fr_idxs)-1]*curr_seg_lab.seg.get_n_frames())
                frames_to_query = [min(max(0, next_fr), curr_seg_lab.seg.get_n_frames()-1)]
            elif DAVIS_frame_query_method == 'choose_from_distant':
                frame_dists = np.ones((curr_seg_lab.seg.get_n_frames(),), dtype=np.int32)
                frame_dists[annotated_fr_idxs] = 0
                frame_dists = distance_transform_cdt(frame_dists)
                frames_to_query = list(np.argsort(frame_dists)[(3*frame_dists.shape[0])//4:])
                frames_to_query = frames_to_query
                
            # submit predicted masks to DAVIS benchmark, resize predictions if necessary
            if Config.DATAGENTR_TRAINING_MODE == 'simple':
                pred_label_im = curr_seg_lab.get_predictions_image()   # in davis mode, 'pred_label_im' is generated earlier

            if RENDER_RESULTS_PAPER is True:
                root_folder_path = '/home/vavsaai/git/rlseg2/temp_results/ijcnn21_qual/'
                step_idx = len(annotated_fr_idxs)
                PaperUtils.save_imgs_with_label_overlay(root_folder_path, vidname, \
                                fname_prefix='pred' + str(seq_idx) + '_' + str(step_idx) + '_', \
                                label_ims=pred_label_im, bgr_ims=segs[vidname].ims['im_bgr'], \
                                bgr_saturation=.5, label_alpha=0.7, render_scale=1.)

            if curr_vidname in DAVIS17.IM_RESIZE_DICT.keys():
                pred_label_im = ImUtil.fast_resize_video_nearest_singlech(pred_label_im, DAVIS17.IM_RESIZE_DICT[curr_vidname])

            print("---> DAVIS working...")
            DavisUtils.fix_label_image_error(vidname, pred_label_im)
            sess.submit_masks(pred_label_im, next_scribble_frame_candidates=frames_to_query)

        # Get the global summary
        report_save_fpath = os.path.join(DAVIS_report_dir, 'summary.json')
        summary = sess.get_global_summary(save_file=report_save_fpath)

    # DESTRUCTORS
    lab_model.destroy()

