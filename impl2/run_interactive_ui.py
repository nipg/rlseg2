
#
# Runner script for running the interactive UI. RLseg2 implementation #2
#
#   @author Viktor Varga
#

import sys
sys.path.append('..')

import os
import numpy as np
import pickle

from data_if import DataIF
from segmentation import Segmentation
from segmentation_labeling import SegLabeling

from label_estimation.basic_label_estimation import BasicLabelEstimation
from label_estimation.mrf_label_estimation import MRFLabelEstimation
from label_estimation.logreg_label_model import LogRegLabelModel

from seed_propagation.dummy_seed_propagation import DummySeedPropagation

from segmentation_algorithm import watershed_segmentation_algorithm as WatershedSegAlg
#from segmentation_algorithm import slic_segmentation_algorithm as SLICSegAlg

from interactive.annotator import InteractiveAnnotator
import tkinter as tk

import config as Config
if Config.LABEL_ESTIMATION_ALGORITHM == 'gnn':
    from label_estimation.gnn_label_estimation import GNNLabelEstimation   # only import pytorch if used

if __name__ == '__main__':

    INTERACTIVE_MODE = 'seg'
    assert INTERACTIVE_MODE in ['seg', 'seed']
    
    #CURR_VIDNAME = 'bear'
    #CURR_VIDNAME = 'car-shadow'
    CURR_VIDNAME = 'dance-twirl'
    #CURR_VIDNAME = 'gold-fish'

    print("CONFIG --->")
    print({var: Config.__dict__[var] for var in dir(Config) if not var.startswith("__")})
    print("<--- CONFIG")

    # LOAD DATA, INIT SegLabeling, LOAD image data if necessary
    data_if = DataIF()
    load_data_protocol = 'all' if INTERACTIVE_MODE == 'seed' else 'cached+bgr_im'
    data_if.load_videos([CURR_VIDNAME], load_data=load_data_protocol)

    curr_seg = data_if.get_seg_obj(CURR_VIDNAME)
    curr_seg_lab = SegLabeling(curr_seg, labeling_mode=INTERACTIVE_MODE)
    max_n_labels = curr_seg_lab.get_n_labels()
    if INTERACTIVE_MODE == 'seed':
        curr_seg.set_seg_alg(WatershedSegAlg)
        #curr_seg.set_seg_alg(SLICSegAlg)
    print("Loaded videos & data for video:", CURR_VIDNAME)

    # INIT LabelModel
    assert Config.LABEL_MODEL_METHOD == 'logreg', "Other algorithms are not implemented for impl#2."
    n_labmodel_features = curr_seg.sp_chans['fvecs'].shape[-1] if Config.FIRST_N_FEATURES_TO_USE is None else Config.FIRST_N_FEATURES_TO_USE
    lab_model = LogRegLabelModel()

    # INIT LABEL ESTIMATION
    assert Config.LABEL_ESTIMATION_ALGORITHM in ['basic', 'mrf', 'gnn']
    if Config.LABEL_ESTIMATION_ALGORITHM == 'basic':
        lab_est = BasicLabelEstimation(curr_seg, 'fvecs', curr_seg_lab.n_labels, lab_model, n_labmodel_features)
    elif Config.LABEL_ESTIMATION_ALGORITHM == 'mrf':
        lab_est = MRFLabelEstimation(curr_seg, 'fvecs', curr_seg_lab.n_labels, lab_model, n_labmodel_features)
    elif Config.LABEL_ESTIMATION_ALGORITHM == 'gnn':
        p_dim = max_n_labels if Config.GNNBIN_NATIVE_MULTICLASS else None
        lab_est = GNNLabelEstimation('fvecs', lab_model, n_labmodel_features, multiclass_n_cats=p_dim)
        lab_est.pretrain_graphs(tr_dataloader=None, val_dataloader=None)   # loads pretrained model (since None is passed instead of dataloaders)
        lab_est.set_prediction_video(curr_seg, curr_seg_lab.n_labels)
    curr_seg_lab.set_label_estimation(lab_est)

    # INIT SEED PROPAGATION
    if INTERACTIVE_MODE == 'seed':
        seed_prop = DummySeedPropagation()
        curr_seg_lab.set_seed_propagation(seed_prop)

    # LOAD TRAINED LABEL MODEL
    lab_model.reset(n_features=n_labmodel_features, n_cats=None)
    lab_model.pretrain_mutliple_label_setups(xss_train=None, yss_train=None, xss_val=None, yss_val=None)

    # INIT Interactive annotator
    root_widget = tk.Tk()
    annotator = InteractiveAnnotator(root_widget, curr_seg.ims['im_bgr'], curr_seg_lab, CURR_VIDNAME, mode=INTERACTIVE_MODE)
    root_widget.mainloop()    # takes over control of the main thread

    # DESTRUCTORS
    lab_model.destroy()
