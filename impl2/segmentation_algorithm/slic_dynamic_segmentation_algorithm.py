
#
# SLIC Superpixel interface - dynamically generated (no pregeneration available)
#

import numpy as np
from skimage.segmentation import slic
import skimage.measure
from sklearn.neighbors import KNeighborsClassifier
import cv2

from util import imutil as ImUtil

N_SEGMENTS_PER_FRAME = 256   # approximate target
SLIC_COMPACTNESS = 15
SLIC_ZERO_ENABLED = False
N_NEW_SEGMENTS_AT_SPLIT = 4
KNN_SPATIAL_WEIGHT = 0.02

def get_n_base_segs_per_frame():
    '''
    Returns:
        n_segs: int; approximate target
    '''
    return N_SEGMENTS_PER_FRAME

def get_base_sp_segmentation(**kwargs):
    '''
    Creates SLIC segmentation, no hierarchy support.
    Parameters (kwargs):
        ims_bgr: ndarray(n_ims, sy, sx, 3:bgr) of uint8
    Returns:
        seg_arr: ndarray(n_ims, sy, sx) of uint32
    '''
    ims_bgr = kwargs['ims_bgr']
    assert im.dtype == np.uint8
    assert ims_bgr.shape[3:] == (3,)
    im_fl32 = ims_bgr[:,:,:,::-1].astype(np.float32)/255.  # bgr -> rgb float [0,1]
    seg_arr = np.empty(im_fl32.shape[:3], dtype=np.int32)
    offset = 0
    for fr_idx in range(im_fl32.shape[0]):
        seg = slic(im_fl32[fr_idx], n_segments=N_SEGMENTS_PER_FRAME, compactness=SLIC_COMPACTNESS, sigma=0, multichannel=True, start_label=0)
        # TODO TEMP DEBUG check
        u_seg = np.unique(seg)
        assert u_seg.shape[0] == u_seg[-1]+1
        # TODO TEMP DEBUG check END
        seg_arr[fr_idx,:,:] = seg + offset
        offset += u_seg.shape[0]
    return seg_arr


def split_seg(im_bgr_fl32, seg, seg_to_split_idx, seg_idx_end_offset):
    '''
    Splits a segment with SLIC segmentation. Image and mask size must match, not using bbox in this method.
    Parameters:
        im_bgr_fl32: ndarray(sy, sx, 3:bgr) of float32 in range [0..1]; the image
        (MODIFIED) seg: ndarray(sy, sx) of int32;
        seg_to_split_idx: int; the ID of the segment to be split
        seg_idx_end_offset: int; new segment IDs are given out starting from this idx (exception: 'seg_to_split_idx' is reused)
    Returns:
        seg: ndarray(sy, sx) of int32; new seg IDs indexed from 0, background is -1
                leaves all previous IDs untouched except for 'seg_to_split_idx'; new IDs are given starting from 'seg_idx_end_offset'
        seg_idx_new_end_offset: int; maximum ID assigned +1
    '''
    seg_mask = seg == seg_to_split_idx
    assert np.count_nonzero(seg_mask) > 1
    seg_split = slic(im_bgr_fl32, n_segments=N_NEW_SEGMENTS_AT_SPLIT, compactness=SLIC_COMPACTNESS,\
                    sigma=0, multichannel=True, start_label=1, slic_zero=SLIC_ZERO_ENABLED, mask=seg_mask)
        # while start_label is set to 1, unfortunately this SLIC implementation sometimes still leaves zero areas inside of mask area
    seg_split_masked = seg_split[seg_mask]
    seg_split_min = np.amin(seg_split_masked)  # should be 0 or 1
    seg_split_max = np.amax(seg_split_masked)
    assert seg_split_min < seg_split_max, "Split failed."
    # reuse old ID: assign old ID to new segment with 'seg_split_min' ID
    seg_nonmin_mask = seg_mask & (seg_split != seg_split_min)   # mask indexing all new segments except the one with the minimum ID
    seg[seg_nonmin_mask] = seg_split[seg_nonmin_mask] + (seg_idx_end_offset - seg_split_min - 1)
    seg_idx_new_end_offset = seg_split_max + seg_idx_end_offset - seg_split_min
    assert np.amax(seg)+1 == seg_idx_new_end_offset   # TODO debug, remove

    return seg, seg_idx_new_end_offset

def merge_unlabeled_segs(im_bgr_fl32, seg, seg_labels):
    '''
    Merges unlabeled segments into labeled segments.
    Parameters:
        im_bgr_fl32: ndarray(sy, sx, 3:bgr) of float32
        (MODIFIED) seg: ndarray(sy, sx) of int32; bg (labeled with 0) is ignored.
        seg_labels: ndarray(n_labels,) of int32; the label associated with each segment; multiple segments may have the same labels
                                -1 corresponds to unlabeled segments
    Returns:
        seg: ndarray(sy, sx) of int32;
    '''
    # get mean bgr for each segment
    rprops = skimage.measure.regionprops(seg, cache=True)
    n_segs = len(rprops)
    assert seg_labels.shape == (n_segs,)
    seg_features = np.empty((n_segs, 3), dtype=np.float32)
    for rprop in rprops:
        seg_pixs = im_bgr_fl32[rprop.bbox[0]:rprop.bbox[2], rprop.bbox[1]:rprop.bbox[3],:][rprop.image,:]  # (n_pix, 3)
        seg_features[rprop.label-1,:] = np.mean(seg_pixs, axis=0)
    
    # get adjacencies, compute color distance of each adjacent pair of segs
    seg_adjs = ImUtil.get_adj_graph_edge_list_fast(seg.astype(np.uint32)).astype(np.int32) # adjacencies (1-connectivity)
    assert np.all(seg_adjs[:,0] < seg_adjs[:,1])  # TODO debug, remove
    seg_adjs = seg_adjs[np.all(seg_adjs > 0, axis=1)]   # filter edges to/from bg
    seg_adj_dists = np.linalg.norm(seg_features[seg_adjs[:,0]-1,:] - seg_features[seg_adjs[:,1]-1,:], axis=1, ord=2)

    # sort edges by dists, start with smallest color distance
    seg_adj_sorter = np.argsort(seg_adj_dists)
    seg_adjs = seg_adjs[seg_adj_sorter,:]   # 'seg_adj_dists' array is not maintained

    # store seg parents and labels, loop over edges and merge segs
    seg_parents = np.arange(n_segs+1)    # (n_segs+1,), idx#0 is bg
    seg_labels = seg_labels.copy()       # (n_segs,)
    for (seg_from, seg_to) in seg_adjs:
        seg_from_p, seg_to_p = seg_parents[seg_from], seg_parents[seg_to]
        # skip if already share parents
        if seg_from_p == seg_to_p:
            continue
        # skip if both are labeled differently
        if (seg_labels[seg_from-1] > 0) and (seg_labels[seg_to-1] > 0) and (seg_labels[seg_from-1] != seg_labels[seg_to-1]):
            continue
        # merge otherwise
        new_parent = min(seg_from_p, seg_to_p)
        affected_segs_mask = (seg_parents == seg_from_p) | (seg_parents == seg_to_p)
        seg_parents[affected_segs_mask] = new_parent
        new_label = max(seg_labels[seg_from-1], seg_labels[seg_to-1])  # at least one of them is -1
        seg_labels[affected_segs_mask[1:]] = new_label

    # check if all segments are labeled, reassign segment IDs
    assert np.all(seg_labels >= 0)
    seg = seg_parents[seg]
    return seg

def split_seg_with_seeds(im_bgr, seg_bbox_tlbr, seg_mask_in_bbox, seed_points):
    '''
    Splits a segment with SLIC segmentation with respect to sets of differently labeled seed points.
    Tries to return a segmentation with the minimum possible number of segments.
    Parameters:
        im_bgr: ndarray(sy, sx, 3:bgr) of uint8; full image
        seg_bbox_tlbr: array-like(4:[tlbr]) of ints
        seg_mask_in_bbox: ndarray(b-t, r-l) of bool_
        seed_points: ndarray(n_seeds, 3:[py, px, label]) of ints; must contain seeds with at least two distinct labels
                                     labels must be from range 0..n_labels-1; indexing original image
    Returns:
        new_seg_in_bbox: ndarray(bbox_sy, bbox_sx) of int32; new seg IDs indexed from 0, background is -1
        new_bboxes_tlbr: ndarray(n_new_segs, 4:[tlbr]) of int32; new seg bboxes (RELATIVE to 'new_seg_in_bbox')
        new_seg_sizes: ndarray(n_new_segs,) of int32; new seg sizes
    '''
    assert im_bgr.shape[2:] == (3,)
    assert seed_points.shape[1:] == (3,)
    u_labels = np.unique(seed_points[:,2])
    n_labels = len(u_labels)
    assert (np.amin(u_labels) == 0) and (np.amax(u_labels) == n_labels-1)
    assert 16 >= n_labels > 1
    assert np.issubdtype(seed_points.dtype, np.integer)
    assert np.unique(seed_points[:,:2], axis=0).shape[0] == seed_points.shape[0], "All seeds must be unique"
    N_MAX_ITERATIONS = 20

    seed_points = seed_points - np.array([seg_bbox_tlbr[0], seg_bbox_tlbr[1], 0])   # seed points relative to bbox
    new_seg_in_bbox = seg_mask_in_bbox.astype(np.int32)   # (bbox_sy, bbox_sx), -1 bg, segs starting from 0
    im_bgr_fl32 = im_bgr[seg_bbox_tlbr[0]:seg_bbox_tlbr[2], seg_bbox_tlbr[1]:seg_bbox_tlbr[3],:].astype(np.float32)/255.  # (bbox_sy, bbox_sx)
    segment_idxs_to_split = [0]
    seg_idx_end_offset = 1

    for iter_idx in range(N_MAX_ITERATIONS):

        # split segments
        for seg_idx in segment_idxs_to_split:
            new_seg_in_bbox, seg_idx_new_end_offset = split_seg(im_bgr_fl32, new_seg_in_bbox, seg_to_split_idx=seg_idx,\
                                                                                seg_idx_end_offset=seg_idx_end_offset)
            seg_idx_end_offset = seg_idx_new_end_offset

        # check for conflicting seeds
        seed_point_segs = new_seg_in_bbox[seed_points[:,0], seed_points[:,1]]    # (n_seeds) of int, current seg idxs of seeds
        labels_in_segs = np.zeros((seg_idx_end_offset, n_labels), dtype=np.bool_)  # (n_segs, n_labels)
        labels_in_segs[seed_point_segs, seed_points[:,2]] = 1
        n_labels_in_segs = np.count_nonzero(labels_in_segs, axis=1)  # (n_segs,)
        seg_labels = np.argmax(labels_in_segs, axis=1)
        seg_labels[n_labels_in_segs < 1] = -1
        segment_idxs_to_split, = np.where(n_labels_in_segs > 1)

        if len(segment_idxs_to_split) == 0:
            break

    print(">> SLIC SPLIT, after splits", np.unique(new_seg_in_bbox, return_counts=True))
    new_seg_in_bbox_c_temp = new_seg_in_bbox.copy()   # TODO disable if image file saving debug is not used

    if len(segment_idxs_to_split) != 0:
        assert False, "Iteration limit reached, failed to segment with seeds."

    assert seg_idx_end_offset >= n_labels
    # check if there are more segments than labels: if yes, try to merge the similarly labeled segments
    new_seg_in_bbox += 1    # bg is 0, labels start from 1
    new_seg_in_bbox_c2_temp = None   # TODO disable if image file saving debug is not used
    if seg_idx_end_offset > n_labels:
        new_seg_in_bbox = merge_unlabeled_segs(im_bgr_fl32, new_seg_in_bbox, seg_labels)
        print(">> SLIC SPLIT, after merge", np.unique(new_seg_in_bbox, return_counts=True))
        new_seg_in_bbox_c2_temp = new_seg_in_bbox.copy()   # TODO disable if image file saving debug is not used
        new_seg_in_bbox, n_merged_segs = skimage.measure.label(new_seg_in_bbox, return_num=True)  # 0 is ignored as bg
    
    print(">> SLIC SPLIT, after merge&relabel", np.unique(new_seg_in_bbox, return_counts=True))

    # run rprops again
    rprops = skimage.measure.regionprops(new_seg_in_bbox, cache=True)
    new_bboxes_tlbr = np.empty((n_merged_segs, 4), dtype=np.int32)    # (n_merged_segs, 4)
    new_seg_sizes = np.empty((n_merged_segs,), dtype=np.int32)    # (n_merged_segs,)
    for rprop in rprops:
        new_bboxes_tlbr[rprop.label-1, :] = rprop.bbox
        new_seg_sizes[rprop.label-1] = rprop.area

    # DEBUG code: render input mask and split results before and after merging
    colors = np.random.randint(255, size=(seg_idx_end_offset+1, 3))
    im_saved0 = colors[seg_mask_in_bbox.astype(np.int32),:]
    im_saved1 = colors[new_seg_in_bbox_c_temp,:]
    im_saved3 = colors[new_seg_in_bbox,:]
    cv2.imwrite('slic0.png', im_saved0)    # orig fg mask
    cv2.imwrite('slic1.png', im_saved1)    # after split
    if new_seg_in_bbox_c2_temp is not None:
        im_saved2 = colors[new_seg_in_bbox_c2_temp,:]
        cv2.imwrite('slic2.png', im_saved2)    # after merge, before separation of disconnected segs
    cv2.imwrite('slic3.png', im_saved3)    # final results
    # END DEBUG code

    return new_seg_in_bbox-1, new_bboxes_tlbr, new_seg_sizes

