
#
# Pregenerated superpixel segmentation loader, no dynamic generation & split support.
#

import numpy as np
import h5py
import os

LOAD_ID = 'slich3_opt1_canny200'   # one of the keys in BASE_PATHS dict

BASE_PATHS = {
        'slich_256_4_1024_max2': '/home/vavsaai/databases/DAVIS/rlseg2_data/davis2017_slich_256_4_1024_max2/',
        'slich2_128_4_512_max2': '/home/vavsaai/databases/DAVIS/rlseg2_data/davis2017_slich2_128_4_512_max2/',
        'slich2_256_4_1024_max2': '/home/vavsaai/databases/DAVIS/rlseg2_data/davis2017_slich2_256_4_1024_max2/',
        'slichgt_256_4_800_max2_zero': '/home/vavsaai/databases/DAVIS/rlseg2_data/davis2017_slichgt_256_4_800_max2_zero/',
        'slich3_opt1_canny200': '/home/vavsaai/databases/DAVIS/rlseg2_data/davis2017_slich3_opt1_canny200/',
        'davis2017_slich_gt100': '/home/vavsaai/databases/DAVIS/rlseg2_data/davis2017_slich_gt100/'
              }

H5_PREFIXES = {'slich_256_4_1024_max2': 'slich_',
               'slich2_128_4_512_max2': 'slich2_',
               'slich2_256_4_1024_max2': 'slich2_',
               'slichgt_256_4_800_max2_zero': 'slich2_',
               'slich3_opt1_canny200': 'slich3_',
               'davis2017_slich_gt100': 'slich_gt100_'}

def get_n_base_segs_per_frame():
    '''
    Returns:
        n_segs: int; approximate target
    '''
    if LOAD_ID == 'slich_256_4_1024_max2':
        return 256
    elif LOAD_ID == 'slich2_128_4_512_max2':
        return 128
    elif LOAD_ID == 'slich2_256_4_1024_max2':
        return 256
    elif LOAD_ID == 'slichgt_256_4_800_max2_zero':
        return 256
    elif LOAD_ID == 'slich3_opt1_canny200':
        return 449
    elif LOAD_ID == 'davis2017_slich_gt100':
        return 256
    else:
        assert False, "Unsupported ID."

def get_base_sp_segmentation(**kwargs):
    '''
    Creates SLIC segmentation, no hierarchy support.
    Parameters (kwargs):
        vidname: str
    Returns:
        seg_arr: ndarray(n_ims, sy, sx) of uint32
    '''
    vidname = kwargs['vidname']
    seg_h5_path = os.path.join(BASE_PATHS[LOAD_ID], H5_PREFIXES[LOAD_ID] + vidname + '.h5')
    h5f = h5py.File(seg_h5_path, 'r')
    sp_seg_arr = h5f['lvl0_seg'][:].astype(np.uint32)
    h5f.close()
    return sp_seg_arr


def split_seg_with_seeds(im_bgr, seg_bbox_tlbr, seg_mask_in_bbox, seed_points):
    assert False, "Segment splitting is not implemented with this algorithm."

