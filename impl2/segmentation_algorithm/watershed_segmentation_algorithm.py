
#
# SLIC Superpixel interface - dynamically generated (no pregeneration available)
#

import numpy as np
import skimage.measure

import cv2
from util import imutil as ImUtil


def get_n_base_segs_per_frame():
    assert False, "Base segmentation is not implemented with this algorithm."

def get_base_sp_segmentation(**kwargs):
    assert False, "Base segmentation is not implemented with this algorithm."


def split_seg_with_seeds(im_bgr, seg_bbox_tlbr, seg_mask_in_bbox, seed_points):
    '''
    Splits a segment with SLIC segmentation with respect to sets of differently labeled seed points.
    Tries to return a segmentation with the minimum possible number of segments.
    Parameters:
        im_bgr: ndarray(sy, sx, 3:bgr) of uint8; full image
        seg_bbox_tlbr: array-like(4:[tlbr]) of ints
        seg_mask_in_bbox: ndarray(b-t, r-l) of bool_
        seed_points: ndarray(n_seeds, 3:[py, px, label]) of ints; must contain seeds with at least two distinct labels
                                     labels must be from range 0..n_labels-1; indexing original image
    Returns:
        new_seg_in_bbox: ndarray(bbox_sy, bbox_sx) of int32; new seg IDs indexed from 0, background is -1
        new_bboxes_tlbr: ndarray(n_new_segs, 4:[tlbr]) of int32; new seg bboxes (RELATIVE to 'new_seg_in_bbox')
        new_seg_sizes: ndarray(n_new_segs,) of int32; new seg sizes
    '''
    assert im_bgr.shape[2:] == (3,)
    assert seg_mask_in_bbox.ndim == 2
    assert seed_points.shape[1:] == (3,)
    u_labels = np.unique(seed_points[:,2])
    n_labels = len(u_labels)
    assert (np.amin(u_labels) == 0) and (np.amax(u_labels) == n_labels-1)
    assert 16 >= n_labels > 1
    assert np.issubdtype(seed_points.dtype, np.integer)
    assert np.unique(seed_points[:,:2], axis=0).shape[0] == seed_points.shape[0], "All seeds must be unique"

    # generate seed image, pad with a border (watershed does not work properly if seed is on border)
    seed_points = seed_points - np.array([seg_bbox_tlbr[0], seg_bbox_tlbr[1], 0])   # seed points relative to bbox
    assert np.all(seg_mask_in_bbox[seed_points[:,0], seed_points[:,1]])   # assert all seeds within mask
    seed_points[:,2] += 1    # labels starting from 1
    im_bgr_cut = im_bgr[seg_bbox_tlbr[0]:seg_bbox_tlbr[2], seg_bbox_tlbr[1]:seg_bbox_tlbr[3],:]
    im_bgr_cut = np.pad(im_bgr_cut, ((1,1), (1,1), (0,0)), mode='constant', constant_values=0)
    new_seg_in_bbox = np.zeros(im_bgr_cut.shape[:2], dtype=np.int32)
    new_seg_in_bbox[seed_points[:,0]+1, seed_points[:,1]+1] = seed_points[:,2]
    print(">> WATERSHED SPLIT, seeds", np.unique(new_seg_in_bbox, return_counts=True))

    # run watershed, merge border region (-1) into one of the nearest labeled regions
    new_seg_in_bbox = cv2.watershed(im_bgr_cut, new_seg_in_bbox)  # new_seg_in_bbox is modified
                                # -> the result contains -1 values (borders including padded img border) and 1..n_labels values
    new_seg_in_bbox = new_seg_in_bbox[1:-1,1:-1]   # remove padding

    print(">> WATERSHED SPLIT, result raw", np.unique(new_seg_in_bbox, return_counts=True))

    N_MERGE_ITER_LIMIT = 5
    for iter_idx in range(N_MERGE_ITER_LIMIT):
        for syf, eyf, sxf, exf, syt, eyt, sxt, ext in [(None, -1, None, None, 1, None, None, None),
                                                       (None, None, None, -1, None, None, 1, None),
                                                       (1, None, None, None, None, -1, None, None),
                                                       (None, None, 1, None, None, None, None, -1)]:
            neg_fullmask = new_seg_in_bbox < 0
            neg_mask = neg_fullmask[syf:eyf,sxf:exf]

            c_neg = np.count_nonzero(neg_fullmask)
            if c_neg == 0:
                break
            new_seg_in_bbox[syf:eyf,sxf:exf][neg_mask] = new_seg_in_bbox[syt:eyt,sxt:ext][neg_mask]

    print(">> WATERSHED SPLIT, borders removed", np.unique(new_seg_in_bbox, return_counts=True))
    #new_seg_in_bbox_c_temp = new_seg_in_bbox.copy()   # TODO disable if image file saving debug is not used
    new_seg_in_bbox[~seg_mask_in_bbox] = 0    # assigning pixels outside of mask to background

    assert c_neg == 0, "Failed to remove all borders (-1 items)"
    assert np.all(new_seg_in_bbox[seg_mask_in_bbox] > 0)

    # separate disconnected segments
    new_seg_in_bbox, n_watershed_segs = skimage.measure.label(new_seg_in_bbox, return_num=True)  # 0 is ignored as bg
    new_seg_sizes = np.bincount(new_seg_in_bbox.reshape(-1))[1:]

    print(">> WATERSHED SPLIT, separate disconnected", np.unique(new_seg_in_bbox, return_counts=True))
    #new_seg_in_bbox_c2_temp = new_seg_in_bbox.copy()   # TODO disable if image file saving debug is not used

    # merge small segments (without any seeds) into adjacent large/seeded ones (only works with 1-connected neighbors)
    SMALL_SEG_RATIO = 0.02
    seeded_seg_labs = np.unique(new_seg_in_bbox[seed_points[:,0], seed_points[:,1]])
    small_seg_size_limit = np.sum(new_seg_sizes) * SMALL_SEG_RATIO    # if seg is smaller and unseeded, merge into a larger one
    assert np.sum(new_seg_sizes) == np.count_nonzero(seg_mask_in_bbox)  # TODO debug, remove
    seg_adjs = ImUtil.get_adj_graph_edge_list_fast(new_seg_in_bbox.astype(np.uint32)).astype(np.int32) # adjacencies (1-connectivity)
    assert np.all(seg_adjs[:,0] < seg_adjs[:,1])  # TODO debug, remove
    good_seg_labs = np.union1d(seeded_seg_labs, np.where(new_seg_sizes > small_seg_size_limit)[0]+1)  # seeded or large segs (labels will be kept)
    seg_labs_handled = good_seg_labs.copy()
    relabel_mapping = np.arange(n_watershed_segs+1)

    N_MERGE_ITER_LIMIT = 5
    for iter_idx in range(N_MERGE_ITER_LIMIT):
        all_new_handled = []
        for lab_handled in seg_labs_handled:
            adj_segs = np.unique(seg_adjs[np.any(seg_adjs == lab_handled, axis=1)])
            adj_segs = np.setdiff1d(adj_segs, list(seg_labs_handled) + [0,lab_handled])
            relabel_mapping[adj_segs] = relabel_mapping[lab_handled]
            all_new_handled.append(adj_segs)
        all_new_handled = np.concatenate(all_new_handled)
        seg_labs_handled = np.union1d(seg_labs_handled, all_new_handled)
        if seg_labs_handled.shape[0] == n_watershed_segs:
            assert np.all(np.unique(seg_labs_handled) == np.arange(1, n_watershed_segs+1))
            break

    new_seg_in_bbox = relabel_mapping[new_seg_in_bbox]

    print(">> WATERSHED SPLIT, merge small", np.unique(new_seg_in_bbox, return_counts=True))

    # recompute rprops
    new_seg_in_bbox, n_merged_segs = skimage.measure.label(new_seg_in_bbox, return_num=True)  # 0 is ignored as bg
    rprops = skimage.measure.regionprops(new_seg_in_bbox, cache=True)
    new_bboxes_tlbr = np.empty((len(rprops), 4), dtype=np.int32)    # (n_merged_segs, 4)
    new_seg_sizes = np.empty((len(rprops),), dtype=np.int32)    # (n_merged_segs,)
    for rprop in rprops:
        new_bboxes_tlbr[rprop.label-1, :] = rprop.bbox
        new_seg_sizes[rprop.label-1] = rprop.area

    print(">> WATERSHED SPLIT, final+1", np.unique(new_seg_in_bbox, return_counts=True))
    
    # DEBUG code: render input mask, seeds and watershed results to image files
    '''
    colors = np.random.randint(255, size=(n_watershed_segs+1, 3))
    im_saved0 = colors[seg_mask_in_bbox.astype(np.int32),:]
    im_saved1 = np.zeros(new_seg_in_bbox.shape + (3,), dtype=np.uint8)
    im_saved1[seed_points[:,0], seed_points[:,1], :] = colors[seed_points[:,2],:]
    im_saved2 = colors[new_seg_in_bbox_c_temp,:]
    im_saved3 = colors[new_seg_in_bbox_c2_temp,:]
    im_saved4 = colors[new_seg_in_bbox,:]
    cv2.imwrite('watershed0.png', im_saved0)    # orig fg mask
    cv2.imwrite('watershed1.png', im_saved1)    # seeds
    cv2.imwrite('watershed2.png', im_saved2)    # watershed results, borders removed
    cv2.imwrite('watershed3.png', im_saved3)    # mask applied to watershed results
    cv2.imwrite('watershed4.png', im_saved4)    # small segments merged to large segmets where possible, final seg
    '''
    
    return new_seg_in_bbox-1, new_bboxes_tlbr, new_seg_sizes




