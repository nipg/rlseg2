
#
# CCS Superpixel interface - only base segmentation available (loading from pregenerated archives)
#

import os
import skimage.measure
import h5py
import numpy as np
import util.util as Util

CCS_SEG_H_LVL_TO_USE = 7
N_SEGMENTS_PER_FRAME = 1024   # 1024 or 2048 available

# HELPER
def _create_h_lvl_sv_seg(seg_lvl0, to_level, hierarchy):
    '''
    Creates level h SV segmentation by merging level 0 SVs following the 'hierarchy' array.
    Parameters:
        seg_lvl0: ndarray(n_frames, size_y, size_x) of uint32
        to_level: int; the target level (or h)
        hierarchy: ndarray(n_total_hier_segs,) of uint32; for each node, the ID of its parent.
                            IDs of level#i should be in range [sum_{j=0..i-1}(n_seg_lvl_j), sum_{j=0..i}(n_seg_lvl_j))
    Returns:
        seg_lvl_h: ndarray(n_frames, size_y, size_x) of uint32; IDs are left as they are in the 'hierarchy' array
    '''
    assert to_level > 0
    n_segs_lvl0 = np.amin(hierarchy)
                    # smallest parent is the ID of the smallest lvl1 seg == number of lvl0 segs (starting from ID 0)
    curr_seg_ids = np.arange(n_segs_lvl0)
    p_seg_ids = hierarchy[curr_seg_ids]
    from_seg_id = curr_seg_ids

    for lvl_idx in range(1,to_level):
        curr_seg_ids = p_seg_ids
        p_seg_ids = hierarchy[curr_seg_ids]

    assert Util.is_sorted(from_seg_id)
    seg_lvl_h = p_seg_ids[seg_lvl0].astype(np.uint32)
    return seg_lvl_h

def get_n_base_segs_per_frame():
    '''
    Returns:
        n_segs: int; approximate target
    '''
    return N_SEGMENTS_PER_FRAME

def get_base_sp_segmentation(**kwargs):
    '''
    Parameters (kwargs):
        base_folder_path: str
    Returns:
        seg_arr: ndarray(n_ims, sy, sx) of uint32
    '''
    base_folder_path = kwargs['base_folder_path']
    vidname = kwargs['vidname']
    seg_h5_path = os.path.join(base_folder_path, 'ccs_' + vidname + '.h5')
    h5f = h5py.File(seg_h5_path, 'r')
    sv_seg_arr = h5f['lvl0_seg'][:].astype(np.uint32)
    if CCS_SEG_H_LVL_TO_USE > 0:
        print("CCS_segmentation_algorithm: Converting segmentation to level#" + str(CCS_SEG_H_LVL_TO_USE) + "...")
        hierarchy = h5f['hierarchy'][:].astype(np.uint32)
        sv_seg_arr = _create_h_lvl_sv_seg(sv_seg_arr, CCS_SEG_H_LVL_TO_USE, hierarchy)
        orig_shape = sv_seg_arr.shape
        _, sv_seg_arr = np.unique(sv_seg_arr, return_inverse=True)
        sv_seg_arr = sv_seg_arr.reshape(orig_shape)
    h5f.close()
    # convert SV to SP segmentation: also assign different labels to separated regions in same frame with same labels
    seg_arr = np.empty(sv_seg_arr.shape, dtype=np.int32)
    offset = 0
    for fr_idx in range(sv_seg_arr.shape[0]):
        seg_arr_fr_sep, n_segs = skimage.measure.label(sv_seg_arr[fr_idx,:,:]+1, return_num=True, connectivity=2) # adding 1, because 0 is ignored (as background)
        seg_arr[fr_idx,:,:] = seg_arr_fr_sep+(offset-1)
        offset += n_segs
    return seg_arr


def split_seg_with_seeds(im_bgr, seg_bbox_tlbr, seg_mask_in_bbox, seed_points):
    assert False, "Segment splitting is not implemented with this algorithm."


    