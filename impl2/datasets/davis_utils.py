
#
# Utils for the DAVIS interactive benchmark. RLseg2 implementation #2
#
#   @author Viktor Varga
#

import numpy as np
import cv2

# HELPERS for the DAVIS interactive challenge benchmark

def _downscale_im_by_int_factor(im, ds_factor, ds_op):
    '''
    Downscales an image by an integer factor. Remainder cols/rows are dropped.
    Parameters:
        im: ndarray(sy, sx, ...) of ?
        ds_factor: int; downscale factor
        ds_op: Callable; the downscale operation - must have an "axis" parameter; compatible with np.mean(), np.any()
    Returns:
        im_ds: ndarray(dsy, dsx, ...) of ?; where dsy = sy // ds_factor, ...
    '''
    ds_size_yx = (im.shape[0]//ds_factor, im.shape[1]//ds_factor)
    im_size_yx_div = (ds_size_yx[0]*ds_factor, ds_size_yx[1]*ds_factor)
    im = im[:im_size_yx_div[0], :im_size_yx_div[1]]
    im = im.reshape((ds_size_yx[0], ds_factor, ds_size_yx[1], ds_factor) + im.shape[2:])
    im_ds = ds_op(im, axis=(1,3)).astype(im.dtype, copy=False)
    return im_ds

def _upscale_im_by_int_factor(im_ds, us_factor, target_size):
    '''
    Upscales an image by an integer factor by repeating items. Pads the upscaled image to "target_size" with zeros.
    Parameters:
        im_ds: ndarray(dsy, dsx, ...) of ?
        us_factor: int; upscale factor
        target_size: tuple(2); size of the returned image (sy, sx) dims only; padded cols/rows are set to zero.
    Returns:
        im: ndarray(sy, sx, ...) of ?;
    '''
    assert len(target_size) == 2
    im = np.empty(target_size + im_ds.shape[2:], dtype=im_ds.dtype)
    us_unpadded_size_yx = (im_ds.shape[0]*us_factor, im_ds.shape[1]*us_factor)
    #pad_widths = (target_size[0] - us_unpadded_size_yx[0], target_size[1] - us_unpadded_size_yx[1])
    im[us_unpadded_size_yx[0]:, :us_unpadded_size_yx[1]] = 0   # zeroing padded parts in three assignments
    im[:us_unpadded_size_yx[0], us_unpadded_size_yx[1]:] = 0
    im[us_unpadded_size_yx[0]:, us_unpadded_size_yx[1]:] = 0
    im_ds = np.broadcast_to(im_ds[:,None,:,None], (im_ds.shape[0], us_factor, im_ds.shape[1], us_factor) + im_ds.shape[2:])
    im[:us_unpadded_size_yx[0], :us_unpadded_size_yx[1]] = im_ds.reshape(us_unpadded_size_yx + im_ds.shape[4:])
    return im


def get_new_scribbles(vidname, prev_scribble_dict, curr_scribble_dict, im_size_yx):
    '''
    Extracts labeled seed points from newly returned scribbles. The new scribbles are found by comparing the previously
            returned scribble dict with the updated one.
    Parameters:
        vidname: str; only for handling the 'tennis' sequence label error
        prev_scribble_dict: None OR dict; dict returned in PREVIOUS STEP at #1 by iterator 
                                    from DavisInteractiveSession.scribbles_iterator()
                                    None if there was no previous step;
                                    See: https://interactive.davischallenge.org/docs/session/
        curr_scribble_dict: dict; dict returned in CURRENT STEP at #1 by iterator
                                    from DavisInteractiveSession.scribbles_iterator()
        im_size_yx: tuple(2); target image size
    Returns:
        annotated_frame_idx: int; the frame idx annotated in the CURRENT STEP
        scribble_arrs: dict{lab - int: list(n_scribbles_with_lab) of ndarray(n_points_in_scribble, 2:[y,x]) of int32}
    '''
    im_size_yx = np.array(im_size_yx, dtype=np.int32)
    assert im_size_yx.shape == (2,)

    # get newly annotated frame index
    curr_lens = np.array([len(scrib_fr) for scrib_fr in curr_scribble_dict['scribbles']], dtype=np.int32)
    prev_lens = np.zeros((curr_lens.shape[0],), dtype=np.int32) if prev_scribble_dict is None else \
                                np.array([len(scrib_fr) for scrib_fr in prev_scribble_dict['scribbles']], dtype=np.int32)
    diff_lens = curr_lens - prev_lens
    if np.count_nonzero(diff_lens) > 1:
        # Rarely happens in DAVIS benchmark: i.e. one of the 'lindy-hop' & 'longboard' sequences contains initial scribbles in two frames
        print("!!! davis_utils.get_new_scribbles(): Warning! Multiple frames were annotated since previous scribble dict. ")
    elif np.count_nonzero(diff_lens) < 1:
        # TODO might happen?
        print("!!! davis_utils.get_new_scribbles(): Warning! No frames were annotated since previous scribble dict. ")
    annotated_frame_idx = np.argmax(diff_lens)

    scribble_arrs = {}
    for scribble_data in curr_scribble_dict['scribbles'][annotated_frame_idx]:
        scrib_points, label = scribble_data['path'], scribble_data['object_id']
        if label == 255:
            assert vidname == 'tennis'
            print("!!! davis_utils.get_new_scribbles(): Info: label value 255 was replaced with 3 (suspecting 'tennis' sequence label error) ")
            label = 3
        scrib_points = np.round(np.array(scrib_points, dtype=np.float64)[:,::-1] * im_size_yx).astype(np.int32)
        scrib_points = np.clip(scrib_points, a_min=None, a_max=im_size_yx-1)
        assert scrib_points.shape[1:] == (2,)
        #assert scrib_points.shape[0] >= 2    # assuming at least two points per scribble
        if scrib_points.shape[0] < 2:
            print("!!! Warning! DavisUtils.get_new_scribbles() -> Less than 2 points in scribble, shape:", scrib_points.shape)
        if label not in scribble_arrs.keys():
            scribble_arrs[label] = []
        scribble_arrs[label].append(scrib_points)

    return annotated_frame_idx, scribble_arrs

def _render_scribbles(scribble_arrs, im_size_yx, generate_bg_mask=False):
    '''
    Renders all scribbles into masks for each label.
    Parameters:
        scribble_arrs: dict{fg_lab - int: list(n_scribbles_with_lab) of ndarray(n_points_in_scribble, 2:[y,x]) of int32}
                                keys must be from arange(1, n_fg_labels+1); bg label is not allowed
        im_size_yx: tuple(2) of ints
        generate_bg_mask: bool; if True, the method expects no background scribbles and generates extra bg seed points (DAVIS step#1)
                           if False, the method does not generate extra bg points (DAVIS step#2...)
    Returns:
        labs_present: ndarray(n_labels_among_scribbles,) of int32; the corresponding labels to the masks
        scribble_masks: ndarray(n_labels_among_scribbles, sy, sx) of int32; bg mask is at label idx#-1 if 'generate_bg_mask' is True
                            pixel values are (indices of corresponding scribbles)+1
    '''
    labs_present = list(scribble_arrs.keys())
    assert len(labs_present) >= 1
    scribble_masks = np.zeros((len(labs_present)+1,) + im_size_yx, dtype=np.int32)  # +1 to leave space for bg if needed

    for lab_idx in range(len(labs_present)):
        # render all scribbles to images, each label category independently
        lab = int(labs_present[lab_idx])
        scribbles_ls = scribble_arrs[lab]
        scribbles_ls = [scribble_arr[:,::-1] for scribble_arr in scribbles_ls]  # y,x -> x,y lowres
        for scrib_idx in range(len(scribbles_ls)):
            cv2.polylines(scribble_masks[lab_idx,:,:], [scribbles_ls[scrib_idx]], isClosed=False, color=scrib_idx+1, \
                                                                                thickness=1, lineType=cv2.LINE_8)
    if generate_bg_mask is True:
        assert 0 not in labs_present
        DILATE_RADIUS = 6
        DOWNSCALE_FACTOR = 8

        # get mask showing all scribbles at once, then downscale
        fullres_any_scribble = np.any(scribble_masks, axis=0)
        lowres_any_scribble = _downscale_im_by_int_factor(fullres_any_scribble, DOWNSCALE_FACTOR, np.any).astype(np.uint8)

        # dilate any-scribble mask
        pad_width = DILATE_RADIUS+1
        kernel_dilate = np.empty((2*DILATE_RADIUS+1, 2*DILATE_RADIUS+1), dtype=np.uint8)
        kernel_dilate[:] = 0
        kernel_dilate = cv2.circle(kernel_dilate, (DILATE_RADIUS, DILATE_RADIUS), radius=DILATE_RADIUS, color=1, thickness=-1)
        lowres_any_scribble = np.pad(lowres_any_scribble, ((pad_width, pad_width), (pad_width, pad_width)))
        lowres_any_scribble = cv2.dilate(lowres_any_scribble, kernel_dilate, iterations=1)
        lowres_any_scribble = lowres_any_scribble[pad_width:-pad_width, pad_width:-pad_width]  # unpad

        # upscale dilated any-scribble mask, then sample background from the negated mask
        scribble_masks[-1,:,:] = _upscale_im_by_int_factor(1-lowres_any_scribble, DOWNSCALE_FACTOR, im_size_yx)  # negation op '~' works differently with uint8
        labs_present.append(0)
        
    return scribble_masks, labs_present

def fix_label_image_error(vidname, pred_label_im):
    '''
    In case of the 'tennis' sequence, replaces the tennis-ball segment label value 3 with the original erroneous value 255.
    Parameters:
        vidname: str
        (MODIFIED) pred_label_im: ndarray(n_frames, sy, sx) of ui8
    '''
    if vidname == 'tennis':
        pred_label_im[pred_label_im == 255] = 3
    #


# newer, universal process for both initial and later steps in DAVIS sessions

def davis_scribbles2seeds_uniform(scribble_arrs, im_size_yx, n_seeds_per_cat, generate_bg=False, sample_from_each_scribble=True):
    '''
    Extracts seed points from DAVIS scribbles. 
    Uniform sampling within each category, based on pixels of rendered scribbles. No error correction.
    Parameters:
        scribble_arrs: dict{fg_lab - int: list(n_scribbles_with_lab) of ndarray(n_points_in_scribble, 2:[y,x]) of int32}
                                keys must be from arange(1, n_fg_labels+1); bg label is not allowed
        im_size_yx: tuple(2) of ints
        n_seeds_per_cat: int; return this many seed points per category
        generate_bg: bool; if True, the method expects no background scribbles and generates extra bg seed points (DAVIS step#1)
                           if False, the method does not generate extra bg points (DAVIS step#2...)
        sample_from_each_scribble: bool; if True, tries to sample from each scribble at least one seed
    Returns:
        ret_scribble_arrs: ndarray(n_seeds, 3:[y,x, label]) of int32
    '''
    all_scrib_labels = list(scribble_arrs.keys())
    if len(all_scrib_labels) == 0:
        print("!!!! davis_utils.davis_scribbles2seeds_uniform(): Warning! Got empty scribble dict, empty array is returned. !!!! ")
        return np.zeros((0, 3), dtype=np.int32)
    scribble_masks, labs_present = _render_scribbles(scribble_arrs, im_size_yx, generate_bg_mask=generate_bg)
    ret_scribble_arrs = []
    for lab_idx in range(len(labs_present)):
        lab = labs_present[lab_idx]
        lab_pixs = np.argwhere(scribble_masks[lab_idx,:,:])  # (c_bg_true, 2), transposed np.where
        n_seed_to_sample_from_lab = n_seeds_per_cat
        if (sample_from_each_scribble is True) and ((lab != 0) or (generate_bg is False)):
            # if 'sample_from_each_scribble' and not generated bg, random sample a single seed from each scribble
            shuffler = np.random.permutation(lab_pixs.shape[0])
            lab_pixs = lab_pixs[shuffler,:]
            lab_pix_seed_idxs = scribble_masks[lab_idx, lab_pixs[:,0], lab_pixs[:,1]]
            u_seed_idxs, first_idx_in_seed = np.unique(lab_pix_seed_idxs, return_index=True)
            if first_idx_in_seed.shape[0] > n_seed_to_sample_from_lab:
                # if too many scribbles, select randomly
                np.random.shuffle(first_idx_in_seed)
                first_idx_in_seed = first_idx_in_seed[:n_seed_to_sample_from_lab]
                print("Warning! Too many scribbles, could not sample from each one.")
            lab_pix_chosen = lab_pixs[first_idx_in_seed,:]
            lab_pix_chosen_labeled = np.pad(lab_pix_chosen, ((0,0), (0,1)), constant_values=lab)   # (n_seeds_per_cat, 3)
            ret_scribble_arrs.append(lab_pix_chosen_labeled)
            n_seed_to_sample_from_lab -= lab_pix_chosen_labeled.shape[0]
        # sample the remaining randomly from all scribbles
        if n_seed_to_sample_from_lab > 0:
            sample_idxs = np.random.choice(lab_pixs.shape[0], size=(n_seed_to_sample_from_lab,))
            lab_pixs_labeled = np.pad(lab_pixs[sample_idxs,:], ((0,0), (0,1)), constant_values=lab)   # (n_seeds_per_cat, 3)
            ret_scribble_arrs.append(lab_pixs_labeled)

    return np.concatenate(ret_scribble_arrs, axis=0)


# legacy

def davis_initial_scribbles2seeds(scribble_arrs, im_size_yx, n_seeds_per_cat):
    '''
    Extracts seed points from initial DAVIS scribbles. 
    Initial DAVIS scribbles are user-drawn and follow an inconsistent protocol. Some annotate the approximate skeleton,
        some are densely drawn inside the object and in some cases scribbles annotate the border of the object as well.
    Scribbles must be present for all foreground labels, but not for the background (zero) labels.
    Parameters:
        scribble_arrs: dict{fg_lab - int: list(n_scribbles_with_lab) of ndarray(n_points_in_scribble, 2:[y,x]) of int32}
                                keys must be from arange(1, n_fg_labels+1); bg label is not allowed
        im_size_yx: tuple(2) of ints
        n_seeds_per_cat: int; return this many seed points per category
    Returns:
        ret_scribble_arrs: ndarray(n_seeds, 3:[y,x, label]) of int32
    '''
    assert False, "Disabled. Use new method by default."

    n_fg_labels = max(scribble_arrs.keys())
    assert set(scribble_arrs.keys()) == set(range(1, n_fg_labels+1))

    # render all scribbles, full resolution
    fullres_fg_scribble_im = np.zeros(im_size_yx, dtype=np.uint8)
    for fg_lab in range(1, n_fg_labels+1):
        scribbles_ls = scribble_arrs[fg_lab]
        scribbles_ls = [scribble_arr[:,::-1] for scribble_arr in scribbles_ls]  # y,x -> x,y
        cv2.polylines(fullres_fg_scribble_im, scribbles_ls, isClosed=False, color=fg_lab, thickness=1, lineType=cv2.LINE_8)

    # TEMP save img - rendered scribbles
    '''
    LABEL_COLORS_RGB = np.array([[128,128,128], [255,0,0], [0,255,0], [0,0,255],\
                                                [255,140,0], [0,255,255], [255,0,255],\
                                                [192,0,192], [127,0,255], [0,128,255],\
                                                [255,255,0], [0,204,102], [51,153,255]], dtype=np.uint8)
    LABEL_COLORS_RGB = LABEL_COLORS_RGB[:,::-1]  # rgb -> bgr
    _temp_scribbles_rendered_im = LABEL_COLORS_RGB[fullres_fg_scribble_im,:]
    rnd_fname_tag = str(np.random.randint(100000))
    cv2.imwrite('davis_scribbles_' + rnd_fname_tag + '.png', _temp_scribbles_rendered_im)
    '''
    # TEMP END

    # convert scribble im to one-hot (masks), downscale masks (integer block downscale, dropping remainder cols/rows)
    DOWNSCALE_FACTOR = 8
    fullres_fg_scribble_masks = np.zeros(im_size_yx + (n_fg_labels,), dtype=np.uint8)   # (sy, sx, n_fg_labels)
    fullres_fg_anymask = fullres_fg_scribble_im > 0   # (sy, sx)
    fullres_fg_scribble_masks[fullres_fg_anymask, fullres_fg_scribble_im[fullres_fg_anymask]-1] = 1
    ds_fg_scribble_masks = _downscale_im_by_int_factor(fullres_fg_scribble_masks, DOWNSCALE_FACTOR, np.any)
    c_ds_fg = np.count_nonzero(ds_fg_scribble_masks, axis=(0,1))   # (n_fg_labels,)
    
    # pad downscaled image, dilate and erode channels, erosion radius is greater, count eroded mask size
    DILATE_RADIUS = 6
    ERODE_RADIUS = 8
    CIRCULAR_KERNEL = True
    pad_width = DILATE_RADIUS+1
    kernel_dilate = np.empty((2*DILATE_RADIUS+1, 2*DILATE_RADIUS+1), dtype=np.uint8)
    kernel_erode = np.empty((2*ERODE_RADIUS+1, 2*ERODE_RADIUS+1), dtype=np.uint8)
    if CIRCULAR_KERNEL:
        kernel_dilate[:] = 0
        kernel_erode[:] = 0
        kernel_dilate = cv2.circle(kernel_dilate, (DILATE_RADIUS, DILATE_RADIUS), radius=DILATE_RADIUS, color=1, thickness=-1)
        kernel_erode = cv2.circle(kernel_erode, (ERODE_RADIUS, ERODE_RADIUS), radius=ERODE_RADIUS, color=1, thickness=-1)
    else:
        kernel_dilate[:] = 1
        kernel_erode[:] = 1

    ds_fg_scribble_masks_pad = np.pad(ds_fg_scribble_masks, ((pad_width, pad_width), (pad_width, pad_width), (0,0)))
    ds_fg_scribble_masks_dilated = np.empty_like(ds_fg_scribble_masks_pad)
    ds_fg_scribble_masks_transformed = np.empty_like(ds_fg_scribble_masks_pad)
    for fg_label_idx in range(ds_fg_scribble_masks_pad.shape[2]):
        ds_fg_scribble_masks_dilated[:,:,fg_label_idx] = cv2.dilate(ds_fg_scribble_masks_pad[:,:,fg_label_idx], kernel_dilate, iterations=1)
        ds_fg_scribble_masks_transformed[:,:,fg_label_idx] = cv2.erode(ds_fg_scribble_masks_dilated[:,:,fg_label_idx], kernel_erode, \
                                iterations=1, borderType=cv2.BORDER_CONSTANT, borderValue=0)
    ds_fg_scribble_masks_dilated = ds_fg_scribble_masks_dilated[pad_width:-pad_width, pad_width:-pad_width, :]  # unpad
    ds_fg_scribble_masks_transformed = ds_fg_scribble_masks_transformed[pad_width:-pad_width, pad_width:-pad_width, :]  # unpad
    ds_fg_scribble_masks_transformed[np.count_nonzero(ds_fg_scribble_masks_transformed, axis=2) > 1, :] = 0    # masks must be disjoint
    ds_fg_scribble_masks_within_transformed = ds_fg_scribble_masks & ds_fg_scribble_masks_transformed
    c_ds_fg_within_transformed = np.count_nonzero(ds_fg_scribble_masks_within_transformed, axis=(0,1))   # (n_fg_labels,)

    # TEMP save img - transformed label masks
    '''
    for lab_fg_idx in range(ds_fg_scribble_masks_transformed.shape[2]):
        _temp_overlay_fullsize = _upscale_im_by_int_factor(ds_fg_scribble_masks_transformed[:,:,lab_fg_idx], \
                                                                DOWNSCALE_FACTOR, im_size_yx)
        _temp_overlay_fullsize = np.broadcast_to(_temp_overlay_fullsize[:,:,None], _temp_overlay_fullsize.shape + (3,))*255
        _temp_scribbles_fg_overlay = cv2.addWeighted(_temp_scribbles_rendered_im, 0.7, _temp_overlay_fullsize, 0.3, 0.)
        cv2.imwrite("davis_scribbles_" + rnd_fname_tag + '_fg' + str(lab_fg_idx+1) + '.png', _temp_scribbles_fg_overlay)
    '''
    # TEMP END

    # apply transformed (dilate&erode) mask on scribbles
    #    -> if too big part of scribbles are lost, assuming 'skeleton-type' scribbles, using the whole scribble
    #        -> otherwise, using only the parts of the scribbles intersecting with the transformed mask
    SKELETON_TYPE_RATIO_LIMIT = 1.   # TODO taking original skeleton, other methods are disabled for now
    label_is_skeleton = c_ds_fg_within_transformed < c_ds_fg*SKELETON_TYPE_RATIO_LIMIT    # (n_fg_labels,)

    # sample given number of points from each foreground category
    ret_scribble_arrs = np.empty((n_fg_labels+1, n_seeds_per_cat, 2), dtype=np.int32)
    for fg_label_idx in range(n_fg_labels):
        scribbles = scribble_arrs[fg_label_idx+1]
        scribbles = np.concatenate(scribbles, axis=0)

        if not label_is_skeleton[fg_label_idx]:
            # upscale transformed mask, use intersection of mask & scribbles
            #     check whether enough points can be sampled from intersection
            #     if intersection is too small, sample from all scribbles
            fullres_fg_scribble_masks_transformed = _upscale_im_by_int_factor(ds_fg_scribble_masks_transformed[:,:,fg_label_idx], \
                                                                DOWNSCALE_FACTOR, im_size_yx)
            scribbles_inside_mask = fullres_fg_scribble_masks_transformed[scribbles[:,0], scribbles[:,1]]
            inside_intersection_ratio = np.count_nonzero(scribbles_inside_mask)/float(scribbles_inside_mask.size)
            print("        DAVIS scribbles2seeds, non-skeleton; fg scribble lab#", fg_label_idx, "; inside", c_ds_fg_within_transformed[fg_label_idx], "of", c_ds_fg[fg_label_idx])
            scribbles = scribbles[scribbles_inside_mask,:]   # sample only from within the intersection of scribbles and transformed mask
            assert scribbles.shape[0] >= 2
        else:
            print("        DAVIS scribbles2seeds, skeleton;", fg_label_idx, "(ratio:", c_ds_fg_within_transformed[fg_label_idx]/c_ds_fg[fg_label_idx], ")")

        sample_idxs = np.linspace(0, scribbles.shape[0], num=n_seeds_per_cat, endpoint=False, dtype=np.int32)
        ret_scribble_arrs[fg_label_idx+1,:,:] = scribbles[sample_idxs]

    # sample given number of points from background category
    #     generate union mask from DILATED fg masks, random sample bg points from negated union mask
    ds_fg_union_dilated = np.any(ds_fg_scribble_masks_dilated, axis=2)   # (dsy, dsx) union of dilated scribble masks
    fullres_bg = _upscale_im_by_int_factor(~ds_fg_union_dilated, DOWNSCALE_FACTOR, im_size_yx)
    bg_idxs = np.argwhere(fullres_bg)  # (c_bg_true, 2), transposed np.where
    sample_idxs = np.random.choice(bg_idxs.shape[0], size=(n_seeds_per_cat,))
    ret_scribble_arrs[0,:,:] = bg_idxs[sample_idxs,:]

    # convert 'ret_scribble_arrs' to final form
    ret_scribble_arrs = np.pad(ret_scribble_arrs, ((0,0), (0,0), (0,1)))
    ret_scribble_arrs[:,:,2] = np.arange(ret_scribble_arrs.shape[0])[:,None]
    ret_scribble_arrs = ret_scribble_arrs.reshape((-1, 3))

    # TEMP save img - bg
    '''
    _fullres_bg_im = np.broadcast_to(fullres_bg[:,:,None].astype(np.uint8), fullres_bg.shape + (3,))
    _temp_scribbles_bg_overlay = cv2.addWeighted(_temp_scribbles_rendered_im, 0.7, _fullres_bg_im*255, 0.3, 0.)
    cv2.imwrite('davis_scribbles_' + rnd_fname_tag + '_bg.png', _temp_scribbles_bg_overlay)
    '''
    # TEMP END

    return ret_scribble_arrs

def davis_simple_scribbles2seeds(scribble_arrs, im_size_yx, n_seeds_max):
    '''
    Extracts seed points from any-round DAVIS scribbles. No need to have scribbles from each category.
    Parameters:
        scribble_arrs: dict{lab - int: list(n_scribbles_with_lab) of ndarray(n_points_in_scribble, 2:[y,x]) of int32}
                                keys must be from arange(1, n_fg_labels+1); bg label is not allowed
        im_size_yx: tuple(2) of ints
        n_seeds_max: int; return up to this many seed points total (all categories together)
    Returns:
        ret_scribble_arrs: ndarray(n_seeds, 3:[y,x, label]) of int32
    '''
    assert False, "Disabled. Use new method by default."
    all_seeds = []
    all_labels = []
    for lab, scribbles in scribble_arrs.items():
        all_seeds.extend(scribbles)
        all_labels.append(np.full((sum([scrib.shape[0] for scrib in scribbles])), dtype=np.int32, fill_value=lab))
    all_seeds = np.concatenate(all_seeds, axis=0)   # (n_seeds, 2)
    all_labels = np.concatenate(all_labels, axis=0)   # (n_seeds,)
    ret_scribble_arrs = np.concatenate([all_seeds, all_labels[:,None]], axis=1)
    sample_idxs = np.random.choice(ret_scribble_arrs.shape[0], size=(n_seeds_max,))
    ret_scribble_arrs = ret_scribble_arrs[sample_idxs,:]

    return ret_scribble_arrs







