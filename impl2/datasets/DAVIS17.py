
# 
# RLseg2 implementation #2, DAVIS2017 dataset interface, storing paths and providing dataset-specific data loading functionality
#   @author Viktor Varga
#

import os
import numpy as np
import h5py
import pickle
import cv2


ID = 'davis2017'

#ROOT_FOLDER = '/home/vavsaai/databases/DAVIS/'
ROOT_FOLDER = '/media/ssd/vavsaai/databases/DAVIS/'

IM_FOLDER = os.path.join(ROOT_FOLDER, 'DAVIS2017/DAVIS/JPEGImages/480p_unisize/')
DATA_FOLDER_GT_ANNOT = os.path.join(ROOT_FOLDER, 'rl_seg_data/davis2017_annot480p/')
DATA_FOLDER_OPTFLOWS = os.path.join(ROOT_FOLDER, 'rlseg2_data/davis2017_optflows_fixedoccl/')
DATA_FOLDER_IMFEATURES = os.path.join(ROOT_FOLDER, 'rlseg2_data/davis2017_imfeatures/')
CACHE_FOLDER = os.path.join(ROOT_FOLDER, 'rlseg2_data/cache2017/')
ANNOT_FILENAME_PREFIX = 'annot480p_'
ANNOT_H5_KEY = 'davis2017_annot_480p'
IMFEATURES_MODELKEY = 'MobileNetV2'

DATA_FOLDER_SEG_H5_CCS1024 = os.path.join(ROOT_FOLDER, 'rl_seg_data/davis2017_ccs_processed/fn2_1024/')
DATA_FOLDER_SEG_H5_CCS2048 = os.path.join(ROOT_FOLDER, 'rl_seg_data/davis2017_ccs_processed/fn2_2048/')

VIDEO_SETS = {
    'full (45+15+30)': {'train': ['bear', 'bmx-bumps', 'boat', 'boxing-fisheye', 'breakdance-flare', 'bus', 'car-turn',\
                                   'cat-girl', 'classic-car', 'color-run', 'crossing', 'dance-jump', 'dancing', 'disc-jockey',\
                                   'dog-agility', 'dog-gooses', 'dogs-scale', 'drift-turn', 'drone', 'elephant', 'flamingo',\
                                   'hike', 'hockey', 'horsejump-low', 'kid-football', 'kite-walk', 'koala', 'lady-running',\
                                   'lindy-hop', 'longboard', 'lucia', 'mallard-fly', 'mallard-water', 'miami-surf',\
                                   'motocross-bumps', 'motorbike', 'night-race', 'paragliding', 'planes-water', 'rallye',\
                                   'rhino', 'rollerblade', 'schoolgirls', 'scooter-board', 'scooter-gray'],
                         'val': ['sheep',\
                                'skate-park', 'snowboard', 'soccerball', 'stroller', 'stunt', 'surf', 'swing', 'tennis',\
                                'tractor-sand', 'train', 'tuk-tuk', 'upside-down', 'varanus-cage', 'walking'],
                        'test': ['bike-packing', 'blackswan', 'bmx-trees', 'breakdance', 'camel', 'car-roundabout',\
                                 'car-shadow', 'cows', 'dance-twirl', 'dog', 'dogs-jump', 'drift-chicane', 'drift-straight',\
                                 'goat', 'gold-fish', 'horsejump-high', 'india', 'judo', 'kite-surf', 'lab-coat', 'libby',\
                                 'loading', 'mbike-trick', 'motocross-jump', 'paragliding-launch', 'parkour', 'pigs',\
                                 'scooter-black', 'shooting', 'soapbox']},
    'full_trainval (45+15+1)': {'train': ['bear', 'bmx-bumps', 'boat', 'boxing-fisheye', 'breakdance-flare', 'bus', 'car-turn',\
                                   'cat-girl', 'classic-car', 'color-run', 'crossing', 'dance-jump', 'dancing', 'disc-jockey',\
                                   'dog-agility', 'dog-gooses', 'dogs-scale', 'drift-turn', 'drone', 'elephant', 'flamingo',\
                                   'hike', 'hockey', 'horsejump-low', 'kid-football', 'kite-walk', 'koala', 'lady-running',\
                                   'lindy-hop', 'longboard', 'lucia', 'mallard-fly', 'mallard-water', 'miami-surf',\
                                   'motocross-bumps', 'motorbike', 'night-race', 'paragliding', 'planes-water', 'rallye',\
                                   'rhino', 'rollerblade', 'schoolgirls', 'scooter-board', 'scooter-gray'],
                         'val': ['sheep',\
                                'skate-park', 'snowboard', 'soccerball', 'stroller', 'stunt', 'surf', 'swing', 'tennis',\
                                'tractor-sand', 'train', 'tuk-tuk', 'upside-down', 'varanus-cage', 'walking'],
                        'test': ['bike-packing']},
    'reduced_test (45+15+15)': {'train': ['bear', 'bmx-bumps', 'boat', 'boxing-fisheye', 'breakdance-flare', 'bus', 'car-turn',\
                                           'cat-girl', 'classic-car', 'color-run', 'crossing', 'dance-jump', 'dancing', 'disc-jockey',\
                                           'dog-agility', 'dog-gooses', 'dogs-scale', 'drift-turn', 'drone', 'elephant', 'flamingo',\
                                           'hike', 'hockey', 'horsejump-low', 'kid-football', 'kite-walk', 'koala', 'lady-running',\
                                           'lindy-hop', 'longboard', 'lucia', 'mallard-fly', 'mallard-water', 'miami-surf',\
                                           'motocross-bumps', 'motorbike', 'night-race', 'paragliding', 'planes-water', 'rallye',\
                                           'rhino', 'rollerblade', 'schoolgirls', 'scooter-board', 'scooter-gray'],
                                 'val': ['sheep',\
                                        'skate-park', 'snowboard', 'soccerball', 'stroller', 'stunt', 'surf', 'swing', 'tennis',\
                                        'tractor-sand', 'train', 'tuk-tuk', 'upside-down', 'varanus-cage', 'walking'],
                                'test': ['bike-packing', 'blackswan', 'bmx-trees', 'breakdance', 'camel', 'car-roundabout',\
                                         'car-shadow', 'cows', 'dance-twirl', 'dog', 'dogs-jump', 'drift-chicane', 'drift-straight',\
                                         'goat', 'gold-fish']},
    'reduced (30+10+15)': {'train': ['bmx-bumps', 'boat', 'boxing-fisheye', 'breakdance-flare', 'bus', 'car-turn',\
                                   'cat-girl', 'classic-car', 'color-run', 'crossing', 'dance-jump', 'dancing', 'disc-jockey',\
                                   'dog-agility', 'dog-gooses', 'dogs-scale', 'drift-turn', 'drone', 'elephant', 'flamingo',\
                                   'hike', 'hockey', 'horsejump-low', 'kid-football', 'kite-walk', 'koala', 'lady-running',\
                                   'lindy-hop', 'longboard', 'lucia'],
                             'val': ['sheep',\
                                    'skate-park', 'snowboard', 'soccerball', 'stroller', 'stunt', 'surf', 'swing', 'tennis',\
                                    'tractor-sand'],
                            'test': ['bike-packing', 'blackswan', 'bmx-trees', 'breakdance', 'camel', 'car-roundabout',\
                                     'car-shadow', 'cows', 'dance-twirl', 'dog', 'dogs-jump', 'drift-chicane', 'drift-straight',\
                                     'goat', 'gold-fish']},
    'trainval_reduced (30+10+1)': {'train': ['bmx-bumps', 'boat', 'boxing-fisheye', 'breakdance-flare', 'bus', 'car-turn',\
                                            'cat-girl', 'classic-car', 'color-run', 'crossing', 'dance-jump', 'dancing', 'disc-jockey',\
                                            'dog-agility', 'dog-gooses', 'dogs-scale', 'drift-turn', 'drone', 'elephant', 'flamingo',\
                                            'hike', 'hockey', 'horsejump-low', 'kid-football', 'kite-walk', 'koala', 'lady-running',\
                                            'lindy-hop', 'longboard', 'lucia'],
                                     'val': ['sheep',\
                                             'skate-park', 'snowboard', 'soccerball', 'stroller', 'stunt', 'surf', 'swing', 'tennis',\
                                             'tractor-sand'],
                                    'test': ['bike-packing']},
    'test_only (1+1+30)': {'train': ['bear'],
                             'val': ['sheep'],
                            'test': ['bike-packing', 'blackswan', 'bmx-trees', 'breakdance', 'camel', 'car-roundabout',\
                                     'car-shadow', 'cows', 'dance-twirl', 'dog', 'dogs-jump', 'drift-chicane', 'drift-straight',\
                                     'goat', 'gold-fish', 'horsejump-high', 'india', 'judo', 'kite-surf', 'lab-coat', 'libby',\
                                     'loading', 'mbike-trick', 'motocross-jump', 'paragliding-launch', 'parkour', 'pigs',\
                                     'scooter-black', 'shooting', 'soapbox']},
    'test_only_reduced (1+1+15)': {'train': ['bear'],
                                     'val': ['sheep'],
                                    'test': ['bike-packing', 'blackswan', 'bmx-trees', 'breakdance', 'camel', 'car-roundabout',\
                                             'car-shadow', 'cows', 'dance-twirl', 'dog', 'dogs-jump', 'drift-chicane', 'drift-straight',\
                                             'goat', 'gold-fish']},
    'debug_train (3+1+1)': {'train': ['bear', 'bmx-bumps', 'boat'],
                             'val': ['sheep'],
                            'test': ['bike-packing']},
    'debug_test (1+1+3)': {'train': ['bear'],
                             'val': ['sheep'],
                            'test': ['bike-packing', 'blackswan', 'bmx-trees']}

}

IM_RESIZE_DICT = {'bike-packing': (480, 910), 'disc-jockey': (480, 1138), 'cat-girl': (480, 911), 'shooting': (480, 1152)}


def get_seg_folder_path(segalg_name, n_segs=None):
    '''
    Parameters:
        segalg_name: str
        (OPTIONAL) n_segs: int
    Returns:
        seg_folder_path: None OR str; returns None if no path
    '''
    if segalg_name == 'ccs':
        if n_segs == 1024:
            return DATA_FOLDER_SEG_H5_CCS1024
        elif n_segs == 2048:
            return DATA_FOLDER_SEG_H5_CCS2048
    elif segalg_name == 'slic_dynamic':
        return None
    elif segalg_name == 'pregen':
        return None, "TODO remove all DAVIS related paths and prefix strings from pregen SegAlg module."
    else:
        assert False, "Not implemented."

def get_video_set_vidnames(video_set_id, split_id):
    '''
    Parameters:
        vidname: str
        video_set_id, split_id: str
    Returns:
        list of str; vidnames in set & split
    '''
    assert split_id in ['train', 'val', 'test']
    return VIDEO_SETS[video_set_id][split_id]

def get_true_annots(vidname):
    '''
    Parameters:
        vidname: str
    Returns:
        gt_annot: ndarray(n_ims, sy, sx) of uint8
    '''
    davis_annot_h5_path = os.path.join(DATA_FOLDER_GT_ANNOT, ANNOT_FILENAME_PREFIX + vidname + '.h5')
    h5f = h5py.File(davis_annot_h5_path, 'r')
    gt_annot = h5f[ANNOT_H5_KEY][:].astype(np.uint8)
    h5f.close()
    if vidname == 'tennis':   # erroeneous label in 'tennis' sequence annotations: the tennis ball is labeled 255 instead of 3
        error_mask = gt_annot == 255
        assert np.any(error_mask)
        gt_annot[error_mask] = 3
    return gt_annot

def get_optflow_occlusion_data(vidname):
    '''
    Parameters:
        vidname: str
    Returns:
        flow_fw, flow_bw: ndarray(n_ims, sy, sx, 2:[dy, dx]) of fl16
        occl_fw, occl_bw: ndarray(n_ims, sy, sx) of bool_
    '''
    flows_h5_path = os.path.join(DATA_FOLDER_OPTFLOWS, 'flownet2_' + vidname + '.h5')
    h5f = h5py.File(flows_h5_path, 'r')
    flow_fw = h5f['flows'][:].astype(np.float16)
    flow_bw = h5f['inv_flows'][:].astype(np.float16)
    occl_fw = h5f['occls'][:].astype(np.bool_)
    occl_bw = h5f['inv_occls'][:].astype(np.bool_)
    h5f.close()
    return flow_fw, flow_bw, occl_fw, occl_bw

def get_featuremap_data(vidname):
    '''
    Parameters:
        vidname: str
    Returns:
        fmaps: dict{str - ndarray of fl16}; see details in the funciton body
    '''
    features_pkl_path = os.path.join(DATA_FOLDER_IMFEATURES, \
               'features_' + IMFEATURES_MODELKEY + '_' + vidname + '.pkl')
    with open(features_pkl_path, 'rb') as f:
        # for each vid a dict: 
        # {'expanded_conv_project_BN': nd(82, 240, 427, 16) of fl16,
        #  'block_2_add': nd(82, 120, 213, 24) of fl16,
        #  'block_5_add': nd(82, 60, 106, 32) of fl16,
        #  'block_12_add': nd(82, 30, 53, 96) of fl16,
        #  'block_16_project_BN': nd(82, 15, 26, 320) of fl16,
        #  'out_relu': nd(82, 15, 26, 1280) of fl16}

        fmaps = pickle.load(f)
        del fmaps['out_relu']   # dropping 'out_relu'
    return fmaps

def get_img_data(vidname, return_bgr=True, return_lab=False):
    '''
    Parameters:
        vidname: str
        return_bgr: bool; whether to return 'img_data_bgr'
        return_lab: bool; whether to return 'img_data_lab'
    Returns:
        (OPTIONAL) ims_bgr: ndarray(n_imgs, 480, 854, 3:BGR) of uint8
        (OPTIONAL) ims_lab: ndarray(n_imgs, 480, 854, 3:LAB) of uint8
    '''
    assert return_bgr or return_lab
    foldername = os.path.join(IM_FOLDER, vidname)
    n_imgs = len(os.listdir(foldername))
    ims_bgr = []
    ims_lab = []
    for im_idx in range(n_imgs):
        fpath = os.path.join(foldername, str(im_idx).zfill(5) + ".jpg")
        assert os.path.isfile(fpath)
        im = cv2.imread(fpath, cv2.IMREAD_COLOR)
        im_bgr = cv2.imread(fpath, cv2.IMREAD_COLOR)
        ims_bgr.append(im_bgr)
        if return_lab:
            im_lab = cv2.cvtColor(im_bgr, cv2.COLOR_BGR2LAB)
            ims_lab.append(im_lab)

    if return_bgr:
        ims_bgr = np.stack(ims_bgr, axis=0)
        assert ims_bgr.shape == (n_imgs, 480, 854, 3)
        assert ims_bgr.dtype == np.uint8
    if return_lab:
        ims_lab = np.stack(ims_lab, axis=0)
        assert ims_lab.shape == (n_imgs, 480, 854, 3)
        assert ims_lab.dtype == np.uint8

    if return_bgr and return_lab:
        return ims_bgr, ims_lab
    elif return_bgr:
        return ims_bgr
    elif return_lab:
        return ims_lab

