
#
# Runner script for running the debugging UI for the DAVIS interactive benchmark. RLseg2 implementation #2
#
#   @author Viktor Varga
#

import sys
sys.path.append('..')

import os
import numpy as np
import pickle

from data_if import DataIF
from segmentation import Segmentation
from segmentation_labeling import SegLabeling

from label_estimation.basic_label_estimation import BasicLabelEstimation
from label_estimation.mrf_label_estimation import MRFLabelEstimation
from label_estimation.logreg_label_model import LogRegLabelModel

from seed_propagation.basic_optflow_seed_propagation import BasicOptflowSeedPropagation
from datasets import DAVIS17
from davisinteractive.session import DavisInteractiveSession    # install from pip, see https://interactive.davischallenge.org
import tkinter as tk

import config as Config
if Config.LABEL_ESTIMATION_ALGORITHM == 'gnn':
    from label_estimation.gnn_label_estimation import GNNLabelEstimation   # only import pytorch if used
if Config.DATAGENTR_TRAINING_MODE == 'simple':
    from interactive.davis_debug_gui import DAVISDebugGUI
elif Config.DATAGENTR_TRAINING_MODE == 'davis':
    from interactive.davis_debug_gui_davismode import DAVISDebugGUI_DAVIS

if __name__ == '__main__':

    LABELING_MODE = 'seed_nosplit' if Config.DATAGENTR_DAVIS_USE_SEEDPROP else 'seg'
    assert LABELING_MODE in ['seg', 'seed_nosplit', 'seed']

    #SET_NAME = 'test_only (1+1+30)'    # loading this in 'cached+bgr_im+of' protocol takes 310 sec from remote hdd, 195 sec from local ssd
    SET_NAME = 'debug_test (1+1+3)'

    print("CONFIG --->")
    print({var: Config.__dict__[var] for var in dir(Config) if not var.startswith("__")})
    print("<--- CONFIG")

    # LOAD DATA, INIT SegLabeling, LOAD image data if necessary
    data_if = DataIF()
    DatasetClass = data_if.get_dataset_class()
    assert DatasetClass == DAVIS17
    test_vidnames = DatasetClass.get_video_set_vidnames(SET_NAME, 'test')
    load_data_rule = 'cached+bgr_im+of'
    data_if.load_videos(test_vidnames, load_data=load_data_rule)   # TODO need to load single videos on demand, with 'all' data loading rule

    segs = {vidname: data_if.get_seg_obj(vidname) for vidname in test_vidnames}
    seg_labs = {vidname: SegLabeling(vidname, segs[vidname], labeling_mode='seg') for vidname in test_vidnames}   # TODO, use seed mode
    max_n_labels = max([seg_labs[vidname].get_n_labels() for vidname in test_vidnames])

    print("Loaded videos & data:", SET_NAME)

    # INIT LabelModel
    assert Config.LABEL_MODEL_METHOD == 'logreg', "Other algorithms are not implemented for impl#2."
    lab_model = LogRegLabelModel()

    # INIT SEED PROPAGATION
    #   seedprop_alg = DummySeedPropagation() if LABELING_MODE == 'seed' else None
    seedprop_alg = BasicOptflowSeedPropagation(segs) if Config.DATAGENTR_DAVIS_USE_SEEDPROP is True else None

    # LOAD TRAINED LABEL MODEL
    n_labmodel_features = list(segs.values())[0].sp_chans['fvecs'].shape[-1] if Config.FIRST_N_FEATURES_TO_USE is None\
                                                                         else Config.FIRST_N_FEATURES_TO_USE
    lab_model.reset(n_features=n_labmodel_features, n_cats=None)
    lab_model.pretrain_mutliple_label_setups(xss_train=None, yss_train=None, xss_val=None, yss_val=None)

    # INIT LabelEstimation (gnn only), load trained LabelEstimation model
    if Config.LABEL_ESTIMATION_ALGORITHM == 'gnn':
        p_dim = max_n_labels if Config.GNNBIN_NATIVE_MULTICLASS else None
        lab_est = GNNLabelEstimation('fvecs', lab_model, n_labmodel_features, multiclass_n_cats=p_dim, seedprop_alg=seedprop_alg)
        lab_est.pretrain_graphs(tr_iter=None, val_iter=None)   # loads pretrained model (since None is passed instead of dataloaders)
    else:
        assert False, "Not implemented."


    # ---------------------------- DAVIS Interactive Challenge - benchmark interaction visualization ----------------------------

    DAVIS_report_dir = '../temp_results/davis_interactive_reports/'
    os.makedirs(DAVIS_report_dir, exist_ok=True)
    DAVIS_max_nb_interactions = 8 # Maximum number of interactions

    # Metric to optimize
    DAVIS_metric = 'J'
    assert DAVIS_metric in ['J', 'F', 'J_AND_F']

    with DavisInteractiveSession(host='localhost',
                            user_key=None,
                            davis_root=DAVIS17.ROOT_FOLDER,
                            subset='val',
                            shuffle=False,
                            max_time=2**30,
                            max_nb_interactions=DAVIS_max_nb_interactions,
                            metric_to_optimize=DAVIS_metric,
                            #report_save_dir=DAVIS_report_dir) as sess:
                            report_save_dir=None) as sess:

        # Create GUI window
        root_widget = tk.Tk()
        if Config.DATAGENTR_TRAINING_MODE == 'davis':
            annotator = DAVISDebugGUI_DAVIS(root_widget, seg_labs, sess, mode=LABELING_MODE, lab_est=lab_est, seed_prop=seedprop_alg)
        elif Config.DATAGENTR_TRAINING_MODE == 'simple':
            annotator = DAVISDebugGUI(root_widget, seg_labs, sess, mode=LABELING_MODE, lab_est=lab_est, seed_prop=seedprop_alg)
        root_widget.mainloop()    # takes over control of the main thread

        # Get the global summary
        #report_save_fpath = os.path.join(DAVIS_report_dir, 'summary.json')
        #summary = sess.get_global_summary(save_file=report_save_fpath)

    # DESTRUCTORS
    lab_model.destroy()
