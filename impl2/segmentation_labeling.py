
#
# Class storing segmentation and true labeling (from user). RLseg2 implementation #2
#
#   @author Viktor Varga
#

import os
import numpy as np

import util.imutil as ImUtil

class SegLabeling:

    '''
    Member fields:
        
        vidname: str; TODO move to Segmentation later
        seg: Segmentation instance

        labeling_mode: str; in ['seg', 'seed_nosplit', 'seed']
            IF 'seg': the minimum unit to be labeled is a SP segment
            IF 'seed_nosplit': the minimum unit to be labeled is a SP segment but annotations are given as seeds, and seedprop is supported
            IF 'seed': the minimum unit to be labeled is a pixel (segments can be split to be consistently labeled in thsi case)
        
        label_estimation: None OR LabelEstimation subclass instance
        seed_propagation: None OR SeedPropagation subclass instance

        n_labels: int

        pred_up_to_date: bool; True if 'self.pred_labels' is up to date and no changes were made since then to class members
        user_labels: ndarray(n_sps,) of int32; -1 if missing; labels added by user (all others are -1)
        pred_labels: ndarray(n_sps,) of int32; -1 if missing; labels predicted (-1 if no labels predicted)

    (SEED MODE)
        user_seed_annot: list(n_user_annots) of ndarray(n_seeds_in_user_annot, 4:[fr_idx, py, px, label] of ints);
                            ONLY in 'seed_nosplit' or 'seed' labeling_mode, stores propagated seeds as well: see 'n_user_seed_annots_before_prop'
        n_user_seed_annots_before_prop: list(n_user_annots) of int; the number of seed annots given by the user in each round;
                            in 'user_seed_annot[i]' array user seeds are located in idxs 0..n_user_seed_annots_before_prop[i]
                                while propagated seeds are located in idxs n_user_seed_annots_before_prop[i]..
    (SEG MODE)
        user_seg_annot: list(n_user_annots) of ndarray(n_segs_in_user_annot, 2:[SP id, label]); ONLY in 'seg' labeling_mode

    '''

    def __init__(self, vidname, seg, labeling_mode):
        assert labeling_mode in ['seg', 'seed_nosplit', 'seed']
        self.labeling_mode = labeling_mode
        self.vidname = vidname
        self.seg = seg
        self.label_estimation = None
        self.seed_propagation = None

        u_labels = np.unique(self.seg.sp_chans['gt_rounded'])
        assert np.array_equal(u_labels, np.arange(u_labels.shape[0])), "Assert labels from 0 to n_labels-1"
        assert u_labels.shape[0] >= 2
        self.n_labels = u_labels.shape[0]

        self.reset()

    # RESET

    def reset(self):
        '''
        Clears all user annotations.
        Parameters:
            lab_est: LabelEstimation subclass instance
        '''
        self.user_seed_annot = []
        self.n_user_seed_annots_before_prop = []
        self.user_seg_annot = []
        self.user_labels = np.full((self.seg.get_n_sps_total(),), dtype=np.int32, fill_value=-1)
        self.pred_labels = np.copy(self.user_labels)
        self.pred_up_to_date = True

    def set_label_estimation(self, lab_est):
        '''
        Parameters:
            lab_est: LabelEstimation subclass instance
        '''
        self.label_estimation = lab_est

    def set_seed_propagation(self, seed_prop):
        '''
        Parameters:
            seed_prop: SeedPropagation subclass instance
        '''
        assert self.labeling_mode in ['seed_nosplit', 'seed'], "Seed propagation can only be used in 'seed_nosplit' or 'seed' mode."
        self.seed_propagation = seed_prop

    # CONST

    def get_n_labels(self):
        return self.n_labels

    def get_true_im_labels(self):
        '''
        Returns:
            ndarray(n_frames, sy, sx) of int
        '''
        return self.seg.ims['gt_raw'][:,:,:,0]

    def get_true_seg_labels(self):
        '''
        Returns:
            ndarray(n_sps) of int
        '''
        return self.seg.sp_chans['gt_rounded'].reshape(-1)

    def get_n_user_inputs(self):
        # Returns the number of submits (with possibly multiple clicks), not the actual number of clicks.
        return len(self.user_seg_annot) if self.labeling_mode == 'seg' else len(self.user_seed_annot)

    def get_user_input_mask(self):
        '''
        Returns:
            user_input_mask: ndarray(n_sps) of bool_
        '''
        return self.user_labels >= 0

    def get_n_user_annots_per_category(self, include_propagated_seeds=False):
        '''
        Parameters:
            include_propagated_seeds: bool; if True and in 'seed_nosplit' or 'seed' mode, includes propagated seed counts as well
        Returns:
            n_user_annots_per_cat: ndarray(n_cats,) of int; the number of user annots in each category
        '''
        if self.labeling_mode == 'seg':
            if len(self.user_seg_annot) == 0:
                return np.zeros((self.n_labels), dtype=np.int32)
            all_annots = np.concatenate(self.user_seg_annot, axis=0)[:,1]

        elif self.labeling_mode in ['seed_nosplit', 'seed']:
            if len(self.user_seed_annot) == 0:
                return np.zeros((self.n_labels), dtype=np.int32)
            if include_propagated_seeds:
                all_seed_annots_ls = self.user_seed_annot
            else:
                all_seed_annots_ls = [annot_arr[:prop_offset,:] for annot_arr, prop_offset in zip(self.user_seed_annot, self.n_user_seed_annots_before_prop)]
            all_annots = np.concatenate(all_seed_annots_ls, axis=0)[:,3]

        n_user_annots_per_cat = np.bincount(all_annots, minlength=self.n_labels)
        return n_user_annots_per_cat

    def get_predictions(self):
        '''
        Returns the predicted label array. Ensures prediction is up-to-date.
        Returns:
            ndarray(n_sps,) of int32;
        '''
        assert self.pred_up_to_date is True
        assert np.all(self.pred_labels) >= 0   # TODO DEBUG, can remove later
        return self.pred_labels

    def get_predictions_image(self):
        '''
        Returns the full pixelwise predicted label array. Ensures prediction is up-to-date.
        Returns:
            ndarray(n_frames, sy, sx) of int32;
        '''
        assert self.pred_up_to_date is True
        assert np.all(self.pred_labels) >= 0   # TODO DEBUG, can remove later
        seg_im = self.seg.get_seg_region(bbox_stlebr=None, framewise_seg_ids=False)
        return self.pred_labels[seg_im]

    def evaluate_predictions(self, method):
        '''
        Evaluates predicted labeling compared to the true labeling.
        Paramters:
            method: str; any of ['sp_accuracy', 'sp_size_accuracy', 'mean_j_sp', 'mean_j_raw'];
        '''
        assert self.pred_up_to_date is True
        assert method in ['sp_accuracy', 'sp_size_accuracy', 'mean_j_sp', 'mean_j_raw']

        if method in ['sp_accuracy', 'sp_size_accuracy', 'mean_j_sp']:
            true_labels = self.get_true_seg_labels()
            seg_sizes = self.seg.get_seg_sizes()
            return ImUtil.compute_segmentation_labeling_error(true_labels, self.pred_labels, self.n_labels, \
                                                                                    method, seg_sizes=seg_sizes)
        elif method == 'mean_j_raw':
            true_lab_im = self.get_true_im_labels()
            pred_lab_im = self.get_predictions_image()
            return ImUtil.compute_pixelwise_labeling_error(true_lab_im, pred_lab_im, self.n_labels)
        #

    # NON-CONST PUBLIC

    def add_true_user_seg_annot(self, sp_ids):
        '''
        Adds user seg annot using the true labels. Requires 'update_predictions()' call to recompute label predictions.
        Parameters:
            sp_ids: ndarray(n_segs_annotated) of int32
        '''
        labels = self.get_true_seg_labels()[sp_ids]
        self.add_custom_user_seg_annot(sp_ids, labels)

    def add_custom_user_seg_annot(self, sp_ids, labels):
        '''
        Adds user annot with custom labels. Requires 'update_predictions()' call to recompute label predictions.
        Parameters:
            sp_ids: ndarray(n_segs_annotated) of int32
            labels: ndarray(n_segs_annotated) of int32
        '''
        assert self.labeling_mode == 'seg', "Segment annotations cannot be added in 'seed_nosplit' or 'seed' mode."
        self.pred_up_to_date = False
        assert sp_ids.size > 0
        assert sp_ids.shape == labels.shape
        assert np.all(labels >= 0) and np.all(labels < self.n_labels)
        #assert np.all(self.user_labels[sp_ids] == -1)  # assert segments annotated were not already annotated
        if not np.all(self.user_labels[sp_ids] == -1):
            print("!!! Warning! SegLabeling -> Custom labels added correspond to already annotated segments!")
        self.user_seg_annot.append(np.stack([sp_ids, labels], axis=-1))
        self.user_labels[sp_ids] = labels

    def add_true_user_seed_annot(self, fr_idxs, points):
        '''
        Adds user seed annot using the true labels. Requires 'update_predictions()' call to recompute label predictions.
        Parameters:
            fr_idxs: ndarray(n_seed_annots,) of int32
            points: ndarray(n_seed_annots, 2:[py, px]) of int32
        '''
        label_im = self.get_true_im_labels()
        labels = label_im[fr_idxs, points[:,0], points[:,1]]
        assert labels.shape == fr_idxs.shape
        self.add_custom_user_seed_annot(fr_idxs, points, labels)

    def add_custom_user_seed_annot(self, fr_idxs, points, labels):
        '''
        Adds user seed annot with custom labels. Requires 'update_predictions()' call to recompute label predictions.
        Parameters:
            fr_idxs: ndarray(n_seed_annots,) of int32
            points: ndarray(n_seed_annots, 2:[py, px]) of int32
            labels: ndarray(n_seed_annots,) of int32
        '''
        assert self.labeling_mode in ['seed_nosplit', 'seed'], "Seed annotations cannot be added in 'seg' mode."
        self.pred_up_to_date = False
        assert fr_idxs.shape == labels.shape == points.shape[:1]
        assert points.shape[1:] == (2,)
        assert np.all(labels >= 0) and np.all(labels < self.n_labels)

        # propagate seed points with self.seed_propagation
        self.n_user_seed_annots_before_prop.append(points.shape[0])
        user_seed_annots_current = []
        user_seed_annots_current.append(np.stack([fr_idxs, points[:,0], points[:,1], labels], axis=-1))
        if self.seed_propagation is not None:
            prop_points = self.seed_propagation.propagate(self.vidname, fr_idxs, points, labels)
            for fr_idx, prop_arr in prop_points.items():
                fr_idxs = np.full(prop_arr.shape[:1], fill_value=fr_idx, dtype=np.int32)
                user_seed_annots_current.append(np.stack([fr_idxs, prop_arr[:,0], prop_arr[:,1], prop_arr[:,2]], axis=-1))
        user_seed_annots_current = np.concatenate(user_seed_annots_current, axis=0)
        self.user_seed_annot.append(user_seed_annots_current)

        user_seeds = np.concatenate(self.user_seed_annot, axis=0)   # (n_seeds, 4:[fr_idx, py, px, label])
        if self.labeling_mode == 'seed_nosplit':
            # ignore inconsistent labeling, assign labels
            self.user_labels = np.full((self.seg.get_n_sps_total(),), dtype=np.int32, fill_value=-1)
            self.pred_labels = np.copy(self.user_labels)
            user_seed_seg_idxs = self.seg.get_sp_ids_from_coords(user_seeds[:,0], user_seeds[:,1:3])
            self.user_labels[user_seed_seg_idxs] = user_seeds[:,3]

        elif self.labeling_mode == 'seed':
            # check if current segmentation is consistent with user seeds (no differently labeled seeds in same segment)
            user_seed_seg_idxs = self.seg.get_sp_ids_from_coords(user_seeds[:,0], user_seeds[:,1:3])   # (n_seeds,)
            print("US->", user_seeds, user_seed_seg_idxs)
            seg_labeling = np.zeros((self.user_labels.shape[0], self.n_labels), dtype=np.bool_)  # (n_segs, n_labels)
            seg_labeling[user_seed_seg_idxs, user_seeds[:,3]] = 1
            n_labels_in_segs = np.count_nonzero(seg_labeling, axis=1)  # (n_segs,)
            print("INCONSISTENT SEEDS:", np.where(n_labels_in_segs > 1))

            if np.any(n_labels_in_segs > 1):
                # current segmentation is inconsistent: find segments which contain multiple seeds with different labels
                inconsistent_seg_idxs = np.where(n_labels_in_segs > 1)[0]    # (n_inconsistent_segs,)
                conflicting_seeds = []
                for inconsistent_seg_idx in inconsistent_seg_idxs:
                    confl_seeds_in_seg = user_seeds[user_seed_seg_idxs == inconsistent_seg_idx, 1:]   # (n_confl_seeds, 3:[py, px, label])
                    conflicting_seeds.append((inconsistent_seg_idx, confl_seeds_in_seg))  # ordered by SP ID

                # split segments with conflicting seeds
                self.seg.split_segments(conflicting_seeds)

                # Warning! self.user_labels, self.pred_labels are invalid here as the number of segments might have changed

                # DEBUG check consistency again, TODO remove
                user_seed_seg_idxs = self.seg.get_sp_ids_from_coords(user_seeds[:,0], user_seeds[:,1:3])   # (n_seeds,)
                print("US->", user_seeds, user_seed_seg_idxs)
                seg_labeling = np.zeros((self.seg.get_n_sps_total(), self.n_labels), dtype=np.bool_)  # (n_segs, n_labels)
                seg_labeling[user_seed_seg_idxs, user_seeds[:,3]] = 1
                n_labels_in_segs = np.count_nonzero(seg_labeling, axis=1)  # (n_segs,)
                print("INCONSISTENT SEEDS#2:", np.where(n_labels_in_segs > 1))
                assert np.all(n_labels_in_segs <= 1)

            # no inconsistencies, assign labels
            self.user_labels = np.full((self.seg.get_n_sps_total(),), dtype=np.int32, fill_value=-1)
            self.pred_labels = np.copy(self.user_labels)
            user_labeled_segs = np.any(seg_labeling, axis=1)  # (n_segs,)
            self.user_labels[user_labeled_segs] = np.argmax(seg_labeling[user_labeled_segs,:], axis=1)

    def remove_last_user_annot(self):
        '''
        Removes the last user annot. Requires 'update_predictions()' call to recompute label predictions.
        '''
        assert self.get_n_user_inputs() > 0
        self.pred_up_to_date = False
        if self.labeling_mode == 'seg':
            self.user_seg_annot.pop()
            self.user_labels[:] = -1
            all_user_seg_annots = np.concatenate(self.user_seg_annot)   # (n_labeled_seg, 2:[SP id, label])
            self.user_labels[all_user_seg_annots[:,0]] = all_user_seg_annots[:,1]
        elif self.labeling_mode in ['seed_nosplit', 'seed']:
            self.user_seed_annot.pop()
            self.n_user_seed_annots_before_prop.pop()
            all_user_seed_annots = np.concatenate(self.user_seed_annot)   # (n_seeds, 4:[fr_idx, py, px, label])
            all_user_seed_seg_idxs = self.seg.get_sp_ids_from_coords(all_user_seed_annots[:,0], all_user_seed_annots[:,1:3])   # (n_seeds,)
            self.user_labels[:] = -1
            self.user_labels[all_user_seed_seg_idxs] = all_user_seed_annots[:,3]


    def update_predictions(self):
        '''
        Updates self.pred_labels, self.prev_update.
        '''
        assert self.label_estimation is not None

        # fit label estimation
        user_annot_mask = self.get_user_input_mask()
        user_labeled_seg_ids = np.where(user_annot_mask)[0]
        user_labeled_seg_labs = self.user_labels[user_labeled_seg_ids]
        user_labels = np.stack([user_labeled_seg_ids, user_labeled_seg_labs], axis=1)
        self.label_estimation.fit(user_labels)

        # predict with label estimation, overwrite predictions where the true labels are known
        self.pred_labels = self.label_estimation.predict_all(return_probs=False)
        assert self.pred_labels.shape == self.user_labels.shape
        self.pred_labels[user_annot_mask] = self.user_labels[user_annot_mask]
        self.pred_up_to_date = True

