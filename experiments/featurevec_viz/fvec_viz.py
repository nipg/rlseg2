
# Visualizes first 3 components of feature map PCAs

import numpy as np
import cv2
from sklearn.decomposition import PCA
import os
import pickle

vidname = 'soapbox'
fr_idx = 0
IM_FOLDER = '/home/vavsaai/databases/DAVIS/DAVIS2017/DAVIS/JPEGImages/480p_unisize/'
DATA_FOLDER_IMFEATURES = '/home/vavsaai/databases/DAVIS/rlseg2_data/davis2017_imfeatures/'
IMFEATURES_MODELKEY = 'MobileNetV2'

def create_img_from_pca_out(fs_red, target_size_yx):
    '''
    Resizes and normalizes image.
    Paramters:
        fs_red: ndarray(ds_size_y, ds_size_x, 3)
        target_size_yx: (size_y, size_x)
    Returns:
        fs_im: ndarray(size_y, size_x, 3) of uint8
    '''
    assert fs_red.ndim == 3
    max_ch_vals = np.amax(fs_red, axis=(0,1))
    min_ch_vals = np.amin(fs_red, axis=(0,1))
    fs_red = 255.*(fs_red - min_ch_vals) / (max_ch_vals - min_ch_vals + 1e-8)
    fs_red = np.clip(fs_red, 0., 255.)
    fs_red = fs_red.astype(np.uint8)
    fs_im = cv2.resize(fs_red, target_size_yx[::-1])
    return fs_im

def create_top_map_ims(fs, target_size_yx):
    '''
    Resizes and normalizes image.
    Paramters:
        fs: ndarray(ds_size_y, ds_size_x, n_features)
        target_size_yx: (size_y, size_x)
    Returns:
        fs_im: ndarray(size_y, size_x, 3) of uint8
    '''
    assert fs.ndim == 3
    fstd = np.std(fs, axis=(0,1), dtype=np.float64)
    top3_f_idxs = np.argsort(fstd)[::-1][:3]
    fs = fs[:,:,top3_f_idxs]
    max_ch_vals = np.amax(fs, axis=(0,1))
    min_ch_vals = np.amin(fs, axis=(0,1))
    fs = 255.*(fs - min_ch_vals) / (max_ch_vals - min_ch_vals + 1e-8)
    fs = np.clip(fs, 0., 255.)
    fs = fs.astype(np.uint8)
    fs_im = cv2.resize(fs, target_size_yx[::-1])
    # uncomment next line for single chan (top-1)
    # fs_im = fs_im[:,:,[0]]*[1,1,1]
    return fs_im


if __name__ == '__main__':


    print("Loading image data...")
    foldername = os.path.join(IM_FOLDER, vidname)
    fpath = os.path.join(foldername, str(fr_idx).zfill(5) + ".jpg")
    assert os.path.isfile(fpath)
    im = cv2.imread(fpath, cv2.IMREAD_COLOR)
    assert im.shape == (480, 854, 3)
    assert im.dtype == np.uint8

    print("Loading feature data...")
    features_pkl_path = os.path.join(DATA_FOLDER_IMFEATURES,  'features_' + IMFEATURES_MODELKEY + '_' + vidname + '.pkl')
    with open(features_pkl_path, 'rb') as f:
        # for each vid a dict: 
        # {'expanded_conv_project_BN': nd(82, 240, 427, 16) of fl16,
        #  'block_2_add': nd(82, 120, 213, 24) of fl16,
        #  'block_5_add': nd(82, 60, 106, 32) of fl16,
        #  'block_12_add': nd(82, 30, 53, 96) of fl16,
        #  'block_16_project_BN': nd(82, 15, 26, 320) of fl16,
        #  'out_relu': nd(82, 15, 26, 1280) of fl16}
        feature_raw_data = pickle.load(f)

    print("Computing feature vector PCAs & saving images...")
    cv2.imwrite("img_" + vidname + "_" + str(fr_idx) + ".png", im)

    for lname, fs in feature_raw_data.items():
        print("    layer", lname)
        print("        shape:", fs.shape)

        # creating and saving PCA first 3 components as image
        pca_obj = PCA(n_components=3)
        fs_red = pca_obj.fit_transform(fs[fr_idx].reshape(-1, fs.shape[-1]))
        print("        explained var:", pca_obj.explained_variance_ratio_)
        fs_red = fs_red.reshape(fs.shape[1:3] + (3,))

        fs_pca_im = create_img_from_pca_out(fs_red, im.shape[:2])
        cv2.imwrite("fs_pca_" + lname + "_" + vidname + "_" + str(fr_idx) + ".png", fs_pca_im)

        # get top 3 feature map and save as images
        fs_top_im = create_top_map_ims(fs[fr_idx], im.shape[:2])
        cv2.imwrite("fs_top_" + lname + "_" + vidname + "_" + str(fr_idx) + ".png", fs_top_im)




