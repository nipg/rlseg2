
# 
# RLseg2 experimental script: CNN feature extraction from videos, plotting feature distributions w. and w/o. labels
#
# @author Viktor Varga
#

import matplotlib
matplotlib.use('Agg')

import sys
sys.path.append('../..')

import gc
import os
import cv2
import h5py
import numpy as np
import pickle
import matplotlib.pyplot as plt
from util import util as Util

from keras.models import Model
from keras.applications.mobilenet_v2 import MobileNetV2

IM_FOLDER = '/home/vavsaai/databases/DAVIS/DAVIS2017/DAVIS/JPEGImages/480p_unisize/'
DATA_FOLDER_GT_ANNOT = '/home/vavsaai/databases/DAVIS/rl_seg_data/davis2017_annot480p/'
DATA_FOLDER_IMFEATURES = '/home/vavsaai/databases/DAVIS/rlseg2_data/davis2017_imfeatures/'
ANNOT_FILENAME_PREFIX = 'annot480p_'
ANNOT_H5_KEY = 'davis2017_annot_480p'
FOLDER_OUT_PLOTS = '/home/vavsaai/git/rlseg2/temp_results/feature_plots/'


LAYER_NAMES_MOBILENET_V2 = ['expanded_conv_project_BN', 'block_2_add', 'block_5_add', 'block_12_add',\
                       'block_16_project_BN', 'out_relu']

# orig DAVIS 2017 split (90 vids: 60 tr, 30 test)
        
# TR_SET_VIDNAMES = ['bear']
# TEST_SET_VIDNAMES = ['blackswan', 'gold-fish']
# TR_SET_VIDNAMES = ['bear', 'bmx-bumps', 'boat', 'boxing-fisheye', 'breakdance-flare', 'bus', 'car-turn',\
#                     'cat-girl', 'classic-car', 'color-run', 'crossing', 'dance-jump', 'dancing', 'disc-jockey',\
#                     'dog-agility', 'dog-gooses', 'dogs-scale', 'drift-turn', 'drone', 'elephant']
# TEST_SET_VIDNAMES = ['bike-packing', 'blackswan', 'bmx-trees', 'breakdance', 'camel', 'car-roundabout',\
#                       'car-shadow', 'cows', 'dance-twirl', 'dog']
TR_SET_VIDNAMES = ['bear', 'bmx-bumps', 'boat', 'boxing-fisheye', 'breakdance-flare', 'bus', 'car-turn',\
                   'cat-girl', 'classic-car', 'color-run', 'crossing', 'dance-jump', 'dancing', 'disc-jockey',\
                   'dog-agility', 'dog-gooses', 'dogs-scale', 'drift-turn', 'drone', 'elephant', 'flamingo',\
                   'hike', 'hockey', 'horsejump-low', 'kid-football', 'kite-walk', 'koala', 'lady-running',\
                   'lindy-hop', 'longboard', 'lucia', 'mallard-fly', 'mallard-water', 'miami-surf',\
                   'motocross-bumps', 'motorbike', 'night-race', 'paragliding', 'planes-water', 'rallye',\
                   'rhino', 'rollerblade', 'schoolgirls', 'scooter-board', 'scooter-gray', 'sheep',\
                   'skate-park', 'snowboard', 'soccerball', 'stroller', 'stunt', 'surf', 'swing', 'tennis',\
                   'tractor-sand', 'train', 'tuk-tuk', 'upside-down', 'varanus-cage', 'walking']
TEST_SET_VIDNAMES = ['bike-packing', 'blackswan', 'bmx-trees', 'breakdance', 'camel', 'car-roundabout',\
                     'car-shadow', 'cows', 'dance-twirl', 'dog', 'dogs-jump', 'drift-chicane', 'drift-straight',\
                     'goat', 'gold-fish', 'horsejump-high', 'india', 'judo', 'kite-surf', 'lab-coat', 'libby',\
                     'loading', 'mbike-trick', 'motocross-jump', 'paragliding-launch', 'parkour', 'pigs',\
                     'scooter-black', 'shooting', 'soapbox']

def load_img_data(vidnames_to_load):
    '''
    Parameters:
        vidnames_to_load: list of str
    Returns:
        img_data_rgb: dict{str - vidname: ndarray(n_imgs, 480, 854, 3:RGB) of uint8}
    '''
    img_data_rgb = {}
    for vidname in vidnames_to_load:
        foldername = os.path.join(IM_FOLDER, vidname)
        n_imgs = len(os.listdir(foldername))
        ims = []
        for im_idx in range(n_imgs):
            fpath = os.path.join(foldername, str(im_idx).zfill(5) + ".jpg")
            assert os.path.isfile(fpath)
            im = cv2.imread(fpath, cv2.IMREAD_COLOR)
            im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
            ims.append(im)
        ims = np.stack(ims, axis=0)
        assert ims.shape == (n_imgs, 480, 854, 3)
        assert ims.dtype == np.uint8
        img_data_rgb[vidname] = ims
    return img_data_rgb

def load_annot_data(vidnames_to_load):
    '''
    Parameters:
        vidnames_to_load: list of str
    Returns:
        annot_data: dict{str - vidname: ndarray(n_imgs, 480, 854) of uint8}
    '''
    annot_data = {}
    for vidname in vidnames_to_load:
        davis_annot_h5_path = os.path.join(DATA_FOLDER_GT_ANNOT, ANNOT_FILENAME_PREFIX + vidname + '.h5')
        h5f = h5py.File(davis_annot_h5_path, 'r')
        annot_data[vidname] = h5f[ANNOT_H5_KEY][:].astype(np.uint8)
        h5f.close()
    return annot_data

def extract_and_save_features(ims_dict, model_name, verbose=False):
    '''
    Parameters:
        ims_dict: dict{str - vidname: ndarray(n_imgs, 480, 854, 3) of uint8}
        model_name: str
        verbose: bool
    '''
    assert model_name in ['MobileNetV2']
    if model_name == 'MobileNetV2':
        # original pretrained model
        model_orig = MobileNetV2(input_shape=None, alpha=1.0, include_top=False, weights='imagenet', input_tensor=None, pooling=None)

        # model wrapper to extract activations of intermediate layers
        # MobileNetV2 expects input format: RGB channel order, [0,255] float32, any input size larger than 32x32
        outputs_list = [model_orig.get_layer(layer_name).output for layer_name in LAYER_NAMES_MOBILENET_V2]
        model = Model(inputs=model_orig.get_layer('input_1').input, \
                            outputs=outputs_list)
        model.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=['accuracy'])
        BATCH_SIZE = 16

    for vidname in ims_dict.keys():
        ims = ims_dict[vidname]
        yss = []
        if verbose:
            print("    at video:", vidname)
        for fr_offset in range(0, ims.shape[0], BATCH_SIZE):
            fr_end_offset = min(fr_offset+BATCH_SIZE, ims.shape[0])
            xs = ims[fr_offset:fr_end_offset,:,:,:].astype(np.float32)  # (batch_size, size_y, size_x, 3)
            ys = model.predict(xs) # tuple of (batch_size, ?sy, ?sx, ?n_ch) of float32
            yss.append(ys)

        # concatenate batches
        feature_viddict = {ln:[yss_batch[l_idx] for yss_batch in yss] for l_idx, ln in enumerate(LAYER_NAMES_MOBILENET_V2)}
        feature_viddict = {ln: np.concatenate(yss, axis=0).astype(np.float16) for ln, yss in feature_viddict.items()}

        # save features to a pickle archive
        davis_features_pkl_path = os.path.join(DATA_FOLDER_IMFEATURES, \
                                                    'features_' + model_name + '_' + vidname + '.pkl')
        with open(davis_features_pkl_path, 'wb') as f:
            pickle.dump(feature_viddict, f)

def load_features(vidnames_to_load, feature_modelkey, fvec_normalizer=None):
    '''
    Parameters:
        vidnames_to_load: list of str
        feature_modelkey: str
        fvec_normalizer: None OR callable, accepts and returns a single 4D feature vec array
    Returns:
        feature_data: dict{str - vidname: dict{str - layer_name: ndarray(n_imgs, ?sy, ?sx, ?n_ch) of float32}}
    '''
    feature_data = {}
    for vidname in vidnames_to_load:
        davis_features_pkl_path = os.path.join(DATA_FOLDER_IMFEATURES, \
                                                    'features_' + feature_modelkey + '_' + vidname + '.pkl')
        with open(davis_features_pkl_path, 'rb') as f:
            vid_feature_data = pickle.load(f)
            if fvec_normalizer is not None:
                vid_feature_data = {layer_name: fvec_normalizer(fvec.astype(np.float32))\
                                                         for layer_name, fvec in vid_feature_data.items()}
            feature_data[vidname] = vid_feature_data

    return feature_data

def compute_feature_stats(feature_data):
    '''
    Computes 10th and 90th percentile for each feature vector time series array, per feature ('feature_stats').
    Also computes minimum of 10th percentiles and maximum of 90th percentiles across all videos.
    Paramters:
        feature_data: dict{str - vidname: dict{str - layer_name: ndarray(n_imgs, ?sy, ?sx, ?n_ch) of float32}}
    Returns:
        feature_stats: dict{str - vidname: dict{str - layer_name: ndarray(2:[p10,p90], ?n_ch) of float32}}
        feature_stats_all: dict{str - layer_name: ndarray(2:[p10,p90], ?n_ch) of float32}}
    '''
    feature_stats = {}
    feature_stats_all = {}
    for vidname in feature_data.keys():
        print("    at video: ", vidname)
        feature_stats_vid = {}
        feature_stats[vidname] = feature_stats_vid
        feature_data_vid = feature_data[vidname]
        layer_names = list(feature_data_vid.keys())
        for layer_name in layer_names:
            fvec = feature_data_vid[layer_name]   # (n_frames, sy, sx, n_features)
            percentiles = np.percentile(fvec, q=[10., 90.], axis=(0,1,2))  # (2, n_features)
            assert percentiles.shape == (2, fvec.shape[-1])
            feature_stats_vid[layer_name] = percentiles

    feature_stats_all = {ln: np.stack([feature_stats[vidname][ln] for vidname in feature_data.keys()], axis=0)\
                                                             for ln in layer_names}
    feature_stats_all = {ln: np.stack([np.amin(feature_stats_all[ln][:,0,:], axis=0),
                                      np.amax(feature_stats_all[ln][:,1,:], axis=0)], axis=0) for ln in layer_names}
    return feature_stats, feature_stats_all

def normalize_feature_data(feature_data, feature_stats_all):
    '''
    Scales features in min-max rescale style, per feature, in-place.
    Paramters:
        feature_data: dict{str - vidname: dict{str - layer_name: ndarray(n_imgs, ?sy, ?sx, ?n_ch) of float32}}
        feature_stats_all: dict{str - layer_name: ndarray(2:[p10,p90], ?n_ch) of float32}}
    '''
    EPS = 1e-8
    norm_fvec = lambda fvec, fmin, fmax: np.clip((fvec-fmin)/(fmax-fmin+EPS), 0., 1.)
    vidnames = list(feature_data.keys())
    layer_names = feature_data[vidnames[0]].keys()

    for vidname in vidnames:
        print("    at video: ", vidname)
        for ln in layer_names:
            feature_data[vidname][ln] = norm_fvec(feature_data[vidname][ln],\
                                         feature_stats_all[ln][0,:], feature_stats_all[ln][1,:]).astype(np.float16)

    return feature_data
#

def get_feature_means_per_label(fvec_img, annot_img=None, n_labels=None):
    '''
    Computes mean features for each label. If 'annot_img' is None, computes regular mean. Feature images
        may be downsampled compared to mask sizes.
    Parameters:
        fvec_img: ndarray(downs_sy, downs_sx, n_features) of float
        annot_img: None OR ndarray(sy, sx) of float
        n_labels: None OR int; optional
    Returns:
        fmeans: ndarray(n_labels, n_features) of float
    '''
    assert fvec_img.ndim == 3
    assert (annot_img is None) or (annot_img.ndim == 2)
    if annot_img is None:
        return np.mean(fvec_img, axis=(0,1))[None,:]

    annot_img_ds = cv2.resize(annot_img, dsize=fvec_img.shape[1::-1], interpolation=cv2.INTER_NEAREST)
    if n_labels is None:
        n_labels = len(np.unique(annot_img_ds))
    fmeans = []
    for lab in range(n_labels):
        mask = annot_img_ds == lab
        fmeans.append(np.mean(fvec_img[mask,:], axis=0))
    fmeans = np.stack(fmeans, axis=0)
    assert fmeans.shape == (n_labels, fvec_img.shape[2])
    return fmeans


def plot_feature_distributions(folderpath_out, feature_data, annot_data=None, verbose=False):
    '''
    Plots a single feature distribution plot for each frame in each video (a pcolor subplot for each feature vector).
    If annot_data is given, plots a subplot for each feature vector for each label.
    Paramters:
        folderpath_out: str; folder to save plots into
        feature_data: dict{str - vidname: dict{str - layer_name: ndarray(n_imgs, ?sy, ?sx, ?n_ch) of float32}}
        annot_data: None OR dict{str - vidname: ndarray(n_imgs, 480, 854) of uint8}
        verbose: bool
    '''

    PCOLOR_SHAPES = {16:(1,16), 24:(1,24), 32:(2,16), 96:(4,24), 320:(8,40), 1280:(16,80)}
    for vidname in feature_data.keys():
        folderpath_vid = os.path.join(folderpath_out, vidname)
        feature_data_vid = feature_data[vidname]
        vidlen = list(feature_data_vid.values())[0].shape[0]
        os.makedirs(folderpath_vid, exist_ok=True)
        n_labels = 1
        if annot_data is not None:
            annot_data_vid = annot_data[vidname]
            n_labels = len(np.unique(annot_data_vid))
        if verbose:
            print("    at video:", vidname)

        for fr_idx in range(vidlen):
            fig, axes = plt.subplots(nrows=len(feature_data_vid), ncols=n_labels)
            for fvec_idx, (layer_name, feature_vec) in enumerate(feature_data_vid.items()):
                assert feature_vec.ndim == 4
                annot_img = None if annot_data is None else annot_data_vid[fr_idx]
                fvec_means = get_feature_means_per_label(feature_vec[fr_idx], annot_img, n_labels)
                fvec_2d_shape = PCOLOR_SHAPES[fvec_means.shape[-1]]  # reshape to 2D for better pcolor plotting
                fvec_means = fvec_means.reshape((fvec_means.shape[0],) + fvec_2d_shape)
                assert fvec_means.ndim == 3
                for lab in range(fvec_means.shape[0]):
                    ax = axes[fvec_idx] if n_labels == 1 else axes[fvec_idx][lab]
                    c = ax.pcolormesh(fvec_means[lab], vmin=0., vmax=1., cmap='jet')
            fpath_plt = os.path.join(folderpath_vid, 'fvec_laball_fr' + str(fr_idx).zfill(3) + '.png')
            fig.suptitle("Feature vectors, all labels, video: " + vidname + ", frame:" + str(fr_idx))
            plt.savefig(fpath_plt)
            plt.close()
            # temp
            #assert False

def _linear_normalizer_per_feature(arr):
    # scales arr to [0,1], per feature (feature axis: #-1)
    assert arr.ndim == 4  # (n_frames, sy, sx, n_features)
    percentiles = np.percentile(arr, q=[10., 90.], axis=(0,1,2))  # (2, n_features)
    assert percentiles.shape == (2, arr.shape[-1])
    EPS = 1e-8
    #arr = (arr - np.amin(arr, axis=(0,1,2)))/(np.amax(arr, axis=(0,1,2)) - np.amin(arr, axis=(0,1,2)) + EPS)
    arr = (arr - percentiles[0,:])/(percentiles[1,:] - percentiles[0,:] + EPS)
    arr = np.clip(arr, 0., 1.)
    return arr

if __name__ == '__main__':

    all_vids = TR_SET_VIDNAMES + TEST_SET_VIDNAMES

    print("Loading image data for", len(all_vids), "videos...")
    img_data_rgb = load_img_data(all_vids)
    print("Loading annot data for", len(all_vids), "videos...")
    annot_data = load_annot_data(all_vids)

    #print("Extracting features from image data with convnet...")
    #extract_and_save_features(img_data_rgb, 'MobileNetV2', verbose=True)

    del img_data_rgb

    print("Loading features...")
    #fvec_normalizer = lambda arr: Util.to_logscale_bidir(arr, base=2., min_exp=-4., max_exp=5.)
    feature_data = load_features(all_vids, 'MobileNetV2', fvec_normalizer=None)

    print("Computing feature stats for normalization...")
    feature_stats, feature_stats_all = compute_feature_stats(feature_data)
    print("Normalizing features...")
    normalize_feature_data(feature_data, feature_stats_all)

    print("Plotting feature distributions...")
    plot_feature_distributions(FOLDER_OUT_PLOTS, feature_data, annot_data=annot_data, verbose=True)
