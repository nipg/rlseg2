
# Script to compare DAVIS gt labeling to optical flow visualizations

import sys
sys.path.append('/home/vavsaai/git/nipgutils/src/')
import nipgutils.visualization as Viz

import numpy as np
import cv2
import os
import h5py

OPTFLOW_H5_SOURCE_FOLDER = '/home/vavsaai/databases/DAVIS/rlseg2_data/davis2017_optflows_fixedoccl/'
GT_LABELS_FOLDER = '/home/vavsaai/databases/DAVIS/rl_seg_data/davis2017_annot480p/'
TARGET_FOLDER = '/home/vavsaai/git/rlseg2/temp_results/davis_optflow_canny/'

CANNY_HIGH = 200
CANNY_LOW = 100

def get_video_list():
    '''
    Returns:
        list of str; video names
    '''
    assert os.path.isdir(OPTFLOW_H5_SOURCE_FOLDER)
    vidlist = os.listdir(OPTFLOW_H5_SOURCE_FOLDER)
    vidlist = [fname[9:-3] for fname in vidlist if (fname[:9] == 'flownet2_') and (fname[-3:] == '.h5')]
    return vidlist

def load_optflow_data(vidname):
    '''
    Parameters:
        vidname: str
    Returns:
        flow_fw, flow_bw: ndarray(n_frs-1, sy, sx, 2:[dy, dx]) of fl32
    '''
    flows_h5_path = os.path.join(OPTFLOW_H5_SOURCE_FOLDER, 'flownet2_' + vidname + '.h5')
    h5f = h5py.File(flows_h5_path, 'r')
    flow_fw = h5f['flows'][:].astype(np.float32)
    flow_bw = h5f['inv_flows'][:].astype(np.float32)
    h5f.close()
    return flow_fw, flow_bw

def load_gt_annots(vidname):
    '''
    Parameters:
        vidname: str
    Returns:
        annot_arr: ndarray(n_frs, sy, sx) of i32
    '''
    annot_h5_path = os.path.join(GT_LABELS_FOLDER, 'annot480p_' + vidname + '.h5')
    h5f = h5py.File(annot_h5_path, 'r')
    annot_arr = h5f['davis2017_annot_480p'][:].astype(np.int32)          # (n_frames, sy, sx) of i32
    h5f.close()
    return annot_arr

def save_output_images(vidname, flow_fw, flow_bw, annot_arr):
    #
    folder_path = os.path.join(TARGET_FOLDER, vidname)
    os.makedirs(folder_path, exist_ok=True)

    annot_border_mask = np.zeros_like(annot_arr, dtype=np.bool_)
    annot_border_mask[:,1:,:] |= annot_arr[:,1:,:] != annot_arr[:,:-1,:]
    annot_border_mask[:,:,1:] |= annot_arr[:,:,1:] != annot_arr[:,:,:-1]

    for fr_idx in range(flow_fw.shape[0]):
        flowim_fw = Viz.create_flow_box_visualization(flow_fw[fr_idx], brightness_mul=40.)   # (sy, sx, 3) of ui8
        flowim_bw = Viz.create_flow_box_visualization(flow_bw[fr_idx], brightness_mul=40.)   # (sy, sx, 3) of ui8
        flowim_fw[annot_border_mask[fr_idx,:,:],:] = 255.
        flowim_bw[annot_border_mask[fr_idx+1,:,:],:] = 255.
        impath_fw = os.path.join(folder_path, 'fw_' + str(fr_idx).zfill(3) + '.png')
        impath_bw = os.path.join(folder_path, 'bw_' + str(fr_idx).zfill(3) + '.png')
        cv2.imwrite(impath_fw, flowim_fw)
        cv2.imwrite(impath_bw, flowim_bw)

def save_canny_images(vidname, flow_fw, flow_bw, annot_arr):
    #
    #folder_path = os.path.join(TARGET_FOLDER, vidname + '_' + str(CANNY_HIGH) + '_' + str(CANNY_LOW))
    folder_path = os.path.join(TARGET_FOLDER, vidname)
    os.makedirs(folder_path, exist_ok=True)

    annot_border_mask = np.zeros_like(annot_arr, dtype=np.bool_)
    annot_border_mask[:,1:,:] |= annot_arr[:,1:,:] != annot_arr[:,:-1,:]
    annot_border_mask[:,:,1:] |= annot_arr[:,:,1:] != annot_arr[:,:,:-1]

    for fr_idx in range(flow_fw.shape[0]+1):

        if fr_idx < flow_fw.shape[0]:
            flowim_fw = Viz.create_flow_box_visualization(flow_fw[fr_idx], brightness_mul=40.)   # (sy, sx, 3) of ui8
            edges_fw = cv2.Canny(flowim_fw, CANNY_LOW, CANNY_HIGH)
        else:
            edges_fw = np.zeros(flow_fw.shape[1:3], dtype=np.uint8)

        if fr_idx > 0:
            flowim_bw = Viz.create_flow_box_visualization(flow_bw[fr_idx-1], brightness_mul=40.)   # (sy, sx, 3) of ui8
            edges_bw = cv2.Canny(flowim_bw, CANNY_LOW, CANNY_HIGH)
        else:
            edges_bw = np.zeros(flow_bw.shape[1:3], dtype=np.uint8)

        im_comp = np.zeros(edges_fw.shape + (3,), dtype=np.uint8)
        im_comp[:,:,0] = edges_fw
        im_comp[:,:,2] = edges_bw
        im_comp[:,:,1] = annot_border_mask[fr_idx,:,:].astype(np.uint8)*255
        impath_comp = os.path.join(folder_path, 'canny_c_' + str(fr_idx).zfill(3) + '.png')
        cv2.imwrite(impath_comp, im_comp)

        #flowim_fw = Viz.create_flow_box_visualization(flow_fw[fr_idx], brightness_mul=40.)   # (sy, sx, 3) of ui8
        #flowim_bw = Viz.create_flow_box_visualization(flow_bw[fr_idx], brightness_mul=40.)   # (sy, sx, 3) of ui8
        #edges_fw = cv2.Canny(flowim_fw, CANNY_LOW, CANNY_HIGH)
        #edges_bw = cv2.Canny(flowim_bw, CANNY_LOW, CANNY_HIGH)
        #impath_fw = os.path.join(folder_path, 'canny_fw_' + str(fr_idx).zfill(3) + '.png')
        #impath_bw = os.path.join(folder_path, 'canny_bw_' + str(fr_idx).zfill(3) + '.png')
        #cv2.imwrite(impath_fw, edges_fw)
        #cv2.imwrite(impath_bw, edges_bw)

if __name__ == '__main__':

    vidnames = get_video_list()    # (folder_name, fnames list)
    os.makedirs(TARGET_FOLDER, exist_ok=True)

    #vidnames = ['drift-straight']
        
    for vidname in vidnames:
        print("Working on", vidname)
        flow_fw, flow_bw = load_optflow_data(vidname)
        annot_arr = load_gt_annots(vidname)
        #save_output_images(vidname, flow_fw, flow_bw, annot_arr)
        save_canny_images(vidname, flow_fw, flow_bw, annot_arr)
        