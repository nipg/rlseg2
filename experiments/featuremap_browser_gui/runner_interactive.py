
#
# Runner script for feature browser GUI window
#
#   @author Viktor Varga
#

import sys
sys.path.append('..')

import os
import numpy as np
import pickle
import cv2

from gui import GUI
import tkinter as tk


VIDNAME = 'soapbox'
IM_FOLDER = '/home/vavsaai/databases/DAVIS/DAVIS2017/DAVIS/JPEGImages/480p_unisize/'
DATA_FOLDER_IMFEATURES = '/home/vavsaai/databases/DAVIS/rlseg2_data/davis2017_imfeatures/'
IMFEATURES_MODELKEY = 'MobileNetV2'

if __name__ == '__main__':

    print("Loading feature data...")
    features_pkl_path = os.path.join(DATA_FOLDER_IMFEATURES,  'features_' + IMFEATURES_MODELKEY + '_' + VIDNAME + '.pkl')
    with open(features_pkl_path, 'rb') as f:
        # for each vid a dict: 
        # {'expanded_conv_project_BN': nd(82, 240, 427, 16) of fl16,
        #  'block_2_add': nd(82, 120, 213, 24) of fl16,
        #  'block_5_add': nd(82, 60, 106, 32) of fl16,
        #  'block_12_add': nd(82, 30, 53, 96) of fl16,
        #  'block_16_project_BN': nd(82, 15, 26, 320) of fl16,
        #  'out_relu': nd(82, 15, 26, 1280) of fl16}
        feature_raw_data = pickle.load(f)

    print("Loading image data...")
    ims = []
    foldername = os.path.join(IM_FOLDER, VIDNAME)
    for fr_idx in range(feature_raw_data['block_2_add'].shape[0]):
        fpath = os.path.join(foldername, str(fr_idx).zfill(5) + ".jpg")
        assert os.path.isfile(fpath)
        im = cv2.imread(fpath, cv2.IMREAD_COLOR)
        assert im.shape == (480, 854, 3)
        assert im.dtype == np.uint8
        ims.append(im)
    ims = np.stack(ims, axis=0)

    print("GUI window init...")
    root_widget = tk.Tk()
    annotator = GUI(root_widget, ims, feature_raw_data, VIDNAME)
    root_widget.mainloop()    # takes over control of the main thread

