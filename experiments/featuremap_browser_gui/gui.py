
import numpy as np

import tkinter as tk
from PIL import Image, ImageTk
import cv2

class GUI:

    '''
    Parameters:
        master: Tk; parent GUI item
        canvas: Tk.Canvas class for displaying image layers and text info
        vidname: str
        feature_raw_data_dict: dict, see __init__() for details
        feature_mins: dict, {layer_name - str: ndarray(n_features) of fl32}
        feature_maxs: dict, {layer_name - str: ndarray(n_features) of fl32}

        (BASE IMG ARRAY)
        bg_ims: ndarray(n_frs, size_y, size_x, n_ch=3 [BGR]) of uint8; original color images

        (DISPLAYED PHOTO IMAGES)
        photo_ims_rgb: list(n_frs) of tk.PhotoImage, RBG mode (3 channel uint8); CONSTANT
        curr_displayed_fmap_img: tk.PhotoImage, the currently or previously displayed feature map photo img (to keep a reference)

        (DISPLAY STATE)
        curr_fr_idx: int
        curr_fmap_idx: int
        bg_display_mode: int; 0 is rgb, 1..6 are the different feature map layers

    '''

    LAYER_KEYS = {1: 'expanded_conv_project_BN', 2: 'block_2_add', 3: 'block_5_add',
                  4: 'block_12_add', 5: 'block_16_project_BN', 6: 'out_relu'}

    def __init__(self, master, bg_ims, feature_raw_data_dict, vidname):
        '''
        Parameters:
            master: Tk instance, parent GUI item
            bg_ims: ndarray(n_frs, size_y, size_x, n_ch=3 [BGR]) of uint8
            feature_raw_data_dict: dict{layer_name - str: ndarray}, items:
                            {'expanded_conv_project_BN': nd(82, 240, 427, 16) of fl16,
                             'block_2_add': nd(82, 120, 213, 24) of fl16,
                             'block_5_add': nd(82, 60, 106, 32) of fl16,
                             'block_12_add': nd(82, 30, 53, 96) of fl16,
                             'block_16_project_BN': nd(82, 15, 26, 320) of fl16,
                             'out_relu': nd(82, 15, 26, 1280) of fl16
                            }
            vidname: str
        '''
        self.master = master
        master.title("Feature map browser window")
        self.vidname = vidname
        self.feature_raw_data_dict = feature_raw_data_dict
        self._compute_feature_mins_maxs()

        # create constant images
        assert bg_ims.shape[3:] == (3,)
        assert bg_ims.dtype == np.uint8
        self.bg_ims = bg_ims[...,::-1]  # BGR -> RGB
        self.curr_fr_idx = 0
        self.curr_fmap_idx = 0
        self.bg_display_mode = 0
        self.photo_ims_rgb = [ImageTk.PhotoImage(Image.fromarray(im)) for im in self.bg_ims]

        # create canvas
        self.canvas = tk.Canvas(self.master, width=854, height=480)
        self.canvas.pack()

        # bind keypress functions, see Event key details: https://anzeljg.github.io/rin2/book2/2405/docs/tkinter/key-names.html
        self.master.bind("<Left>", self.on_keypress_left)   # frame -= 1
        self.master.bind("<Right>", self.on_keypress_right)   # frame += 1
        self.master.bind("<Prior>", self.on_keypress_pageup)   # frame -= 5
        self.master.bind("<Next>", self.on_keypress_pagedown)   # frame += 5
        self.master.bind("<Home>", self.on_keypress_home)   # frame = 0
        self.master.bind("<End>", self.on_keypress_end)   # frame = <last frame idx>
        self.master.bind("<Key-0>", self.on_keypress_0)     # show bgr
        self.master.bind("<Key-1>", self.on_keypress_1)     # show feature layer#1
        self.master.bind("<Key-2>", self.on_keypress_2)     # show feature layer#2
        self.master.bind("<Key-3>", self.on_keypress_3)     # show feature layer#3
        self.master.bind("<Key-4>", self.on_keypress_4)     # show feature layer#4
        self.master.bind("<Key-5>", self.on_keypress_5)     # show feature layer#5
        self.master.bind("<Key-6>", self.on_keypress_6)     # show feature layer#6
        self.master.bind("<Key-q>", self.on_keypress_q)    # fmap -= 5
        self.master.bind("<Key-a>", self.on_keypress_a)    # fmap -= 1
        self.master.bind("<Key-d>", self.on_keypress_d)    # fmap += 1
        self.master.bind("<Key-e>", self.on_keypress_e)    # fmap += 5
        self.master.bind("<Key-y>", self.on_keypress_y)    # fmap = 0
        self.master.bind("<Key-c>", self.on_keypress_c)    # fmap = <last fmap idx>

        # draw images
        self._redraw_bg_img()
        self._redraw_info_textbox()

    def _generate_info_text(self):
        '''
        Returns:
            text: str
        '''
        text = []
        text.append("Video name: " + str(self.vidname))
        text.append("Frame idx: " + str(self.curr_fr_idx))
        text.append("Feature map layer: " + str(self.bg_display_mode))
        text.append("Feature map idx: " + str(self.curr_fmap_idx))
        return '\n'.join(text)

    def _compute_feature_mins_maxs(self):
        '''
        Sets self.feature_mins, self.feature_maxs.
        '''
        self.feature_mins = {}
        self.feature_maxs = {}
        for key, val in self.feature_raw_data_dict.items():
            assert val.ndim == 4
            val = val.astype(np.float32)
            self.feature_mins[key] = np.amin(val, axis=(0,1,2))
            self.feature_maxs[key] = np.amax(val, axis=(0,1,2))
        #

    # QUERIES

    def _get_bg_photoimg(self):
        '''
        Returns:
            ImageTk.PhotoImage: the rgb or feature map image at the current frame
        '''
        assert self.bg_display_mode in range(7)
        if self.bg_display_mode == 0:
            return self.photo_ims_rgb[self.curr_fr_idx]
        else:
            layer_name = GUI.LAYER_KEYS[self.bg_display_mode]
            fmap_img = self.feature_raw_data_dict[layer_name][self.curr_fr_idx][:,:,self.curr_fmap_idx]
            assert fmap_img.ndim == 2
            fmin = self.feature_mins[layer_name][self.curr_fmap_idx]
            fmax = self.feature_maxs[layer_name][self.curr_fmap_idx]
            fmap_img = ((255.*(fmap_img - fmin)) / (fmax - fmin + 1e-8)).astype(np.uint8)
            fmap_img = cv2.resize(fmap_img, (854, 480))
            photo_img = ImageTk.PhotoImage(Image.fromarray(fmap_img, 'L'))
            self.curr_displayed_fmap_img = photo_img
            return self.curr_displayed_fmap_img

    # CONTROL

    def _redraw_bg_img(self):
        '''
        Redraws background image. Deletes drawn user click circles.
        '''
        self.canvas.delete('bg')
        self.canvas.create_image((0,0), image=self._get_bg_photoimg(), anchor='nw', tags=('bg',))
        self.canvas.tag_lower('bg')

    def _redraw_info_textbox(self):
        '''
        Redraws info textbox.
        '''
        self.canvas.delete('infobox')
        info_text = self._generate_info_text()
        FONT = ("Courier", 12, 'bold')
        self.canvas.create_text((420,20), text=info_text, fill='#FF00FF', anchor='nw', \
                                width=400, font=FONT, tags=('infobox',))


    # ON KEY EVENTS

    def on_keypress_left(self, event):
        self.curr_fr_idx = max(self.curr_fr_idx-1, 0)
        self._redraw_bg_img()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_right(self, event):
        self.curr_fr_idx = min(self.curr_fr_idx+1, len(self.photo_ims_rgb)-1)
        self._redraw_bg_img()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_pageup(self, event):
        self.curr_fr_idx = max(self.curr_fr_idx-5, 0)
        self._redraw_bg_img()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_pagedown(self, event):
        self.curr_fr_idx = min(self.curr_fr_idx+5, len(self.photo_ims_rgb)-1)
        self._redraw_bg_img()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_home(self, event):
        self.curr_fr_idx = 0
        self._redraw_bg_img()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_end(self, event):
        self.curr_fr_idx = len(self.photo_ims_rgb)-1
        self._redraw_bg_img()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_a(self, event):
        self.curr_fmap_idx = max(self.curr_fmap_idx-1, 0)
        self._redraw_bg_img()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_d(self, event):
        max_fmap_idx = self.feature_raw_data_dict[GUI.LAYER_KEYS[self.bg_display_mode]].shape[-1]-1
        self.curr_fmap_idx = min(self.curr_fmap_idx+1, max_fmap_idx)
        self._redraw_bg_img()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_q(self, event):
        self.curr_fmap_idx = max(self.curr_fmap_idx-5, 0)
        self._redraw_bg_img()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_e(self, event):
        max_fmap_idx = self.feature_raw_data_dict[GUI.LAYER_KEYS[self.bg_display_mode]].shape[-1]-1
        self.curr_fmap_idx = min(self.curr_fmap_idx+5, max_fmap_idx)
        self._redraw_bg_img()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_y(self, event):
        self.curr_fmap_idx = 0
        self._redraw_bg_img()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_c(self, event):
        max_fmap_idx = self.feature_raw_data_dict[GUI.LAYER_KEYS[self.bg_display_mode]].shape[-1]-1
        self.curr_fmap_idx = max_fmap_idx
        self._redraw_bg_img()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_0(self, event):
        self.bg_display_mode = 0
        self._redraw_bg_img()
        self._redraw_info_textbox()
        self.canvas.update()

    def _switch_display_mode(self, new_bg_display_mode):
        assert new_bg_display_mode in range(7)
        self.bg_display_mode = new_bg_display_mode
        max_fmap_idx = self.feature_raw_data_dict[GUI.LAYER_KEYS[self.bg_display_mode]].shape[-1]-1
        self.curr_fmap_idx = min(self.curr_fmap_idx, max_fmap_idx)
        self._redraw_bg_img()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_1(self, event):
        self._switch_display_mode(1)

    def on_keypress_2(self, event):
        self._switch_display_mode(2)

    def on_keypress_3(self, event):
        self._switch_display_mode(3)

    def on_keypress_4(self, event):
        self._switch_display_mode(4)

    def on_keypress_5(self, event):
        self._switch_display_mode(5)

    def on_keypress_6(self, event):
        self._switch_display_mode(6)



