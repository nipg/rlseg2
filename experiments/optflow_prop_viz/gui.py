
import numpy as np

import tkinter as tk
from PIL import Image, ImageTk
import cv2

class GUI:

    '''
    Parameters:
        master: Tk; parent GUI item
        canvas: Tk.Canvas class for displaying image layers and text info
        vidname: str

        (BASE IMG ARRAY)
        bg_ims: ndarray(n_frs, size_y, size_x, n_ch=3 [BGR]) of uint8; original color images
        optflow_fw: ndarray(n_frs, size_y, size_x, 2:[dy, dx]) of fl32
        optflow_bw: ndarray(n_frs, size_y, size_x, 2:[dy, dx]) of fl32
        occl_fw: ndarray(n_frs, size_y, size_x) of bool_
        occl_bw: ndarray(n_frs, size_y, size_x) of bool_

        (DISPLAYED PHOTO IMAGES)
        photo_ims_rgb: list(n_frs) of tk.PhotoImage, RBG mode (3 channel uint8); CONSTANT
        photo_ims_optflow_fw: list(n_frs) of tk.PhotoImage, RBG mode (3 channel uint8); CONSTANT
        photo_ims_optflow_bw: list(n_frs) of tk.PhotoImage, RBG mode (3 channel uint8); CONSTANT
        photo_ims_occl_fw: list(n_frs) of tk.PhotoImage, L mode (1 channel uint8); CONSTANT
        photo_ims_occl_bw: list(n_frs) of tk.PhotoImage, L mode (1 channel uint8); CONSTANT

        (DISPLAY STATE)
        curr_fr_idx: int
        bg_display_mode: str; any of ["rgb", "of_fw", "of_bw", "occl_fw", "occl_bw"]
        user_clicks: list of tuple(pos_y, pos_x)
        user_clicks_fr_idx: int; the idx of frame with user clicks, -1 if no user clicks are added
        user_click_props: None OR ndarray(n_frames, n_user_clicks, 3:[prop1, prop2_fwd, prop2_bwd], 2:[y, x]) of float32
                                                                            NaNs represent missing points

    '''

    def __init__(self, master, bg_ims, optflow_fw, optflow_bw, occl_fw, occl_bw, vidname):
        '''
        Parameters:
            master: Tk instance, parent GUI item
            bg_ims: ndarray(n_frs, size_y, size_x, n_ch=3 [BGR]) of uint8
            optflow_fw: ndarray(n_frs-1, size_y, size_x, 2:[dy, dx]) of fl16
            optflow_bw: ndarray(n_frs-1, size_y, size_x, 2:[dy, dx]) of fl16
            occl_fw: ndarray(n_frs-1, size_y, size_x) of bool_
            occl_bw: ndarray(n_frs-1, size_y, size_x) of bool_
            vidname: str
        '''
        self.master = master
        master.title("Feature map browser window")
        self.vidname = vidname
        assert bg_ims.shape[0] == optflow_fw.shape[0]+1 == occl_fw.shape[0]+1 == optflow_bw.shape[0]+1 == occl_bw.shape[0]+1
        self.optflow_fw = np.pad(optflow_fw, ((0,1), (0,0), (0,0), (0,0)), constant_values=0).astype(np.float32)
        self.optflow_bw = np.pad(optflow_bw, ((1,0), (0,0), (0,0), (0,0)), constant_values=0).astype(np.float32)
        self.occl_fw = np.pad(occl_fw, ((0,1), (0,0), (0,0)), constant_values=0)
        self.occl_bw = np.pad(occl_bw, ((1,0), (0,0), (0,0)), constant_values=0)

        # create constant images
        assert bg_ims.shape[3:] == (3,)
        assert bg_ims.dtype == np.uint8
        self.bg_ims = bg_ims[...,::-1]  # BGR -> RGB
        self.curr_fr_idx = 0
        self.bg_display_mode = "rgb"
        self.photo_ims_rgb = [ImageTk.PhotoImage(Image.fromarray(im)) for im in self.bg_ims]
        self.photo_ims_optflow_fw = [ImageTk.PhotoImage(Image.fromarray(self._create_flowviz_img(im))) for im in self.optflow_fw]
        self.photo_ims_optflow_bw = [ImageTk.PhotoImage(Image.fromarray(self._create_flowviz_img(im))) for im in self.optflow_bw]
        self.photo_ims_occl_fw = [ImageTk.PhotoImage(Image.fromarray(self._create_occl_img(im))) for im in self.occl_fw]
        self.photo_ims_occl_bw = [ImageTk.PhotoImage(Image.fromarray(self._create_occl_img(im))) for im in self.occl_bw]

        self.user_clicks = []
        self.user_clicks_fr_idx = -1
        self.user_click_props = None

        # create canvas
        self.canvas = tk.Canvas(self.master, width=854, height=480)
        self.canvas.pack()

        # bind keypress functions, see Event key details: https://anzeljg.github.io/rin2/book2/2405/docs/tkinter/key-names.html
        self.master.bind("<Left>", self.on_keypress_left)   # frame -= 1
        self.master.bind("<Right>", self.on_keypress_right)   # frame += 1
        self.master.bind("<Prior>", self.on_keypress_pageup)   # frame -= 5
        self.master.bind("<Next>", self.on_keypress_pagedown)   # frame += 5
        self.master.bind("<Home>", self.on_keypress_home)   # frame = 0
        self.master.bind("<End>", self.on_keypress_end)   # frame = <last frame idx>
        self.master.bind("<Key-0>", self.on_keypress_0)     # show bgr
        self.master.bind("<Key-1>", self.on_keypress_1)     # show optflow fw
        self.master.bind("<Key-2>", self.on_keypress_2)     # show optflow bw
        self.master.bind("<Key-3>", self.on_keypress_3)     # show occl fw
        self.master.bind("<Key-4>", self.on_keypress_4)     # show occl fw
        self.master.bind("<Key-space>", self.on_keypress_space)     # compute flow for user clicks
        self.master.bind("<Button-1>", self.on_click_left)   # left click - add user input

        # draw images
        self._redraw_bg_img()
        self._redraw_info_textbox()

    def _generate_info_text(self):
        '''
        Returns:
            text: str
        '''
        text = []
        text.append("Video name: " + str(self.vidname))
        text.append("Frame idx: " + str(self.curr_fr_idx))
        text.append("Display mode: " + str(self.bg_display_mode))
        text.append("User inputs: " + str(len(self.user_clicks)) + " at frame " + str(self.user_clicks_fr_idx))
        return '\n'.join(text)

    def _cart2polar(self, x, y):
        """
        Elementwise Cartesian2Polar for arrays. x and y should be of the same size.
        Parameters:
            x, y: ndarray(...); cartesian coordinate arrays
        Returns:
            r: ndarray(...); radius
            phi: ndarray(...); angle in radians, -pi..pi
        """
        r = np.sqrt(np.square(x) + np.square(y))
        phi = np.arctan2(y, x)
        return r, phi

    def _create_flowviz_img(self, flow, brightness_mul=10.):
        """
        Parameters:
            flow: ndarray(size_y, size_x, 2:[y,x]) of float32; output of optical flow algorithms
            brightness_mul: float;
        Returns:
            flowviz: ndarray(size_y, size_x, 3) of uint8
        """
        assert flow.ndim == 3
        assert flow.shape[2] == 2
        MAX_HUE = 179.
        flowviz = np.empty((flow.shape[0], flow.shape[1], 3), dtype=np.uint32)
        flowviz.fill(255)
        r, phi = self._cart2polar(flow[:, :, 1], flow[:, :, 0])
        flowviz[:, :, 0] = ((phi + np.pi) / (2. * np.pi) * MAX_HUE).astype(np.uint32)
        flowviz[:, :, 2] = (r * brightness_mul).astype(np.uint32)
        flowviz[:, :, 1:] = np.clip(flowviz[:, :, 1:], 0, 255)
        flowviz[:, :, 0] = np.clip(flowviz[:, :, 0], 0, int(MAX_HUE))
        flowviz = flowviz.astype(np.uint8)
        flowviz = cv2.cvtColor(flowviz, cv2.COLOR_HSV2BGR)
        return flowviz

    def _create_occl_img(self, occl_arr):
        assert occl_arr.ndim == 2
        return occl_arr.astype(np.uint8)*255

    def _project_points_with_flow(self, flow, points):
        """
        Projects points using optical flow..
        Parameters:
            flow: ndarray(size_y, size_x, 2:[dy,dx]) of float32; output of optical flow algorithms
            points: ndarray(nPoints, 2:[y,x]) of float32;
        Returns:
            points_proj: ndarray(nPoints, 2:[y,x]) of float32; can be NaN if points are not inside the flow
        """
        box_size = np.asarray(flow.shape[:2], dtype=np.int32)  # (2:[y,x])
        valid_point_idxs = ~np.any(np.isnan(points), axis=1)  # (nPoints,)
        inside_box_point_idxs = np.all(points >= 0, axis=1) & np.all(points < box_size, axis=1)  # (nPoints,)
        valid_point_idxs = valid_point_idxs & inside_box_point_idxs

        # rounding of point coordinates only for indexing, the movement from the flow is added to the original float point values
        points_i = np.around(points).astype(np.int32)
        valid_points = points_i[valid_point_idxs, :]  # (nValidPoints,)

        delta_valid_points = flow[valid_points[:, 0], valid_points[:, 1]]  # (nValidPoints, 2:[y,x])
        delta_points = np.full_like(points, fill_value=np.nan)
        delta_points[valid_point_idxs, :] = delta_valid_points
        points_proj = points + delta_points
        return points_proj

    def _propagate_points(self):
        '''
        Expects self.user_click_props to be filled with correct coordinates to
                         propagate at frame idx# 'self.user_clicks_fr_idx'. Fills self.user_click_props.
        '''
        # going towards front of video
        for fr_idx in range(self.user_clicks_fr_idx-1, -1, -1):
            flow_bwd = self.optflow_bw[fr_idx+1]
            flow_fwd = self.optflow_fw[fr_idx]
            self.user_click_props[fr_idx,:,0,:] = self._project_points_with_flow(flow_bwd,\
                                             self.user_click_props[fr_idx+1,:,0,:])
            self.user_click_props[fr_idx+1,:,2,:] = self._project_points_with_flow(flow_fwd,\
                                             self.user_click_props[fr_idx,:,0,:])
        # going towards end of video
        for fr_idx in range(self.user_clicks_fr_idx+1, self.user_click_props.shape[0]):
            flow_fwd = self.optflow_fw[fr_idx-1]
            flow_bwd = self.optflow_bw[fr_idx]
            self.user_click_props[fr_idx,:,0,:] = self._project_points_with_flow(flow_fwd,\
                                             self.user_click_props[fr_idx-1,:,0,:])
            self.user_click_props[fr_idx-1,:,1,:] = self._project_points_with_flow(flow_bwd,\
                                             self.user_click_props[fr_idx,:,0,:])
        #


    # QUERIES

    def _get_bg_photoimg(self):
        '''
        Returns:
            ImageTk.PhotoImage: the rgb or feature map image at the current frame
        '''
        assert self.bg_display_mode in ["rgb", "of_fw", "of_bw", "occl_fw", "occl_bw"]
        if self.bg_display_mode == "rgb":
            return self.photo_ims_rgb[self.curr_fr_idx]
        elif self.bg_display_mode == "of_fw":
            return self.photo_ims_optflow_fw[self.curr_fr_idx]
        elif self.bg_display_mode == "of_bw":
            return self.photo_ims_optflow_bw[self.curr_fr_idx]
        elif self.bg_display_mode == "occl_fw":
            return self.photo_ims_occl_fw[self.curr_fr_idx]
        elif self.bg_display_mode == "occl_bw":
            return self.photo_ims_occl_bw[self.curr_fr_idx]

    # CONTROL

    def _redraw_bg_img(self):
        '''
        Redraws background image. Deletes drawn user click circles.
        '''
        self.canvas.delete('bg')
        self.canvas.create_image((0,0), image=self._get_bg_photoimg(), anchor='nw', tags=('bg',))
        self.canvas.tag_lower('bg')

    def _redraw_info_textbox(self):
        '''
        Redraws info textbox.
        '''
        self.canvas.delete('infobox')
        info_text = self._generate_info_text()
        FONT = ("Courier", 12, 'bold')
        self.canvas.create_text((420,20), text=info_text, fill='#FF00FF', anchor='nw', \
                                width=400, font=FONT, tags=('infobox',))

    def _draw_click_to_canvas(self, pos_yx, radius, color):
        pos_y, pos_x = pos_yx
        self.canvas.create_oval(pos_x-radius, pos_y-radius, pos_x+radius, pos_y+radius, \
                                                            fill=color, tags=('click',))

    def _redraw_clicks(self):
        '''
        Redraws click points (and propagated points).
        '''
        COLOR_ORIGIN = '#DDDD00'  # darker lime
        COLOR_PROP1 = '#FF00FF'  # magenta
        COLOR_PROP2_FWD = '#00DDDD'  # darker cyan
        COLOR_PROP2_BWD = '#FFBB00'  # orange
        RADIUS = 4
        self.canvas.delete('click')
        # draw user clicks if at origin frame
        if self.curr_fr_idx == self.user_clicks_fr_idx:
            for click_pos_yx in self.user_clicks:
                self._draw_click_to_canvas(click_pos_yx, RADIUS, COLOR_ORIGIN)

        # draw propagated points for current frame
        if self.user_click_props is None:
            return

        ps_valid_prop1 = np.any(self.user_click_props[self.curr_fr_idx,:,0,:] != np.nan, axis=-1)
        ps_valid_prop2fwd = np.any(self.user_click_props[self.curr_fr_idx,:,1,:] != np.nan, axis=-1)
        ps_valid_prop2bwd = np.any(self.user_click_props[self.curr_fr_idx,:,2,:] != np.nan, axis=-1)
        for p_idx in np.where(ps_valid_prop1)[0]:
            self._draw_click_to_canvas(self.user_click_props[self.curr_fr_idx,p_idx,0,:], RADIUS, COLOR_PROP1)
        for p_idx in np.where(ps_valid_prop2fwd)[0]:
            self._draw_click_to_canvas(self.user_click_props[self.curr_fr_idx,p_idx,1,:], RADIUS, COLOR_PROP2_FWD)
        for p_idx in np.where(ps_valid_prop2bwd)[0]:
            self._draw_click_to_canvas(self.user_click_props[self.curr_fr_idx,p_idx,2,:], RADIUS, COLOR_PROP2_BWD)
        #


    # ON KEY EVENTS

    def on_keypress_left(self, event):
        self.curr_fr_idx = max(self.curr_fr_idx-1, 0)
        self._redraw_bg_img()
        self._redraw_clicks()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_right(self, event):
        self.curr_fr_idx = min(self.curr_fr_idx+1, len(self.photo_ims_rgb)-1)
        self._redraw_bg_img()
        self._redraw_clicks()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_pageup(self, event):
        self.curr_fr_idx = max(self.curr_fr_idx-5, 0)
        self._redraw_bg_img()
        self._redraw_clicks()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_pagedown(self, event):
        self.curr_fr_idx = min(self.curr_fr_idx+5, len(self.photo_ims_rgb)-1)
        self._redraw_bg_img()
        self._redraw_clicks()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_home(self, event):
        self.curr_fr_idx = 0
        self._redraw_bg_img()
        self._redraw_clicks()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_end(self, event):
        self.curr_fr_idx = len(self.photo_ims_rgb)-1
        self._redraw_bg_img()
        self._redraw_clicks()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_0(self, event):
        self.bg_display_mode = "rgb"
        self._redraw_bg_img()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_1(self, event):
        self.bg_display_mode = "of_fw"
        self._redraw_bg_img()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_2(self, event):
        self.bg_display_mode = "of_bw"
        self._redraw_bg_img()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_3(self, event):
        self.bg_display_mode = "occl_fw"
        self._redraw_bg_img()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_4(self, event):
        self.bg_display_mode = "occl_bw"
        self._redraw_bg_img()
        self._redraw_info_textbox()
        self.canvas.update()

    def on_keypress_space(self, event):
        if len(self.user_clicks) <= 0:
            return
        self.user_click_props = np.full((self.bg_ims.shape[0], len(self.user_clicks), 3, 2), \
                                                    dtype=np.float32, fill_value=np.nan)
        user_clicks_arr = np.array(self.user_clicks, dtype=np.float32)
        assert user_clicks_arr.shape[1:] == (2,)
        self.user_click_props[self.user_clicks_fr_idx,:,0,:] = user_clicks_arr
        self._propagate_points()
        self._redraw_clicks()
        self._redraw_info_textbox()
        self.canvas.update()


    # ON MOUSE EVENTS

    def on_click_left(self, event):
        '''
        Adds user input to the 'self.pending_user_clicks' list.
        If there were any states in REDO, they are deleted and 'self.undo_idx' is set to the end
            of the user input list.
        '''
        coords = (event.y, event.x)
        self.user_click_props = None
        if self.user_clicks_fr_idx == self.curr_fr_idx:
            self.user_clicks.append(coords)
        else:
            self.user_clicks.clear()
            self.user_clicks_fr_idx = self.curr_fr_idx
            self.user_clicks.append(coords)

        self._redraw_clicks()
        self._redraw_info_textbox()
        self.canvas.update()