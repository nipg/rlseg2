
#
# Runner script for interactive optical flow propagation visualizer GUI
#
#   @author Viktor Varga
#

import sys
sys.path.append('..')

import os
import numpy as np
import pickle
import h5py
import cv2

from gui import GUI
import tkinter as tk


VIDNAME = 'motocross-jump'
IM_FOLDER = '/home/vavsaai/databases/DAVIS/DAVIS2017/DAVIS/JPEGImages/480p_unisize/'
DATA_FOLDER_OPTFLOWS = '/home/vavsaai/databases/DAVIS/rl_seg_data/davis2017_optflows/'

if __name__ == '__main__':

    print("Loading optflow data...")
    flows_h5_path = os.path.join(DATA_FOLDER_OPTFLOWS, 'flownet2_' + VIDNAME + '.h5')
    h5f = h5py.File(flows_h5_path, 'r')
    optflow_fw_raw_data = h5f['flows'][:]
    optflow_bw_raw_data = h5f['inv_flows'][:]
    occl_fw_raw_data = h5f['occls'][:]
    occl_bw_raw_data = h5f['inv_occls'][:]
    h5f.close()

    print("Loading image data...")
    ims = []
    foldername = os.path.join(IM_FOLDER, VIDNAME)
    for fr_idx in range(optflow_fw_raw_data.shape[0]+1):
        fpath = os.path.join(foldername, str(fr_idx).zfill(5) + ".jpg")
        assert os.path.isfile(fpath)
        im = cv2.imread(fpath, cv2.IMREAD_COLOR)
        assert im.shape == (480, 854, 3)
        assert im.dtype == np.uint8
        ims.append(im)
    ims = np.stack(ims, axis=0)

    print("GUI window init...")
    root_widget = tk.Tk()
    annotator = GUI(root_widget, ims, optflow_fw_raw_data, optflow_bw_raw_data, occl_fw_raw_data, occl_bw_raw_data, VIDNAME)
    root_widget.mainloop()    # takes over control of the main thread

