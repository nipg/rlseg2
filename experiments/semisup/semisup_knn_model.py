

import numpy as np
from sklearn.neighbors import KNeighborsClassifier

from semisup_abstract_model import SemisupervisedAbstractModel

KNN_N_NEIGHBORS = 5

class SemisupervisedKNNModel(SemisupervisedAbstractModel):

    '''
    Member fields:
        n_features_total: int; input shape to model is (n_features_total,)
        n_cats: int; number of categories to classify into

        model: sklearn.neighbors.KNeighborsClassifier
    '''

    def __init__(self, wrapped_model=None):
        self.n_features_total = None
        self.n_cats = None
        assert wrapped_model is None

    def __str__(self):
        return "kNN(" + str(KNN_N_NEIGHBORS) + ")"

    def reset(self, n_features_total, n_cats):
        self.n_features_total = n_features_total
        self.n_cats = n_cats
        self.model = KNeighborsClassifier(KNN_N_NEIGHBORS)

    def fit(self, xs, ys, verbose=False):
        '''
        Parameters:
            xs: ndarray(n_sps, n_features_total) of float32; the feature vectors for each SP
            ys: ndarray(n_sps) of int32; the true labels of each SP
        '''
        #  n_neighbors <= n_samples must be True
        if xs.shape[0] < KNN_N_NEIGHBORS:
            self.model = KNeighborsClassifier(xs.shape[0])
        
        assert xs.shape[1:] == (self.n_features_total,)
        assert np.amax(ys) < self.n_cats
        self.model.fit(xs, ys)

    def predict(self, xs, return_probs=False, verbose=False):
        '''
        Parameters:
            xs: ndarray(n_sps, n_features_total) of float32; the feature vectors for each SP
            return_probs: bool;
        Returns:
            ys_pred: ndarray(n_sps, n_cat) of fl32 (IF return_probs == True)
                     ndarray(n_sps,) of i32        (IF return_probs == False)

        '''
        assert xs.shape[1:] == (self.n_features_total,)
        if return_probs:
            ys_pred = self.model.predict_proba(xs)
        else:
            ys_pred = self.model.predict(xs)
        return ys_pred

    def evaluate(self, xs, ys, return_preds=False, verbose=False):
        '''
        Parameters:
            xs: ndarray(n_sps, n_features_total) of float32; the feature vectors for each SP
            ys: ndarray(n_sps) of int32; the true labels of each SP
            return_preds: bool
        Returns:
            acc: float; accuracy of predictions (ratio of correctly predicted datapoints)
            (OPTIONAL if return_preds is True) ys_pred: ndarray(n_sps,) of i32
        '''
        assert np.amax(ys) < self.n_cats
        ys_pred = self.predict(xs, return_probs=False, verbose=verbose)
        acc = np.count_nonzero(ys_pred == ys)/float(ys.shape[0])
        if return_preds:
            return acc, ys_pred
        else:
            return acc





