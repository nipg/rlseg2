
import numpy as np

import sys
sys.path.append('../..')
from util import util as Util
from semisup_abstract_model import SemisupervisedAbstractModel

class SemisupervisedBenchmark:

    '''
    Generates a set of random benchmarks on initialization.
    Benchmarks:
        Phase#1: reveal label of 'n_sps_revealed' randomly selected SPs fr each label from across video,
                     train model on them and evaluate on full dataset.
                 repeat experiment 'n_repeats' times
                 (when selecting SPs, SP idx repetitions may happen within a single experiment)
        Phase#2: reveal label of all SPs in 'n_frs_revealed' randomly selected frames from across video,
                     train model on them and evaluate on full dataset.
                 repeat experiment 'n_repeats' times
                 (when selecting frames, no fr idx repetitions are allowed within a single experiment)


    Member fields:
        n_sps_revealed_dict: dict{n_sps_revealed - int: n_repeats - int}
        n_frs_revealed_dict: dict{n_frs_revealed - int: n_repeats - int}
        orig_sp_ids_dict: dict{vidname - str: ndarray(n_sps, 2:[seg_id, fr_idx]) of int32}
        ys_dict: dict{vidname - str: ndarray(n_sps) of i32}; the labels
        
        id: str; random generated ID of benchmark instance
        phase1_benchmark_data: dict{n_sps_revealed - int:
                             dict{vidname - str:
                                     ndarray(n_repeats, n_labels, n_sps_revealed) of i32}} indexing orig_sp_ids_dict
        phase2_benchmark_data: dict{n_frs_revealed - int:
                             dict{vidname - str: ndarray(n_repeats, n_frs_revealed) of i32 - fr idxs}}
    '''

    def __init__(self, n_sps_revealed_dict, n_frs_revealed_dict, orig_sp_ids_dict, ys_dict):
        '''
        Parameters: <see member fields>
        '''
        self.n_sps_revealed_dict = n_sps_revealed_dict
        self.n_frs_revealed_dict = n_frs_revealed_dict
        self.id = Util.generate_session_id()
        self.orig_sp_ids_dict = orig_sp_ids_dict
        self.ys_dict = ys_dict
        self._generate_benchmarks()

    def _to_string_full_info(self):
        content = ["SemisupervisedBenchmark instance, ID: " + str(self.id) +\
                  "\n    n_sps_revealed_dict: " + str(self.n_sps_revealed_dict) +\
                  "\n    n_frs_revealed_dict: " + str(self.n_frs_revealed_dict) + "\n"]
        for n_sps_revealed in self.phase1_benchmark_data.keys():
            content.append("\n\nPHASE 1 EXPERIMENT, n_sps_revealed: " + str(n_sps_revealed) + ": \n")
            for vidname in self.phase1_benchmark_data[n_sps_revealed].keys():
                content.append("  --> video " + str(vidname) + ": \n")
                for repeat_idx in range(self.phase1_benchmark_data[n_sps_revealed][vidname].shape[0]):
                    content.append("    --> repeat idx #" + str(repeat_idx) + ": \n")
                    for lab in range(self.phase1_benchmark_data[n_sps_revealed][vidname].shape[1]):
                        content.append("      --> label #" + str(lab) + ": \n")
                        arr = self.phase1_benchmark_data[n_sps_revealed][vidname][repeat_idx, lab]  # (n_sps,)
                        sp_ids = [(sp_idx, self.orig_sp_ids_dict[vidname][sp_idx,0], self.orig_sp_ids_dict[vidname][sp_idx,1])\
                                                    for sp_idx in arr]
                        content.append("          IDs(sp_idx, orig_sv_id, fr_idx): " + str(sp_ids) + "\n")

        for n_frs_revealed in self.phase2_benchmark_data.keys():
            content.append("\nPHASE 2 EXPERIMENT, n_frs_revealed: " + str(n_frs_revealed) + ": \n")
            for vidname in self.phase2_benchmark_data[n_frs_revealed].keys():
                content.append("  --> video " + str(vidname) + ": \n")
                for repeat_idx in range(self.phase2_benchmark_data[n_frs_revealed][vidname].shape[0]):
                    content.append("    --> repeat idx #" + str(repeat_idx) + ": \n")
                    arr = self.phase2_benchmark_data[n_frs_revealed][vidname][repeat_idx]  # (n_frs_revealed,)
                    content.append("          fr idxs: " + str(list(arr)) + "\n")
        content = ''.join(content)
        return content


    def _generate_benchmarks(self):
        '''
        Sets self.phase1_benchmark_data, self.phase2_benchmark_data.
        '''
        # PHASE 1
        self.phase1_benchmark_data = {}
        for n_sps_revealed, n_repeats in self.n_sps_revealed_dict.items():
            self.phase1_benchmark_data[n_sps_revealed] = {}

            for vidname in self.orig_sp_ids_dict.keys():
                n_labels = np.amax(self.ys_dict[vidname])+1
                out_array = np.empty((n_repeats, n_labels, n_sps_revealed), dtype=np.int32)
                yss = self.ys_dict[vidname]                     # (n_sps)
                orig_sp_ids = self.orig_sp_ids_dict[vidname]    # (n_sps, 2:[seg_orig_id, fr_idx])
                assert yss.shape[0] == orig_sp_ids.shape[0]

                for repeat_idx in range(n_repeats):
                    for lab in range(n_labels):
                        sps_idxs_with_lab = np.where(yss == lab)[0]         # (n_sps_with_given_label)
                        assert sps_idxs_with_lab.shape[0] > 0
                        out_array[repeat_idx, lab, :] = np.random.choice(sps_idxs_with_lab, size=(n_sps_revealed,))
                
                self.phase1_benchmark_data[n_sps_revealed][vidname] = out_array

        # PHASE 2
        self.phase2_benchmark_data = {}
        for n_frs_revealed, n_repeats in self.n_frs_revealed_dict.items():
            self.phase2_benchmark_data[n_frs_revealed] = {}

            for vidname in self.orig_sp_ids_dict.keys():
                out_array = np.empty((n_repeats, n_frs_revealed), dtype=np.int32)
                n_frames = np.amax(self.orig_sp_ids_dict[vidname][:,1])+1    # getting n_frames of video
                assert n_frames >= n_frs_revealed

                for repeat_idx in range(n_repeats):
                    out_array[repeat_idx, :] = np.random.choice(n_frames, size=(n_frs_revealed,), replace=False)

                self.phase2_benchmark_data[n_frs_revealed][vidname] = out_array
        #

    def evaluate_model(self, model, xs_dict, return_preds=False):
        '''
        Evaluates given model on full benchmark.
        Parameters:
            model: SemisupervisedAbstractModel instance (subclass)
            xs_dict: dict{vidname - str: ndarray(n_sps, n_features_total) of fl32}
            return_preds: bool;
        Returns:
            (OPTIONAL if return_preds is True) ys_preds_dict: dict{n_sps_revealed - int: dict{vidname - str:
                                     ndarray(n_repeats, n_sps) of i32}} indexing orig_sp_ids_dict
            (OPTIONAL if return_preds is True) sp_is_gt_dict: dict{n_sps_revealed - int: dict{vidname - str:
                                     ndarray(n_repeats, n_sps) of bool_}}; whether label in ys_preds_dict is GT.
        '''
        assert set(xs_dict.keys()) == set(list(self.phase1_benchmark_data.values())[0].keys())

        # PHASE 1
        ys_preds_dict = {}
        sp_is_gt_dict = {}
        print("SemisupervisedBenchmark: Evaluating model", model, ", PHASE#1")
        for n_sps_revealed in self.phase1_benchmark_data.keys():
            print("  Experiment - n_sps_revealed:", n_sps_revealed)

            ys_preds_dict[n_sps_revealed] = {}
            sp_is_gt_dict[n_sps_revealed] = {}
            vid_mean_accs = []
            for vid_idx, vidname in enumerate(self.phase1_benchmark_data[n_sps_revealed].keys()):
                assert xs_dict[vidname].shape[0] == self.ys_dict[vidname].shape[0]
                revealed_sp_idxs = self.phase1_benchmark_data[n_sps_revealed][vidname]  # (n_repeats, n_labels, n_sps_revealed)
                n_repeats, n_labels, _ = revealed_sp_idxs.shape
                n_sps = self.ys_dict[vidname].shape[0]
                accs = np.empty((len(self.phase1_benchmark_data[n_sps_revealed]), n_repeats), dtype=np.float32)
                if return_preds:
                    ys_preds = np.empty((n_repeats, n_sps), dtype=np.int32)
                    sp_is_gt_arr = np.zeros((n_repeats, n_sps), dtype=np.bool_)
                    ys_preds_dict[n_sps_revealed][vidname] = ys_preds
                    sp_is_gt_dict[n_sps_revealed][vidname] = sp_is_gt_arr

                for repeat_idx in range(n_repeats):
                    train_sp_idxs = revealed_sp_idxs[repeat_idx,:,:].reshape(-1)
                    test_sp_idxs = np.setdiff1d(np.arange(n_sps), train_sp_idxs)
                    xs_train = xs_dict[vidname][train_sp_idxs,:]  # (n_labels*n_sps_revealed, n_features) of fl32
                    ys_train = self.ys_dict[vidname][train_sp_idxs]  # (n_labels*n_sps_revealed,) of i32
                    xs_test = xs_dict[vidname][test_sp_idxs,:]  # (n_unlabeled_sps, n_features) of fl32
                    ys_test = self.ys_dict[vidname][test_sp_idxs]  # (n_unlabeled_sps,) of i32

                    model.reset(xs_train.shape[1], n_labels)
                    model.fit(xs_train, ys_train, verbose=True)
                    if return_preds:
                        accs[vid_idx, repeat_idx], ys_test_preds =\
                                     model.evaluate(xs_test, ys_test, return_preds=True, verbose=True)
                        ys_preds[repeat_idx, train_sp_idxs] = ys_train
                        ys_preds[repeat_idx, test_sp_idxs] = ys_test_preds
                        sp_is_gt_arr[repeat_idx, train_sp_idxs] = True
                    else:
                        accs[vid_idx, repeat_idx] = model.evaluate(xs_test, ys_test, return_preds=False, verbose=True)
                print("      -> model accuracies for video: " + str(accs[vid_idx,:]))
                vid_mean_acc = np.mean(accs[vid_idx,:])
                vid_mean_accs.append(vid_mean_acc)
                print("      -> model mean accuracy for video: " + str(vid_mean_acc))
            print("    -> model mean accuracy: " + str(np.mean(np.array(vid_mean_accs))))

        # PHASE 2
        # print("SemisupervisedBenchmark: Evaluating model", model, ", PHASE#2 TODO")
        # TODO

        if return_preds:
            return ys_preds_dict, sp_is_gt_dict