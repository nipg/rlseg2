
import numpy as np

from keras.models import Model
import keras.layers as KL
import keras.backend as KB

from semisup_abstract_model import SemisupervisedAbstractModel

import sys
sys.path.append('../..')

import util.util as Util

PARAM_BATCH_SIZE = 64
PARAM_HIDDEN_SIZE = 64
N_PREDICT_MATCHES = 10
N_PREDICTION_FROM_N_TOP_SAMPLE_AVGS = 3   # int; to predict a label for a sample from comparison probabilities,


class SemisupervisedSiameseModel(SemisupervisedAbstractModel):

    '''
    Member fields:
        n_features_total: int; input shape to model is (n_features_total,)

        model: keras.models.Model instance

        fitted_data_xs: ndarray(n_sps, n_features_total) of float32
        fitted_data_ys: ndarray(n_sps,) of int32
        fitted_data_n_labels: int
        fitted_data_label_idxs_ls: list(n_labels) of ndarray(n_idxs) of int32
    '''

    def __init__(self, wrapped_model=None):
        self.n_features_total = None
        self.n_cats = None
        self.model = None
        assert wrapped_model is None

    def __str__(self):
        return "SiameseNet"

    def reset(self, n_features_total, n_cats):
        assert (self.n_features_total is None) or (n_features_total == self.n_features_total),\
                                             "SiameseModel does not allow modifying feature count."
        self.n_features_total = n_features_total
        if self.model is None:
            self.model = self._build_siamese_model()

    def pretrain(self, xs_train_dict, ys_train_dict, xs_val_dict, ys_val_dict, verbose=False):
        '''
        Parameters:
            xs_train_dict, xs_val_dict: dict{vidname - str: ndarray(n_sps, n_features_total) of float32;}
            ys_train_dict, ys_val_dict: dict{vidname - str: ndarray(n_sps) of int32;}
        '''
        assert set(xs_train_dict.keys()) == set(ys_train_dict.keys())
        assert set(xs_val_dict.keys()) == set(ys_val_dict.keys())
        if self.model is None:
            self.reset(n_features_total=list(xs_train_dict.values())[0].shape[1], n_cats=None)
        n_labels_train_dict = {vidname: len(np.unique(ys_train_dict[vidname])) for vidname in ys_train_dict.keys()}
        n_labels_val_dict = {vidname: len(np.unique(ys_val_dict[vidname])) for vidname in ys_val_dict.keys()}
        pretrain_train_gen = self._pretrain_generator(xs_train_dict, ys_train_dict, n_labels_train_dict)
        pretrain_val_gen = self._pretrain_generator(xs_val_dict, ys_val_dict, n_labels_val_dict)
        n_steps_per_epoch = max(1, 8192//PARAM_BATCH_SIZE)
        n_val_steps_per_epoch = max(1, 512//PARAM_BATCH_SIZE)
        self.model.fit_generator(pretrain_train_gen, validation_data=pretrain_val_gen,\
                        steps_per_epoch=n_steps_per_epoch, validation_steps=n_val_steps_per_epoch, epochs=100, verbose=2)


    def fit(self, xs, ys, verbose=False):
        '''
        Parameters:
            xs: ndarray(n_sps, n_features_total) of float32; the feature vectors for each SP
            ys: ndarray(n_sps) of int32; the true labels of each SP
        '''
        assert xs.shape[1:] == (self.n_features_total,)
        self.fitted_data_xs = xs
        self.fitted_data_ys = ys
        self.fitted_data_n_labels = np.unique(self.fitted_data_ys)
        self.fitted_data_label_idxs_ls = \
                            [np.where(self.fitted_data_ys == lab)[0] for lab in range(self.fitted_data_n_labels)]

    def predict(self, xs, return_probs=False, verbose=False):
        '''
        Parameters:
            xs: ndarray(n_sps, n_features_total) of float32; the feature vectors for each SP
            return_probs: bool;
        Returns:
            ys_pred: ndarray(n_sps, n_cat) of fl32 (IF return_probs == True)
                     ndarray(n_sps,) of i32        (IF return_probs == False)

        '''
        assert xs.shape[1:] == (self.n_features_total,)
        assert not return_probs  # not implemented

        fitted_sample_idxs = [np.random.choice(idxs_with_label, size=(N_PREDICT_MATCHES,))\
                                            for idxs_with_label in self.fitted_data_label_idxs_ls]  
        fitted_sample_idxs = np.stack(fitted_sample_idxs, axis=0)  #  nd(n_labels, N_PREDICT_MATCHES)
        fitted_xs_sample = self.fitted_data_xs[fitted_sample_idxs,:] # nd(n_labels, N_PREDICT_MATCHES, n_features)
        fitted_xs_sample = fitted_xs_sample.reshape((fitted_sample_idxs.size, self.n_features_total))
                                                                     # nd(n_labels*N_PREDICT_MATCHES, n_features)
        xs0 = np.broadcast_to(xs[:,None,:], (xs.shape[0], fitted_xs_sample.shape[0], xs.shape[1]))
        xs1 = np.broadcast_to(fitted_xs_sample[None,:,:], (xs.shape[0], fitted_xs_sample.shape[0], xs.shape[1]))
        xs_pred = (xs0, xs1)   # tuple(2) of nd(n_sps, n_labels*N_PREDICT_MATCHES, n_features)
        ys_pred = self.model.predict(xs_pred)   # nd(n_sps, n_labels*N_PREDICT_MATCHES) probs
        ys_pred = ys_pred.reshape((ys_pred.shape[0], len(self.fitted_data_label_idxs_ls), N_PREDICT_MATCHES))
                                                   # nd(n_sps, n_labels, N_PREDICT_MATCHES) probs
        ys_pred = self._predict_label_from_comparisons(ys_pred)  # nd(n_sps) labels
        return ys_preds


    def evaluate(self, xs, ys, return_preds=False, verbose=False):
        '''
        Parameters:
            xs: ndarray(n_sps, n_features_total) of float32; the feature vectors for each SP
            ys: ndarray(n_sps) of int32; the true labels of each SP
            return_preds: bool
        Returns:
            acc: float; accuracy of predictions (ratio of correctly predicted datapoints)
            (OPTIONAL if return_preds is True) ys_pred: ndarray(n_sps,) of i32
        '''
        ys_pred = self.predict(xs, return_probs=False, verbose=verbose)
        acc = np.count_nonzero(ys_pred == ys)/float(ys.shape[0])
        if return_preds:
            return acc, ys_pred
        else:
            return acc


    # PRIVATE

    def _pretrain_generator(self, xs_dict, ys_dict, n_labels_dict):
        '''
        GENERATOR
        Parameters:
            xs_dict: dict{vidname - str: ndarray(n_sps, n_features_total) of float32;}
            ys_dict: dict{vidname - str: ndarray(n_sps) of int32;}
            n_labels_dict: dict{vidname - str: n_labels: int}
        Yields:
            tuple( tuple( ndarray(batch_size, n_features_total), ndarray(batch_size, n_features_total ) ),
                   ndarray(batch_size,) of float32 - ys probabilities)
        '''
        assert PARAM_BATCH_SIZE >= 16
        vidnames = list(xs_dict.keys())
        # collect matching and unmatching indices in each video for each label 
        #     dict{vidname: list(n_labels) of ndarray(n_idxs)}
        pos_label_idx_dict = {vidname: [np.where(ys_dict[vidname] == label)[0]\
                                                                     for label in range(n_labels_dict[vidname])]
                                                                             for vidname in ys_dict.keys()}
        neg_label_idx_dict = {vidname: [np.where(ys_dict[vidname] != label)[0]\
                                                                     for label in range(n_labels_dict[vidname])]\
                                                                             for vidname in ys_dict.keys()}
        while True:
            vidname = np.random.choice(vidnames)
            xs_arr = xs_dict[vidname]
            ys_arr = ys_dict[vidname]
            n_labels = n_labels_dict[vidname]
            pos_idxs_ls = pos_label_idx_dict[vidname]  # list(n_labels) of ndarray(n_pos_idxs)}
            neg_idxs_ls = neg_label_idx_dict[vidname]  # list(n_labels) of ndarray(n_neg_idxs)}
            bucket_sizes = Util.split_equally_random_remainders(PARAM_BATCH_SIZE, n_labels*2)  # (n_labels*2,)
            sample_idxs_ls = []
            sample_ys_ls = []
            for label in range(n_labels):
                # generate positive sample pairs
                n_pos_sample_pairs = bucket_sizes[label*2]
                sample_idxs_pos = np.random.choice(pos_idxs_ls[label], size=(n_pos_sample_pairs, 2))
                sample_idxs_ls.append(sample_idxs_pos)
                sample_ys_ls.append(np.ones(sample_idxs_pos.shape[0], dtype=np.float32))

                # generate negative sample pairs
                n_neg_sample_pairs = bucket_sizes[label*2+1]
                sample_idxs_neg1 = np.random.choice(pos_idxs_ls[label], size=(n_neg_sample_pairs,))
                sample_idxs_neg2 = np.random.choice(neg_idxs_ls[label], size=(n_neg_sample_pairs,))
                sample_idxs_neg = np.stack([sample_idxs_neg1, sample_idxs_neg2], axis=-1)
                sample_idxs_ls.append(sample_idxs_neg)
                sample_ys_ls.append(np.zeros(sample_idxs_neg.shape[0], dtype=np.float32))

            sample_idxs = np.concatenate(sample_idxs_ls, axis=0)  # (n_samples, 2)
            sample_xs = xs_arr[sample_idxs]  # (n_samples, 2, n_features)
            sample_ys = np.concatenate(sample_ys_ls, axis=0)  # (n_samples,)
            yield [[sample_xs[:,0,:], sample_xs[:,1,:]], sample_ys]


    def _build_siamese_model(self):
        '''
        Returns:
            model: keras.models.Model instance
        '''

        input1 = KL.Input(shape=(self.n_features_total,))
        input2 = KL.Input(shape=(self.n_features_total,))
        dense1 = KL.Dense(PARAM_HIDDEN_SIZE, activation='sigmoid')  # shared weights before subtract & abs op
        layer_b1 = dense1(input1)
        layer_b2 = dense1(input2)
        layer_merged = KL.Lambda(lambda xs_ls: KB.abs(xs_ls[0] - xs_ls[1]))([layer_b1, layer_b2])   # problems if 2 args
        dense2 = KL.Dense(1, activation='sigmoid')(layer_merged)
        model = Model(inputs=[input1, input2], outputs=dense2)

        model.compile(optimizer='rmsprop', loss='binary_crossentropy', metrics=['mae'])
        return model

    def _predict_label_from_comparisons(self, ys_pred_comparisons):
        '''
        Parameters:
            ys_pred_comparisons: ndarray(n_sps, n_labels, N_PREDICT_MATCHES) of float32; probabilities
        Returns:
            ys_preds: ndarray(n_sps) of int32; predicted labels
        '''
        assert 1 <= N_PREDICTION_FROM_N_TOP_SAMPLE_AVGS <= N_PREDICT_MATCHES
        ys_pred_comparisons = np.sort(ys_pred_comparisons, axis=2)[:,:,-N_PREDICTION_FROM_N_TOP_SAMPLE_AVGS:]
        ys_pred_comparisons = np.mean(ys_pred_comparisons, axis=2)   # (n_sps, n_labels) mean probs per label
        ys_preds = np.argmax(ys_pred_comparisons, axis=1)
        return ys_preds



