
#
# A SV/SP segmentation of a video clip and all corresponding data. RL_seg implementation #4.
#
#   @author Viktor Varga
#

#import impl_util as ImplUtil

import sys
sys.path.append('../..')

import util.util as Util
import util.imutil as ImUtil

sys.path.append('/home/vavsaai/git/nipgutils/src/')
import nipgutils.visualization as Viz

import os
import numpy as np
import skimage.measure
import itertools

class Segmentation:

    '''
    Member fields:

        IMPL#3a INFO: image chans are not serialized (except for key: 'gt_raw',
                      centroids are not computed from regionprop

        data_source: str; either 'cache' or 'init' based on whether 'self' object was
                     created by loading from cache or throug __init__ from raw data
                     (NOT SERIALIZED) 

        lvl0_seg: ndarray(n_frames, size_y, size_x) of uint32
        downscaled_lvl0_segs: dict{tuple(size_y, size_x) of ints: ndarray(n_frames, dsize_y, dsize_x) of uint32}

        seg_hier_parent_ids: ndarray(n_total_hier_segs,) of uint32

        hier_lvls_used: list of ints; the hierarchy levels used excluding 0.
        downscale_sizes_yx = list of tuple of ints

        image_chans: dict{channel_name - str: ndarray(n_frames, size_y, size_x, n_chans) of ?}
        downscaled_image_chans: dict{channel_name - str: dict{tuple(size_y, size_x) of ints):
                                                     ndarray(n_frames, dsize_y, dsize_x, n_chans) of ?}

        image_sp_reduce_funcs: dict{output_seg_attr_key - str: (input_chan_key - str, func - callable, named_args - dict)}
                    where callable receives a single positional arg: ndarray(n_pixels, n_chans) and 
                        named args passed in the named_args dict, while returns a vector of fixed length.
                    (NOT SERIALIZED)

        image_downscale_funcs: dict{output_seg_attr_key - str: (input_chan_key - str, func - callable, named_args - dict)}
                    where callable receives a single positional arg: ndarray(n_frames, size_y, size_x, n_chans) and 
                        named args passed in the named_args dict, while returns a downscaled array of any size.
                    (NOT SERIALIZED)

        #

        rprops: dict{hier_lvl - int: dict{fr_idx - int: skimage.measure.regionprops instance}}
                                !!! seg IDs stored in rprops are greater by 1 than original IDs to avoid zero label !!!
                    (NOT SERIALIZED) 
        sp_chans: dict{hier_lvl - int: dict{channel_name - str: dict{fr_idx - int: ndarray(n_segs_in_fr, n_sp_chans) of ?}}}

        #

        seg_sizes_per_frame: dict{hier_lvl - int: dict{fr_idx - int: ndarray(n_segs_in_fr, 2:[seg_id, size]) of uint32}
                            where seg_id is sorted

        seg_id_intervals_in_level: dict{hier_lvl - int: tuple(from_id_inclusive, to_id_exclusive)};
                            contains the seg ID interval for each hierarchical level
        
        hierarchy_parent_matrix: dict{(hier_lvl_from - int, hier_lvl_to - int):
                                     ndarray(n_segs_in_hier_from, 2:[from_seg_id, to_seg_id])}
        hierarchy_children_arrays: dict{hier_lvl_from - int: 
                                    tuple(ndarray(n_total_children,) of int32, ndarray(lists of ints) of object_)}
                            for each hier lvl used, for each seg in 'hier_lvl_from' level, contains all lvl0 children IDs:
                                tuple[0] are the IDs of all children, 
                                tuple[1][p_id] stores the list of indices of lvl0 children of p_id in tuple[0] array

    '''

    STORE_IMAGE_CHANS_IN_CACHE = False

    def __init__(self, lvl0_seg, seg_hier_parent_ids, hier_lvls_used, downscale_sizes_yx, max_hier_lvl):
        self.data_source = 'init'

        assert lvl0_seg.ndim == 3
        self.lvl0_seg = lvl0_seg.astype(np.uint32)
        assert seg_hier_parent_ids.ndim == 1
        self.seg_hier_parent_ids = seg_hier_parent_ids.astype(np.uint32)

        if 0 in hier_lvls_used:
            hier_lvls_used.remove(0)
        self.hier_lvls_used = hier_lvls_used
        assert len(downscale_sizes_yx) >= 1
        assert all([ds[0] < ds[1]] for ds in downscale_sizes_yx)  # safety check
        self.downscale_sizes_yx = downscale_sizes_yx
        assert max_hier_lvl > 0
        self.max_hier_lvl = max_hier_lvl

        self.image_chans = {}
        self.sp_chans = {}
        self.downscaled_image_chans = {}
        self.image_sp_reduce_funcs = {}
        self.image_downscale_funcs = {}
        self.rprops = {}
        self.sp_dist_idxs = {}
        self.sp_dists = {}

        # process hierarchy
        self._processHierarchy()

        # create regionprops, calculate centroids
        self._initRegionProps()

        # downscale lvl0_seg
        self._compute_downscaled_seg()
        
    # CUSTOM SERIALIZATION: to avoid serializing functions

    def __getstate__(self):
        assert self.data_source == 'init', "Object loaded from cache cannot be serialized again"
        d = {}
        d['lvl0_seg'] = self.lvl0_seg
        d['downscaled_lvl0_segs'] = self.downscaled_lvl0_segs
        d['seg_hier_parent_ids'] = self.seg_hier_parent_ids
        d['hier_lvls_used'] = self.hier_lvls_used
        d['max_hier_lvl'] = self.max_hier_lvl
        d['downscale_sizes_yx'] = self.downscale_sizes_yx
        if Segmentation.STORE_IMAGE_CHANS_IN_CACHE:
            d['image_chans'] = self.image_chans
        else:
            imchans_to_store = {'gt_raw': self.image_chans['gt_raw']}
            d['image_chans'] = imchans_to_store
        d['downscaled_image_chans'] = self.downscaled_image_chans
        d['sp_chans'] = self.sp_chans
        d['seg_sizes_per_frame'] = self.seg_sizes_per_frame
        d['seg_id_intervals_in_level'] = self.seg_id_intervals_in_level
        d['hierarchy_parent_matrix'] = self.hierarchy_parent_matrix
        d['hierarchy_children_arrays'] = self.hierarchy_children_arrays
        return d

    def __setstate__(self, d):
        self.__dict__ = d
        self.data_source = 'cache'

    # GET

    def getShape(self):
        '''
        Returns:
            (tuple of ints): (n_frames, size_y, size_x); the shape of the video
        '''
        return self.lvl0_seg.shape

    def getDownscaleSizes(self):
        '''
        Returns:
            list of tuple of ints: (dsize_y, dsize_x);
        '''
        return self.downscale_sizes_yx[:]

    def getTotalSegCountInHierarchy(self, hier_lvl):
        '''
        Returns:
            int: total number of segments in video in hierarchical level.
        '''
        from_id, to_id = self.getSegIDIntervalInHierLevel(hier_lvl)
        return to_id - from_id

    def getSegIDIntervalInHierLevel(self, hier_lvl):
        '''
        Returns the seg ID interval in a specific hierarchical level.
        Parameters:
            hier_lvl: int
        Returns:
            from_id_inclusive, to_id_exclusive: int
        '''
        #assert hier_lvl in [0] + self.hier_lvls_used
        return self.seg_id_intervals_in_level[hier_lvl]

    def getSegIDsInHierLevelInFrame(self, hier_lvl, fr_idx):
        '''
        Returns all seg IDs in a specific hierarchical level, in a specific frame.
        Parameters:
            hier_lvl, fr_idx: int
        Returns:
            ndarray(n_hier_segs_in_fr,) np.uint32; sorted
        '''
        assert hier_lvl in [0] + self.hier_lvls_used
        return self.seg_sizes_per_frame[hier_lvl][fr_idx][:,0]

    def getSegSizesInHierLevel(self, hier_lvl):
        '''
        Returns all seg sizes in a specific hierarchical level.
        Parameters:
            hier_lvl: int
        Returns:
            dict{fr_idx - int: ndarray(n_hier_segs_in_fr,) np.uint32;} sorted along seg IDs (matching order with the array returned by
                                            self.getSegIDsInHierLevelInFrame())
        '''
        assert hier_lvl in [0] + self.hier_lvls_used
        return {fr_idx: self.seg_sizes_per_frame[hier_lvl][fr_idx][:,1] for fr_idx in\
                         self.seg_sizes_per_frame[hier_lvl].keys()}

    def createEmptySPAnnotDict(self, hier_lvl, n_chans, init_vals):
        '''
        Parameters:
            hier_lvl, n_chans: int
            init_vals: list of ints
        Returns:
            dict{fr_idx - int: ndarray(n_segs_h_in_fr, n_chans+1:[seg_id, OPTIONAL annot_chans]) of int32}
        '''
        assert hier_lvl in [0] + self.hier_lvls_used
        assert n_chans >= 0
        assert len(init_vals) == n_chans

        annot_dict = {}
        for fr_idx in range(self.lvl0_seg.shape[0]):
            seg_ids = self.getSegIDsInHierLevelInFrame(hier_lvl, fr_idx)
            if n_chans > 0:
                annot_arr = np.empty((seg_ids.shape[0], n_chans+1), dtype=np.int32)
                annot_arr[:,0] = seg_ids
                for init_val_idx in range(len(init_vals)):
                    annot_arr[:, init_val_idx+1] = init_vals[init_val_idx]
            else:
                annot_arr = seg_ids
            annot_dict[fr_idx] = annot_arr
        return annot_dict

    def getParentSegments(self, seg_ids, from_lvl, to_lvl):
        '''
        Returns parent seg IDs in hierarchical level 'to_lvl' of seg IDs (from 'from_lvl').
        Parameters:
            seg_ids: ndarray(n_segs_to_check,) of uint32; seg IDs in hierarchy level 'from_lvl'
            from_lvl, to_lvl: int
        Returns:
            seg_p_ids: ndarray(n_segs_to_check,) of uint32; the parent seg IDs of 'seg_ids' in level 'to_lvl'
        '''
        assert to_lvl > from_lvl
        id_offset, id_end_offset = self.getSegIDIntervalInHierLevel(from_lvl)
        assert np.all((id_offset <= seg_ids) & (seg_ids < id_end_offset)) 
                                                    # assert all IDs are from the hierarchical level 'from_lvl'
        all_parents = self.hierarchy_parent_matrix[(from_lvl, to_lvl)]        # (n_seg_ids_in_from_lvl, 2)
        idxs = np.searchsorted(all_parents[:,0], seg_ids)
        seg_p_ids = all_parents[idxs,1]
        return seg_p_ids

    def getFinestCommonAncestorLevelPairwise(self, seg_ids1, seg_ids2):
        '''
        For each lvl0 segment-pair, returns lowest hierarchical level in which they are part of the same SV.
        Parameters:
            seg_ids1, seg_ids2: ndarray(n_segs_to_check,) of uint32; level0 seg IDs
        Returns:
            common_ancestor_lvls: ndarray(n_segs_to_check,) of int32; lowest common ancestor levels
                                         from self.hier_lvls_used; -1 if no common ancestor levels in self.hier_lvls_used
        '''
        id_offset, id_end_offset = self.getSegIDIntervalInHierLevel(0)
        assert seg_ids1.shape == seg_ids2.shape
        assert seg_ids1.ndim == 1
        assert np.all(id_offset <= seg_ids1) and np.all(id_offset <= seg_ids2)
        assert np.all(seg_ids1 < id_end_offset) and np.all(seg_ids2 < id_end_offset)
        common_ancestor_lvls = np.full_like(seg_ids1, dtype=np.int32, fill_value=-1)

        for hier_lvl in sorted(self.hier_lvls_used)[::-1]:
            p1 = self.getParentSegments(seg_ids1, 0, hier_lvl)
            p2 = self.getParentSegments(seg_ids2, 0, hier_lvl)
            common_ancestor_lvls[p1 == p2] = hier_lvl

        # check level 0:
        common_ancestor_lvls[seg_ids1 == seg_ids2] = 0

        return common_ancestor_lvls

    def getFinestCommonAncestorLevelWithAnySV(self, seg_ids1, seg_ids2):
        '''
        For each lvl0 SV in seg_ids1 returns the finest hier lvl (-1 if None) in which a common ancestor 
                is present with any SV from seg_ids2.
        Parameters:
            seg_ids1: ndarray(n_segs_to_check,) of uint32; level0 seg IDs
            seg_ids2: ndarray(n_segs_to_compare_to,) of int32; lowest common ancestor levels
                                         -1 if no common ancestor levels
        Returns:
            common_ancestor_lvls: ndarray(n_segs_to_check,) of uint32;
        '''
        id_offset, id_end_offset = self.getSegIDIntervalInHierLevel(0)
        assert seg_ids1.ndim == 1 and seg_ids2.ndim == 1
        assert np.all(id_offset <= seg_ids1) and np.all(id_offset <= seg_ids2)
        assert np.all(seg_ids1 < id_end_offset) and np.all(seg_ids2 < id_end_offset)

        # collect all ancestors of seg_ids1 and seg_ids2 SVs in each hierarchical level
        all_ancestors1 = {}
        all_ancestors2 = {}
        all_ancestors1[0] = seg_ids1
        all_ancestors2[0] = seg_ids2
        prev_seg_ids1 = seg_ids1
        prev_seg_ids2 = seg_ids2
        for hier_lvl in range(1,self.max_hier_lvl+1):
            prev_seg_ids1 = self.getParentSegments(prev_seg_ids1, hier_lvl-1, hier_lvl)
            prev_seg_ids2 = np.unique(self.getParentSegments(prev_seg_ids2, hier_lvl-1, hier_lvl))
            all_ancestors1[hier_lvl] = prev_seg_ids1
            all_ancestors2[hier_lvl] = prev_seg_ids2

        # filling return array, iterating hierarchical levels from highest to finest
        common_ancestor_lvls = np.full_like(seg_ids1, dtype=np.int32, fill_value=-1)
        for hier_lvl in range(self.max_hier_lvl, -1, -1):
            ancestor_mask1 = np.isin(all_ancestors1[hier_lvl], all_ancestors2[hier_lvl])
            common_ancestor_lvls[ancestor_mask1] = hier_lvl
        return common_ancestor_lvls


    def getAllChildSegments(self, seg_ids, from_lvl, to_lvl):
        '''
        Returns child seg IDs in hierarchical level 'to_lvl' of seg IDs (from 'from_lvl').
        ! This function only returns all child segments corresponding to the given segments, but no
            individual seg -> seg relations are analyzed.
        Parameters:
            seg_ids: ndarray(n_segs_to_check,) of uint32; seg IDs in hierarchy level 'from_lvl'
            from_lvl, to_lvl: int
        Returns:
            child_seg_ids: ndarray(n_child_segs,) of uint32; the child seg IDs of 'seg_ids' in level 'to_lvl'
                            ! NOT SAME SHAPE AS 'seg_ids' param !
        '''
        assert from_lvl > to_lvl
        assert from_lvl in self.hier_lvls_used
        assert to_lvl in [0] + self.hier_lvls_used
        all_parents = self.hierarchy_parent_matrix[(to_lvl, from_lvl)]        # (n_seg_ids_in_from_lvl, 2)
        parents_mask = np.isin(all_parents[:,1], seg_ids)

        # TODO next unique might not be necessary, as [:,0] are already unique
        child_seg_ids = np.unique(all_parents[parents_mask,0])
        return child_seg_ids

    def getAllLvl0ChildSegments_fast(self, seg_ids, from_lvl):
        '''
        Returns child seg IDs in hierarchical level 0 of seg IDs (from 'from_lvl').
        ! This function only returns all child segments corresponding to the given segments, but no
            individual seg -> seg relations are analyzed.
        Parameters:
            seg_ids: ndarray(n_segs_to_check,) of uint32; seg IDs in hierarchy level 'from_lvl'
            from_lvl: int
        Returns:
            child_seg_ids: ndarray(n_child_segs,) of uint32; the child seg IDs of 'seg_ids' in level 0
                            ! NOT SAME SHAPE AS 'seg_ids' param !
        '''
        h_lvl_id_offset, _ = self.getSegIDIntervalInHierLevel(from_lvl)
        seg_ids = seg_ids - h_lvl_id_offset
        ch_arr, ch_ranges = self.hierarchy_children_arrays[from_lvl]  # tuple(1D arr, 1D obj array of list of ints)
        selected_ranges = ch_ranges[seg_ids]  # 1D obj arr of lists of int
        selected_ch_idxs = list(itertools.chain(*selected_ranges))   # list of ints
        return ch_arr[selected_ch_idxs]

    def getSPChannelForHierFrame(self, channel_name, hier_lvl, fr_idx=None):
        '''
        Parameters:
            channel_name: str
            hier_lvl: int
            fr_idx: None OR int
        Returns:
            IF fr_idx is not None: ndarray(n_segs_in_fr, n_sp_chans) of ?
            IF fr_idx is None: dict{fr_idx - int: ndarray(n_segs_in_fr, n_sp_chans) of ?}
        '''
        if fr_idx is None:
            return self.sp_chans[hier_lvl][channel_name]
        else:
            return self.sp_chans[hier_lvl][channel_name][fr_idx]

    def getSPDistanceDicts(self, channel_name, hier_lvl):
        '''
        Parameters:
            channel_name: str
            hier_lvl: int
        Returns:
            sp_dist_idxs: dict{fr_idx - int: ndarray(n_segs_in_fr, 10) of uint32}}
            sp_dists: dict{fr_idx - int: ndarray(n_segs_in_fr, 10) of float32}}
        '''
        return self.sp_dist_idxs[hier_lvl][channel_name], self.sp_dists[hier_lvl][channel_name]

    def getDownscaledImgChan(self, channel_name, size_yx):
        '''
        Returns:
            ds_im: ndarray(n_frames, size_y, size_x, n_chans) of ?
        '''
        return self.downscaled_image_chans[channel_name][size_yx]

    # ADD, SET

    def addImage(self, name, img):
        '''
        Adds an image channel.
        Parameters:
            name: str; the ID of the image channel
            img: ndarray(n_frames, size_y, size_x, n_ch) of ?;
        '''
        assert name not in self.image_chans.keys()
        assert img.shape[:3] == self.lvl0_seg.shape
        assert img.ndim == 4
        self.image_chans[name] = img

    def addDownscaledImage(self, name, img):
        '''
        Adds an image channel.
        Parameters:
            name: str; the ID of the image channel
            img: ndarray(n_frames, downscaled_size_y, downscaled_size_x, n_ch) of ?;
        '''
        assert img.shape[0] == self.lvl0_seg.shape[0]
        im_shape_yx = img.shape[1:3]
        assert name not in self.downscaled_image_chans.keys() or\
               im_shape_yx not in self.downscaled_image_chans[name].keys()
        assert im_shape_yx in self.downscale_sizes_yx
        assert img.ndim == 4
        if name not in self.downscaled_image_chans.keys():
            self.downscaled_image_chans[name] = {}
        self.downscaled_image_chans[name][im_shape_yx] = img

    def addSegChannel(self, name, hier_lvl, values_per_frame):
        '''
        Adds a seg channel.
        Parameters:
            name: str; the ID of the seg channel
            hier_lvl: int
            values_per_frame: dict{fr_idx: ndarray(n_hier_segs_in_frame, n_sp_chans) of ?;
                        where SP IDs are in ascending order
        '''
        _ = Util.ensureMultiLevelDictKey(self.sp_chans, (hier_lvl, name))
        assert len(self.sp_chans[hier_lvl][name]) == 0    # assert it was just created
        for fr_idx in range(self.lvl0_seg.shape[0]):
            vals = values_per_frame[fr_idx]
            assert vals.ndim == 2
            self.sp_chans[hier_lvl][name][fr_idx] = vals
        #

    def addImageReduceFuncs(self, out_name, img_chan_name, func, func_named_args):
        '''
        Adds a function which reduces image channels in regions to a fixed length vector.
            (functions create a new SP channel from an image channel).
        Parameters:
            out_name: str; name of the output seg channel
            img_chan_name: str; name of the input img name
            func: callable; receives a single positional arg: ndarray(n_pixels, n_chans) and 
                        named args passed in the 'func_named_args' dict, while returns a vector of fixed length.
            func_named_args: dict; named args for func
        '''
        assert self.data_source == 'init', "This object was loaded from cache, no more functions can be added"
        assert out_name not in self.image_sp_reduce_funcs.keys()
        self.image_sp_reduce_funcs[out_name] = (img_chan_name, func, func_named_args)

    def addImageDownscaleFuncs(self, out_name, img_chan_name, func, func_named_args):
        '''
        Adds a function which downscales image channels.
        Parameters:
            out_name: str; name of the output region channel
            img_chan_name: str; name of the input img name
            func: callable; receives two positional args: ndarray(n_sps, n_chans), tuple(size_y, size_x) and 
                        named args passed in the 'func_named_args' dict, while returns a vector of fixed length.
            func_named_args: dict; named args for func
        '''
        assert self.data_source == 'init', "This object was loaded from cache, no more functions can be added"
        assert out_name not in self.image_downscale_funcs.keys()
        self.image_downscale_funcs[out_name] = (img_chan_name, func, func_named_args)

    # COMPUTE

    def computeSegChansFromReduceFuncs(self):
        '''
        Creates seg channels by executing all added functions from self.image_sp_reduce_funcs over image channels.
        '''
        assert self.data_source == 'init', "This object was loaded from cache, compute methods are not available"
        for out_sp_key in self.image_sp_reduce_funcs.keys():
            input_chan_key, func, named_args = self.image_sp_reduce_funcs[out_sp_key]
            input_img = self.image_chans[input_chan_key]
            for hier_lvl in [0] + self.hier_lvls_used:

                _ = Util.ensureMultiLevelDictKey(self.sp_chans, (hier_lvl, out_sp_key))
                
                for fr_idx in range(self.lvl0_seg.shape[0]):
                    rprop = self.rprops[hier_lvl][fr_idx]
                    seg_ids = self.getSegIDsInHierLevelInFrame(hier_lvl, fr_idx)
                    seg_pixs_ls = ImUtil.get_segment_pixels_from_regionprop(input_img[fr_idx],\
                                                 self.rprops[hier_lvl][fr_idx], seg_ids)
                    sp_vals = []
                    for seg_pixs in seg_pixs_ls:
                        out = func(seg_pixs, **named_args)
                        sp_vals.append(out)
                    sp_vals = np.stack(sp_vals, axis=0)
                    if sp_vals.ndim == 1:
                        assert len(seg_pixs_ls) >= 1
                        sp_vals = sp_vals[:,np.newaxis]
                    assert sp_vals.ndim == 2
                    self.sp_chans[hier_lvl][out_sp_key][fr_idx] = sp_vals
        #

    def computeDownscaleFuncs(self):
        '''
        Creates downscaled img channels by executing all added functions from 
                                    self.downscaled_image_chans over image channels.
        '''
        assert self.data_source == 'init', "This object was loaded from cache, compute methods are not available"
        for out_ds_key in self.image_downscale_funcs.keys():
            input_chan_key, func, named_args = self.image_downscale_funcs[out_ds_key]
            assert out_ds_key not in self.downscaled_image_chans
            self.downscaled_image_chans[out_ds_key] = {}
            for size_yx in self.downscale_sizes_yx:
                self.downscaled_image_chans[out_ds_key][size_yx] = \
                                    func(self.image_chans[input_chan_key], size_yx, **named_args)
        #

    # HELPER

    def convertFrameAnnotToLowerLevel(self, annots_from, fr_idx, from_lvl, to_lvl):
        '''
        Converts high level frame annotation to a lower hierarchical level.
        Parameters:
            annots_from: ndarray(n_segs_h_in_fr,) of int
            fr_idx: int
            from_lvl, to_lvl: int
        Returns:
            annots_to: ndarray(n_segs_h_lower_in_fr,) of int
        '''
        assert from_lvl > to_lvl

        from_seg_ids = self.getSegIDsInHierLevelInFrame(from_lvl, fr_idx)
        to_seg_ids = self.getSegIDsInHierLevelInFrame(to_lvl, fr_idx)
        assert from_seg_ids.shape[0] == annots_from.shape[0]
        annots_to = np.full(to_seg_ids.shape, dtype=np.int32, fill_value=-2)

        annots_from_u, annots_from_inv = np.unique(annots_from, return_inverse=True)
        for annot_idx in range(annots_from_u.shape[0]):
            from_annot_mask = annots_from_inv == annot_idx
            
            if to_lvl == 0:
                # new code
                to_seg_ids_annot = self.getAllLvl0ChildSegments_fast(from_seg_ids[from_annot_mask], from_lvl)
            else:
                to_seg_ids_annot = self.getAllChildSegments(from_seg_ids[from_annot_mask], from_lvl, to_lvl)
            
            #to_seg_ids_annot = self.getAllChildSegments(from_seg_ids[from_annot_mask], from_lvl, to_lvl)
            to_annot_mask = np.isin(to_seg_ids, to_seg_ids_annot, assume_unique=True)
            annots_to[to_annot_mask] = annots_from_u[annot_idx]
        
        assert np.all(annots_to > -2)   # TODO DEBUG
        return annots_to

    def convertFrameAnnotToHigherLevel(self, annots_from, fr_idx, from_lvl, to_lvl, n_labels=None):
        '''
        Converts low level frame annotation to a higher hierarchical level by choosing majority label.
        Parameters:
            annots_from: ndarray(n_segs_h_in_fr,) of int
            fr_idx: int
            from_lvl, to_lvl: int
            n_labels: None OR int; maximum number of labels, calculated if not given
        Returns:
            annots_to: ndarray(n_segs_h_higher_in_fr,) of int
        '''
        assert from_lvl < to_lvl
        if n_labels is None:
            n_labels = np.amax(annots_from)+1

        from_seg_ids = self.getSegIDsInHierLevelInFrame(from_lvl, fr_idx)   # (n_seg_ids_in_from_lvl,)
        #to_seg_ids = self.getSegIDsInHierLevelInFrame(to_lvl, fr_idx)
        assert from_seg_ids.shape[0] == annots_from.shape[0]
        seg_sizes_from_lvl = self.seg_sizes_per_frame[from_lvl][fr_idx][:,1]   # (n_seg_ids_in_from_lvl,)

        to_seg_ids = self.getParentSegments(from_seg_ids, from_lvl, to_lvl)   # (n_seg_ids_in_from_lvl,)
        to_seg_ids_u, to_seg_ids_inv = np.unique(to_seg_ids, return_inverse=True)
        seg_sizes_to_per_annot = np.zeros((to_seg_ids_u.shape[0], n_labels), dtype=np.int32)
                                                                # (n_seg_ids_in_to_lvl, n_labels)
        np.add.at(seg_sizes_to_per_annot, (to_seg_ids_inv, annots_from), seg_sizes_from_lvl)
                         # -> seg_sizes_to_per_annot[(to_seg_ids_inv, annots_from)] += seg_sizes_from_lvl
        annots_to = np.argmax(seg_sizes_to_per_annot, axis=1)   # (n_seg_ids_in_to_lvl,)
        return annots_to

    # PRIVATE MEMBER FUNCS


    def _calculateHierarchicalConnections(self, from_level, to_level):
        '''
        For all segments level 'from_level', returns parent node IDs in level 'to_level'.
        Parameters:
            from_level, to_level: int
        Returns:
            parents: ndarray(n_segs_in_from_level, 2:[from_seg_id, to_seg_id]) of uint32;
                            from_seg_ids are sorted
        '''
        assert from_level < to_level
        n_segs_lvl0 = np.amin(self.seg_hier_parent_ids)
                        # smallest parent is the ID of the smallest lvl1 seg == number of lvl0 segs (starting from ID 0)

        curr_seg_ids = np.arange(n_segs_lvl0)
        p_seg_ids = self.seg_hier_parent_ids[curr_seg_ids]
        from_seg_id = curr_seg_ids

        for lvl_idx in range(1,to_level):
            curr_seg_ids = p_seg_ids

            if from_level == lvl_idx:
                curr_seg_ids = np.unique(curr_seg_ids)
                from_seg_id = curr_seg_ids

            p_seg_ids = self.seg_hier_parent_ids[curr_seg_ids]


        assert Util.is_sorted(from_seg_id)
        parents = np.stack([from_seg_id, p_seg_ids], axis=1)
        return parents.astype(np.uint32)


    def _processHierarchy(self):
        '''
        Processes hierarchical connections for faster access.
        Sets self.hierarchy_parent_matrix, self.seg_id_intervals_in_level, self.seg_sizes_per_frame.
        '''
        # get seg ID intervals for each hierarchical level and check consistency of seg IDs
        self.seg_id_intervals_in_level = {}
        self.seg_id_intervals_in_level[0] = (0, np.amin(self.seg_hier_parent_ids))
        assert len(np.unique(self.lvl0_seg)) == self.seg_id_intervals_in_level[0][1] # DEBUG check, can be removed later
        for hier_lvl in range(self.max_hier_lvl):
            ps = self._calculateHierarchicalConnections(hier_lvl, hier_lvl+1)
            u_ps = np.unique(ps[:,1])
            min_ps = np.amin(u_ps)
            max_ps = np.amax(u_ps)
            assert len(u_ps) == max_ps-min_ps+1 and min_ps == self.seg_id_intervals_in_level[hier_lvl][1]
            self.seg_id_intervals_in_level[hier_lvl+1] = (min_ps, max_ps+1)

        # create parent matrix between each pair of hierarchical levels
        self.hierarchy_parent_matrix = {}
        hier_pairs = [(hlvl1, hlvl2) for hlvl1 in range(self.max_hier_lvl+1) \
                                     for hlvl2 in range(1, self.max_hier_lvl+1) if hlvl1 < hlvl2]
        #hier_pairs = [(hlvl1, hlvl2) for hlvl1 in sorted([0] + self.hier_lvls_used) \
        #                             for hlvl2 in sorted(self.hier_lvls_used) if hlvl1 < hlvl2]

        for hier_pair in hier_pairs:
            self.hierarchy_parent_matrix[hier_pair] = \
                            self._calculateHierarchicalConnections(hier_pair[0], hier_pair[1])
        
        # create per frame seg ID collections, calc SP sizes for each hier lvls
        self.seg_sizes_per_frame = {}
        self.seg_sizes_per_frame[0] = {}

        for fr_idx in range(self.lvl0_seg.shape[0]):
            seg_fr_u, seg_fr_c = np.unique(self.lvl0_seg[fr_idx], return_counts=True)
            self.seg_sizes_per_frame[0][fr_idx] = np.stack([seg_fr_u, seg_fr_c], axis=-1).astype(np.uint32)

        #for hier_lvl in range(1, self.max_hier_lvl+1):
        for hier_lvl in sorted(self.hier_lvls_used):

            _ = Util.ensureMultiLevelDictKey(self.seg_sizes_per_frame, (hier_lvl,))
            for fr_idx in range(self.lvl0_seg.shape[0]):
                seg_fr_u = self.seg_sizes_per_frame[0][fr_idx][:,0]
                seg_fr_c = self.seg_sizes_per_frame[0][fr_idx][:,1]
                seg_fr_h = self.getParentSegments(seg_fr_u, 0, hier_lvl)
                seg_fr_hu, seg_fr_hinv = np.unique(seg_fr_h, return_inverse=True)
                seg_fr_hc = np.zeros_like(seg_fr_hu, dtype=np.uint32)
                np.add.at(seg_fr_hc, seg_fr_hinv, seg_fr_c)
                self.seg_sizes_per_frame[hier_lvl][fr_idx] =\
                             np.stack([seg_fr_hu, seg_fr_hc], axis=-1).astype(np.uint32)

        # create children arrays for each hier level pair (h, 0), where h comes from self.hier_lvls_used
        self.hierarchy_children_arrays = {}
        for hier_lvl in sorted(self.hier_lvls_used):
            p_arr = self.hierarchy_parent_matrix[(0, hier_lvl)]   # (n_segs_in_hier_from, 2:[from_seg_id, to_seg_id]))
            children_dict = {p_id: [] for p_id in np.unique(p_arr[:,1])}   # dict{parent_id - int: list of child_ids}
            for rel_idx in range(p_arr.shape[0]):
                children_dict[p_arr[rel_idx,1]].append(p_arr[rel_idx,0])
            
            children_list = []  # list of lists
            ch_ranges = np.empty((len(children_dict.keys()),), dtype=np.object_)      # 1D object array of lists
            idx_offset = 0
            for idx, k in enumerate(sorted(children_dict.keys())):
                ch_ls = children_dict[k]
                children_list.append(ch_ls)
                ch_ranges[idx] = range(idx_offset, idx_offset+len(ch_ls))
                idx_offset += len(ch_ls)

            ch_arr = np.concatenate(children_list, axis=0)
            self.hierarchy_children_arrays[hier_lvl] = (ch_arr, ch_ranges)



    def _initRegionProps(self):
        '''
        Sets self.rprops. Adds 'centroids_yx' channels to self.sp_chans
        '''

        for hier_lvl in [0] + self.hier_lvls_used:

            # calculate higher level segmentations
            if hier_lvl > 0:
                hier_parents = self.hierarchy_parent_matrix[(0, hier_lvl)]
                seg_h = Util.replace(self.lvl0_seg, hier_parents[:,0], hier_parents[:,1], copy=True)
            else:
                seg_h = self.lvl0_seg

            _ = Util.ensureMultiLevelDictKey(self.rprops, (hier_lvl,))

            centroids_dict = {}
            for fr_idx in range(self.lvl0_seg.shape[0]):

                # create regionprop; rprops ignores 0 labels, so we add 1 to all seg labels stored in rprops
                regions = skimage.measure.regionprops(seg_h[fr_idx]+1, cache=True)
                self.rprops[hier_lvl][fr_idx] = regions

                # centroids: disabled (code is working)
                '''
                seg_h_idxs_in_fr = self.getSegIDsInHierLevelInFrame(hier_lvl, fr_idx)
                centroids_dict[fr_idx] = ImUtil.get_regionprop_centroids(regions, seg_h_idxs_in_fr)
                                              # (n_seg_h_in_fr, 2:[cy, cx])
                '''

            #self.addSegChannel('centroids_yx', hier_lvl, centroids_dict)
        #

    def _compute_downscaled_seg(self):
        '''
        Sets self.downscaled_lvl0_segs.
        '''
        self.downscaled_lvl0_segs = {}
        for ds_size_yx in self.downscale_sizes_yx:
            ds_im = ImplUtil.imdownscale(self.lvl0_seg[:,:,:,np.newaxis], ds_size_yx, interp_smooth=False)
            self.downscaled_lvl0_segs[ds_size_yx] = ds_im[:,:,:,0].astype(np.int32)



