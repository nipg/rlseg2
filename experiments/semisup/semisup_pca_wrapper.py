
import numpy as np
from sklearn.decomposition import PCA

from semisup_abstract_model import SemisupervisedAbstractModel

N_TARGET_FEATURES = 40
N_MAX_PCA_INPUT_SAMPLES = 100000

class SemisupervisedPCAWrapper(SemisupervisedAbstractModel):

    '''
    Member fields:
        n_features_total: int; input shape to model is (n_features_total,)

        pca_model: sklearn.decomposition.PCA
        wrapped_model: SemisupervisedAbstractModel instance
    '''

    def __init__(self, wrapped_model=None):
        self.n_features_total = None
        self.n_cats = None
        assert wrapped_model is not None
        self.wrapped_model = wrapped_model

    def __str__(self):
        return "PCA + " + str(self.wrapped_model)

    def pretrain_unsupervised(self, xss):
        '''
        Must be called before training or evaluation, to compute PCA transform for dataset.
        Parameters:
            xss: ndarray(n_sps, n_features_total) of float32; the feature vectors for each SP
        '''
        assert xss.ndim == 2
        print("Computing PCA over dataset random slice, original shape:", xss.shape)
        if xss.shape[0] > N_MAX_PCA_INPUT_SAMPLES:
            xss = np.random.permutation(xss)[:N_MAX_PCA_INPUT_SAMPLES,:]
        self.pca_model = PCA(N_TARGET_FEATURES, whiten=False)
        self.pca_model.fit(xss)
        print("PCA ready.")
        print("    explained variance ratio of " + str(N_TARGET_FEATURES) + " components: " +\
                             str(self.pca_model.explained_variance_ratio_[:N_TARGET_FEATURES]))
        print("    explained total variance ratio of " + str(N_TARGET_FEATURES) + " components: " +\
                             str(np.sum(self.pca_model.explained_variance_ratio_[:N_TARGET_FEATURES])))

    def reset(self, n_features_total, n_cats):
        assert self.n_features_total is None or (self.n_features_total == n_features_total),\
                                         "Reset with different number of features is not supported in PCA."
        self.n_features_total = n_features_total
        self.wrapped_model.reset(N_TARGET_FEATURES, n_cats)

    def fit(self, xs, ys, verbose=False):
        '''
        Parameters:
            xs: ndarray(n_sps, n_features_total) of float32; the feature vectors for each SP
            ys: ndarray(n_sps) of int32; the true labels of each SP
        '''
        assert xs.shape[1:] == (self.n_features_total,)
        xs = self.pca_model.transform(xs)
        self.wrapped_model.fit(xs, ys, verbose)

    def predict(self, xs, return_probs=False, verbose=False):
        '''
        Parameters:
            xs: ndarray(n_sps, n_features_total) of float32; the feature vectors for each SP
            return_probs: bool;
        Returns:
            ys_pred: ndarray(n_sps, n_cat) of fl32 (IF return_probs == True)
                     ndarray(n_sps,) of i32        (IF return_probs == False)

        '''
        assert xs.shape[1:] == (self.n_features_total,)
        xs = self.pca_model.transform(xs)
        return self.wrapped_model.predict(xs, return_probs, verbose)

    def evaluate(self, xs, ys, return_preds=False, verbose=False):
        '''
        Parameters:
            xs: ndarray(n_sps, n_features_total) of float32; the feature vectors for each SP
            ys: ndarray(n_sps) of int32; the true labels of each SP
            return_preds: bool
        Returns:
            acc: float; accuracy of predictions (ratio of correctly predicted datapoints)
            (OPTIONAL if return_preds is True) ys_pred: ndarray(n_sps,) of i32
        '''
        xs = self.pca_model.transform(xs)
        return self.wrapped_model.evaluate(xs, ys, return_preds, verbose)





