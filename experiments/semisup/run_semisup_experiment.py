
# 
# RLseg2 experimental script: CNN feature extraction from videos, plotting feature distributions w. and w/o. labels
#
# @author Viktor Varga
#

import sys
sys.path.append('../..')

import os
import cv2
import h5py
import numpy as np
import pickle
import matplotlib.pyplot as plt
from util import util as Util
from util import imutil as ImUtil
from util.vid_writer import VideoWriter
import util.viz as Viz
import skimage.measure
import skimage.transform

from segmentation import Segmentation 
from semisup_benchmark import SemisupervisedBenchmark
from semisup_abstract_model import SemisupervisedAbstractModel
from semisup_svm_model import SemisupervisedSVMModel
from semisup_logreg_model import SemisupervisedLogRegModel
from semisup_knn_model import SemisupervisedKNNModel
from semisup_dectree_model import SemisupervisedDecTreeModel
from semisup_pca_wrapper import SemisupervisedPCAWrapper
from semisup_siamese_model import SemisupervisedSiameseModel

IM_FOLDER = '/home/vavsaai/databases/DAVIS/DAVIS2017/DAVIS/JPEGImages/480p_unisize/'
DATA_FOLDER_GT_ANNOT = '/home/vavsaai/databases/DAVIS/rl_seg_data/davis2017_annot480p/'
DATA_FOLDER_IMFEATURES = '/home/vavsaai/databases/DAVIS/rlseg2_data/davis2017_imfeatures/'
ANNOT_FILENAME_PREFIX = 'annot480p_'
ANNOT_H5_KEY = 'davis2017_annot_480p'
SEG_DATA = '/home/vavsaai/databases/DAVIS/rl_seg_data/cache2017/'
FOLDER_OUT_DATASET = '/home/vavsaai/databases/DAVIS/rlseg2_data/_temp_semisup2017_dataset/'
BENCHMARK_FOLDER = '/home/vavsaai/databases/DAVIS/rlseg2_data/_temp_semisup2017_benchmark/'
FOLDER_OUT_BENCHMARK_PRED_VIDEO = '/home/vavsaai/databases/DAVIS/rlseg2_data/_temp_semisup2017_vid_benchmark_preds/'

LAYER_NAMES_MOBILENET_V2 = ['expanded_conv_project_BN', 'block_2_add', 'block_5_add', 'block_12_add',\
                       'block_16_project_BN', 'out_relu']

# orig DAVIS 2017 split (90 vids: 60 tr, 30 test)
        
# TR_SET_VIDNAMES = ['bear', 'boat', 'bmx-bumps']
# VAL_SET_VIDNAMES = ['blackswan']
# TEST_SET_VIDNAMES = ['blackswan']

# TEMP: mixed sets

TR_SET_VIDNAMES = ['bear', 'bmx-bumps', 'boat', 'boxing-fisheye', 'breakdance-flare', 'bus', 'car-turn',\
                    'cat-girl', 'classic-car', 'color-run', 'dance-jump', 'dancing', 'disc-jockey',\
                    'dog-gooses', 'dogs-scale', 'drift-turn', 'drone', 'elephant', 'flamingo', 'hike',\
                    'hockey', 'horsejump-low', 'kid-football', 'kite-walk', 'koala', 'lady-running',\
                    'lindy-hop', 'lucia', 'mallard-fly', 'mallard-water', 'bike-packing', 'blackswan', 'bmx-trees', 'breakdance', 'camel', 'car-roundabout',\
                    'car-shadow', 'cows', 'dance-twirl', 'dog', 'dogs-jump', 'drift-chicane', 'drift-straight',\
                    'goat', 'gold-fish', 'horsejump-high', 'india', 'judo', 'kite-surf', 'lab-coat', 'libby',\
                    'loading', 'mbike-trick', 'motocross-jump', 'paragliding-launch']
VAL_SET_VIDNAMES = [ 'parkour', 'pigs', 'scooter-black', 'shooting', 'soapbox']
TEST_SET_VIDNAMES = ['shooting', 'soapbox']

# TEMP END: mixed sets

# TR_SET_VIDNAMES = ['bear', 'bmx-bumps', 'boat', 'boxing-fisheye', 'breakdance-flare', 'bus', 'car-turn',\
#                    'cat-girl', 'classic-car', 'color-run', 'crossing', 'dance-jump', 'dancing', 'disc-jockey',\
#                    'dog-agility', 'dog-gooses', 'dogs-scale', 'drift-turn', 'drone', 'elephant', 'flamingo',\
#                    'hike', 'hockey', 'horsejump-low', 'kid-football', 'kite-walk', 'koala', 'lady-running',\
#                    'lindy-hop', 'longboard', 'lucia', 'mallard-fly', 'mallard-water', 'miami-surf',\
#                    'motocross-bumps', 'motorbike', 'night-race', 'paragliding', 'planes-water', 'rallye',\
#                    'rhino', 'rollerblade', 'schoolgirls', 'scooter-board', 'scooter-gray', 'sheep',\
#                    'skate-park', 'snowboard', 'soccerball', 'stroller', 'stunt', 'surf', 'swing', 'tennis',\
#                    'tractor-sand', 'train', 'tuk-tuk', 'upside-down', 'varanus-cage', 'walking']
# TEST_SET_VIDNAMES = ['bike-packing', 'blackswan', 'bmx-trees', 'breakdance', 'camel', 'car-roundabout',\
#                      'car-shadow', 'cows', 'dance-twirl', 'dog', 'dogs-jump', 'drift-chicane', 'drift-straight',\
#                      'goat', 'gold-fish', 'horsejump-high', 'india', 'judo', 'kite-surf', 'lab-coat', 'libby',\
#                      'loading', 'mbike-trick', 'motocross-jump', 'paragliding-launch', 'parkour', 'pigs',\
#                      'scooter-black', 'shooting', 'soapbox']


def load_img_data(vidnames_to_load):
    '''
    Parameters:
        vidnames_to_load: list of str
    Returns:
        img_data_rgb: dict{str - vidname: ndarray(n_imgs, 480, 854, 3:RGB) of uint8}
    '''
    img_data_rgb = {}
    for vidname in vidnames_to_load:
        foldername = os.path.join(IM_FOLDER, vidname)
        n_imgs = len(os.listdir(foldername))
        ims = []
        for im_idx in range(n_imgs):
            fpath = os.path.join(foldername, str(im_idx).zfill(5) + ".jpg")
            assert os.path.isfile(fpath)
            im = cv2.imread(fpath, cv2.IMREAD_COLOR)
            im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
            ims.append(im)
        ims = np.stack(ims, axis=0)
        assert ims.shape == (n_imgs, 480, 854, 3)
        assert ims.dtype == np.uint8
        img_data_rgb[vidname] = ims
    return img_data_rgb

def load_annot_data(vidnames_to_load):
    '''
    Parameters:
        vidnames_to_load: list of str
    Returns:
        annot_data: dict{str - vidname: ndarray(n_imgs, 480, 854) of uint8}
    '''
    annot_data = {}
    for vidname in vidnames_to_load:
        davis_annot_h5_path = os.path.join(DATA_FOLDER_GT_ANNOT, ANNOT_FILENAME_PREFIX + vidname + '.h5')
        h5f = h5py.File(davis_annot_h5_path, 'r')
        annot_data[vidname] = h5f[ANNOT_H5_KEY][:].astype(np.uint8)
        h5f.close()
    return annot_data

def load_features(vidnames_to_load, feature_modelkey, fvec_normalizer=None):
    '''
    Parameters:
        vidnames_to_load: list of str
        feature_modelkey: str
        fvec_normalizer: None OR callable, accepts and returns a single 4D feature vec array
    Returns:
        feature_data: dict{str - vidname: dict{str - layer_name: ndarray(n_imgs, ?sy, ?sx, ?n_ch) of float32}}
    '''
    feature_data = {}
    for vidname in vidnames_to_load:
        davis_features_pkl_path = os.path.join(DATA_FOLDER_IMFEATURES, \
                                                    'features_' + feature_modelkey + '_' + vidname + '.pkl')
        with open(davis_features_pkl_path, 'rb') as f:
            vid_feature_data = pickle.load(f)
            if fvec_normalizer is not None:
                vid_feature_data = {layer_name: fvec_normalizer(fvec.astype(np.float32))\
                                                         for layer_name, fvec in vid_feature_data.items()}
            feature_data[vidname] = vid_feature_data

    return feature_data

def load_segmentation_data(vidnames_to_load):
    '''
    Returns:
        dict{vidname - str: Segmentation object}
    '''
    seg_objs = {}
    for vidname in vidnames_to_load:
        cache_fpath = os.path.join(SEG_DATA, 'davis2017_ccs2048_' + vidname + '.pkl')
        pkl_file = open(cache_fpath, 'rb')
        pkl_dict = pickle.load(pkl_file, encoding='latin1')  # encoding: to be able to load pickle protocol 2
        pkl_file.close()
        pkl_seg_obj = pkl_dict['seg_obj']
        seg_objs[vidname] = pkl_seg_obj
    return seg_objs

def _generate_dataset_from_seg_and_features(seg_obj, feature_viddata, annot_viddata, reduce_features,\
                                                flayers_to_use=None):
    '''
    Generates a dataset where each sample is a feature vector and a true label corresponding to a SP.
    Parameters:
        seg_obj: Segmentation object
        feature_viddata: dict{str - layer_name: ndarray(n_imgs, ?sy, ?sx, ?n_ch) of float32}
        annot_viddata: ndarray(n_imgs, 480, 854) of uint8
        reduce_features: Callable; nd(n_seg_pix, n_features) -> nd(n_output_features)
        flayers_to_use: None OR list of str; feature layer names to use; if None, all layers are used
    Returns:
        xs: ndarray(n_sps, n_features_total) of float32; the feature vectors for each SP
        ys: ndarray(n_sps) of int32; the true labels of each SP
        orig_sp_ids: ndarray(n_sps, 2:[seg_id, fr_idx]) of int32; seg ID and frame idx 
                                                                    for each SP in the original segmentation
    '''
    xs = []
    ys = []
    orig_sp_ids = []
    orig_sp_fr_idxs = []
    n_frames = annot_viddata.shape[0]
    annot_img = annot_viddata[...,None]
    assert annot_img.ndim == 4   # (n_frames, sy, sx, 1) of u8
    flayers_to_use = LAYER_NAMES_MOBILENET_V2 if flayers_to_use is None else flayers_to_use
    assert set(feature_viddata.keys()) >= set(flayers_to_use)

    # iterating frames with loop: upscaled feature vectors for whole video might not fit into memory
    for fr_idx in range(n_frames):

        print("        at fr#" + str(fr_idx))

        # creating regionprops
        lvl0_seg = seg_obj.lvl0_seg[fr_idx]  # (sy, sx)
        rprops = skimage.measure.regionprops(lvl0_seg+1, cache=True)   # +1, because rprops ignore label 0 as bg
        labels_in_fr = np.array([rprop.label for rprop in rprops], dtype=np.int32)

        # concatenating all feature heatmaps into one
        allfvecs_fr = [skimage.transform.resize(feature_viddata[ln][fr_idx].astype(np.float32),\
                                                     annot_viddata.shape[1:], order=0, anti_aliasing=False)\
                                                            for ln in flayers_to_use]
        #allfvecs_fr = [cv2.resize(feature_viddata[ln][fr_idx], annot_viddata.shape[:0:-1], interpolation=cv2.INTER_NEAREST)\
        #                                 for ln in flayers_to_use]
        allfvecs_fr = np.concatenate(allfvecs_fr, axis=-1)  # (sy, sx, n_all_features)

        # reducing feature vectors in each SP to a single feature vector
        seg_fvecs_ls = ImUtil.get_segment_pixels_from_regionprop(allfvecs_fr, rprops, labels_in_fr)
            # list(n_segs) of nd(n_seg_pix, n_features); list of arrays containing fvecs for each pixel in an SP
        seg_annots_ls = ImUtil.get_segment_pixels_from_regionprop(annot_img[fr_idx], rprops, labels_in_fr)
            # list(n_segs) of nd(n_seg_pix, 1); list of arrays containing the annot label for each pixel in an SP

        # collecting dataset
        for (seg_fvecs, seg_annots), lab in zip(zip(seg_fvecs_ls, seg_annots_ls), labels_in_fr):
            assert seg_fvecs.ndim == 2
            fvec_out = reduce_features(seg_fvecs)   # (n_out_features)
            xs.append(fvec_out)
            annot = ImUtil.imsp_reduce_most_frequent_item(seg_annots)   # scalar int
            ys.append(annot)
            orig_sp_ids.append(lab-1)
            orig_sp_fr_idxs.append(fr_idx)

    xs = np.stack(xs, axis=0).astype(np.float16, copy=False)
    ys = np.array(ys, dtype=np.int32)
    orig_sp_ids = np.array(list(zip(orig_sp_ids, orig_sp_fr_idxs)), dtype=np.int32)
    assert xs.ndim == 2
    assert ys.shape == (xs.shape[0],)
    assert orig_sp_ids.shape == (xs.shape[0], 2)
    return xs, ys, orig_sp_ids

def generate_and_save_datasets(seg_objs, feature_data, annot_data, reduce_features, flayers_to_use):
    '''
    Generates and saves a semisupervised dataset for each video.
    Parameters:
        seg_objs: dict{vidname - str: Segmentation objects}
        feature_data: dict{str - vidname: dict{str - layer_name: ndarray(n_imgs, ?sy, ?sx, ?n_ch) of float32}}
        annot_data: dict{str - vidname: ndarray(n_imgs, 480, 854) of uint8}
        reduce_features: Callable; nd(n_seg_pix, n_features) -> nd(n_output_features)
        flayers_to_use: None OR list of str; feature layer names to use; if None, all layers are used
    '''
    os.makedirs(FOLDER_OUT_DATASET, exist_ok=True)
    for vidname in seg_objs.keys():
        pkl_fpath = os.path.join(FOLDER_OUT_DATASET, 'semisup_' + vidname + '.pkl')
        if os.path.isfile(pkl_fpath):
            print("    skipped video: ", vidname)
            continue

        print("    at video: ", vidname)
        xs, ys, orig_sp_ids = _generate_dataset_from_seg_and_features(seg_objs[vidname],\
                               feature_data[vidname], annot_data[vidname], reduce_features, flayers_to_use)
                                        # (n_sps, n_features_total), (n_sps), (n_sps, 2:[seg_id, fr_idx])
        dataset_vid = (xs, ys, orig_sp_ids)
        with open(pkl_fpath, 'wb') as f:
            pickle.dump(dataset_vid, f)


def load_datasets(vidnames, f_transform_features, f_kwargs={}):
    '''
    Loads semisupervised datasets from archives generated by generate_and_save_datasets().
    Parameters:
        vidnames: list of str
        f_transform_features: Callable; nd(n_samples, n_features) -> nd(n_samples, n_output_features)
        f_kwargs: dict - keyword arguments for f_transform_features
    Returns:
        datasets_dict: dict{vidname - str: tuple(xs: ndarray(n_sps, n_tranformed_features_total) of float32,
                                                 ys: ndarray(n_sps) of int32,
                                                 orig_sp_ids: ndarray(n_sps, 2:[seg_id, fr_idx]) of int32)}
    '''
    datasets_dict = {}
    for vidname in vidnames:
        pkl_fpath = os.path.join(FOLDER_OUT_DATASET, 'semisup_' + vidname + '.pkl')
        with open(pkl_fpath, 'rb') as f:
            xs, ys, orig_sp_ids = pickle.load(f)
            xs = f_transform_features(xs, **f_kwargs)
            assert xs.ndim == 2
        datasets_dict[vidname] = (xs, ys, orig_sp_ids)
    return datasets_dict

def init_semisup_model(name, use_pca=False):
    '''
    Parameters:
        name: str
    Returns:
        model: SemisupervisedAbstractModel (subclass) instance
    '''
    assert name in ['logreg', 'knn', 'dectree', 'svm', 'siamese']
    if name == 'svm':
        model = SemisupervisedSVMModel()
    elif name == 'logreg':
        model = SemisupervisedLogRegModel()
    elif name == 'knn':
        model = SemisupervisedKNNModel()
    elif name == 'dectree':
        model = SemisupervisedDecTreeModel()
    elif name == 'siamese':
        assert not use_pca
        model = SemisupervisedSiameseModel()
    #
    if use_pca:
        model = SemisupervisedPCAWrapper(wrapped_model=model)
    return model

def normalize_features_meanstd(xs_dict):
    '''
    Parameters:
        xs_dict: dict{vidname - str: ndarray(ndarray(n_sps, n_tranformed_features_total) of float32)}
    '''
    EPS = 1e-8
    xss = np.concatenate(list(xs_dict.values()), axis=0)
    xss_mean = np.mean(xss, axis=0, dtype=np.float64)
    xss_std = np.std(xss, axis=0, dtype=np.float64) + EPS
    xs_dict = {vidname: (xs-xss_mean)/xss_std for vidname, xs in xs_dict.items()}
    return xs_dict

def _f_reduce_features(arr):
    # nd(n_seg_pix, n_features) -> nd(n_output_features)
    assert arr.shape[1:] == (488,)
    return np.mean(arr, axis=0)

def _f_range_select_features(arr, offset=None, end_offset=None):
    # nd(n_samples, n_features) -> nd(n_samples, (end_offset-offset))
    assert arr.shape[1:] == (488,)
    offset = 0 if offset is None else offset
    end_offset = arr.shape[1] if end_offset is None else end_offset
    arr = arr[:,offset:end_offset]
    assert arr.shape[1] > 0
    return arr


def render_predictions_videos(folder_out, ys_preds_dict, sp_is_gt, img_data, seg_data, orig_sp_ids_dict,\
                                    method_name=""):
    '''
    Parameters:
        folder_out: str
        ys_preds_dict: dict{n_sps_revealed - int: dict{vidname - str:
                                     ndarray(n_repeats, n_sps) of i32}} indexing orig_sp_ids_dict
        sp_is_gt: dict{n_sps_revealed - int: dict{vidname - str:
                                     ndarray(n_repeats, n_sps) of bool_}}; whether label in ys_preds_dict is GT.
        img_data: dict{str - vidname: ndarray(n_imgs, 480, 854, 3:RGB) of ui8}
        seg_data: dict{vidname - str: Segmentation object}
        orig_sp_ids_dict: dict{vidname - str: orig_sp_ids: ndarray(n_sps, 2:[seg_id, fr_idx]) of i32}
        method_name: str, added to the name of the video
    '''
    LABEL_COLORS = {0:(192,192,192), 1:(255,0,0), 2:(0,0,255), 3:(0,255,0), 4:(255,0,255),\
                    5:(0,255,255), 6:(255,127,0), 7:(192,127,255)}
    for n_sps_revealed in ys_preds_dict.keys():
        print("    rendering n_sps_revealed:", n_sps_revealed)
        for vidname in ys_preds_dict[n_sps_revealed].keys():
            folder_vid_out = os.path.join(folder_out, vidname)
            ys_preds_vid = ys_preds_dict[n_sps_revealed][vidname]  # (n_repeats, n_sps) of i32
            sp_is_gt_vid = sp_is_gt[n_sps_revealed][vidname]  # (n_repeats, n_sps) of bool_
            seg_obj_vid = seg_data[vidname].lvl0_seg   # (n_frames, size_y, size_x)
            img_data_vid = img_data[vidname]   # (n_imgs, 480, 854, 3:RGB)
            orig_sp_ids_vid = orig_sp_ids_dict[vidname]    # (n_sps, 2:[seg_id, fr_idx])

            n_frames = img_data_vid.shape[0]
            n_repeats = ys_preds_vid.shape[0]
            for repeat_idx in range(n_repeats):
                vid_fpath = os.path.join(folder_vid_out,\
                                 method_name + "_sp" + str(n_sps_revealed) + "_r" + str(repeat_idx) + ".avi")
                vid_writer = VideoWriter(img_data_vid.shape[1:3], vid_fpath, vr_fps=25.)

                for fr_idx in range(n_frames):
                    bg_img = img_data_vid[fr_idx,:,:,::-1]

                    # transform segment image: original SV seg ID -> SP ID
                    sp_idxs_in_fr = np.where(orig_sp_ids_vid[:,1] == fr_idx)[0]   # (n_sps_in_fr,) indexing n_sp
                    sps_in_fr_seg_ids = orig_sp_ids_vid[sp_idxs_in_fr, 0]   # (n_sps_in_fr,) seg IDs of these SPs
                    seg_obj_sp_idxs = Util.replace(seg_obj_vid[fr_idx], \
                                            sps_in_fr_seg_ids.astype(np.uint32), sp_idxs_in_fr.astype(np.uint32))

                    render_info_str = "nSP revealed: " + str(n_sps_revealed) + ", repeat idx: " + str(repeat_idx) +\
                                 ", frame idx: " + str(fr_idx)
                    viz_img = Viz.render_segmentation_labels(segs=seg_obj_sp_idxs, seg_labels=ys_preds_vid[repeat_idx,:],\
                                         seg_color_max_alpha=sp_is_gt_vid[repeat_idx,:], bg_img=bg_img,\
                                         label_colors=LABEL_COLORS, color_alpha=.5, bg_alpha=.5,\
                                          boundary_color_rgb=(200,200,0), render_info_str=render_info_str)
                    vid_writer.write(viz_img)
                vid_writer.release()




if __name__ == '__main__':

    # FEATURE LAYER INFO, n_features in each layer output:
    #   'expanded_conv_project_BN': 16
    #   'block_2_add': 24
    #   'block_5_add': 32
    #   'block_12_add': 96
    #   'block_16_project_BN': 320

    all_vids = TR_SET_VIDNAMES + VAL_SET_VIDNAMES + TEST_SET_VIDNAMES
    #assert len(set(TR_SET_VIDNAMES) | set(VAL_SET_VIDNAMES) | set(TEST_SET_VIDNAMES)) == len(all_vids)  # assert disjoint sets

    GENERATE_DATASET = True
    GENERATE_BENCHMARK = False
    VISUALIZE_PREDICTIONS = False

    print("Loading image data for", len(all_vids), "videos...")
    img_data_rgb = load_img_data(all_vids)
    print("Loading annot data for", len(all_vids), "videos...")
    annot_data = load_annot_data(all_vids)
    print("Loading features...")
    #fvec_normalizer = lambda arr: Util.to_logscale_bidir(arr, base=2., min_exp=-4., max_exp=5.)
    feature_data = load_features(all_vids, 'MobileNetV2', fvec_normalizer=None)

    print("Loading segmentation data from cache...")
    seg_objs = load_segmentation_data(all_vids)

    if GENERATE_DATASET:
        print("Generating segment feature/label dataset...")
        flayers_to_use = ['expanded_conv_project_BN', 'block_2_add', 'block_5_add', 'block_12_add',\
                           'block_16_project_BN']
        generate_and_save_datasets(seg_objs, feature_data, annot_data, _f_reduce_features, flayers_to_use)

    print("Loading semisupervised feature/label dataset...")
    dataset_dict_tr = load_datasets(TR_SET_VIDNAMES, _f_range_select_features, f_kwargs={'offset': None, 'end_offset': None})
    orig_sp_ids_dict_tr = {vidname: orig_sp_ids for vidname, (xs, ys, orig_sp_ids) in dataset_dict_tr.items()}
    dataset_dict_val = load_datasets(VAL_SET_VIDNAMES, _f_range_select_features, f_kwargs={'offset': None, 'end_offset': None})
    orig_sp_ids_dict_val = {vidname: orig_sp_ids for vidname, (xs, ys, orig_sp_ids) in dataset_dict_val.items()}
    dataset_dict_test = load_datasets(TEST_SET_VIDNAMES, _f_range_select_features, f_kwargs={'offset': None, 'end_offset': None})
    orig_sp_ids_dict_test = {vidname: orig_sp_ids for vidname, (xs, ys, orig_sp_ids) in dataset_dict_test.items()}

    if GENERATE_BENCHMARK:
        print("Generating and saving semisupervised benchmark...")
        benchmark_n_sps_revealed_dict = {1:5, 2:5, 5:5, 10:5, 20:5, 50:5, 100:5, 200:5, 500:5}
        benchmark_n_frs_revealed_dict = {1:5, 2:5, 5:5, 10:5}
        ys_dict = {vidname: ys for vidname, (xs, ys, orig_sp_ids) in dataset_dict_test.items()}
        benchmark = SemisupervisedBenchmark(benchmark_n_sps_revealed_dict, benchmark_n_frs_revealed_dict,\
                                                 orig_sp_ids_dict_test, ys_dict)
        os.makedirs(BENCHMARK_FOLDER, exist_ok=True)
        pkl_fpath = os.path.join(BENCHMARK_FOLDER, 'benchmark.pkl')
        benchmark_infotxt_path = os.path.join(BENCHMARK_FOLDER, 'benchmark_info.txt')
        with open(pkl_fpath, 'wb') as f:
            pickle.dump(benchmark, f)
        with open(benchmark_infotxt_path, 'w') as f:
            f.write(benchmark._to_string_full_info())

    print("Loading semisupervised benchmark...")
    pkl_fpath = os.path.join(BENCHMARK_FOLDER, 'benchmark.pkl')
    with open(pkl_fpath, 'rb') as f:
        benchmark = pickle.load(f)

    # best: logreg acc % (1,2,5,10,20,50,100,200,500) -> 56, 58, 76.7, 84.9, 91.9, 93.7, 94.7, 96.2, 96.8

    METHOD_NAME = 'logreg'   # in ['logreg', 'knn', 'dectree', 'svm']
    assert METHOD_NAME in ['logreg', 'knn', 'dectree', 'svm', 'siamese']
    USE_PCA = False

    print("Evaluating model on benchmark...")
    model = init_semisup_model(METHOD_NAME, use_pca=USE_PCA)
    xs_dict_test = {vidname: xs for vidname, (xs, ys, orig_sp_ids) in dataset_dict_test.items()}
    #xs_dict_test = normalize_features_meanstd(xs_dict_test)

    print("    Unsupervised pretraining...")
    xss_test = np.concatenate(list(xs_dict_test.values()), axis=0)
    model.pretrain_unsupervised(xss_test)   # used by PCA only, otherwise does nothing

    print("    Supervised pretraining...")
    xs_tr_dict = {vidname: xs for vidname, (xs, ys, orig_sp_ids) in dataset_dict_tr.items()}
    ys_tr_dict = {vidname: ys for vidname, (xs, ys, orig_sp_ids) in dataset_dict_tr.items()}
    xs_val_dict = {vidname: xs for vidname, (xs, ys, orig_sp_ids) in dataset_dict_val.items()}
    ys_val_dict = {vidname: ys for vidname, (xs, ys, orig_sp_ids) in dataset_dict_val.items()}
    model.pretrain(xs_tr_dict, ys_tr_dict, xs_val_dict, ys_val_dict)

    ys_preds_dict, sp_is_gt_dict = benchmark.evaluate_model(model, xs_dict_test, return_preds=True)

    if VISUALIZE_PREDICTIONS:
        print("Rendering benchmark prediction videos...")
        method_name_tag = METHOD_NAME + '_pca' if USE_PCA else METHOD_NAME
        render_predictions_videos(FOLDER_OUT_BENCHMARK_PRED_VIDEO, ys_preds_dict, sp_is_gt_dict,\
                         img_data_rgb, seg_objs, orig_sp_ids_dict_test, method_name=method_name_tag)

